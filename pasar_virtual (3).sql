-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Mei 2018 pada 05.11
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pasar_virtual`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`admin_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status` varchar(6) NOT NULL,
  `foto` varchar(25) NOT NULL,
  `id_role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`, `nama`, `email`, `status`, `foto`, `id_role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@pvi.com', 'On', '', 'SU'),
(2, 'lailyindaryani', '195f6ad3b8ae791d438f830fc0c42a4d', 'Laily Indaryani', 'lailyindaryani@gmail.com', 'Off', '', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas_pemasukan`
--

CREATE TABLE IF NOT EXISTS `aktivitas_pemasukan` (
`id_aktivitas_pemasukan` int(11) NOT NULL,
  `tgl_pemasukan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nominal` int(11) NOT NULL,
  `ket_pemasukan` varchar(255) NOT NULL,
  `sisa_saldo` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `id_dompet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aktivitas_pemasukan`
--

INSERT INTO `aktivitas_pemasukan` (`id_aktivitas_pemasukan`, `tgl_pemasukan`, `nominal`, `ket_pemasukan`, `sisa_saldo`, `id_transaksi`, `id_dompet`) VALUES
(3, '2018-05-12 15:20:07', 400000, 'Penjualan produk', 34000, 1, 1),
(4, '2018-05-12 15:37:37', 100000, '', 34000, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas_penarikan`
--

CREATE TABLE IF NOT EXISTS `aktivitas_penarikan` (
`id_aktivitas_tarik` int(11) NOT NULL,
  `tgl_penarikan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ket_penarikan` varchar(255) NOT NULL,
  `nominal` int(11) NOT NULL,
  `sisa_saldo` int(11) NOT NULL,
  `status_penarikan` varchar(55) NOT NULL,
  `id_dompet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas_produsen`
--

CREATE TABLE IF NOT EXISTS `aktivitas_produsen` (
`id` int(11) NOT NULL,
  `id_aktivitas` int(25) NOT NULL,
  `status_aktivitas` int(11) NOT NULL,
  `waktu_aktivitas` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_produsen` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aktivitas_produsen`
--

INSERT INTO `aktivitas_produsen` (`id`, `id_aktivitas`, `status_aktivitas`, `waktu_aktivitas`, `id_produsen`) VALUES
(14, 41, 2, '2018-05-14 07:25:05', '1'),
(15, 41, 2, '2018-05-14 07:28:33', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aktivitas_transfer`
--

CREATE TABLE IF NOT EXISTS `aktivitas_transfer` (
`id_aktivitas_transfer` int(11) NOT NULL,
  `tgl_transfer` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nominal` int(11) NOT NULL,
  `ket_transfer` varchar(255) NOT NULL,
  `sisa_saldo` int(11) NOT NULL,
  `tujuan_transfer` int(11) NOT NULL,
  `id_dompet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aktivitas_transfer`
--

INSERT INTO `aktivitas_transfer` (`id_aktivitas_transfer`, `tgl_transfer`, `nominal`, `ket_transfer`, `sisa_saldo`, `tujuan_transfer`, `id_dompet`) VALUES
(1, '2018-05-12 16:19:40', 50000, 'Hebat jualan nya.. sedikit rejeki dari kami', 290000, 3759235, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `id_artikel` bigint(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `tgl_post` date NOT NULL,
  `penulis` varchar(40) NOT NULL,
  `status` varchar(7) NOT NULL,
  `gambar` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul`, `deskripsi`, `tgl_post`, `penulis`, `status`, `gambar`) VALUES
(20180131052019, 'bagaimana bisa', '<p>										</p><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALAAAACSCAYAAADl00BjAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAABIAAAASABGyWs+AAAc6klEQVR42u2deXQbx33HvzOL+yAAkuCtixKtw7ptHdYVx4diObbruGllO2nStInapE2apmnStC9xXmu3fWndponbxE2bw01tVY3dyokdybLTyJYtW5IjmtZhiZRE8b5BAsSN3ekfFCUSXOxilgsSS+3nPTyJ2N3BDvDFD7/5/X4zQxhjMDExKnS2b8DEZDqYAjYxNKaATQyNKWATQ2MK2MTQmAI2MTSmgE0MjSlgE0NjCtjE0JgCNjE0poBNDI0pYBNDYwrYxNCYAjYxNKaATQyNKWATQ2MK2MTQmAI2MTSmgE0MjSlgE0NjCtjE0JgCNjE0ltm+AZPiQpIYO3muDfsOHcev3mvDmUtdGI0lQQhQWVqC9csWYMuaxXj/zcuwfFE1BErJbN4vMdeFKD4YoPlDIYAmQTHG2Ksnz+Nr392PY6cuIZFKK57vtNtQX1uOu7etxt1bV2FNwzwESlwzLmZTwEXGm0MZ9unGmKZrBQL823o31voELiElUmn25088h+8+exjxZIr7dSklqKsIYOuaJbh3x1rcetNSBANeWITCW2dTwEXGty8k2eeatAnYYyFou8uHgJXkLZxYIsX2PPYU/vPnb+rWB7fTjo03LsKuLSuxa+sqrFhUA0rzvyceTAEXGXtOxtj3WpOart0QsODYrd68hSJKEvvUoz/CD55/vWD9ESjFotpy3L11Fe7ZvgY71t0Au82im5jNKESRcSosar52c6nAdf5TPzuKH7+on+WVQ5QktLT34Vt7X8Guz34T7//9v8Ob717UzWqaAi4i4iJjF6LaBXxLaf5BpdFYkv3ZEz9BOqP99XgRJQlHmy7gw1/+Dtp6hnQRsSngIqI1JiGS0XatUyBY68vfAv/wZ6+jbygyK/3s7AvhiX2/0KUtMw7MC2OMhYfARH6lEbcPxO7I6f81j0pIiNoMU62DYIErP3uUSKXZ9/cfUT2PUoKlC6qwZfUSDIWjOH2hEy0dfZCk6RvPI43NSGdEZrXwRUyyMQXMCUtEEf3GHkj9XdzXuj77OCyrtuQ8fjoiag4Ar/YJcAn5jfRPtXSiqblD8RyrRcAje+7DHz14BzwuOwGASCzB3mvtwYtHmvDzN07h7KVuhKNxTffrsFkh0Ok7AKaAeUklIQ32gI0O810nWEA8PsVTpjOA4/F/9718AqIkKZ7zwG3r8eWP75oUy/W6HGTDioXYsGIhvvrJe9mlrgEc/tU5vHCkCf934j2MjMbzts633rRUl9CaKWBOWCoBFh/lvo7YnSDeQM7jEgM7PQMCHo0l2f5fnlQ970sfu0sxEUEpIYvrglhcF8Tv3LcNsUSKHWlsxgtHmvDKsbM429qdU8zLFlbh8w/fobmvEzEFzAmLjwJp/mwVcbhAPP6cx1MSw7lRKf8GJ1BqI1jqzW8A99api7jQ0a94zor6Gqy5YR7XPbgcNrJz843YuflGJFMZdrGzHy8cacKBo6dw+kIXhsJRlLgd2LH+Bnzriw+hxO3UJRZsCpgTFh7SdB3x+EEcuWsFWqLaB3DLvQL81vz08N95uA+779wwrSIdu81Cli+qxvJF1fjCR3aygeFRhKNxuJ12VJaW6JqVMwXMCRsZ1HQdKatWPD4d//dmvwALUS/iiSVSbP/hRsVznHYbfu19a6f/Rl2BUkIqSr2oKPXq1uak9gvS6hxG0miBabBW8fi70xDwlrL87NCht86gLxRWPGd1Qx1WN9RN812aOUwBc8LC2iwwLS+MBSYANgfUBcwYY/sOHVeNEjy4cwMIKUzhTSHQ1YVgjLHG8+3Yf7gR77Z0IJnSmFbiwO914YkvPQy/d6p/+WbHU6x1+Bh3m0tKt+Pmmt2yH6JWH5iW1eQ8FhMZuxDVNoCrdVLMyyOBMRyJ4+dvvKt4jtflwAe3rdZ0H7OFbgJmjLEnnzuML/3TTxCJJWasA2U+DyKxBPxe15RjI4kudIabuNuscDfk7qdmFyK3gEfSDO0xbQLeXGrJq4j9wNFTCIWVyzQ3raxHfV1Q033MFroJ+Mylbnz+8b0zYnUnkkpnkErL//xSoq17gsJ1kpZBnMUK4s8tjMsxCeGMtgjE1jziv5LE2NMH1KvOHrpr46xPEeJFNx/43/73tRkXLwCkMyKSOaa/UMJXXnjtuhyiYIyxSIi7PeLxg9idOY9r9X8tBLg5oN7H9t4hvP5Oi+I5RnQfAB0FfKGjb1Y6kMpkkMpREihQq7Y3JYeAWTIOluTP/VNvACiAgP1WgmV5JDB+/sa7qu7DHZtWIOgvTKirkOgmYFHU5sNNF0liiCfkM2OaXQiaQ8CxCJDRkIXzlYJY7TmPaw2hNXgElNuUIwaMMfbMQeWBLCEEu+/cULBpP4VENwHXVQam34hGcg0atQo453XxUSDNP92HlteOqUSuSZGxFo0RiHziv++19uDts5cVzyktcePubas03cNso5uAa4OzJ+DRmLyoBJ19YBaLgKXTnK0BVCEL1xFnCKW0DeBuyWMK0U9fewfRuPKX7oPbVsPrchjO+gI6RiHyscDV5T54XQ7dOyEI8t9D7RZYXhhsdASQ+H/uiUIS43JMxKiGCIRTIFjjU+/fXhX3gVKC37zzZk3vUzGgm4DzscBf3/Nr2PPAjhn7pmsdxOUKo0mas3C5Y8CnwpKmIvaFLooaFaN55mIXO3muTfGc+VVl2L6uAUZlRn3gzn7+ENS0OqezD6w5iaEgYK0DuDV5zMD4zwNvqbZzz7bVupU2zga6Cbim3A+1FHpH70wLWJsPnDMKoSWJIVhAAhU5D2sNoW1WSWDEEin2vErlGaUEu3du0PT6xYJuAnY5bCgtcSueM9MWWCD6xoGlCL8FpoEgiCB/H2kJ7NyoNgGrDeDeOd+OM5eU5+0tXVCFdUvna3r9YkE3H5hSgupyHwZHck+36R4YgShJTCld2dbYxdrf6eZ67YrFZWjYtnBKm7q7ECP8Aial1UCO7l6IiohqGMD5rAQ3ligL+JmDx1Qrz+6/dR3cTrth3QdARwELlKK63IdTFzpznjMyGkckKl94M07PuX689u/HuV57+W2L0bBt4ZTnKdXbB+Z3IWhZFUDkFXw2IkHLJIx1PgEOhZKFcDTOXjiiXMRECMFDH9io6f0pJnRzISglpCboVzwnEk2oVqo5vHbwkorL12BojQMLOg7ilOqAtU6j3xCwQFCYgfHGOxdwuVv5y7ZqSS1WLlYusjcCuha011YoRyLC0TjC0TwEzPmjlo7nKubR0QdmEpM0FPLQ8twi0TKAI1AfwO17+Xhe896MVLieC10FXKci4IwooWdgRPEch9euGs3IJpVDwIKOiQwWDQMip+AIyWmBkxLYuQi/gB0CwXp/7l+WcDTOnj/8jmIbbqcdD9x2k6b3ptjQ2QKrh9LUIhEOj34C1l7MM9Vys/AQwLsUrWDJWQccExkuaqiBqFFZQurg0dMIRaKKbaxbOh9LF1Rqem+KDV0FXFlaAoug3KRaLNjusenoQuhXCyGFQ+Bd+Z9YbCC+ctljbRqL2DcpzMCQpHznvW2cE+4DoLOAK0pLYLUoi6azf1jxuBYXIp3IAGyqurTXA8u4EBENFthmB/WVyR7SmsBQWoFnKBzFwaOnFa/3eZyGrTyTQ3cLrCZgVQvsskGw8N0WkxjSyamRCD2nFGnJwtFABSDIr0auJYVsIcBGhRkYz7/aqBrl2ba2AQuqyjBX0FXALoeNTDsbR0B4Q2lMYsjIClg/F0JbCE2piIdfwBV2inq3fJ9ESWL7DqnHz3fvNGbhei50XxdCLZTW1T8MSVL+LeYWMGNjbkQW2qMQcj6whixcjjrguMZp9A0eijKbvPZauwZxtOmi4vU+jxO7tswd9wEoiID9isdjiZTqKNlZwlcznMuFIEQA4e4ikfeBdbTA/UmGngS/gG9RGMD97LV3VNfqvWvLSpT5lH8hjYbuAlaLBafSourS9o4SfSwwIRSE8HWREjp1EMkkpknAOdaCaItLGEnzRyByDeAkSX3eG6VkziQvJvVL7wbVBZxBf0hZwE5eAUuQ9YEJCCi3gGWsdiYDFgtztQNCcq7GczrMn0K2U+RMYJy+2Il3W5RXXC/3e3Dnphs5X7X4mXEfWJQk9AyqZ+N4YIzlEDC/BSaEAllGiolpsCjfhijE5gDx+mWPaRnANXiEnP7v/sONiCWUZ0vfu33t1a0C5hIzboEBoKNPJRunxQfO5UKALxJBIWBKnUwmAxYd4WqHuEtAXPLrLGgJoa3zC3DKzMDIZ9q8QKnhC9dzobuAS31uOO02xXPUYsG8LgQgn04mhGp0IbIscDoJFo9ytUNcXlkBpyQwLSux5/J/T55rw9lLyvXT9XVB3LJqMfdrGgHdBVzidsLrVhZgPtk4XuTSyQREgwshAFnXsNHhMUebpx1/UDaJ0RaXEOYcwBEAm3Isobr3peNQ2y74nm2r56T7ABRAwD6PU3XqfPeAciyYNwoByFtgqiGMJmuBNczEyBVCaxkVkeDcZ81jIVglMwNjNJZkP321Ufk+5sC8N8X+6d1gXtm4vmEo5TLsLhsoZzo5FZO3wLzZuDG/OUvAWmZi5FiR/XRYBO8+gRsCAqx0avz3xNlWnLvcq3jtqiXGWnGdl4Ks0F5XWap4vHtgBJJCwbXVaeWuh5CtSNMUBxamxIG1rAeRKwunZQCXy/995uBbqu7D/beug9Num5PuA1AgAdeqTC2KJ1MYCuceFFkdFgg2PsuZkolCUFBuF2Ls/GwLrGUmxlQBSwzsbITTl8ZYCWU2oXCMHXjjlPK1hODBncaf96ZEYQScRyits2845zGb0wqLlU/AsoM4wu9CyPnNmlwIGR84w4CznLMwvBaCG2WWUD3S2Kwajly3dD6WLayas9YXKJQLkYeA23tzD4ysDisETgHLhtE0JDIoEaZUG/CWUhKHW3ZTw9aYiAhnEXudk2KhzAyMpw+8pVq4/pFdm7hey4gUyAeengUWrJRYnXzF6OlEBixrZDg2IOMPo2Vfw1uJRvzlgMxiJlr8340BATRrVBkKx9iBo8rug9flwD3b13C/ntGYFR8YUK8LdnLGgjMJfVLJUwZxYoaxKF8dBPWXg1inCvhUmD+BITcD+cXXmzAyqlx5dvOKhWiYV4G5TkEEXOpzw+WYXjaONxacTqSnjMgJIaAaBnETDR5LxoFkjK+NQCVAhSm+J28NBCVTB3Bj895OqEYf5tK8NyUKImCb1YJgQHm/hc7+EJjCp+D08dVDpBKZKckybeWUk10IloiCJfm2DZOLASdExpo510Grc1LMd06+/57BEbx87IzidUZecZ2XggjYnoeAB4ZHkUzn3tXI4dHiQmRZYFBCuKMQdPIgLhEDUpwClgmhDacZ2uN8LsQyj4BAVgj3+VfVK892rL8BNeV+rtcyKgURsNUioEJFwKFwTPGD4C3ouTKIm/I87/JSVwZxV1XD4lEwnn0xCJHdUqArwb+VwKZSYdIMjLF5bydUr5tr896UKIiACSFELRY8FI4qCpi3pHLMB5bpIOe8uOx5dCwS4ptOTylIadWUp09pKGLPzsC1tPfh2OlLiteUlrhx1y0rOV/JuBRss2+1WHA0nkRIIRvHa4EzKRGSzFZfvGsEZwueO41M6NiKlFnwhtCsdGwRv4n87y9P5rVhi8/jxPVCwQSsNrkTUI4FayqplEsncy6xmi1g3ko04nCBuH1TnueNQCz3CvBbr3kB+bgPYxu2zL15b4p9LlTDNUH1ddKa23uRTGWYKJNS4nUhAPl0Mn8qOduF4BMwLauesidcSgI7zxmBuNlvgWVCPK+puQNnLiqvuF5ZWoLbNizjft+MjG4LXGdTGwyAEGX38XN/9wweeXI/FtdVoL42yBrmVeCGBZWorw0iQOygApV1C3Ihl07mdSGyB328aWS55VQ7ExIGOQdw2VsIPPvK20iklPeo+9D718PlmLuVZ3IUTMA1QT8oIVDbRCoUjuHEmVacONN67aYECpvFAjujqC8JwJMRUE7sqHV6YYlKCFgdsEsEYmKyVZMvaud0IWi2D8zpQshMpb8ck7hqIGwUuMl/7T5ESWL/pbLqjkWg+I07jLvfm1YKJuAynxsOuzXnLppKZEQJGTGFGIC3BybM9woBFkrhczhgFQmqXB7UONxwJggWeHy41DMIxzwPczvtcNqtIIQQ/ijEZIvNW4kmF0I7w1nE7rcSLPde8+6On25FS7vyZupL5lVgw4qF3O+10SmYgAkhpDYYYOcu9+jabkaSMBgbS+32JKNoHD8QAv7xL07C53GiJujH/KpSLKmrYPCG4SwPoqoyDYmGAcJASG41TXcQJ1dGyTuAW+uzwDFhBvLel46pXnPfjrWG37BFCwUTMACsXzYfegtYCcYYhiMxDEdiWQOesZCe112FBfMtcHpHUVcjoaIyA4dnFNWVBDZnDBJLTx70pZOMxUfzvwEqyIbQmjgFvHXCJt7haJz99FXlFdcFSvHgHNiwRQsFFfBDH9iEn7zyNtIZbWvh6k0kKuHU2RQAG8Y9SkHwwmEHBIuEeXUWvFYziFfm/w9rmF+JJQEbyuFCmR2wpmKqM5OJ3QninRz/HpuFkX//x/bAuPYlOtp0EZe6BhSvWbt0HpYvqsb1SEEFfPe2Vfjk/dvxr8+9qrrpyGwhigzRGAAQnD4j4vSZTuzH2FZhhBCUOZejVMigXEhjXQCoEoex3JXGDfYEvPEQ3IIEq5QBYRJgd04pZL/MOY3ebyVY5rkm4L0vHVOtPPvQ+9fDYbNed+4DUGABC5SSb/3pw2zzqno89cJRHD/dqrqCYjHBGMNALIMBAOdhxRvDAHBtz4sSO8USD0WpFMFKr4j6pBPzjrWgYUEVq68th9NuI2fDItdecAtcFDVXKtAGR0bZS28qr7g+vmjf9QpR+3brBWNjxZO9Q2Gcb+vB+cu9aG7vQ3NbLy53D6KzP4SB4VHVaTJGgBACgRLUVZYiUF2FDmcA7ppaxD0+ZLx+0JIAhiEgLfOjtGehHU+ucxEA+O+XT7DdX3lS0QJvWb0Yr3//K9el9QUKbIEnQshYgrO63Ifqch/et37p1WPJVIZFE0kMR2Joae/D+bZeNLf1ormtD5e6+jEUjmI4EkdKofyymGCMISMytHYNoPWK/9o/9h7AZreCWO1wedwIzKtD3O2DJVgJIViBkMWJNfZSiJKDCZSSfNyH63XwNs6MWWCtpDMiGxwZRX8ogtauwWvibh8T+HAkhmQ6YxhxK+Gw21Dm96Ay4EV9bTleOPIu4sncFXslbidOPv011NcGr1sLXPQCVoIxxgaGR3G5exCt3YNXhX2hox8t7X3oGRyZEy5JLnZuvhEHn/jj61a8wAy6ENMlnkyxUDgGu80Cv9cFgVJCCCHBgBfBgBc3T8hCSRJjoiRhNJZES8eYnz1uuS929qOrfxi9Q2EkU8a22tfDtHk1itoC9wyOsGcOHsNPX30H59t6EU+kYLFQlLid2L6uAbvv3Ihbb1oKu82StxUSJYlFogmEowm09w7h/OVenG/rQXNbH1o6+tA3FEY4mkAskVL1P2eTYMCLxqcfQU3Qf11b4KIUcEaU2JPPHsbf/PBFdCqsPiNQits2LMM//smDuLG+ZtofJGOMhSIx9A1F0NkXQnN775VoyZi/3dkfQiotIp0RZ13cH779Juz729+/rmp/5Sg6AaczIvvUoz/Cf7x4NG//NRjw4pnH9uD2jcsL+mFG40k20d8+39aLCx19aO0aRGv3wIy6JM9+4zN44Lb117V4gSITsChJ7M++/Swe//FL3Bau3O/BK9/5IlY31M3ohypJjGVEEcl0Bm09Q5P87ea2PnQPjPnb4WhCN6tdE/Tj3LOPzdlFq3koKgEf/tU5dudn/kFz7cSO9TfgwLc/XzTLiTLGWDSewshoDL1DYTS39U0KA3b0hhCOJhCNJ/NOtQuU4h++sBufe/D2oujjbFM0AhYlie367Ddx6K0zmtuglODZb3wG99+6zhAfbjyZYt0DI+geGMGFjr5J2ckLHf2IJ1MQJemqK+V1OfCnH7sLX/nE3bAI1BB9LDRFI+Dmtl62/qN/qakAfiK7d27A3r/+PcN/uKIksY7eEC529uNy9yA8Lgc2r6pHXUXA8H3Tk6KJAx8/0zpt8QLA0aYLSKUzzGbNP7Q2XURJYo3n2vHSm6dx4mwrOnpDsFktqAn6sXXNEty1ZSUa5lVwRQwESsmC6jIsqJ47O8sXgqIR8OVu/kWk5RiNJTEUjqKqzDf9xvKg8Xw7e+S7+3HwzVOyUYh9h47D63Lg4bs24Wufupdd73FbvSnYtHpelHL+PIiShFR6Zgro9x06zu749ON4/tVGxRBaJJbAk88dxu2f/nucvdRdHD7bHKFoBJzPtgT54LBZobZLkh787LUm9rFH/h2DI/lPOXqvtQe7PvdNXOoaMEWsE0Uj4NVL6mARpn87DfMrCx4f7R4YYb/7Vz/QlLi43D2IPY8+hWSKc68BE1mKRsArl9Ricd30VxS/Z/vqgt/r4z8+iL4hvs2/J/LLt9/Di683Ffw+rweKRsBel4P83q+/b1ptVJf78NFdmwt6n8ORGHvuF7+aVhsZUcJTLxwt6H1eLxSNgAFgz4d2YNPKem0doQR/9en7UVvgOOnZS91o1SFicqSxGdF40nQjpklRCdjttJOnH/sU6muDXNcJlOKLH/0AfvverQW/x0tdA7rUNMSTafSFtLshJmMUlYABoL42SF765y9g29oG0DyypW6nHY9+5kN47A8egJDPBdNEaYdRHjKiiEh08tYFbAKF7sdcoWgSGRNZXBckh/7lC+yZA8fwrb0vo6mlY0pp5dg+aKvx5Y/vwpob5ukq3HENSZIExtikh9/Nv26xHIJAIWUSGBjIGVK7+rxcAi/7OULI1Yfa33IPSun4/w2VaClKAQOAw2Yln7hvK37rg7ew9t4hnGrpRPfACFwOG+ZVBbBu6Xx4XQ7FN1ySJDZRhJIkKT7GzxtHzhBW+Z2wCBQZjmVf5ShxORBwO5BOp6fVjl5MeBvZREFTSnM+ss6bFeEXrYCv3qBAyaKaciyqKZ9yTJIkJooiE0UR2Y9copwui2rKsLi2HOfa+qbVzo61i2Hj3E63kGR/caU8yjsnWndKKaOUQhCEKY8rgi+IwItewOMwxlg6nUYmk8H4v+NinUlcDht+4/Z1ePQHBzW3Ybda8OAd62f0vgvBuOiVBD9upQVBYFarFRaLBeP/6uGuFLWAGWMsHo8jkUgglSqeSZYfv3sj9h56Gy0dA5quv2/7KmxcsWC2uzEjMMauGppU6lq9C6UUNpuNORwOOBwOzWIumnrgbNLpNAuFQjNuYfPl1MVuPPTVH2GQMypx07J5+PHXPwafm38PkLmKxWJBaWkpBEHgFnHRhdHGydcPmy1W1lfjB199GIvryvM6nxKCW9c34Pt//rAp3izGxytaKFoLDACZTIbFYjEkEomitcR9oVF8b/8b+OGLb11ZS2LycUIIaspL8Icf3oHfvH0dnHa+TWfmMoIgwOl0wul0wmLRNgGhqAU8kUwmw1Kp1KwO4JSIxBI40nQJ77Z0oXtwZGxGRrkPG1cswIbl82ARiifiMBsIggCLxXL1YbPZNIt2IoYRcDaMMSZJEjKZzCRBj4fQit0FmUtkJ0MmhtDGBXvlmO6hNMMKWIlxcY+LODsuLJfAuHLdxDZmuxszzkR9ZcV483rMRiZvTgqYl/HUsdwjO508btUnPqf09/j/ZV5T8W9APl2c6+9808gAJopt0v9zPIo6tWwK2MTQFG0YzcQkH0wBmxgaU8AmhsYUsImhMQVsYmhMAZsYGlPAJobGFLCJoTEFbGJoTAGbGBpTwCaGxhSwiaExBWxiaEwBmxgaU8AmhsYUsImh+X+L8jBFLlbMogAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOC0wMy0wOVQwMDozNDozNC0wNTowMLZZBxwAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTgtMDMtMDlUMDA6MzQ6MzQtMDU6MDDHBL+gAAAAAElFTkSuQmCC" data-filename="logo.png" style="width: 176px;"><p>bagaimana bisa kami mengerjakan ini semua?									</p>', '2018-03-19', 'Administrator', 'feature', NULL),
(20180131060426, 'ingin membeli', 'apakah kamu ingin membeli apa yang dijual di sini?', '2018-02-20', 'Administrator', 'feature', NULL),
(20180131060554, 'bisakah kamu', 'bisakah kamu menjanjikan semua yang akan dan sedang kuinginkan', '2018-01-18', 'Administrator', 'trash', NULL),
(20180410050022, 'test', '<p>test</p><p>test</p><p>test</p>', '0000-00-00', 'Administrator', 'draft', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
`id` int(9) NOT NULL,
  `id_produsen` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_distributor` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `chattext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status_chat` int(6) NOT NULL,
  `chattime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivered_stats` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=237 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `date`
--

CREATE TABLE IF NOT EXISTS `date` (
  `n` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `date`
--

INSERT INTO `date` (`n`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90),
(91),
(92),
(93),
(94),
(95),
(96),
(97),
(98),
(99),
(100),
(101),
(102),
(103),
(104),
(105),
(106),
(107),
(108),
(109),
(110),
(111),
(112),
(113),
(114),
(115),
(116),
(117),
(118),
(119),
(120),
(121),
(122),
(123),
(124),
(125),
(126),
(127),
(128),
(129),
(130),
(131),
(132),
(133),
(134),
(135),
(136),
(137),
(138),
(139),
(140),
(141),
(142),
(143),
(144),
(145),
(146),
(147),
(148),
(149),
(150),
(151),
(152),
(153),
(154),
(155),
(156),
(157),
(158),
(159),
(160),
(161),
(162),
(163),
(164),
(165),
(166),
(167),
(168),
(169),
(170),
(171),
(172),
(173),
(174),
(175),
(176),
(177),
(178),
(179),
(180),
(181),
(182),
(183),
(184),
(185),
(186),
(187),
(188),
(189),
(190),
(191),
(192),
(193),
(194),
(195),
(196),
(197),
(198),
(199),
(200),
(201),
(202),
(203),
(204),
(205),
(206),
(207),
(208),
(209),
(210),
(211),
(212),
(213),
(214),
(215),
(216),
(217),
(218),
(219),
(220),
(221),
(222),
(223),
(224),
(225),
(226),
(227),
(228),
(229),
(230),
(231),
(232),
(233),
(234),
(235),
(236),
(237),
(238),
(239),
(240),
(241),
(242),
(243),
(244),
(245),
(246),
(247),
(248),
(249),
(250),
(251),
(252),
(253),
(254),
(255),
(256),
(257),
(258),
(259),
(260),
(261),
(262),
(263),
(264),
(265),
(266),
(267),
(268),
(269),
(270),
(271),
(272),
(273),
(274),
(275),
(276),
(277),
(278),
(279),
(280),
(281),
(282),
(283),
(284),
(285),
(286),
(287),
(288),
(289),
(290),
(291),
(292),
(293),
(294),
(295),
(296),
(297),
(298),
(299),
(300),
(301),
(302),
(303),
(304),
(305),
(306),
(307),
(308),
(309),
(310),
(311),
(312),
(313),
(314),
(315),
(316),
(317),
(318),
(319),
(320),
(321),
(322),
(323),
(324),
(325),
(326),
(327),
(328),
(329),
(330),
(331),
(332),
(333),
(334),
(335),
(336),
(337),
(338),
(339),
(340),
(341),
(342),
(343),
(344),
(345),
(346),
(347),
(348),
(349),
(350),
(351),
(352),
(353),
(354),
(355),
(356),
(357),
(358),
(359),
(360),
(361),
(362),
(363),
(364),
(365),
(366),
(367),
(368),
(369),
(370),
(371),
(372),
(373),
(374),
(375),
(376),
(377),
(378),
(379),
(380),
(381),
(382),
(383),
(384),
(385),
(386),
(387),
(388),
(389),
(390),
(391),
(392),
(393),
(394),
(395),
(396),
(397),
(398),
(399),
(400),
(401),
(402),
(403),
(404),
(405),
(406),
(407),
(408),
(409),
(410),
(411),
(412),
(413),
(414),
(415),
(416),
(417),
(418),
(419),
(420),
(421),
(422),
(423),
(424),
(425),
(426),
(427),
(428),
(429),
(430),
(431),
(432),
(433),
(434),
(435),
(436),
(437),
(438),
(439),
(440),
(441),
(442),
(443),
(444),
(445),
(446),
(447),
(448),
(449),
(450),
(451),
(452),
(453),
(454),
(455),
(456),
(457),
(458),
(459),
(460),
(461),
(462),
(463),
(464),
(465),
(466),
(467),
(468),
(469),
(470),
(471),
(472),
(473),
(474),
(475),
(476),
(477),
(478),
(479),
(480),
(481),
(482),
(483),
(484),
(485),
(486),
(487),
(488),
(489),
(490),
(491),
(492),
(493),
(494),
(495),
(496),
(497),
(498),
(499),
(500),
(501),
(502),
(503),
(504),
(505),
(506),
(507),
(508),
(509),
(510),
(511),
(512),
(513),
(514),
(515),
(516),
(517),
(518),
(519),
(520),
(521),
(522),
(523),
(524),
(525),
(526),
(527),
(528),
(529),
(530),
(531),
(532),
(533),
(534),
(535),
(536),
(537),
(538),
(539),
(540),
(541),
(542),
(543),
(544),
(545),
(546),
(547),
(548),
(549),
(550),
(551),
(552),
(553),
(554),
(555),
(556),
(557),
(558),
(559),
(560),
(561),
(562),
(563),
(564),
(565),
(566),
(567),
(568),
(569),
(570),
(571),
(572),
(573),
(574),
(575),
(576),
(577),
(578),
(579),
(580),
(581),
(582),
(583),
(584),
(585),
(586),
(587),
(588),
(589),
(590),
(591),
(592),
(593),
(594),
(595),
(596),
(597),
(598),
(599),
(600),
(601),
(602),
(603),
(604),
(605),
(606),
(607),
(608),
(609),
(610),
(611),
(612),
(613),
(614),
(615),
(616),
(617),
(618),
(619),
(620),
(621),
(622),
(623),
(624),
(625),
(626),
(627),
(628),
(629),
(630),
(631),
(632),
(633),
(634),
(635),
(636),
(637),
(638),
(639),
(640),
(641),
(642),
(643),
(644),
(645),
(646),
(647),
(648),
(649),
(650),
(651),
(652),
(653),
(654),
(655),
(656),
(657),
(658),
(659),
(660),
(661),
(662),
(663),
(664),
(665),
(666),
(667),
(668),
(669),
(670),
(671),
(672),
(673),
(674),
(675),
(676),
(677),
(678),
(679),
(680),
(681),
(682),
(683),
(684),
(685),
(686),
(687),
(688),
(689),
(690),
(691),
(692),
(693),
(694),
(695),
(696),
(697),
(698),
(699),
(700),
(701),
(702),
(703),
(704),
(705),
(706),
(707),
(708),
(709),
(710),
(711),
(712),
(713),
(714),
(715),
(716),
(717),
(718),
(719),
(720),
(721),
(722),
(723),
(724),
(725),
(726),
(727),
(728),
(729),
(730),
(731),
(732),
(733),
(734),
(735),
(736),
(737),
(738),
(739),
(740),
(741),
(742),
(743),
(744),
(745),
(746),
(747),
(748),
(749),
(750),
(751),
(752),
(753),
(754),
(755),
(756),
(757),
(758),
(759),
(760),
(761),
(762),
(763),
(764),
(765),
(766),
(767),
(768),
(769),
(770),
(771),
(772),
(773),
(774),
(775),
(776),
(777),
(778),
(779),
(780),
(781),
(782),
(783),
(784),
(785),
(786),
(787),
(788),
(789),
(790),
(791),
(792),
(793),
(794),
(795),
(796),
(797),
(798),
(799),
(800),
(801),
(802),
(803),
(804),
(805),
(806),
(807),
(808),
(809),
(810),
(811),
(812),
(813),
(814),
(815),
(816),
(817),
(818),
(819),
(820),
(821),
(822),
(823),
(824),
(825),
(826),
(827),
(828),
(829),
(830),
(831),
(832),
(833),
(834),
(835),
(836),
(837),
(838),
(839),
(840),
(841),
(842),
(843),
(844),
(845),
(846),
(847),
(848),
(849),
(850),
(851),
(852),
(853),
(854),
(855),
(856),
(857),
(858),
(859),
(860),
(861),
(862),
(863),
(864),
(865),
(866),
(867),
(868),
(869),
(870),
(871),
(872),
(873),
(874),
(875),
(876),
(877),
(878),
(879),
(880),
(881),
(882),
(883),
(884),
(885),
(886),
(887),
(888),
(889),
(890),
(891),
(892),
(893),
(894),
(895),
(896),
(897),
(898),
(899),
(900),
(901),
(902),
(903),
(904),
(905),
(906),
(907),
(908),
(909),
(910),
(911),
(912),
(913),
(914),
(915),
(916),
(917),
(918),
(919),
(920),
(921),
(922),
(923),
(924),
(925),
(926),
(927),
(928),
(929),
(930),
(931),
(932),
(933),
(934),
(935),
(936),
(937),
(938),
(939),
(940),
(941),
(942),
(943),
(944),
(945),
(946),
(947),
(948),
(949),
(950),
(951),
(952),
(953),
(954),
(955),
(956),
(957),
(958),
(959),
(960),
(961),
(962),
(963),
(964),
(965),
(966),
(967),
(968),
(969),
(970),
(971),
(972),
(973),
(974),
(975),
(976),
(977),
(978),
(979),
(980),
(981),
(982),
(983),
(984),
(985),
(986),
(987),
(988),
(989),
(990),
(991),
(992),
(993),
(994),
(995),
(996),
(997),
(998),
(999),
(1000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaksi`
--

CREATE TABLE IF NOT EXISTS `detail_transaksi` (
`id_dtransaksi` int(11) NOT NULL,
  `harga_modal` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `id_produk_variasi` int(11) NOT NULL,
  `id_dist` varchar(20) NOT NULL,
  `id_transaksi` int(11) NOT NULL,
  `persen_produsen` double(2,2) NOT NULL,
  `persen_distributor` double(2,2) NOT NULL,
  `harga_transaksi` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_dtransaksi`, `harga_modal`, `qty`, `id_produk_variasi`, `id_dist`, `id_transaksi`, `persen_produsen`, `persen_distributor`, `harga_transaksi`) VALUES
(1, 20000, 5, 92, '3759235', 1, 0.90, 0.10, 30000),
(2, 30000, 10, 93, '3759235', 1, 0.90, 0.10, 40000),
(3, 20000, 20, 92, '3759235', 2, 0.90, 0.10, 30000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `distributor`
--

CREATE TABLE IF NOT EXISTS `distributor` (
  `ktp_distributor` varchar(20) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama_distributor` varchar(55) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(11) NOT NULL,
  `hp` varchar(55) NOT NULL,
  `telp` varchar(55) NOT NULL,
  `alamat_distributor` varchar(255) NOT NULL,
  `foto_distributor` varchar(55) NOT NULL,
  `nama_toko` varchar(55) NOT NULL,
  `no_rekening` varchar(25) NOT NULL,
  `nama_bank` varchar(25) NOT NULL,
  `nama_rekening` varchar(25) NOT NULL,
  `logo_toko` varchar(25) NOT NULL,
  `template` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `distributor`
--

INSERT INTO `distributor` (`ktp_distributor`, `username`, `password`, `email`, `nama_distributor`, `tgl_lahir`, `jenis_kelamin`, `hp`, `telp`, `alamat_distributor`, `foto_distributor`, `nama_toko`, `no_rekening`, `nama_bank`, `nama_rekening`, `logo_toko`, `template`) VALUES
('1', '', 'asdasd', '124324', 'guanteng', '2018-03-13', '0', '12413234', '12312423421', 'Komp. Mesjid Nurul Huda Panganak Kelurahan Pintu Kabun Kota Bukittinggi', '001462A.jpg', 'asdasdasd', '', '', '', '', 0),
('2', '', '', '0', 'boyak', '0000-00-00', '0', '', '', '', 'ntap.jpg', '', '', '', '', '', 0),
('2131231234', 'dinda', '$2y$10$sVbbAGVybiu1GNaCkfzdyOBg62xmlBXMob66Vo0MfHkt2chs', 'dindaaisyahjahadi@gmail.com', 'dinda dinda', '0000-00-00', '', '434123213', '', '', '', '', '', '', '', '', 0),
('3759235', 'geri', '$2y$10$nNy3zcoKdu0abbPIKpjQL.b7e5zIIYL9M8sjLsZ8msdSmmKa', 'geri.kurniawan16@gmail.com', 'gerii gerii', '0000-00-00', '', '5363726438', '', '', '', '', '', '', '', '', 0),
('5', '', '12312', '1231', 'geri', '2018-04-12', '0', '123124234', '0987654321', 'asdasoksanfsakf', 'store-512.png', 'bagus', '', '', '', '', 0),
('54323423', 'siaaap', '$2y$11$JnEit3gr3/215OfEe3KBL.cgBzG7L7k5FO80nR31A4O8MIaq1VZOe', 'dindaaisyahjahadi@mail.com', 'siaaap siaaap', '0000-00-00', '', '5435352', '', '', 'organza.jpg', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dompet`
--

CREATE TABLE IF NOT EXISTS `dompet` (
`id_dompet` int(11) NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_pemilik` varchar(25) NOT NULL,
  `status_dompet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dompet`
--

INSERT INTO `dompet` (`id_dompet`, `saldo`, `id_pemilik`, `status_dompet`) VALUES
(1, 290000, '1', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
`id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(55) NOT NULL,
  `id_kategori_umum` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`, `id_kategori_umum`) VALUES
(4, 'Baju', 3),
(5, 'Celana', 3),
(6, 'Accesories', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_khusus`
--

CREATE TABLE IF NOT EXISTS `kategori_khusus` (
`id_kategori_khusus` int(11) NOT NULL,
  `nama_kategori_khusus` varchar(255) NOT NULL,
  `id_kategori` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_khusus`
--

INSERT INTO `kategori_khusus` (`id_kategori_khusus`, `nama_kategori_khusus`, `id_kategori`) VALUES
(4, 'Kemeja', 4),
(5, 'Kaos', 4),
(6, 'jeans', 5),
(7, 'katun', 5),
(8, 'Jam Tangan', 6),
(9, 'Gelang', 6),
(10, 'Topi', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_umum`
--

CREATE TABLE IF NOT EXISTS `kategori_umum` (
`id_kategori_umum` int(11) NOT NULL,
  `nama_kategori_umum` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_umum`
--

INSERT INTO `kategori_umum` (`id_kategori_umum`, `nama_kategori_umum`) VALUES
(3, 'Fashion Pria'),
(4, 'Alat Dapur'),
(5, 'Elektronik'),
(6, 'Perabot'),
(7, 'Fashion Wanita');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsumen`
--

CREATE TABLE IF NOT EXISTS `konsumen` (
`id_konsumen` smallint(6) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `namaDepan` varchar(100) NOT NULL,
  `namaBelakang` varchar(100) NOT NULL,
  `nomorHp` varchar(25) NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `kelurahan` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `kodePos` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsumen`
--

INSERT INTO `konsumen` (`id_konsumen`, `username`, `password`, `namaDepan`, `namaBelakang`, `nomorHp`, `email`, `status`, `alamat`, `kelurahan`, `provinsi`, `kota`, `kodePos`) VALUES
(7, 'khulafaur', '123123', 'Khulafaur', 'Rasyidin', '0822938589405', 'idhinofficial@gmail.com', 'Aktif', 'idhind', 'jnsdnsdsd', 'asass', 'asasas', '12121212'),
(8, 'dindaaisyahj', 'dindadinda', 'Dinda', 'Aisyah', '082218317916', 'dindaaisyahjahadi@gmail.com', 'Aktif', 'Kp. Ciccayur RT 02/01 No. 38 Kecamatan Cisauk', 'Cisauk', 'Banten', 'Kabupaten Tangerang', '15341'),
(9, 'geriaja', '123123', 'geri', 'kurniaganteng', '547586797978', 'geri.kurniawan16@gmail.com', 'Tidak Aktif', 'bandung', 'dayeuhkolot', 'jabar', 'bandung', '123123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
`id_produk` int(5) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `deskripsi_produk` varchar(500) NOT NULL,
  `id_produsen` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `id_kategori_khusus` int(255) NOT NULL,
  `persen_produsen` double(2,2) NOT NULL,
  `persen_distributor` double(2,2) NOT NULL,
  `status_produk` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi_produk`, `id_produsen`, `gambar`, `id_kategori_khusus`, `persen_produsen`, `persen_distributor`, `status_produk`, `created_date`) VALUES
(41, 'Kaos Oblong', 'DI ciptakan dengan perasaan yang dalam dan menggunakan bahan langka', '1', '3426970_B_V7.jpg', 5, 0.95, 0.05, 1, '2018-05-12 02:00:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_distributor`
--

CREATE TABLE IF NOT EXISTS `produk_distributor` (
`id` int(11) NOT NULL,
  `id_produk` bigint(20) NOT NULL,
  `ktp_dist` bigint(20) NOT NULL,
  `status_publish` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_variasi`
--

CREATE TABLE IF NOT EXISTS `produk_variasi` (
`id_produk_variasi` int(11) NOT NULL,
  `nama_variasi` varchar(55) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `gambar_produk` varchar(255) NOT NULL,
  `status_produk_variasi` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_variasi`
--

INSERT INTO `produk_variasi` (`id_produk_variasi`, `nama_variasi`, `stok`, `harga`, `harga_jual`, `id_produk`, `gambar_produk`, `status_produk_variasi`, `created_date`) VALUES
(96, 'Int Size', 20, 10000, 15000, 41, '18760417_B_V21.jpg', 1, '2018-05-14 07:25:05'),
(97, 'int size', 10, 14000, 15000, 41, 'Kaos_Raglan_Lengan_Panjang_Body_Warna_Hitam_Polos_Cowok1.jpg', 1, '2018-05-14 07:28:33'),
(98, 'local', 12, 15000, 17000, 41, 'kaos-34-bj4852.jpg', 1, '2018-05-14 07:28:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produsen`
--

CREATE TABLE IF NOT EXISTS `produsen` (
  `ktp_produsen` varchar(255) NOT NULL,
  `nama_produsen` varchar(50) NOT NULL,
  `username` varchar(55) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `no_rekening` varchar(20) NOT NULL,
  `nama_rekening` varchar(50) NOT NULL,
  `nama_bank` varchar(30) NOT NULL,
  `foto_produsen` varchar(255) NOT NULL,
  `logo_toko` varchar(255) NOT NULL,
  `nama_toko` varchar(30) NOT NULL,
  `created_date` date NOT NULL,
  `deskripsi_toko` varchar(150) NOT NULL,
  `no_npwp` varchar(55) NOT NULL,
  `foto_npwp` varchar(150) NOT NULL,
  `no_siup` varchar(50) NOT NULL,
  `foto_siup` varchar(150) NOT NULL,
  `alamat_toko` varchar(255) NOT NULL,
  `t_daftar_perusahaan` int(11) NOT NULL,
  `status_produsen` int(11) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produsen`
--

INSERT INTO `produsen` (`ktp_produsen`, `nama_produsen`, `username`, `email`, `password`, `alamat`, `tgl_lahir`, `no_hp`, `telp`, `no_rekening`, `nama_rekening`, `nama_bank`, `foto_produsen`, `logo_toko`, `nama_toko`, `created_date`, `deskripsi_toko`, `no_npwp`, `foto_npwp`, `no_siup`, `foto_siup`, `alamat_toko`, `t_daftar_perusahaan`, `status_produsen`, `jenis_kelamin`) VALUES
('1', 'geri kurniawan', 'admin', 'geri.kurniawan16@gmail.com', '$2y$11$nGB/0Dy2XasT4w0uIj4Z4u5W04EpoMLoI3sYt1dlT3GVWhiF/ovgm', 'Bukittinggi', '2018-05-16', '8786', '08213123', '5345353453', 'Geri', 'BNI', 'saiik.jpg', 'ini_nih.jpg', 'Toko Pakaian Sinar sari', '2015-10-16', 'Menjual Pakaian yang berkualitas', '0', '', '', '', 'Bandung', 0, 1, 'laki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produsen_distributor`
--

CREATE TABLE IF NOT EXISTS `produsen_distributor` (
`id_prod_dist` int(11) NOT NULL,
  `id_produsen` varchar(255) NOT NULL,
  `id_distributor` varchar(255) NOT NULL,
  `status_approve` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produsen_distributor`
--

INSERT INTO `produsen_distributor` (`id_prod_dist`, `id_produsen`, `id_distributor`, `status_approve`, `created_date`) VALUES
(1, '1', '54323423', 1, '2018-05-11 05:58:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_pemasukan`
--

CREATE TABLE IF NOT EXISTS `riwayat_pemasukan` (
`id_pemasukan` int(11) NOT NULL,
  `tanggal_pemasukan` date NOT NULL,
  `nominal_pemasukan` int(11) NOT NULL,
  `sisa_saldo_pem` int(11) NOT NULL,
  `id_dompet` int(11) NOT NULL,
  `id_transaksi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id_role` varchar(10) NOT NULL,
  `nama_role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
('FNC', 'Finance'),
('MKT', 'Marketing'),
('SU', 'Super Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok_produk`
--

CREATE TABLE IF NOT EXISTS `stok_produk` (
`id_stok` int(11) NOT NULL,
  `tgl_stok` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stok_lama` bigint(20) NOT NULL,
  `stok_baru` bigint(20) NOT NULL,
  `total_stok` bigint(20) NOT NULL,
  `id_produk_variasi` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok_produk`
--

INSERT INTO `stok_produk` (`id_stok`, `tgl_stok`, `stok_lama`, `stok_baru`, `total_stok`, `id_produk_variasi`) VALUES
(1, '2018-04-19 10:00:00', 12, 14, 0, 10),
(2, '2018-04-19 21:13:11', 1, 6, 7, 11),
(3, '2018-04-19 21:36:08', 15, 12, 27, 9),
(4, '2018-04-19 21:38:44', 15, 12, 27, 9),
(5, '2018-04-19 21:39:05', 15, 12, 27, 9),
(6, '2018-04-19 21:40:30', 7, 5, 12, 8),
(7, '2018-04-19 21:41:24', 27, 12, 39, 9),
(8, '2018-04-22 16:52:35', 39, 1, 40, 9),
(9, '2018-04-22 16:53:56', 40, 2, 42, 9),
(10, '2018-04-22 19:46:57', 42, 2, 44, 9),
(11, '2018-04-23 13:29:40', 44, 2, 46, 1),
(12, '2018-04-23 13:30:15', 44, 2, 46, 1),
(13, '2018-04-23 13:30:19', 44, 2, 46, 1),
(14, '2018-04-23 13:30:45', 44, 2, 46, 1),
(15, '2018-04-23 13:31:14', 22, 2, 24, 3),
(16, '2018-04-29 03:31:40', 22, 20, 42, 2),
(17, '2018-04-29 03:35:48', 22, 20, 42, 2),
(18, '2018-05-08 21:26:10', 22, 2, 24, 3),
(19, '2018-05-11 03:23:49', 20, 10, 30, 78),
(20, '2018-05-11 03:24:37', 10, 20, 30, 80),
(21, '2018-05-11 21:18:25', 30, -34, -4, 93);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
`id_transaksi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status_transaksi` int(11) NOT NULL,
  `no_resi` varchar(50) NOT NULL,
  `jasa_kirim` varchar(50) NOT NULL,
  `id_konsumen` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal`, `status_transaksi`, `no_resi`, `jasa_kirim`, `id_konsumen`) VALUES
(1, '2018-05-12', 3, '1234', 'JNE', 1),
(2, '2018-05-24', 3, '123', 'JNE', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `aktivitas_pemasukan`
--
ALTER TABLE `aktivitas_pemasukan`
 ADD PRIMARY KEY (`id_aktivitas_pemasukan`);

--
-- Indexes for table `aktivitas_penarikan`
--
ALTER TABLE `aktivitas_penarikan`
 ADD PRIMARY KEY (`id_aktivitas_tarik`);

--
-- Indexes for table `aktivitas_produsen`
--
ALTER TABLE `aktivitas_produsen`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aktivitas_transfer`
--
ALTER TABLE `aktivitas_transfer`
 ADD PRIMARY KEY (`id_aktivitas_transfer`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
 ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `date`
--
ALTER TABLE `date`
 ADD PRIMARY KEY (`n`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
 ADD PRIMARY KEY (`id_dtransaksi`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
 ADD PRIMARY KEY (`ktp_distributor`), ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `dompet`
--
ALTER TABLE `dompet`
 ADD PRIMARY KEY (`id_dompet`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
 ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kategori_khusus`
--
ALTER TABLE `kategori_khusus`
 ADD PRIMARY KEY (`id_kategori_khusus`);

--
-- Indexes for table `kategori_umum`
--
ALTER TABLE `kategori_umum`
 ADD PRIMARY KEY (`id_kategori_umum`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
 ADD PRIMARY KEY (`id_konsumen`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
 ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `produk_distributor`
--
ALTER TABLE `produk_distributor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk_variasi`
--
ALTER TABLE `produk_variasi`
 ADD PRIMARY KEY (`id_produk_variasi`);

--
-- Indexes for table `produsen`
--
ALTER TABLE `produsen`
 ADD PRIMARY KEY (`ktp_produsen`);

--
-- Indexes for table `produsen_distributor`
--
ALTER TABLE `produsen_distributor`
 ADD PRIMARY KEY (`id_prod_dist`);

--
-- Indexes for table `riwayat_pemasukan`
--
ALTER TABLE `riwayat_pemasukan`
 ADD PRIMARY KEY (`id_pemasukan`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
 ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `stok_produk`
--
ALTER TABLE `stok_produk`
 ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
 ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `aktivitas_pemasukan`
--
ALTER TABLE `aktivitas_pemasukan`
MODIFY `id_aktivitas_pemasukan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `aktivitas_penarikan`
--
ALTER TABLE `aktivitas_penarikan`
MODIFY `id_aktivitas_tarik` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aktivitas_produsen`
--
ALTER TABLE `aktivitas_produsen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `aktivitas_transfer`
--
ALTER TABLE `aktivitas_transfer`
MODIFY `id_aktivitas_transfer` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
MODIFY `id` int(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=237;
--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
MODIFY `id_dtransaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dompet`
--
ALTER TABLE `dompet`
MODIFY `id_dompet` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kategori_khusus`
--
ALTER TABLE `kategori_khusus`
MODIFY `id_kategori_khusus` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori_umum`
--
ALTER TABLE `kategori_umum`
MODIFY `id_kategori_umum` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
MODIFY `id_konsumen` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
MODIFY `id_produk` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `produk_distributor`
--
ALTER TABLE `produk_distributor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produk_variasi`
--
ALTER TABLE `produk_variasi`
MODIFY `id_produk_variasi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `produsen_distributor`
--
ALTER TABLE `produsen_distributor`
MODIFY `id_prod_dist` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `riwayat_pemasukan`
--
ALTER TABLE `riwayat_pemasukan`
MODIFY `id_pemasukan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stok_produk`
--
ALTER TABLE `stok_produk`
MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
