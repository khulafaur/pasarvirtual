<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Produsen_login/login';
$route['Produsen_laporan/(:num)/(:num)/(:num)'] = 'Produsen_laporan_penjualan/detail_penjualan/$1/$2/$3';
$route['Produsen_dashboard'] = 'Produsen_dashboard/dashboard/';
$route['Produsen_laporan_penjualan'] = 'Produsen_laporan_penjualan/list_tren_penjualan/';
$route['Produsen_detail_penjualan'] = 'Produsen_laporan_penjualan/detail_penjualan_distrubutor/';
$route['Produsen_kelola_bagihasil'] = 'Produsen_kelola_bagihasil/list_kelola_bagihasil/';
$route['Produsen_listing_distributor'] = 'Produsen_monitoring_distributor/list_distributor/';
$route['Produsen_arus_kas'] = 'Produsen_buku_kas/list_arus_kas/';
$route['Produsen_kelola_produk'] = 'Produsen_produk/index_kelola/';
$route['Produsen_login'] = 'Produsen_login/login';
$route['Produsen_registrasi'] = 'registrasi/registrasi';
$route['Produsen_invalid_regis'] = 'Produsen_login/invalid_regis';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
