<?php

class Produsen_chat extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model(array('m_produsen','m_chat'));
        if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
                redirect('login/error_page');
            }
        $dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
        $detailsData    =   $this->session->userdata('logged_in');
        $detailsData['saldo']= $dompet['saldo'];
        $this->session->set_userdata('logged_in', $detailsData);
    }

    public function index()
    {
        $data['dashboard'] = '';
        $data['produk'] = '';
        $data['distributor'] = '';
        $data['aruskas'] = '';
        $data['penjualan'] = '';
        $data['bagi_hasil'] = '';
        $data['kelola_produk'] = '';
        $data['pesanan'] = '';
        $data['chat'] = 'active';
        $data['stok']='';
        $data['tracking'] = '';
        $data['verifikasi'] = '';
        
        $data['saldo'] = $this->session->userdata['logged_in']['saldo'];

        $datestring = "%M %Y";
        $data["date"] = "Dashboard";
        $data['data1'] = 'Home';
        $data['data2'] = 'dashboard';
        $data['data3'] = 'Dashboard';
        $data['data4'] = '';
        $data['active'] = 'active';
        $data['active1'] = '';

         $id_produsen = $this->session->userdata['logged_in']['produsen_id'];
         $data['get_data']=$this->m_chat->my_distributor($id_produsen);
        $this->load->view('produsen/chatting/view_tampilan_chatting', $data);
    }

    public function getPanelChat()
    {
         $id_dist=$_POST['id_distributor'];
         $data["id_dist"]=$id_dist;

         $id_produsen = $this->session->userdata['logged_in']['produsen_id'];
         $data["id_produsen"]=$id_produsen;
         $data['get_data']=$this->m_chat->form_chat($id_produsen,$id_dist);
         $data['get_chat_data']=$this->m_chat->get_chat_data($id_produsen,$id_dist);
         //echo '<pre>'; print_r($id_dist); die;
         $this->load->view('produsen/chatting/chat_dashboard', $data);
    }
     public function submitChat()
    {
        $id_produsen = $this->session->userdata['logged_in']['produsen_id'];
        $chattext =$_POST['chattext'];
        $id_dist=$_POST['id_tuju'];
        date_default_timezone_set('Asia/Jakarta');
        $time = time();
        $object = array(
                'id_produsen'=>$id_produsen,
                'id_distributor'=>$id_dist,
                'chattext'=>$chattext,
                'status_chat'=>"1",
                'chattime'=>mdate("%Y-%m-%d %H:%i:%s", $time),
                );
        $this->m_chat->insert_chat($object);
    }

    public function refresh()
    {
        $id =intval($_POST['lastTimeID']);
        $data=$this->m_chat->get_chat($id);
        foreach ($data as $row) {
            $id = $row->id;
            $object = array(
                    'delivered_stats'=> 1,
                );
            $this->m_chat->delivered($object,$id);
        }
        
        echo json_encode($data);

    }
}