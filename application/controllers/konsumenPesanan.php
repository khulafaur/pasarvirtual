<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class konsumenPesanan extends CI_Controller {


	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('sedangLogin')){
            redirect('konsumenMasuk');
        }
        $this->load->model('m_konsumenPesan','mkp');
    }


	public function index(){


		 $data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();

		$this->load->view('konsumen/index/header',$data);
		$this->load->view('konsumen/pesanan/index');
		$this->load->view('konsumen/index/footer');
		
	}

	public function getCost()
	{
		$origin = $this->input->get('origin');
		$destination = $this->input->get('destination');
		$berat = 1000;
		$courier = $this->input->get('courier');

		$data = array('origin' => $origin,
						'destination' => $destination, 
						'berat' => $berat, 
						'courier' => $courier 

		);
		
		$this->load->view('konsumen/lacak/getCost', $data);
	}


	public function prosesPemesanan(){
       $is_proses = $this->mkp->prosesPemesanan();
       if($is_proses){
           $this->cart->destroy();
           	echo '<script>alert("Sukses Order, Silahkan Verifikasi Pembayaran!");</script>';
           	?>
           	<?php
           redirect('konsumenBeranda','refresh');
       } else {
           $this->session->set_flashdata('error','Failed to processed your order, please try again!');
           redirect('konsumenPesanan/index');
       }
    }


    

}

?>