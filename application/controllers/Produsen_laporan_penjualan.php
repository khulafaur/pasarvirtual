<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produsen_laporan_penjualan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_produsen','m_laporan'));

		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
		$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
		$detailsData    =   $this->session->userdata('logged_in');
		$detailsData['saldo']= $dompet['saldo'];
		$this->session->set_userdata('logged_in', $detailsData);
	}

	public function list_tren_penjualan()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = 'active';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['tracking'] = '';
		$data['verifikasi'] = '';
		
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Laporan Penjualan';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Laporan penjualan';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
		
		if (isset($_POST['cari'])) 
		{
			$month = $this->input->post('bulan');
			$year = $this->input->post('tahun');
			$data['year']=$year;
			if($month){
				$data['month'] = $month;
				$dateObj   = DateTime::createFromFormat('!m',$month);
				$data['date'] = $dateObj->format('F')." ".$year;
				$where="where b.status_transaksi=3 and DATE_FORMAT(b.tanggal,'%m')=".$data['month']." and DATE_FORMAT(b.tanggal,'%Y')=".$year;
			}else{
				$data['date'] = $year; //
				$data['month']="0";
				$where="where b.status_transaksi=3 and DATE_FORMAT(b.tanggal,'%Y')=".$year;
				
			}

		}else{
			$data['month'] = mdate('%m');
			$year = mdate('%Y');
			$data['year']=$year;
			$data['date']=mdate('%M %Y');
			$where="where b.status_transaksi=3 and DATE_FORMAT(b.tanggal,'%m')=".$data['month']." and DATE_FORMAT(b.tanggal,'%Y')=".$year;
			//echo print_r($where); die;
		}

		$select="p.id_produk,p.nama_produk,coalesce(sum(t.qty),0) as qty,k.nama_kategori_khusus, coalesce((t.qty * t.harga_transaksi),0) as total";
		$from="(select * from produk where id_produsen = ".$produsen_id.") p";
		$join="kategori_khusus k";
		$condition = "p.id_kategori_khusus = k.id_kategori_khusus";
		$join1="produk_variasi pv";
		$condition1 = "pv.id_produk = p.id_produk";
		$join2="(select a.id_produk_variasi, a.qty, a.harga_transaksi 
				from detail_transaksi a
	            join transaksi b 
	            on a.id_transaksi=b.id_transaksi "
	            .$where.") t";
		$condition2 = "t.id_produk_variasi = pv.id_produk_variasi";
		$group_by= "p.id_produk";
		$data['get_data']=$this->m_laporan->get_laporan_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$group_by);
			$start    = ($this->session->userdata['logged_in']['created_date']);
			$end      = date("Y-m-d");
			$data['getRangeYear']   = range(gmdate('Y', strtotime($start)), gmdate('Y', strtotime($end)));
			$this->load->view('produsen/laporan/v_laporan_penjualan',$data);
	}

	public function detail_penjualan($id_produk, $month, $year)
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = 'active';
		$data['bagi_hasil'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['kelola_produk'] = '';
		$data['stok']='';
		$data['tracking'] = '';
		$data['verifikasi'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$produk=$this->m_laporan->get_produk($id_produk)->row(0,'array');
		$data['data1'] = 'Penjualan Produk '.$produk["nama_produk"];
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Laporan Penjualan';
		$data['data4'] = 'laporan_penjualan';
		$data['data5'] = 'Detail Penjualan';
		$data['active'] = 'active';

		$data['month']=$month;
		$data['year']=$year;

		$select="d.ktp_distributor,d.nama_distributor,sum(dt.qty) as qty,(dt.qty * dt.harga_transaksi) as pendapatan";
		$from="(select * from produk_variasi where id_produk=".$id_produk.") pv";
		$join="detail_transaksi dt";
		$condition = "pv.id_produk_variasi=dt.id_produk_variasi";
		$join1="distributor d";
		$condition1 = "d.ktp_distributor=dt.id_dist";
		$join2="transaksi t";
		$condition2 = "t.id_transaksi=dt.id_transaksi";
		$group_by= "d.ktp_distributor";

		if($month == 0){
		   $where = "t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$year;
		   $data['date'] = $year;
		}else{ 
		   $dateObj   = DateTime::createFromFormat('!m',$month);
		   $data['date'] = $dateObj->format('F')." ".$year; //
		   $where = "t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%m')=".$data['month']." and DATE_FORMAT(t.tanggal,'%Y')=".$year;
		}
		  

		$data['id_produk']=$id_produk;
		$data['get_data']=$this->m_laporan->get_detail_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where,$group_by);
		
		$this->load->view('produsen/laporan/v_detail_penjualan',$data);
	}

	public function detail_penjualan_distrubutor()
	{
		$get_id_dist=$_POST['id'];
		$get_idproduk=$_POST['id_prdk'];
		$month=$_POST['month'];
		$year = $_POST['year'];

		$select="pv.nama_variasi,sum(dt.qty) as qty,(dt.qty * dt.harga_transaksi) as pendapatan";
		$from="(select * from produk_variasi where id_produk=".$get_idproduk.") pv";
		$join="(select * from detail_transaksi where id_dist=".$get_id_dist.") dt";
		$condition = "pv.id_produk_variasi=dt.id_produk_variasi";
		$join1="transaksi t";
		$condition1 = "t.id_transaksi=dt.id_transaksi";
		$where = "t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%m')=".$month." and DATE_FORMAT(t.tanggal,'%Y')=".$year;
		$group_by= "pv.id_produk_variasi";

		if($month == 0){
		   $where = "t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$year;
		}else{ 
		   $where = "t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%m')=".$month." and DATE_FORMAT(t.tanggal,'%Y')=".$year;
		}

		$data['get_data']=$this->m_laporan->get_penjualan_distributor($select,$from,$join,$condition,$join1,$condition1,$where,$group_by);

		

		$this->load->view('produsen/laporan/v_penjualan_distributor',$data);
	}
}
