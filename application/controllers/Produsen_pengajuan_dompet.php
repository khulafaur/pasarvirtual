<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Produsen_pengajuan_dompet extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_dompet');

		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}

	}

	public function form_pengajuan()
	{
		$data['tanggal']=mdate("%d %M %Y");
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];
		$produsen_id = $this->session->userdata['logged_in']['produsen_id'];
		$data['get_data']=$this->m_dompet->get_data_dompet($produsen_id);

		$this->load->view("produsen/pencairan_dompet/v_form_pengajuan",$data);
	}

	public function insert_pengajuan()
	{
		if($this->input->post("button")){
			$object = array(
				"tgl_penarikan" => mdate('%Y-%m-%d'),
				"ket_penarikan" => $this->input->post("keterangan"),
				"id_dompet" => $this->input->post("id_dompet"),
				"nominal" =>$this->input->post("nominal"),
				"status_penarikan" => "0"
			);
		}
		$this->m_dompet->insert_data_pengajuan($object);

		redirect('Produsen_dashboard');
	}

	public function form_status_pengajuan()
	{
		$id_dompet = $this->session->userdata['logged_in']['id_dompet'];
		$data['get_data']=$this->m_dompet->get_status_pengajuan($id_dompet);
		$this->load->view("produsen/pencairan_dompet/v_form_statuspengajuan",$data);
	}
}
