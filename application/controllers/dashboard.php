<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if (isset($this->session->userdata['logged_in'])) {
                $username = ($this->session->userdata['logged_in']['distributor_id']);
            } else {
				$this->load->view('error_403');
			}
	}

	public function dashboard()
	{
		$data['dashboard'] = 'active';
		$data['explore'] = '';
		$data['monitor'] = '';
		$data['gudang'] = '';
		$data['pesanan'] = '';
		$data['chat']='';
		$data['side']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);
		//echo "<pre>"; print_r($data); die;
		$this->load->view('distributor/dashboard',$data);
	}
}
