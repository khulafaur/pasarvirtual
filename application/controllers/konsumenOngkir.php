<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenOngkir extends CI_Controller {

	public function index()
	{
		$this->load->view('konsumen/index/header');
		$this->load->view('konsumen/ongkir/body');
		$this->load->view('konsumen/index/footer');
	}

}