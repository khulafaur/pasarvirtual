<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produsen_monitoring_distributor extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_produsen','m_monitoring_distributor'));

		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
		$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
		$detailsData    =   $this->session->userdata('logged_in');
		$detailsData['saldo']= $dompet['saldo'];
		$this->session->set_userdata('logged_in', $detailsData);
	}

	public function list_distributor()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = 'active';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['tracking'] = '';
		$data['verifikasi'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Monitoring Distributor';
		$data['date'] = '';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Monitoring Distributor';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select = "pd.id_prod_dist,d.ktp_distributor,d.nama_toko,d.nama_distributor,d.email,d.hp,d.telp";
		$from = "distributor d";
		$join = "produsen_distributor pd";
		$condition = "pd.id_distributor=d.ktp_distributor";
		$join1 = "produsen p";
		$condition1 = "pd.id_produsen=p.ktp_produsen";
		$where="pd.id_produsen=".$produsen_id." and pd.status_approve=1";

		$data["get_data"]=$this->m_monitoring_distributor->get_data_distributor($select,$from,$join,$condition,$join1,$condition1,$where);

		$select="DATE_FORMAT(t.tanggal,'%M %Y') as periode,t.tanggal,d.foto_distributor,d.ktp_distributor,d.nama_distributor,sum(dt.qty) as qty,sum(dt.qty * dt.harga_transaksi) as pendapatan,t.id_transaksi,d.nama_toko,d.alamat_distributor";
		$from="(select * from produk where id_produsen=".$produsen_id.") p";
		$join="produk_variasi pv";
		$condition = "pv.id_produk=p.id_produk";
		$join1="detail_transaksi dt";
		$condition1 = "pv.id_produk_variasi=dt.id_produk_variasi";
		$join2="distributor d";
		$condition2= "d.ktp_distributor=dt.id_dist";
		$join3="transaksi t";
		$condition3 = "t.id_transaksi=dt.id_transaksi";
		$group_by= "dt.id_transaksi";

		$data["get_history_penjualan"]=$this->m_monitoring_distributor->history_penjualan_dist($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$join3,$condition3,$group_by);

		//echo '<pre>'; print_r($data["get_history_penjualan"]); die;
		$this->load->view("produsen/monitoring distributor/v_listing_distributor.php",$data);

	}
	public function detail_distributor()
	{
		$get_id_dist=$_POST['id'];
		$select="*";
		$from="distributor";
		$where="ktp_distributor=".$get_id_dist;

		$data["get_data"]=$this->m_monitoring_distributor->get_data_dist_byid($select,$from,$where);
		$this->load->view('produsen/monitoring distributor/v_detail_distributor',$data);
	}

	public function hapus_distributor()
	{
		$get_id=$_POST['id'];
		$data["get_data"]=$this->m_monitoring_distributor->hapus_distributor_byid($get_id);
	}

	public function detail_transaksi()
	{
		$get_id=$_POST['id_trx'];
		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select = "DATE_FORMAT(t.tanggal,'%d %M %Y') as tanggal,d.foto_distributor,d.nama_distributor,t.id_transaksi,d.nama_toko,d.alamat_distributor,t.id_transaksi,d.email,d.hp";
		$from = "distributor d";
		$join = "detail_transaksi dt";
		$condition="dt.id_dist=d.ktp_distributor";
		$join1 = "transaksi t";
		$condition1="t.id_transaksi=dt.id_transaksi";
		$where = "t.id_transaksi=".$get_id;
		
		$data["get_data_dist"]=$this->m_monitoring_distributor->get_detail_trx_dist($select,$from,$join,$condition,$join1,$condition1,$where);
		$select = "dt.qty,(dt.qty * dt.harga_transaksi) as total,pv.nama_variasi,dt.harga_transaksi,p.nama_produk,t.id_transaksi";
		$from = "(select * from produk where id_produsen = ".$produsen_id.") p";
		$join = "produk_variasi pv";
		$condition="p.id_produk=pv.id_produk";
		$join1 = "detail_transaksi dt";
		$condition1="dt.id_produk_variasi=pv.id_produk_variasi";
		$join2 = "transaksi t";
		$condition2="dt.id_transaksi=t.id_transaksi";
		$where = "dt.id_transaksi=".$get_id;

		$data["get_data_penj"]=$this->m_monitoring_distributor->get_detail_trx_byid($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where);

		$select = "sum(dt.qty) as total_qty,sum(dt.qty * dt.harga_transaksi) as total_harga";
		$from = "(select * from produk where id_produsen = ".$produsen_id.") p";
		$join = "produk_variasi pv";
		$condition="p.id_produk=pv.id_produk";
		$join1 = "detail_transaksi dt";
		$condition1="dt.id_produk_variasi=pv.id_produk_variasi";
		$join2 = "transaksi t";
		$condition2="dt.id_transaksi=t.id_transaksi";
		$where = "dt.id_transaksi=".$get_id;

		$data["total"]=$this->m_monitoring_distributor->get_detail_trx_byid($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where);

		$this->load->view('produsen/monitoring distributor/v_detail_transaksi',$data);
	}
	public function tren_penjualan_dist()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = 'active';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['verifikasi'] = '';
		$data['tracking'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Monitoring Distributor';
		$data['date'] = '';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Monitoring Distributor';
		$data['data4'] = 'listing_distributor';
		$data['data5'] = 'Leaderboard';
		$data['active'] = 'active';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
		$start    = ($this->session->userdata['logged_in']['created_date']);
		$end      = date("Y-m-d");
		$data['getRangeYear']   = range(gmdate('Y', strtotime($start)), gmdate('Y', strtotime($end)));

		if($periode=$this->input->post("cari")){

			$data['bulan'] = $this->input->post("bulan");
			$data['year'] = $this->input->post("tahun");
			$year = $data['year'];

			if ($data['bulan']){
				$where= "where t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$data['year']." and DATE_FORMAT(t.tanggal,'%M')='".$data['bulan']."'" ;
			}else{
				$where= "where t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$data['year'];
			}

			$select="d.ktp_distributor,d.foto_distributor,d.nama_distributor,coalesce(t.qty,0) as qty,coalesce(t.pendapatan,0) as pendapatan,t.tanggal";
			$from="(select d.nama_distributor,pd.id_distributor,d.foto_distributor,d.ktp_distributor
					from produsen_distributor pd
					join distributor d
					on pd.id_distributor=d.ktp_distributor
					where pd.id_produsen=".$produsen_id." and pd.status_approve=1) d";
			$join="(select sum(dt.qty) as qty,sum(dt.qty * dt.harga_transaksi) as pendapatan,t.tanggal,dt.id_dist,t.status_transaksi
					from(select * from produk where id_produsen = ".$produsen_id.") p
					join produk_variasi pv
					on p.id_produk=pv.id_produk
					join detail_transaksi dt
					on pv.id_produk_variasi=dt.id_produk_variasi
					join transaksi t
					on t.id_transaksi=dt.id_transaksi
					".$where."
					group by dt.id_dist) t";
			$condition = "d.ktp_distributor=t.id_dist";
			$data["all"]=$this->m_monitoring_distributor->get_leaderboard($select,$from,$join,$condition);

		}else{
			$year = mdate('%Y');
			$select="d.ktp_distributor,d.foto_distributor,d.nama_distributor,coalesce(t.qty,0) as qty,coalesce(t.pendapatan,0) as pendapatan,t.tanggal";
			$from="(select d.nama_distributor,pd.id_distributor,d.foto_distributor,d.ktp_distributor
					from produsen_distributor pd
					join distributor d
					on pd.id_distributor=d.ktp_distributor
					where pd.id_produsen=".$produsen_id." and pd.status_approve=1) d";
			$join="(select sum(dt.qty) as qty,sum(dt.qty * dt.harga_transaksi) as pendapatan,t.tanggal,dt.id_dist,t.status_transaksi
					from(select * from produk where id_produsen = ".$produsen_id.") p
					join produk_variasi pv
					on p.id_produk=pv.id_produk
					join detail_transaksi dt
					on pv.id_produk_variasi=dt.id_produk_variasi
					join transaksi t
					on t.id_transaksi=dt.id_transaksi 
					where t.status_transaksi=3
					group by dt.id_dist) t";
			$condition = "d.ktp_distributor=t.id_dist";

			$data["all"]=$this->m_monitoring_distributor->get_leaderboard($select,$from,$join,$condition);

		}

		$x=0;
			foreach($data["all"] as $row){
			$x++;
			
			$select="coalesce(t.qty,0) as qty, d.month, coalesce(t.pendapatan,0) as pendapatan";
			$from="(SELECT DATE_FORMAT(day,'%c') as month, day
						from(SELECT '".$year."-01-01' + INTERVAL t.n - 1 DAY as day
							FROM date t
							WHERE t.n <= DATEDIFF(LAST_DAY('".$year."-12-31'), '".$year."-01-01')+1
							group by MONTH(day)) data
					) d";
					
			$join="(select sum(dt.qty) as qty,sum(dt.qty * dt.harga_transaksi) as pendapatan,DATE_FORMAT(t.tanggal,'%c') as tanggal
				 from (select * from produk where id_produsen=".$produsen_id.") p
				 join produk_variasi pv
				 on p.id_produk=pv.id_produk
				 join(select * from detail_transaksi where id_dist=".$row->ktp_distributor.") dt
				 on pv.id_produk_variasi=dt.id_produk_variasi
				 join transaksi t
				 on t.id_transaksi=dt.id_transaksi
				 where t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$year."
				 group by DATE_FORMAT(t.tanggal,'%m')
				 order by t.tanggal) t";			 
			$condition="t.tanggal=d.month";

			$data["a"][$x]=$this->m_monitoring_distributor->get_penjualan_tahunan_dist($select,$from,$join,$condition);
			
			}
		
		$this->load->view("produsen/monitoring distributor/v_leaderboard_period",$data);
	}

	public function kirim_dompet()
	{
		$get_id_dist=$_POST['id'];
		$select="*";
		$from="distributor";
		$where="ktp_distributor=".$get_id_dist;
		$data['saldo'] = ($this->session->userdata['logged_in']['saldo']);

		$data["get_data"]=$this->m_monitoring_distributor->get_data_dist_byid($select,$from,$where);
		$this->load->view('produsen/monitoring distributor/v_form_kirimbonus',$data);
	}
	public function detail_aktivitas($id_distributor)
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = 'active';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['verifikasi'] = '';
		$data['tracking'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Monitoring Distributor';
		$data['date'] = '';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Monitoring Distributor';
		$data['data4'] = 'listing_distributor';
		$data['data5'] = 'Aktivitas penjualan';
		$data['active'] = 'active';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select="DATE_FORMAT(t.tanggal,'%M %Y') as periode,t.tanggal,d.foto_distributor,d.ktp_distributor,d.nama_distributor,sum(dt.qty) as qty,(dt.qty * dt.harga_transaksi) as pendapatan,t.id_transaksi,d.nama_toko,d.alamat_distributor";
		$from="(select * from produk where id_produsen=".$produsen_id.") p";
		$join="produk_variasi pv";
		$condition = "pv.id_produk=p.id_produk";
		$join1="detail_transaksi dt";
		$condition1 = "pv.id_produk_variasi=dt.id_produk_variasi";
		$join2="distributor d";
		$condition2= "d.ktp_distributor=dt.id_dist";
		$join3="transaksi t";
		$condition3 = "t.id_transaksi=dt.id_transaksi";
		$where= "dt.id_dist=".$id_distributor;
		$group_by= "dt.id_transaksi";

		$data["get_history_penjualan"]=$this->m_monitoring_distributor->detail_aktivitas_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$join3,$condition3,$where,$group_by);

		$select="nama_distributor";
		$from="distributor";
		$where="ktp_distributor=".$id_distributor;

		$data["get_data"]=$this->m_monitoring_distributor->get_data_dist_byid($select,$from,$where);

		$this->load->view('produsen/monitoring distributor/v_detail_aktivitas',$data);


	}

	public function kirim_bonus()
	{	
		if($insert=$this->input->post("button")){
			$id_dompet = ($this->session->userdata['logged_in']['id_dompet']);
			$saldo = ($this->session->userdata['logged_in']['saldo']);
			//$_SESSION ['logged_in']['saldo'] = $saldo-$nominal;
			$nominal = $this->input->post("nominal");
			$object = array(
				"saldo" => $saldo-$nominal,
			);
			date_default_timezone_set('Asia/Jakarta');
        	$time = time();
			$object1 = array(
				"tujuan_transfer" => $this->input->post("tujuan"),
				"nominal" => $this->input->post("nominal"),
				"tgl_transfer" => mdate("%Y-%m-%d %H:%i:%s", $time),
				"id_dompet" => $id_dompet,
				"sisa_saldo" => $saldo-$nominal,
				"ket_transfer" => $this->input->post("keterangan")
			);

			$this->m_monitoring_distributor->kirim_bonus($id_dompet,$object,$object1);
			redirect("Produsen_arus_kas");
		}
	}

	public function verifikasi_distributor()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['tracking'] = '';
		$data['verifikasi'] = 'active';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Verifikasi Distributor';
		$data['date'] = '';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Verifikasi Distributor';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select = "pd.id_prod_dist,d.ktp_distributor,d.nama_toko,d.nama_distributor,d.email,d.hp,d.telp";
		$from = "distributor d";
		$join = "produsen_distributor pd";
		$condition = "pd.id_distributor=d.ktp_distributor";
		$join1 = "produsen p";
		$condition1 = "pd.id_produsen=p.ktp_produsen";
		$where="pd.id_produsen=".$produsen_id." and pd.status_approve=0";

		$data["get_data"]=$this->m_monitoring_distributor->get_data_distributor($select,$from,$join,$condition,$join1,$condition1,$where);
		$this->load->view("produsen/monitoring distributor/v_verifikasi_distributor",$data);
	}

	public function approve_distributor()
	{
		$get_id=$_POST['id'];
		$object=array(
				'status_approve'=>"1"
			);
		$data["get_data"]=$this->m_monitoring_distributor->approve_distributor_byid($get_id,$object);
	}
}
