<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registrasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_produsen');
	}

	public function registrasi(){
		if (isset($_POST['submit'])) {

			$data = array(
				'ktp_produsen' => preg_replace("/[^0-9]/", "", $this->input->post('no_ktp')),
				'nama_produsen' => $this->input->post('firstname')." ".$this->input->post('lastname'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'email' => $this->input->post('email'),
				'no_hp' => $this->input->post('number')
			);
			//echo '<pre>'; print_r($data); die;
			$user = $this->input->post('user');
			if ($user == "1") {
				$result = $this->m_produsen->registration_insert($data);

				if ($result == TRUE) {
					$data["regis_success"]="Registration Success";
					$this->load->view('login/login_form',$data);
				} else {
					$data["first_name"]=$this->input->post('firstname');
					$data["last_name"]=$this->input->post('lastname');
					$data["regis_check"]="regis_check";
					$data["user"]=$user;
					$this->load->view('registrasi/invalid_regis',$data);
				}	

			} else {
				echo "test doang";
			}
		} else {
			$data['breadcrumb'] = ["registrasi"];
			$this->load->view('registrasi/registrasi_form', $data);
		}
	}
}
