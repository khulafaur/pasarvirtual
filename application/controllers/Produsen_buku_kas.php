<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produsen_buku_kas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_produsen','m_buku_kas','m_monitoring_distributor'));
		
		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
		$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
		$detailsData    =   $this->session->userdata('logged_in');
		$detailsData['saldo']= $dompet['saldo'];
		$this->session->set_userdata('logged_in', $detailsData);

	}

	public function list_arus_kas()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = 'active';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['stok']='';
		$data['chat'] = '';
		$data['tracking'] = '';
		$data['verifikasi'] = '';

		$data['data1'] = 'Buku kas';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Buku Kas';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';

		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];
		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$data['tabs']="tab-pane fade in active";
		$data['tabs1']="tab-pane fade";
		$data['active_tab']="active";
		$data['active_tab1']="";

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
		$start    = ($this->session->userdata['logged_in']['created_date']);
		$end      = date("Y-m-d");
		$data['getRangeYear']   = range(gmdate('Y', strtotime($start)), gmdate('Y', strtotime($end)));

		$select="a.id_aktivitas_pemasukan,a.tgl_pemasukan,a.tgl_pemasukan,DATE_FORMAT(a.tgl_pemasukan,'%M %Y') as periode,a.nominal,a.sisa_saldo,a.ket_pemasukan,a.id_transaksi";
		$from="dompet d";
		$join="aktivitas_pemasukan a";
		$condition="a.id_dompet=d.id_dompet";
		$where="d.id_pemilik=". $produsen_id;
		$data['get_data_pemasukan']=$this->m_buku_kas->get_data_pemasukan($select,$from,$join,$condition,$where);

		$select="a.id_aktivitas_tarik,a.tgl_penarikan,DATE_FORMAT(a.tgl_penarikan,'%M %Y') as periode,a.nominal,a.sisa_saldo,a.ket_penarikan";
		$from="dompet d";
		$join="aktivitas_penarikan a";
		$condition="a.id_dompet=d.id_dompet";
		$where="d.id_pemilik=". $produsen_id;
		$data['get_data_penarikan']=$this->m_buku_kas->get_data_penarikan($select,$from,$join,$condition,$where);

		$select="a.id_aktivitas_transfer,a.tgl_transfer,DATE_FORMAT(a.tgl_transfer,'%M %Y') as periode,a.nominal,a.sisa_saldo,a.ket_transfer,a.tujuan_transfer";
		$from="dompet d";
		$join="aktivitas_transfer a";
		$condition="a.id_dompet=d.id_dompet";
		$where="d.id_pemilik=". $produsen_id;
		$data['get_data_transfer']=$this->m_buku_kas->get_data_transfer($select,$from,$join,$condition,$where);

		$data["year"] = mdate('%Y');

		if ($this->input->post('cari')){
			$data["year"] = $this->input->post('tahun');
			$data['tabs']="tab-pane fade";
			$data['tabs1']="tab-pane fade in active";
			$data['active_tab']="";
			$data['active_tab1']="active";
		}else{
		    $data["year"] = mdate('%Y');
		}
			

		$select="coalesce(t.nominal,0) as nominal, d.month,d.day";
		$from="(SELECT DATE_FORMAT(day,'%c') as month, day
					from(SELECT '". $data["year"]."-01-01' + INTERVAL t.n - 1 DAY as day
						FROM date t
						WHERE t.n <= DATEDIFF(LAST_DAY('". $data["year"]."-12-31'), '". $data["year"]."-01-01')+1
						group by MONTH(day)) data
				) d";
				
		$join="(select sum(a.nominal) as nominal,DATE_FORMAT(a.tgl_pemasukan,'%c') as tanggal
			 from (select * from dompet where id_pemilik=".$produsen_id." and status_dompet=1) p
			 join aktivitas_pemasukan a
			 on p.id_dompet=a.id_dompet
			 where DATE_FORMAT(a.tgl_pemasukan,'%Y')=".$data["year"]."
			 group by DATE_FORMAT(a.tgl_pemasukan,'%m')
			 order by a.tgl_pemasukan
			 ) t";		 
		$condition="t.tanggal=d.month";

		$data['get_data_masuk']=$this->m_buku_kas->get_data_aktivitas($select,$from,$join,$condition);

		$select="coalesce(t.nominal,0) as nominal, d.month,d.day";
		$from="(SELECT DATE_FORMAT(day,'%c') as month, day
					from(SELECT '". $data["year"]."-01-01' + INTERVAL t.n - 1 DAY as day
						FROM date t
						WHERE t.n <= DATEDIFF(LAST_DAY('". $data["year"]."-12-31'), '". $data["year"]."-01-01')+1
						group by MONTH(day)) data
				) d";
				
		$join="(select sum(a.nominal) as nominal,DATE_FORMAT(a.tgl_penarikan,'%c') as tanggal
			 from (select * from dompet where id_pemilik=".$produsen_id." and status_dompet=1) p
			 join aktivitas_penarikan a
			 on p.id_dompet=a.id_dompet
			 where DATE_FORMAT(a.tgl_penarikan,'%Y')=".$data["year"]."
			 group by DATE_FORMAT(a.tgl_penarikan,'%m')
			 order by a.tgl_penarikan
			 ) t";		 
		$condition="t.tanggal=d.month";

		$data['get_data_tarik']=$this->m_buku_kas->get_data_aktivitas($select,$from,$join,$condition);

		$select="coalesce(t.nominal,0) as nominal, d.month,d.day";
		$from="(SELECT DATE_FORMAT(day,'%c') as month, day
					from(SELECT '". $data["year"]."-01-01' + INTERVAL t.n - 1 DAY as day
						FROM date t
						WHERE t.n <= DATEDIFF(LAST_DAY('". $data["year"]."-12-31'), '". $data["year"]."-01-01')+1
						group by MONTH(day)) data
				) d";
				
		$join="(select sum(a.nominal) as nominal,DATE_FORMAT(a.tgl_transfer,'%c') as tanggal
			 from (select * from dompet where id_pemilik=".$produsen_id." and status_dompet=1) p
			 join aktivitas_transfer a
			 on p.id_dompet=a.id_dompet
			 where DATE_FORMAT(a.tgl_transfer,'%Y')=".$data["year"]."
			 group by DATE_FORMAT(a.tgl_transfer,'%m')
			 order by a.tgl_transfer
			 ) t";		 
		$condition="t.tanggal=d.month";

		$data['get_data_transf']=$this->m_buku_kas->get_data_aktivitas($select,$from,$join,$condition);

		$select="d.month, coalesce(t.keuntungan,0) as pendapatan";
		$from="(SELECT DATE_FORMAT(day,'%c') as month, day
					from(SELECT '".$data["year"]."-01-01' + INTERVAL t.n - 1 DAY as day
						FROM date t
						WHERE t.n <= DATEDIFF(LAST_DAY('".$data["year"]."-12-31'), '".$data["year"]."-01-01')+1
						group by MONTH(day)) data
				) d";
				
		$join="(select DATE_FORMAT(t.tanggal,'%c') as tanggal, sum(dt.qty * ((dt.harga_transaksi*dt.persen_produsen)-dt.harga_modal)) as keuntungan
			 from (select * from produk where id_produsen=".$produsen_id.") p
			 join produk_variasi pv
			 on p.id_produk=pv.id_produk
			 join detail_transaksi dt
			 on pv.id_produk_variasi=dt.id_produk_variasi
			 join transaksi t
			 on t.id_transaksi=dt.id_transaksi
			 where t.status_transaksi=3 and DATE_FORMAT(t.tanggal,'%Y')=".$data["year"]."
			 group by DATE_FORMAT(t.tanggal,'%m')
			 order by t.tanggal) t";			

		$condition="t.tanggal=d.month";

		$data['get_data_keuntungan']=$this->m_buku_kas->get_data_aktivitas($select,$from,$join,$condition);
		//echo '<pre>'; print_r($data['get_data_keuntungan']); die;

		$this->load->view('produsen/buku_kas/v_buku_kas',$data);
	}
	public function detail_pemasukan(){

		$get_id=$_POST['id_trx'];
		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select = "DATE_FORMAT(t.tanggal,'%d %M %Y') as tanggal,d.foto_distributor,d.nama_distributor,t.id_transaksi,d.nama_toko,d.alamat_distributor,t.id_transaksi,d.email,d.hp";
		$from = "distributor d";
		$join = "detail_transaksi dt";
		$condition="dt.id_dist=d.ktp_distributor";
		$join1 = "transaksi t";
		$condition1="t.id_transaksi=dt.id_transaksi";
		$where = "t.id_transaksi=".$get_id;

		$data["get_data_dist"]=$this->m_monitoring_distributor->get_detail_trx_dist($select,$from,$join,$condition,$join1,$condition1,$where);

		$select = "dt.persen_produsen, dt.qty,(dt.qty * (dt.harga_transaksi*dt.persen_produsen)) as total,(dt.qty * ((dt.harga_transaksi*dt.persen_produsen)-dt.harga_modal)) as laba_bersih,pv.nama_variasi,dt.harga_transaksi,p.nama_produk,t.id_transaksi";
		$from = "(select * from produk where id_produsen = ".$produsen_id.") p";
		$join = "produk_variasi pv";
		$condition="p.id_produk=pv.id_produk";
		$join1 = "detail_transaksi dt";
		$condition1="dt.id_produk_variasi=pv.id_produk_variasi";
		$join2 = "transaksi t";
		$condition2="dt.id_transaksi=t.id_transaksi";
		$where = "dt.id_transaksi=".$get_id;

		$data["get_data_penj"]=$this->m_monitoring_distributor->get_detail_trx_byid($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where);

		$select = "sum(dt.qty * ((dt.harga_transaksi*dt.persen_produsen)-dt.harga_modal)) as total_laba,sum(dt.qty) as total_qty,sum(dt.qty * (dt.harga_transaksi*dt.persen_produsen)) as total_harga";
		$from = "(select * from produk where id_produsen = ".$produsen_id.") p";
		$join = "produk_variasi pv";
		$condition="p.id_produk=pv.id_produk";
		$join1 = "detail_transaksi dt";
		$condition1="dt.id_produk_variasi=pv.id_produk_variasi";
		$join2 = "transaksi t";
		$condition2="dt.id_transaksi=t.id_transaksi";
		$where = "dt.id_transaksi=".$get_id;

		$data["total"]=$this->m_monitoring_distributor->get_detail_trx_byid($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where);

		$this->load->view('produsen/buku_kas/v_detail_pemasukan',$data);
	}
	public function detail_transfer()
	{
		$get_id_transfer=$_POST['id_trans'];
		$get_id_dist=$_POST['id_dist'];

		$select="*";
		$from="distributor";
		$where="ktp_distributor=".$get_id_dist;
		$data["get_data_dist"]=$this->m_monitoring_distributor->get_data_dist_byid($select,$from,$where);

		$select="*";
		$from="aktivitas_transfer ";
		$where="id_aktivitas_transfer=". $get_id_transfer;
		$data['get_data_transfer']=$this->m_buku_kas->get_data_transfer_byId($select,$from,$where);

		//echo '<pre>'; print_r($get_id_dist); die;
		$this->load->view('produsen/buku_kas/v_detail_transfer',$data);
	}

	public function detail_penarikan()
	{
		$id_penarikan=$_POST['id_penarikan'];

		$select="*";
		$from="aktivitas_penarikan";
		$where="id_aktivitas_tarik=". $id_penarikan;
		$data['get_data_penarikan']=$this->m_buku_kas->get_data_penarikan_byId($select,$from,$where);

		$this->load->view('produsen/buku_kas/v_detail_penarikan',$data);
	}
}


