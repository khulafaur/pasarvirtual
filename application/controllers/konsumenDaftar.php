<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenDaftar extends CI_Controller{
    public function __construct(){
        parent::__construct();
        
    }public function index(){

		$data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();	
    
        $this->load->view('konsumen/index/header',$data);
        $this->load->view('konsumen/daftar/body');
		$this->load->view('konsumen/index/footer');

    }

	public function verification($key)
	{
	 $this->load->helper('url');
	 $this->load->model('m_konsumenDaftar');
	 $this->m_konsumenDaftar->changeActiveState($key);
	 echo "Selamat kamu telah memverifikasi akun kamu";
	 echo "<br><br><a href='".site_url("login")."'>Kembali ke Menu Login</a>";
	}


    public function prosesDaftar(){
    	
		//passing post data dari view
		$this->load->helper(array('form', 'url'));
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$namaDepan = $this->input->post('namaDepan');
		$namaBelakang = $this->input->post('namaBelakang');
		$nomorHp = $this->input->post('nomorHp');
		$email = $this->input->post('email');
		$alamat = $this->input->post('alamat');
		$kelurahan = $this->input->post('kelurahan');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kodePos = $this->input->post('kodePos');


		//memasukan ke array
		$data = array(
		 'username' => $username,
		 'password' => $password,
		 'namaDepan' => $namaDepan,
		 'namaBelakang' => $namaBelakang,
		 'nomorHp' => $nomorHp,
		 'email' => $email,
		 'alamat' => $alamat,
		 'kelurahan' => $kelurahan,
		 'provinsi' => $provinsi,
		 'kota' => $kota,
		 'kodePos' => $kodePos,
		 'status' => "Tidak Aktif"
		 );
		//tambahkan akun ke database
		$this->load->model('m_konsumenDaftar');
		$id = $this->m_konsumenDaftar->tambahAkun($data);
	  
		//enkripsi id
		$encrypted_email = md5($email);
	
        $this->load->library('email');
        $config = array();
        $config['charset'] = 'utf-8';
        $config['useragent'] = 'Codeigniter';
        $config['protocol']= "smtp";
        $config['mailtype']= "html";
        $config['smtp_host']= "ssl://smtp.gmail.com";
        $config['smtp_port']= "465";
        $config['smtp_timeout']= "400";
        $config['smtp_user']= "admin@trusteddevice.business"; 
        $config['smtp_pass']= "Jancoeg123"; 
        $config['crlf']="\r\n"; 
        $config['newline']="\r\n"; 
        $config['wordwrap'] = TRUE;
    //memanggil library email dan set konfigurasi untuk pengiriman email
    
    $this->email->initialize($config);
    //konfigurasi pengiriman
    $this->email->from($config['smtp_user'],'Pasar Virtual');
    $this->email->to($email);
    $this->email->subject("Verifikasi Akun");
    $this->email->message(
       "terimakasih telah melakuan registrasi, untuk memverifikasi silahkan klik tautan dibawah ini<br><br>".
       site_url("konsumenDaftar/verification/$encrypted_email")
   );
    
    if($this->email->send()){
     echo "Berhasil melakukan registrasi, silahkan cek email kamu";
    }else{
     echo "Berhasil melakukan registrasi, namu gagal mengirim verifikasi email";
    } 
    echo "<br><br><a href='".site_url("login")."'>Kembali ke Menu Login</a>";
}
}

?>  