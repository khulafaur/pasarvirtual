<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor_pesanan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_pesanan');

	}
	function penjualan_distributor(){
		$data['dashboard']="";
	    $data['explore']="";
	    $data['monitor']="";
	    $data['gudang']="";
	    $data['pesanan']="active";
	    $data['side']="";
	    $data['chat']='';
	    $data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
	    $id_distributor= ($this->session->userdata['logged_in']['distributor_id']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);
		$data['rPesanan'] = $this->m_pesanan->pesanan($id_distributor);
		//echo "<pre>"; print_r($data['rPesanan']); die;
		$this->load->view('distributor/pesanan/v_pesanan',$data);
	}

	function detail_penjualan_distributor() {
		$primarykey = $_POST['id_transaksi'];
		$id_distributor= ($this->session->userdata['logged_in']['distributor_id']);
		$data['detail_penjualan_distributor'] = $this->m_pesanan->detail_pesanan($primarykey,$id_distributor);
		$data['total2'] = $this->m_pesanan->total($primarykey,$id_distributor);
		//echo "<pre>"; print_r($data['total']); die;
		$this->load->view('distributor/pesanan/v_detail_pesanan',$data);
	}

}
