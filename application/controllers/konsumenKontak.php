<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenKontak extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}


	public function index(){
		$this->load->view('konsumen/index/header');
		$this->load->view('konsumen/kontak/body');
		$this->load->view('konsumen/index/footer');
	}

}


?>