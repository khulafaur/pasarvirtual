<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produsen_login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_produsen');
	}

	// authentikasi user
	public function login(){
		if (isset($_POST['login'])) {
			$username = $this->input->post('username');
			$result = $this->m_produsen->check_username($username);

		if ($result == 1) {
			$password = $this->input->post('password');
			$hasil = $this->m_produsen->ambil_data_produsen($username)->row(0,'array');
			$dompet = $this->m_produsen->ambil_data_dompet($hasil["ktp_produsen"])->row(0,'array');
			$options = array('cost' => 11);
			if (password_verify($password,$hasil["password"])) {
				if (password_needs_rehash($hash, PASSWORD_DEFAULT, $options)) {
					// If so, create a new hash, and replace the old one
					$newHash = password_hash($password, PASSWORD_DEFAULT, $options);
					$object_hash = array(
						'password' => $newHash
					);
					$this->m_produsen->rehash($object_hash, $username);
				}
				// Add user data in session
				$session_data = array(
			        'produsen_id'  => $hasil["ktp_produsen"],
			        'email'     => $hasil["email"],
			        'created_date'     => $hasil["created_date"],
			        'saldo'     => $dompet["saldo"],
			        'foto_produsen'     => $hasil["foto_produsen"],
			        'logo_toko'     => $hasil["logo_toko"],
			        'id_dompet'     => $dompet["id_dompet"],
				);
				$this->session->set_userdata('logged_in', $session_data);
				$this->session->sess_expiration = '14400';
				redirect('Produsen_dashboard');
			}else{
				$data['inv_password'] = 'Incorrect Password';
				$this->load->view('login/login_form', $data);
			}
		} else {
			$data['inv_username'] = 'Incorrect Password';
			$data['inv_password'] ='Invalid Username';
			$this->load->view('login/login_form', $data);
		}
			

		} else {
			$data['breadcrumb'] = ["login"];
			$this->session->sess_destroy();
			$this->load->view('login/login_form', $data);
		}
	}

	// logout
	public function logout(){
			$this->session->sess_destroy();
			$this->load->view('login/login_form');

	}

	public function error_page()
	{
		$this->load->view("error_403");
	}
}
