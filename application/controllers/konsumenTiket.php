<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenTiket extends CI_Controller {

	public function index(){
		$data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();	
		$this->load->view('konsumen/index/header',$data);
		$this->load->view('konsumen/tiket/index');
	}


}
?>