<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class distributor_registrasi extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_login_distributor');
	}

	public function registrasi(){
		if (isset($_POST['submit'])) {
			$data['side']="";
			$data = array(
				'ktp_distributor' => $this->input->post('ktp_dist'),
				'nama_distributor' => $this->input->post('firstname')." ".$this->input->post('lastname'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'email' => $this->input->post('email'),
				'hp' => $this->input->post('hp')
			);
			//echo '<pre>'; print_r($data); die;
			$user = $this->input->post('user');
			if ($user == "2") {
				$result = $this->m_login_distributor->registration_insert($data);

				if ($result == TRUE) {
					$data['side']="";
					$data["regis_success"]="Registration Success";
					$this->load->view('distributor/login_distributor/login_form',$data);
				} else {
					$data["first_name"]=$this->input->post('firstname');
					$data["last_name"]=$this->input->post('lastname');
					$data["regis_check"]="regis_check";
					$data["user"]=$user;
					$data['side']="";
					$this->load->view('distributor/registrasi_distributor/invalid_regis',$data);
				}	

			} else {
				echo "test doang";
			}
		} else {
			$data['side']="";
			$data['breadcrumb'] = ["registrasi_distributor"];
			$this->load->view('distributor/registrasi_distributor/registrasi_form', $data);
		}
	}
}
