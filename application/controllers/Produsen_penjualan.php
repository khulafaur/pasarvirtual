<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Produsen_penjualan extends CI_Controller {
		public function __construct() {
			parent::__construct();
			
			$this->load->model(array('m_produsen','m_penjualan'));
			
			if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}

			$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
			$detailsData    =   $this->session->userdata('logged_in');
			$detailsData['saldo']= $dompet['saldo'];
			$this->session->set_userdata('logged_in', $detailsData);
		}
		
		function tampil_semua_penjualan() {
			$data['dashboard'] = '';
			$data['produk'] = '';
			$data['distributor'] = '';
			$data['aruskas'] = '';
			$data['penjualan'] = '';
			$data['bagi_hasil'] = '';
			$data['kelola_produk'] = '';
			$data['pesanan'] = 'active';
			$data['chat'] = '';
			$data['verifikasi'] = '';
			$data['stok'] = '';
			$data['tracking'] = '';

			$data['data1'] = 'Pemesanan Produk';
			$data['data2'] = 'dashboard';
			$data['data3'] = 'Pemesanan';
			$data['data4'] = 'penjualan/tampil_semua_penjualan';
			$data['data5'] = 'Daftar Pemesanan Produk';
			$data['active'] = 'active';
			$data['active1'] = '';
			$data['date'] = '';

		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

			///semua penjualan
			$data['hasil'] = $this->m_penjualan->tampil_penjualan();

			///belum bayar penjualan
			$data['hasil_belum_bayar'] = $this->m_penjualan->tampil_belumbayar_penjualan();

			//perlu dikirim
			$data['hasil_perlu_dikirim'] = $this->m_penjualan->tampil_perludikirim_penjualan();

			//sudah dikirim
			$data['hasil_sudah_dikirim'] = $this->m_penjualan->tampil_sudahdikirim_penjualan();

			//sudah selesai/sampai
			$data['hasil_selesai'] = $this->m_penjualan->tampil_sudahsampai_penjualan();

			$this->load->view('produsen/penjualan_produsen/view_penjualan_produsen', $data);
		}

		function detail_penjualan() {
			$primarykey=$_POST['id_transaksi'];
			$data['data_detail_penjualan'] = $this->m_penjualan->get_detail_penjualan($primarykey);
			$data['data_konsumen_penjual'] = $this->m_penjualan->data_konsumen_penjual($primarykey);
			$data['total'] = $this->m_penjualan->total_penjualan($primarykey);
			$this->load->view('produsen/penjualan_produsen/view_detail_penjualan_produsen', $data);
		}

		function sudah_kirim($id_transaksi) {
			$id_transaksi=$this->uri->segment(3);
			$data=array(
					'status_transaksi'=>'2',
				);
			$this->m_penjualan->update_status($id_transaksi,$data);
			redirect ('Produsen_penjualan/tampil_semua_penjualan');
		}

		function form_upload_noresi() {
			$data['id_trx']=$_POST['id_transaksi'];
			$this->load->view('produsen/penjualan_produsen/upload_no_resi', $data);
		}

		function upload_no_resi() {
			$data['dashboard'] = '';
			$data['produk'] = '';
			$data['distributor'] = '';
			$data['aruskas'] = '';
			$data['penjualan'] = '';
			$data['bagi_hasil'] = '';
			$data['kelola_produk'] = '';
			$data['pesanan'] = 'active';
			$data['chat'] = '';
			$data['stok'] = '';
			$data['verifikasi'] = '';
			$data['tracking'] = '';

			$data['data1'] = 'Buku kas';
			$data['data2'] = 'dashboard';
			$data['data3'] = 'Buku Kas';
			$data['data4'] = '';
			$data['active'] = 'active';
			$data['active1'] = '';
			$data['date'] = '';

		    $data['saldo'] = $this->session->userdata['logged_in']['saldo'];

			if($this->input->post('submit')){
			$no_resi=array(
				"no_resi" =>$this->input->post('no_resi'),
			);
			$primarykey = $this->input->post('id_trx');
			$this->m_penjualan->simpan_no_resi($no_resi,$primarykey);
			redirect('Produsen_penjualan/tampil_semua_penjualan');
			}  
		}
	}
?>