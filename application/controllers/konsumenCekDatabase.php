<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenCekDatabase extends CI_Controller {

function cekDatabase()
    {
        $this->load->library('session');
        //validasi kedua dengan cara mengecek database
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        //query ke database dan memanggil model m_login
        $this->load->model('m_konsumen','mm');
        $result = $this->mm->prosesLogin($username,$password);

        //jika hasilnya ada pada maka masukan ke season field nama dan username dengan nama season : login
        if($result)
        {
            foreach($result as $row)
            {
                $sess_array = array(
                    'namaBelakang'=> $row->namaBelakang,
                    'username' => $row->username,
                    'active' => $row->active,
                    'email' => $row->email
                );

                $this->session->set_userdata('login', $sess_array);
            }
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

?>