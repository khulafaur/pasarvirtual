<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenBeranda extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('m_produk','mp');
        $this->load->model('m_distributor','md');
    }

	public function index($offset=null)
	{
        $inputan = 1;

        $validasi = $this->md->get_one($inputan)->row(0,'array');

        if($validasi != NULL){
            $object = array(
                    'ktp_dist'=>$validasi['ktp_distributor']
                );

            $this->session->set_userdata('tampilanDistributor',$object);

             $data['produk'] = $this->mp->get_all_produk($validasi['ktp_distributor']);
             $data['kategori'] =$this->m_kategori->getAllKategori();
            // $data['kategoriKhusus'] = $this->mp->getAllKategoriKhusus();
             $data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();

            $this->load->view('konsumen/index/header',$data);
            $this->load->view('konsumen/index/body',$data);
            $this->load->view('konsumen/index/footer');
        }else{
            echo "id salah";
        }

		
		
	}

	public function add_to_cart($id){
         if($this->session->userdata('sedangLogin') == FALSE ){
         	echo '<script>alert("Silahkan Login Terlebih Dahulu!");</script>';
            redirect('konsumenMasuk','refresh');
         }
        $valproduk = $this->mp->find($id);
        $data = array(
            'id'      => $valproduk -> id_produk_variasi,
            'qty'     => 1,
            'price'   => $valproduk -> harga,
            'name'    => $valproduk -> nama_variasi,
            'foto'    => $valproduk -> gambar_produk
        );
        $this->cart->insert($data);
        redirect('konsumenBeranda');
    }


}
