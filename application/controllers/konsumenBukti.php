<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenBukti extends CI_Controller {

	  public function __construct(){
	    parent::__construct();
	    
	    $this->load->model('m_buktiBayar','mb');
	  }


	  
	 	public function index()
		{
			$data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();
			$this->load->view('konsumen/index/header',$data);
			$this->load->view('konsumen/buktibayar/index');
		}



	  public function tambah(){
		$data = array();
		
		if($this->input->post('submit')){ // Jika user menekan tombol Submit (Simpan) pada form
			$config['upload_path']          = './uploads/buktibayar';
            $config['allowed_types']        = 'gif|jpg|png';
            // $config['encrypt_name'] = TRUE;
            $new_name = "bukti_".time()	;
			$config['file_name'] = $new_name;


            $this->upload->initialize($config);
			if (! $this->upload->do_upload('input_gambar')){
				echo '<script>alert("Gagal, harap periksa kembali file anda!");</script>';
				redirect('KonsumenBukti','refresh');
			}else{
				$gambar = $this->upload->data('file_name');
				$ukuran = $this->upload->data('file_size');	
				$tipe = $this->upload->data('file_type');
			}




			$data = array(
				'tgl_upload' 		=> date("Y-m-d H:i:s"),
				'nama_file' 	=> $gambar,
				'besar_file' 				=> $ukuran,
				'tipe_file'			=> $tipe,
			);

			$this->db->insert('buktibayar', $data);
			echo '<script>alert("Sukses menambahkan bukti bayar!");</script>';
			redirect('konsumenBeranda','refresh');
 

		}
		

	}
																		
	

}

?>