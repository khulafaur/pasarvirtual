<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produsen_dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_produsen');
		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
		$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
		$detailsData    =   $this->session->userdata('logged_in');
		$detailsData['saldo']= $dompet['saldo'];
		$this->session->set_userdata('logged_in', $detailsData);
	}

	public function dashboard()
	{
		$data['dashboard'] = 'active';
	 	$data['produk'] = '';
	 	$data['distributor'] = '';
	 	$data['aruskas'] = '';
	 	$data['penjualan'] = '';
	 	$data['bagi_hasil'] = '';
	 	$data['kelola_produk'] = '';
	 	$data['pesanan'] = '';
	 	$data['chat'] = '';
	 	$data['stok'] = '';
	 	$data['tracking'] = '';
	 	$data['verifikasi'] = '';

		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$datestring = "%M %Y";
		$data['date'] = "Dashboard";
		$data['data1'] = 'Home';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Dashboard';
		$data['data4'] = 'produsen/dashboard';
		$data['active'] = 'active';
		$data['active1'] = '';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$data['distributor2'] = $this->m_produsen->jml_dist($produsen_id);
		$data['produk2'] = $this->m_produsen->jml_prod($produsen_id);
		$data['pendapatan2'] = $this->m_produsen->jml_pendapatan($produsen_id);
		$this->load->view('produsen/dashboard',$data);
	}
}
