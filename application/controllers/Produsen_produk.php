<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Produsen_produk extends CI_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model(array('m_produsen','m_produk'));

			if (isset($this->session->userdata['logged_in']['produsen_id'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
			
			$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
			$detailsData    =   $this->session->userdata('logged_in');
			$detailsData['saldo']= $dompet['saldo'];
			$this->session->set_userdata('logged_in', $detailsData);
		}

	public function index_kelola() {
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = 'active';
		$data['pesanan'] = '';
		$data['stok']='';
		$data['chat'] = '';
		$data['verifikasi'] = '';
		$data['tracking'] = '';

		$data['data1'] = 'kelola produk';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'kelola produk';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
		
		$data['hasil'] = $this->m_produk->tampil_data_produk_variasi($produsen_id);
		$data['hasil1'] = $this->m_produk->tampil_data_produk($produsen_id);
		$data['hasil2'] = $this->m_produk->tampil_data_produk_trash($produsen_id);
		$this->load->view('produsen/kelola_produk/view_kelola_produk',$data);
	}
	
	public function tambah_data_produk() {
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['kelola_produk'] = 'active';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['verifikasi'] = '';
		$data['stok']='';
		$data['tracking'] = '';

		$data['data1'] = 'Tambah Produk';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Kelola Produk';
		$data['data4'] = 'produk/index_kelola';
		$data['data5'] = 'Tambah Data Produk';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		if (isset($_POST['submit'])) {
			$nama_produk = $this->input->post('nama_produk');
			$id_kategori_khusus = $this->input->post('id_kategori_khusus');
			$persen_produsen = $this->input->post('persen_produsen');
			$persen_distributor = $this->input->post('persen_distributor');
			$deskripsi_produk = $this->input->post('deskripsi_produk');
			$nama_variasi = $this->input->post('nama_variasi');
			$harga = $this->input->post('harga');
			$harga_jual = $this->input->post('harga_jual');
			$stok = $this->input->post('stok');

			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';

			date_default_timezone_set('Asia/Jakarta');
			$time=time();

			$this->upload->initialize($config);
			$this->upload->do_upload('gambar_produk');

			$produk = array(
				'nama_produk'=>$nama_produk,
				'id_kategori_khusus'=>$id_kategori_khusus,
				'deskripsi_produk'=>$deskripsi_produk,
				'gambar'=>str_replace(' ', '_', $_FILES['gambar_produk']['name']),
				'persen_produsen'=>$persen_produsen/100,
				'persen_distributor'=>$persen_distributor/100,
				'created_date'=>mdate("%Y-%m-%d %H:%i:%s", $time),
				'id_produsen'=>$this->session->userdata['logged_in']['produsen_id'],
				'status_produk'=>"1"
				);
			
				$id_produk = $this->m_produk->tambah_data_produk($produk);

				$aktivitas = array(
				'id_aktivitas'=>$id_produk,
				'status_aktivitas'=>"1",
				'waktu_aktivitas'=>mdate("%Y-%m-%d %H:%i:%s", $time),
				'id_produsen'=>$this->session->userdata['logged_in']['produsen_id'],
				);

				$this->m_produk->tambah_data_aktivitas($aktivitas);

				//echo "<pre>"; print_r($produk); die;

			$variasi_gambar =count($_FILES['gambar_variasi']['name']);
			$files = $_FILES;
			//echo "<pre>"; print_r($files); die;
			for ($i = 0; $i < $variasi_gambar; $i++) :

				    $_FILES['userfile']['name']= $files['gambar_variasi']['name'][$i];
			        $_FILES['userfile']['type']= $files['gambar_variasi']['type'][$i];
			        $_FILES['userfile']['tmp_name']= $files['gambar_variasi']['tmp_name'][$i];
			        $_FILES['userfile']['error']= $files['gambar_variasi']['error'][$i];
			        $_FILES['userfile']['size']= $files['gambar_variasi']['size'][$i];    

			        $config = array();
				    $config['upload_path'] = './uploads/';
				    $config['allowed_types'] = 'gif|jpg|png|jpeg';

			        $this->upload->initialize($config);
			        $this->upload->do_upload();

			  $stok_produk = array(
			  	 'nama_variasi' => $nama_variasi[$i],
			  	 'gambar_produk' => str_replace(' ', '_', $_FILES['userfile']['name']),
				 'stok' => $stok[$i],
				 'harga' => $harga[$i],
				 'harga_jual' => $harga_jual[$i],
				 'id_produk'     => $id_produk,
				 'created_date'=>mdate("%Y-%m-%d %H:%i:%s", $time),
				 'status_produk_variasi'=>"1"
				 ); 

			  $proses = $this->m_produk->tambah_stok($stok_produk); 
			 endfor; 
			redirect('Produsen_produk/index_kelola');
		} else {
			$data['kategori_umum'] = $this->m_produk->getUmum();
			$this->load->view('produsen/kelola_produk/view_tambah_data_produk', $data);
		}
	}

	function edit_data_produk(){
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = '';
		$data['verifikasi'] = '';
		$data['kelola_produk'] = 'active';
		$data['pesanan'] = '';
		$data['stok']='';
		$data['chat'] = '';
		$data['tracking'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];

		$data['data1'] = 'Tambah Produk';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Kelola Produk';
		$data['data4'] = 'produk/index_kelola';
		$data['data5'] = 'Edit Data Produk';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';

       if (isset($_POST['submit'])) {

       	    $nama_produk = $this->input->post('nama_produk');
			$id_kategori_umum = $this->input->post('id_kategori_umum');
			$id_kategori_khusus = $this->input->post('id_kategori_khusus');
			$deskripsi_produk = $this->input->post('deskripsi_produk');
			if($_FILES['gambar_produk']['name']){
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';

				$this->upload->initialize($config);
				$this->upload->do_upload('gambar_produk');

				$gambar_produk = $_FILES['gambar_produk']['name'];
			}else{
				$gambar_produk = $this->input->post('value_gambar');
			}

			$data = array (
				'nama_produk'=>$nama_produk,
				'gambar'=>$gambar_produk,
				'id_kategori_khusus'=>$id_kategori_khusus,
				'deskripsi_produk'=>$deskripsi_produk);
		  //echo "<pre>"; print_r($data); die;
		  $id_produk=$this->input->post('id_produk');
       	  $this->m_produk->update_onetable($id_produk,'id_produk','produk',$data);

       	   date_default_timezone_set('Asia/Jakarta');
		   $time=time();

       	  $aktivitas = array(
			'id_aktivitas'=>$id_produk,
			'status_aktivitas'=>"2",
			'waktu_aktivitas'=>mdate("%Y-%m-%d %H:%i:%s", $time),
			'id_produsen'=>$this->session->userdata['logged_in']['produsen_id'],
		  );

			$this->m_produk->tambah_data_aktivitas($aktivitas);

       	    $nama_variasi = $this->input->post('nama_variasi');
			$harga = $this->input->post('harga');
			$harga_jual = $this->input->post('harga_jual');
			$stok = $this->input->post('stok');

       	  if($_FILES['gambar_variasi']){
       	  	  $variasi_gambar =count($_FILES['gambar_variasi']['name']);
			  $files = $_FILES;
				//echo "<pre>"; print_r($files); die;

				for ($i = 0; $i < $variasi_gambar; $i++) :

				    $_FILES['userfile']['name']= $files['gambar_variasi']['name'][$i];
			        $_FILES['userfile']['type']= $files['gambar_variasi']['type'][$i];
			        $_FILES['userfile']['tmp_name']= $files['gambar_variasi']['tmp_name'][$i];
			        $_FILES['userfile']['error']= $files['gambar_variasi']['error'][$i];
			        $_FILES['userfile']['size']= $files['gambar_variasi']['size'][$i];    

			        $config = array();
				    $config['upload_path'] = './uploads/';
				    $config['allowed_types'] = 'gif|jpg|png|jpeg';

			        $this->upload->initialize($config);
			        $this->upload->do_upload();

				  $stok_produk = array(
				  	 'nama_variasi' => $nama_variasi[$i],
				  	 'gambar_produk' => str_replace(' ', '_', $_FILES['userfile']['name']),
					 'stok' => $stok[$i],
					 'harga' => $harga[$i],
					 'harga_jual' => $harga_jual[$i],
					 'id_produk'     => $id_produk,
					 'created_date'=>mdate("%Y-%m-%d %H:%i:%s", $time),
					 'status_produk_variasi'=>"1"
					 ); 

				  $proses = $this->m_produk->tambah_stok($stok_produk); 
				endfor;
       	  }
       	  redirect('Produsen_produk/index_kelola');
       }else{

	   $data['kategori_umum'] = $this->m_produk->getUmum();
       $id_produk = $this->uri->segment(3);
       $data['produk'] = $this->m_produk->get_one1($id_produk);
       
       $this->load->view('produsen/kelola_produk/form_edit_produk',$data);
       }
    }

    function hapus_data_produk(){
		$get_id=$_POST['id'];
		$object=array(
			"status_produk"=>"0",
			);
		$object1=array(
			"status_produk_variasi"=>"0",
			);
		$this->m_produk->update_status_byid($get_id,$object,$object1);
	}

	function hapus_data_variasi(){
		$get_id=$_POST['id'];
		$object=array(
			"status_produk_variasi"=>"0",
			);
		$this->m_produk->update_statusvariasi_byid($get_id,$object);
	}

	function active_data_produk(){
		$get_id=$_POST['id'];
		$object=array(
			"status_produk"=>"1",
			);
		$object1=array(
			"status_produk_variasi"=>"1",
			);
		$data["get_data"]=$this->m_produk->update_status_byid($get_id,$object,$object1);
	}
	
	function view_detail_produk() {
		$primarykey=$_POST['id'];
		$data['detail_produk'] = $this->m_produk->detail_produk($primarykey);
		$data['detail_variasi'] = $this->m_produk->detail_variasi($primarykey);
		$this->load->view('produsen/kelola_produk/view_detail_produk',$data);
	}

	function view_edit_variasi() {
		if($this->input->post("submit")){
			$primarykey=$this->input->post("id_variasi");
			if($_FILES['gambar_produk']['name']){
				$config['upload_path']          = './uploads/';
				$config['allowed_types']        = 'gif|jpg|png|jpeg';

				$this->upload->initialize($config);
				$this->upload->do_upload('gambar_produk');

				$gambar_produk = $_FILES['gambar_produk']['name'];
			}else{
				$gambar_produk = $this->input->post('value_gambar');
			}

			$object = array(
				"nama_variasi"=>$this->input->post("nama_variasi"),
				"harga"=>$this->input->post("harga"),
				"gambar_produk"=>str_replace(' ', '_', $gambar_produk),
				"harga_jual"=>$this->input->post("harga_jual"),
				);
			$this->m_produk->update_variasi_produk($primarykey,$object);
			redirect('Produsen_produk/index_kelola');
		}else{
			$primarykey=$_POST['id'];
			$data['get_data'] = $this->m_produk->detail_variasi_produk($primarykey);
			$this->load->view('produsen/kelola_produk/view_edit_variasi',$data);
		}
	}

	function getKategori(){
		$postData = $_POST["umum"];
		$this->m_produk->getKatUmum($postData);
		$data =$this->m_produk->getKatUmum($postData);
		echo json_encode($data);
	}

	function getKatKhusus(){
		$postData = $_POST["kat"];
		$this->m_produk->getKatKatKhusus($postData);
		$data =$this->m_produk->getKatKatKhusus($postData);
		echo json_encode($data);
	}
}
?>