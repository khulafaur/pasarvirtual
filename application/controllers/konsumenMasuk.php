	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KonsumenMasuk extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('m_konsumen','mk');
	}


	public function index(){
		 // $data['produk'] = $this->mp->get_all_produk();
		 // $data['kategori'] =$this->mk->getAllKategori();
		// $data['kategoriKhusus'] = $this->mp->getAllKategoriKhusus();
		$data['kategoriUmum'] = $this->m_kategori->getAllKategoriUmum();

		
		$this->load->view('konsumen/index/header',$data);
		$this->load->view('konsumen/login/body');
		$this->load->view('konsumen/index/footer');
	}

	public function prosesMasuk(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

        $user = $this->input->post('username',true);
        $pass = $this->input->post('password',true);
        $cek = $this->mk->cekAkun($user,$pass)->row(0,'array');
        $hasil = count($cek);
 
        if ($this->form_validation->run() == FALSE){
        	 $this->index();
        }else{
        
        	if($this->mk->cekAkun($username,$password)){


        		$object= array(
        					'konsumen_id'=>$cek['id_konsumen'],
        					'username'=>$cek['username']
        				);

        		$this->session->set_userdata('sedangLogin',$object);
        		 // echo print_r($_SESSION);	die;				

			   redirect('konsumenBeranda');
        	}else{
        		$this->session->set_flashdata('msg', 'Username atau password salah');
					redirect('konsumenMasuk');
        	}
        }
    }

		


	public function keluar(){
		$this->session->sess_destroy();
		redirect('konsumenBeranda');
	}


}


?>