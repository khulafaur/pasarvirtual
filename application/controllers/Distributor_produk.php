 <?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Distributor_produk extends CI_Controller {
    public function __construct() {
      parent::__construct();
      $this->load->model('m_myproduk');
      
    }

  public function index_kelola() {
    $data['dashboard']="";
    $data['explore']="";
    $data['monitor']="";
    $data['gudang']="active";
    $data['pesanan']="";
    $data['side']="";
    $data['chat']='';
    $data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
    $data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);

    $this->load->database();
    $data['hasil'] = $this->m_myproduk->tampil_data_produk();
    $this->load->view('produsen/kelola_produk/view_kelola_produk',$data);
  }
  
  public function tambah_data_produk() {
    if (isset($_POST['submit'])) {
      $nama_produk = $this->input->post('nama_produk');
      $gambar_produk = $this->input->post('gambar_produk');
      $presentase_untung = $this->input->post('presentase_untung');
      $deskripsi_produk = $this->input->post('deskripsi_produk');

      $data = array ('nama_produk'=>$nama_produk,
        'gambar'=>$gambar_produk,
        'presentase_untung'=>$presentase_untung,
        'deskripsi_produk'=>$deskripsi_produk);

      $this->m_myproduk->tambah_data_produk($data);
      redirect('produk/index_kelola');
    }
    $this->load->view('produsen/kelola_produk/view_tambah_data_produk');
  }

  function edit_data_produk(){
       if (isset($_POST['submit'])) {
          $id_produk = $this->input->post('id_produk');
          $nama_produk = $this->input->post('nama_produk');
          $harga = $this->input->post('harga');
          $stok = $this->input->post('stok');
          $kategori = $this->input->post('kategori');
          $gambar = $this->input->post('gambar');
          $deskripsi_produk = $this->input->post('deskripsi_produk');

      $data = array ('nama_produk'=>$nama_produk,
        'harga'=>$harga,
        'stok'=>$stok,
        'kategori'=>$kategori,
        'gambar'=>$gambar,
        'deskripsi_produk'=>$deskripsi_produk);
      //echo "<pre>"; print_r($data); die;
          $this->m_myproduk->update_onetable($id_produk,'id_produk','produk',$data);
          redirect('produk/index_kelola');
       }else{
       $id_produk = $this->uri->segment(3);
       $data['produk'] = $this->m_myproduk->get_one1($id_produk)->row_array();
       $this->load->view('produsen/kelola_produk/form_edit_produk',$data);
       }
    }

    function hapus_data_produk(){
    $id_produk = $this->uri->segment(3);
    $this->m_myproduk->hapus_produk($id_produk);
    redirect('produk/index_kelola');
  }

  function gudang(){
    $data['dashboard']="";
    $data['explore']="";
    $data['monitor']="";
    $data['gudang']="active";
    $data['pesanan']="";
    $data['side']="";
    $data['chat']='';
    $data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
    $data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);

    $id_distributor=$this->session->userdata['logged_in']['distributor_id'];
    $data['rProdukProdusen'] = $this->m_myproduk->gudang_distributor($id_distributor);
    $this->load->view('distributor/gudang/v_gudang',$data);
  }

   function detail_gudang() {
    $primarykey = $_POST['id_produk'];
    $data['detail_produk'] = $this->m_myproduk->detail_produk($primarykey);
    $data['detail_variasi'] = $this->m_myproduk->detail_variasi($primarykey);
    $this->load->view('distributor/gudang/v_detail_gudang',$data);
  }
  

  function insert_barangku(){
    if ($this->input->post("insert")){
    $id_produsen=$this->input->post("id_produsen");
    $id_distributor=$this->session->userdata['logged_in']['distributor_id'];
    // $id_produk = $this->uri->segment(3);
    // $id_produk_variasi = $this->uri->segment(3);
      //$this->session->userdata['logged_in']['ktp_dist'];
      if($id_produsen){
        $object1 = array(
            "ktp_produsen"=>$id_produsen,
            "ktp_distributor"=>$id_distributor,
            // "id_produk_variasi"=>$id_produk_variasi,
            // "id_produk"=>$id_produk,
            "status_approve"=>"0"
          );
        $this->m_myproduk->insert_produsenku($object1);
      }
      
      $produk=$_POST["chek"];
      
      foreach ($produk as $key => $value) {
        $object = array (
          "id_produk"=>$value,
          "ktp_dist"=>$id_distributor,
          "status_publish"=>"0",
          );
          $this->m_myproduk->insert_barangku($object);  
     
      }
      redirect('produk/gudang');
    }
      
  }

  function update_barang_cms(){
    if ($this->input->post("update")){
      $id_produsen=$this->input->post("id_produsen");
      $id_distributor=$this->session->userdata['logged_in']['distributor_id'];
      // $this->session->userdata['logged_in']['distributor_id'];
      
      $produk=$_POST["chek"];
      $object = array (
      "status_publish"=>"0",
      );  
      $this->m_myproduk->update_to_zero($object);

      foreach ($produk as $key => $value) {
        $object1 = array (
          "status_publish"=>"1",
        );
        
        $id_produk = $value;
          $this->m_myproduk->update_barang_cms($object1,$id_produk,$id_distributor);  
      }
      
      redirect('produk/gudang');
    } 
 }

   

}