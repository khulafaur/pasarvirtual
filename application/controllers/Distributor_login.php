<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor_login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_login_distributor');
	}

	public function login(){
		$data['side'] ='';
		if (isset($_POST['login'])) {
			$username = $this->input->post('username');
			$result = $this->m_login_distributor->check_username($username);


		if ($result == 1) {
			$password = $this->input->post('password');
			$hasil = $this->m_login_distributor->ambil_data_distributor($username)->row(0,'array');
			//echo "<pre>"; print_r($hasil); die;
			// $dompet = $this->m_login_distributor->ambil_data_dompet($hasil["ktp_dist"])->row(0,'array');
			$options = array('cost' => 11);
			if (password_verify($password,$hasil["password"])) {
				if (password_needs_rehash($hash, PASSWORD_DEFAULT, $options)) {
					// If so, create a new hash, and replace the old one
					$newHash = password_hash($password, PASSWORD_DEFAULT, $options);
					$object_hash = array(
						'password' => $newHash	
					);
					$this->m_login_distributor->rehash($object_hash, $username);
				}
				// Add user data in session
				$session_data = array(
			        'distributor_id'  => $hasil["ktp_distributor"],
			        'email'     => $hasil["email"],
			        'create_date' => $hasil["create_date"],
			        'foto_distributor' => $hasil["foto_distributor"],
			        'nama_distributor' => $hasil["nama_distributor"],
				);
				//echo "<pre>"; print_r($session_data); die;
				$this->session->set_userdata('logged_in', $session_data);
				$this->session->sess_expiration = '14400';
				redirect('dashboard/dashboard');
				//$this->load->view('distributor/dashboard');
			}else{
				$data['inv_password'] = 'Incorrect Password';
				$this->load->view('distributor/login_distributor/login_form', $data);
			}
		} else {
			$data['inv_username'] = 'Incorrect Password';
			$data['inv_password'] ='Invalid Username';
			$this->load->view('distributor/login_distributor/login_form', $data);
		}
			

		} else {
			$data['breadcrumb'] = ["login_distributor"];
			$this->session->sess_destroy();
			$this->load->view('distributor/login_distributor/login_form', $data);
		}
	}

	// logout
	public function logout(){
			$data['side'] ='';
			$this->session->sess_destroy();
			$this->load->view('distributor/login_distributor/login_form',$data);

	}
}
