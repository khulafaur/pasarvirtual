<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class produsen extends CI_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model(array('m_myprodusen','m_myproduk'));
		}
		
	function dashboard() {
		$this->load->view('produsen/dashboard');
	}
		
	function lihat_profil_produsen() {
		$data['monitor']="active";
		$profil = $this->uri->segment(3);
		$data['detail'] = $this->m_myprodusen->profil_produsen($profil);
		$this->load->view('produsen/profil_produsen/view_profil',$data);
	}
	
	function edit_profil_produsen(){
		if (isset($_POST['submit'])) {
			$no_ktp = $this->input->post('no_ktp');
       	  $nama_lengkap = $this->input->post('nama_lengkap');
		  $email_produsen = $this->input->post('email_produsen');
       	  $foto_produsen = $this->input->post('foto_produsen');
       	  $username = $this->input->post('username');
		  $status = $this->input->post('status');
		  $alamat_rumah = $this->input->post('alamat_rumah');
		  $tgl_lahir = $this->input->post('tgl_lahir');
		  $jenis_kelamin = $this->input->post('jenis_kelamin');
		  $hp = $this->input->post('hp');
		  $no_rekening = $this->input->post('no_rekening');
		  $nama_rekening = $this->input->post('nama_rekening');
		  $nama_bank = $this->input->post('nama_bank');
		  $nama_toko = $this->input->post('nama_toko');
		  $logo_toko = $this->input->post('logo_toko');
		  $alamat_toko = $this->input->post('alamat_toko');
		  $no_siup = $this->input->post('no_siup');
		  $siup = $this->input->post('siup');
		  $no_npwp = $this->input->post('no_npwp');
		  $npwp = $this->input->post('npwp');
		  $tanda_daftar_perusahaan = $this->input->post('tanda_daftar_perusahaan');
		  $desc_toko = $this->input->post('desc_toko');
 
		  $data = array ('no_ktp'=>$no_ktp,
				'nama_lengkap'=>$nama_lengkap,
				'email_produsen'=>$email_produsen,
				'foto_produsen'=>$foto_produsen,
				'username'=>$username,
				'status'=>$status,
				'alamat_rumah'=>$alamat_rumah,
				'tgl_lahir'=>$tgl_lahir,
				'jenis_kelamin'=>$jenis_kelamin,
				'hp'=>$hp,
				'no_rekening'=>$no_rekening,
				'nama_rekening'=>$nama_rekening,
				'nama_bank'=>$nama_bank,
				'nama_toko'=>$nama_toko,
				'logo_toko'=>$logo_toko,
				'alamat_toko'=>$alamat_toko,
				'no_siup'=>$no_siup,
				'siup'=>$siup,
				'no_npwp'=>$no_npwp,
				'npwp'=>$npwp,
				'tanda_daftar_perusahaan'=>$tanda_daftar_perusahaan,
				'desc_toko'=>$desc_toko);
       	  $this->m_myprodusen->update_profil_produsen($data,$no_ktp);
       	  redirect('produsen/lihat_profil_produsen');
       }else{
       $no_ktp = $this->uri->segment(3);
       $data['profil'] = $this->m_myprodusen->get_one($no_ktp)->row_array();
       $this->load->view('produsen/profil_produsen/view_edit_profil_produsen',$data);
       }
    }

    function tampilan_produsen($no_ktp){
    	$data['dashboard']="";
		$data['explore']="active";
		$data['monitor']="";
		$data['gudang']="";
		$data['pesanan']="";
		$data['side']="";
		$data['chat']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);

    	$ktp_dist=$this->session->userdata['logged_in']['distributor_id'];
    	$check= $this->m_myprodusen->checkProdusen($no_ktp,$ktp_dist);
	    	if($check == true){
	    		$data['id_produsen'] = "";
	    	}else{
	    		$data['id_produsen'] = $no_ktp;
	    	}
	    $data['rSatuProdusen'] = $this->m_myprodusen->satuProdusen($no_ktp);
    	$data['rProdukProdusen'] = $this->m_myprodusen->produkProdusen($no_ktp);

    	$data['produsen_activity'] = $this->m_myprodusen->get_activity_produsen($no_ktp);
		$x=0;
		foreach ($data['produsen_activity'] as $key) {
			$sec=$key->time;
			$date1 = new DateTime("@0");
			$date2 = new DateTime("@$sec");
			$interval =  date_diff($date1, $date2);

			if($sec < 60){
				$data['time'][$x]=$interval->format('%s seconds');
			}elseif (($sec >= 60) && ($sec < 3600)) {
				$data['time'][$x]=$interval->format('%i minutes');
			}elseif (($sec >= 3600) && ($sec < 86400)) {
				$data['time'][$x]=$interval->format('%h hours, %i minutes');
			}elseif (($sec >= 86400) && ($sec < 2678400)) {
				$data['time'][$x]=$interval->format('%d days');
			}elseif (($sec >= 2678400) && ($sec < 31536000)) {
				$data['time'][$x]=$interval->format('%m months, %d days');
			}else{
				$data['time'][$x]=$interval->format('%y Years, %m months');
			}
 
			if($key->status_aktivitas == 1){
				$data['activity'][$x] = $this->m_myprodusen->get_activity_inputbrg($key->id);
			}elseif ($key->status_aktivitas == 2) {
				$data['activity'][$x] = $this->m_myprodusen->get_activity_variasi_baru($key->id);
			}else{
				$data['activity'][$x] = $this->m_myprodusen->get_activity_updatebrg($key->id);
			}
			//echo '<pre>'; print_r($data['activity'][$x]); 
			$x=$x+1;
		} //die;
    	$this->load->view('distributor/produsen/v_tampilan_produsen',$data);
    }
	

    function insert_produsenku(){
    	if ($this->input->post("insert")){

    		$id_produsen=$this->input->post("id_produsen");
    		$produk=$_POST["chek"];    		
    		$object = array(
    			"ktp_dist"=>$id_distributor,
				"no_ktp"=>$id_produsen,
				"status"=>"0"
    		);
    		$this->m_myprodusen->insert_produsenku('produsen_distributor',$object);

    		foreach ($id_produsen as $key => $value) {
    			$object = array (
    				"ktp_dist"=>$id_distributor,
    				"no_ktp"=>$id_produsen,
    				"status"=>"0"
       			);
       			$this->m_myprodusen->insert_produku('produk_dsitributor',$object);
       			
    		}
    		redirect('distributor/lihat_produsenku');
    	}
    	
    }

    function lihat_produsen(){
    	$data['dashboard']="";
		$data['explore']="active";
		$data['monitor']="";
		$data['gudang']="";
		$data['pesanan']="";
		$data['side']="sidebar-xs";
		$data['chat']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);

    	$data['rKategori_umum'] = $this->m_myprodusen->kategori_umum();
		$data['rKategori'] = $this->m_myprodusen->kategori();
		$data['rKategori_khusus'] = $this->m_myprodusen->kategori_khusus();
    	$id_kategori = $this->uri->segment(3);
    	$data['rLihatProduk'] = $this->m_myprodusen->produsenAll($id_kategori);
    	$data['rLihatProdusen'] = $this->m_myprodusen->produsenAll2();
    	//echo "<pre>"; print_r($id_kategori); die;
		$this->load->view('distributor/produsen/v_produsen',$data);
	}

	function detail_produk() {
	    $primarykey = $_POST['id_produk'];
	    $data['detail_produk'] = $this->m_myproduk->detail_produk($primarykey);
	    $data['detail_variasi'] = $this->m_myproduk->detail_variasi($primarykey);
	    $this->load->view('distributor/gudang/v_detail_gudang',$data);
  }

	function lihat_produsenku(){
		$data['dashboard']="";
		$data['explore']="";
		$data['monitor']="active";
		$data['gudang']="";
		$data['pesanan']="";
		$data['side']="";
		$data['chat']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);

		$ktp_dist=$this->session->userdata['logged_in']['distributor_id'];
		$data['rLihatProdusen'] = $this->m_myprodusen->myprodusen();
		$data['produsenku'] = $this->m_myprodusen->get_activity($ktp_dist);

		$x=0;
		foreach ($data['produsenku'] as $key) {
			$sec=$key->time;
			$date1 = new DateTime("@0");
			$date2 = new DateTime("@$sec");
			$interval =  date_diff($date1, $date2);

			if($sec < 60){
				$data['time'][$x]=$interval->format('%s seconds');
			}elseif (($sec >= 60) && ($sec < 3600)) {
				$data['time'][$x]=$interval->format('%i minutes');
			}elseif (($sec >= 3600) && ($sec < 86400)) {
				$data['time'][$x]=$interval->format('%h hours, %i minutes');
			}elseif (($sec >= 86400) && ($sec < 2678400)) {
				$data['time'][$x]=$interval->format('%d days');
			}elseif (($sec >= 2678400) && ($sec < 31536000)) {
				$data['time'][$x]=$interval->format('%m months, %d days');
			}else{
				$data['time'][$x]=$interval->format('%y Years, %m months');
			}
 
			if($key->status_aktivitas == 1){
				$data['activity'][$x] = $this->m_myprodusen->get_activity_inputbrg($key->id);
			}elseif ($key->status_aktivitas == 2) {
				$data['activity'][$x] = $this->m_myprodusen->get_activity_variasi_baru($key->id);
			}else{
				$data['activity'][$x] = $this->m_myprodusen->get_activity_updatebrg($key->id);
			}
			//echo '<pre>'; print_r($data['activity'][$x]); 
			$x=$x+1;
		} //die;
		$this->load->view('distributor/produsenku/v_produsenku',$data);
	}

	 function detail_produsen(){
 		
 		$id_produsen=$_POST['id_produsen'];
    	$data['get_data'] = $this->m_myprodusen->satuProdusen($id_produsen);
    	//echo "<pre>"; print_r($data['get_data']); die;
		$this->load->view('distributor/produsenku/v_detail_produsen',$data);
	}

	public function hapus_produsen()
	{
		$get_id=$_POST['id'];
		$data["get_data"]=$this->m_myprodusen->hapus_produsen_byid($get_id);
	}


}
?>