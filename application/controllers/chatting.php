<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class chatting extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('m_chat');
        if (isset($this->session->userdata['logged_in'])) {
                $username = ($this->session->userdata['logged_in']['distributor_id']);
            } else {
				$this->load->view('error_403');
			}
    }

    public function chat()
    {
        $data['dashboard'] = '';
		$data['explore'] = '';
		$data['monitor'] = '';
		$data['gudang'] = '';
		$data['pesanan'] = '';
		$data['side']='';
		$data['chat']='active';
        
         $id_dist = $this->session->userdata['logged_in']['distributor_id'];
         $data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
         $data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);
         $data['get_data']=$this->m_chat->my_produsen($id_dist);
         //echo "<pre>"; print_r($data['get_data']); die;
        $this->load->view('distributor/chat/v_chat_produsen', $data);
    }

    public function getPanelChat()
    {
         $id_produsen=$_POST['id_prdsn'];
         $data["id_produsen"]=$id_produsen;

         $id_dist = $this->session->userdata['logged_in']['distributor_id'];
         $data['get_data']=$this->m_chat->form_chat($id_dist,$id_produsen);
         $data['get_chat_data']=$this->m_chat->get_chat_data($id_dist,$id_produsen);
 
         $this->load->view('distributor/chat/chat_dashboard', $data);
    }
     public function submitChat()
    {
        $id_produsen = $_POST['id_tuju'];
        $chattext =$_POST['chattext'];
        $id_dist=$this->session->userdata['logged_in']['distributor_id'];
        date_default_timezone_set('Asia/Jakarta');
        $time = time();
        $object = array(
                'id_produsen'=>$id_produsen,
                'id_distributor'=>$id_dist,
                'chattext'=>$chattext,
                'status_chat'=>"2",
                'chattime'=>mdate("%Y-%m-%d %H:%i:%s", $time),
                );
        $this->m_chat->insert_chat($object);
    }

    public function refresh()
    {
        $id =intval($_POST['lastTimeID']);
        $data=$this->m_chat->get_chat($id);
        foreach ($data as $row) {
            $id = $row->id;
            $object = array(
                    'delivered_stats'=> 1,
                );
            $this->m_chat->delivered($object,$id);
        }
        
        echo json_encode($data);

    }
}