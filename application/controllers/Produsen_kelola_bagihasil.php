<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Produsen_kelola_bagihasil extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model(array('m_produsen','m_bagi_hasil'));

		if (isset($this->session->userdata['logged_in'])) {
                $produsen_id = ($this->session->userdata['logged_in']['produsen_id']);
            } else {
				redirect('login/error_page');
			}
		$dompet = $this->m_produsen->ambil_data_dompet($produsen_id)->row(0,'array');
		$detailsData    =   $this->session->userdata('logged_in');
		$detailsData['saldo']= $dompet['saldo'];
		$this->session->set_userdata('logged_in', $detailsData);

	}

	public function list_kelola_bagihasil()
	{
		$data['dashboard'] = '';
		$data['produk'] = '';
		$data['distributor'] = '';
		$data['aruskas'] = '';
		$data['penjualan'] = '';
		$data['bagi_hasil'] = 'active';
		$data['kelola_produk'] = '';
		$data['pesanan'] = '';
		$data['chat'] = '';
		$data['stok']='';
		$data['tracking'] = '';
		$data['saldo'] = $this->session->userdata['logged_in']['saldo'];
		$data['verifikasi'] = '';

		$data["date"] = "";
		$datestring = "%M %Y";
		$data['data1'] = 'Kelola Bagi Hasil';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Kelola Bagi Hasil';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';

		$produsen_id = ($this->session->userdata['logged_in']['produsen_id']);

		$select="p.id_produk,p.nama_produk,p.gambar,p.deskripsi_produk,p.persen_produsen,p.persen_distributor,k.nama_kategori_khusus";
		$from="produk p";
		$join="kategori_khusus k";
		$condition="p.id_kategori_khusus=k.id_kategori_khusus";
		$where="p.id_produsen=".$produsen_id;

		$data["get_data"]=$this->m_bagi_hasil->get_data_produk($select,$from,$join,$condition,$where);
		$this->load->view('produsen/bagi hasil/v_listing_bagihasil',$data);
	}

	public function edit_kelola_bagihasil()
	{
		$get_id_produk=$_POST['id'];
		$select="p.id_produk,p.nama_produk,p.persen_produsen,p.persen_distributor";
		$from="produk p";
		$where="id_produk=".$get_id_produk;

		$data["get_data"]=$this->m_bagi_hasil->get_data_produk_byid($select,$from,$where);
		$this->load->view('produsen/bagi hasil/v_edit_bagihasil',$data);
	}

	public function update_bagihasil(){
		$p_produsen = $this->input->post("persen_prdsen");
		$p_distributor =preg_replace('/[^A-Za-z-0-9_~`\/@!$.^#&*\\()+-=]/','',$this->input->post("persen_dist"));
		$id_produk =$this->input->post("id_produk");
		$object = array(
				"persen_produsen" =>$p_produsen/100 ,
				"persen_distributor" =>$p_distributor/100
			);

		$this->m_bagi_hasil->update_produk($object,$id_produk);
		redirect('Produsen_kelola_bagihasil/list_kelola_bagihasil');
	}
}
