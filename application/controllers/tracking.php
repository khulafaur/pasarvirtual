<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class tracking extends CI_Controller {
		public function __construct() {
			parent::__construct();
			$this->load->model('m_tracking');
		}

	public function index_tracking() {
		$data['data1'] = 'Tracking Pengiriman Produk';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Tracking Pengiriman Produk';
		$data['data4'] = '';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['track'] = $this->m_tracking->tampil_tracking();
		$this->load->view('produsen/tracking_produk/view_tracking_produk',$data);
	}
	
	}
?>