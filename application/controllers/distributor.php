<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class distributor extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_distributor');
		$this->load->model('m_myprodusen');
		$this->load->model('m_myproduk');
		$this->load->model('m_pesanan');
		//$this->load->helper(array('form', 'url'));
	}

	function lihat_profil(){
		$data['dashboard']="";
		$data['explore']="";
		$data['monitor']="";
		$data['gudang']="";
		$data['pesanan']="";
		$data['side']="";
		$data['chat']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);
		
		$data['data1'] = 'Profil distributor';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Profil distributor';
		$data['data4'] = 'distributor/lihat_profil';
		$data['data5'] = 'Profil distributor';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';

		$ktp_dist = $this->session->userdata['logged_in']['distributor_id'];
		$data['detail'] = $this->m_distributor->profil_distributor($ktp_dist);
		$this->load->view('distributor/profil_distributor/view_profil', $data);
	}

	function edit_profil($ktp_dist){
		$data['dashboard']="";
		$data['explore']="";
		$data['monitor']="";
		$data['gudang']="";
		$data['pesanan']="";
		$data['side']="";
		$data['chat']='';
		$data['foto_distributor']= ($this->session->userdata['logged_in']['foto_distributor']);
		$data['nama_distributor']= ($this->session->userdata['logged_in']['nama_distributor']);
		
		$data['data1'] = 'Profil distributor';
		$data['data2'] = 'dashboard';
		$data['data3'] = 'Profil distributor';
		$data['data4'] = 'distributor/lihat_profil';
		$data['data5'] = 'Profil distributor';
		$data['active'] = 'active';
		$data['active1'] = '';
		$data['date'] = '';

		if (isset($_POST['submit'])) {
			$ktp_dist = $this->input->post('ktp_distributor');
			$nama_lengkap = $this->input->post('nama_distributor');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$tgl_lahir = $this->input->post('tgl_lahir');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$hp = $this->input->post('hp');
			$telp = $this->input->post('telp');
			$alamat = $this->input->post('alamat_distributor');
			$nama_toko = $this->input->post('nama_toko');
			$no_rekening = $this->input->post('no_rekening');
			$nama_bank = $this->input->post('nama_bank');
			$nama_rekening = $this->input->post('nama_rekening');

			$config['upload_path']          = './assets/images/fotoprodusen';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';

			$this->upload->initialize($config);
			$this->upload->do_upload('foto_produsen');
			$this->upload->do_upload('logo_toko');

			$foto_produsen = $this->input->post('foto_produsen_val');
		  	$logo_toko = $this->input->post('logo_toko_val');

			if($_FILES['foto_produsen']['name']){
			  	$foto_produsen = $_FILES['foto_produsen']['name'];
			}

			if($_FILES['logo_toko']['name']){
		  	$logo_toko = $_FILES['logo_toko']['name'];
		  	}

		  	$data = array (
		  		'ktp_produsen'=>$no_ktp,
				'nama_produsen'=>$nama_lengkap,
				'email'=>$email,
				'foto_produsen'=>$foto_produsen,
				'username'=>$username,
				'alamat'=>$alamat,
				'tgl_lahir'=>$tgl_lahir,
				'jenis_kelamin'=>$jenis_kelamin,
				'no_hp'=>$no_hp,
				'telp'=>$telp,
				'no_rekening'=>$no_rekening,
				'nama_rekening'=>$nama_rekening,
				'nama_bank'=>$nama_bank,
				'nama_toko'=>$nama_toko,
				'logo_toko'=>str_replace(' ', '_',$logo_toko),
				'foto_produsen' =>str_replace(' ', '_',$foto_produsen),
				'alamat_toko'=>$alamat_toko,
				// 'no_siup'=>$no_siup,
				// 'siup'=>$siup,
				// 'no_npwp'=>$no_npwp,
				// 'npwp'=>$npwp,
				// 'tanda_daftar_perusahaan'=>$tanda_daftar_perusahaan,
				'deskripsi_toko'=>$deskripsi_toko);
       	  	$this->m_produsen->update_profil_produsen($data,$no_ktp);
       	  	redirect('distributor/lihat_profil');
       	else{
      	$no_ktp = $this->session->userdata['logged_in']['produsen_id'];;
       	$data['profil'] = $this->m_produsen->get_one($no_ktp)->row_array();
       	$this->load->view('distributor/profil_distributor/view_edit_profil_distributor',$data);
    }

	function update_profil_distributor(){
		$ktp_dist = $this->input->post('ktp_dist');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$hp = $this->input->post('hp');
		$telp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
		$nama_toko = $this->input->post('nama_toko');
		$logo_toko = $this->input->post('logo_toko');
		$no_rekening = $this->input->post('no_rekening');
		$nama_bank = $this->input->post('nama_bank');
		$nama_rekening = $this->input->post('nama_rekening');
		$foto_dist = $this->input->post('foto_dist');

		// $config['upload_path'] = './uploadfile/';
		// $config['allowed_type'] = 'gif|jpg|png|jpeg';
		// $config['max_size'] = '1000';
		// $config['max_width'] = '10240';
		// $config['max_height'] = '7680';

		// $this->load->library('upload', '$config');
		// if (! $this->upload->do_upload()) {
		// 	redirect('distributor/lihat_profil');
		// }else{
			$dokumen = $this->upload->data(); 
			$data = array (
				'nama_lengkap'=>$nama_lengkap,
				'username'=>$username,
				'email'=>$email,
				'tgl_lahir'=>$tgl_lahir,
				'jenis_kelamin'=>$jenis_kelamin,
				'hp'=>$hp,
				'telp'=>$telp,
				'alamat'=>$alamat,
				'nama_toko'=>$nama_toko,
				'logo_toko'=>$logo_toko,
				'no_rekening'=>$no_rekening,
				'nama_bank'=>$nama_bank,
				'nama_rekening'=>$nama_rekening,
				'foto_dist'=>$dokumen['file_name']
			);
			$where = array(
				'ktp_dist' => $ktp_dist
			);
			$this->m_distributor->update_data_profil($where, $data, 'distributor');
			redirect('distributor/lihat_profil');
		
    }

}
?>