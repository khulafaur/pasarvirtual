<!DOCTYPE html>
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if !IE]><!--><html><!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Pasar Virtual Indonesia</title>
        <meta name="description" content="Premium eCommerce Template">

        <!--[if IE]> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicon -->
        <link rel="icon" type="image/png" href="assets/images/favicons/favicon.png">
        <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicons/faviconx57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicons/faviconx72.png">

        <link href='http://fonts.googleapis.com/css?family=Hind:400,700,600,500,300%7CFira+Sans:400,700italic,500italic,400italic,300italic,700,500,300' rel='stylesheet' type='text/css'>

        <!-- Fira Sans Fix - Google Fira Sans sometimes fails to load -->
        <link href='http://code.cdn.mozilla.net/fonts/fira.css' rel='stylesheet'>

        <link rel="stylesheet" href="<?= base_url(); ?>/assets/konsumen/css/plugins.min.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/konsumen/css/settings.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/konsumen/css/navigation.css">
        <link rel="stylesheet" href="<?= base_url(); ?>/assets/konsumen/css/style.css">

  </head>

<body>
        <div id="wrapper">
            <header class="header sticky-header">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="<?= base_url();?>konsumenBeranda" title="Pasar Virtual Indonesia">
                            <img src="<?= base_url(); ?>/assets/konsumen/images/logo.png" alt="Sconto">
                        </a>
                    </div><!-- End .logo -->

                    <nav class="menu-container">
                        <ul class="menu">
                            <li>
                                <a href="<?= base_url(); ?>konsumenBeranda">Home</a>                               
                            </li>


                             <?php $i=3; foreach ($kategoriUmum as $ku): ?>
                            <li class="mmenu-container"><a href="#"><?php echo $ku->nama_kategori_umum;?></a>
                            <?php endforeach; ?>        

                                <div class="megamenu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="mmenu-title">Pakaian</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Atasan</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Aksesoris</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Jam Tangan<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Ikat Pinggang</a></li>
                                                    <li><a href="#"><span>Kaos Kaki<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Topi<span class="tip">New</span></span></a></li>
                                                    <li><a href="#">Gelang</a></li>
                                                    <li><a href="#">Kaca Mata</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Tas</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#"><span>Tas Kerja<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Tas Ransel</a></li>
                                                    <li><a href="#"><span>Tote Bag<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#"><span>Tas Selempang<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Dompet & Aksesoris</a></li>
                                                    
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Alas Kaki</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Sandal</a></li>
                                                    <li><a href="#">Sepatu</a></li>
                                                                                                    </ul>
                                            </div><!-- End .col-5 -->
                                            
                                        </div><!-- End .row -->
                                    </div><!-- End .container -->
                                </div><!-- End .megamenu -->
                            </li>






							

                        <ul class="mobile-menu">
                            <ul class="menu">
                            <li>
                                <a href="#">Home</a>                               
                            </li>

                            <?php $i=3; foreach ($kategoriUmum as $ku): ?>
                            <li class="mmenu-container"><a href="#"><?php echo $ku->nama_kategori_umum;?></a>
                            <?php endforeach; ?>        

                                <div class="megamenu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="mmenu-title">Pakaian</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Atasan</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Aksesoris</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Jam Tangan<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Ikat Pinggang</a></li>
                                                    <li><a href="#"><span>Kaos Kaki<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Topi<span class="tip">New</span></span></a></li>
                                                    <li><a href="#">Gelang</a></li>
                                                    <li><a href="#">Kaca Mata</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Tas</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#"><span>Tas Kerja<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Tas Ransel</a></li>
                                                    <li><a href="#"><span>Tote Bag<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#"><span>Tas Selempang<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Dompet & Aksesoris</a></li>
                                                    
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Alas Kaki</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Sandal</a></li>
                                                    <li><a href="#">Sepatu</a></li>
                                                                                                    </ul>
                                            </div><!-- End .col-5 -->
                                            
                                        </div><!-- End .row -->
                                    </div><!-- End .container -->
                                </div><!-- End .megamenu -->
                            </li>






							<li class="mmenu-container"><a href="#">Wanita</a>
                                <div class="megamenu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="mmenu-title">Pakaian</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Atasan</a></li>
													<li><a href="#">Bawahan</a></li>
													<li><a href="#">Dress</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
											<div class="col-5">
                                                <div class="mmenu-title">Perhiasan	</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Kalung<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Gelang</a></li>
                                                    <li><a href="#"><span>Anting<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Cincin<span class="tip">New</span></span></a></li>
                                                    <li><a href="#">Hiasan Kepala</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Aksesoris</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Syal & Hijab<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Jam Tangan</a></li>
                                                    <li><a href="#"><span>Kaca Mata<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Topi<span class="tip">New</span></span></a></li>
                                                    <li><a href="#">Gelang</a></li>
                                                    <li><a href="#">Kaca Mata</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Tas</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#"><span>Tas Kerja<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Tas Ransel</a></li>
                                                    <li><a href="#"><span>Tote Bag<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#"><span>Tas Selempang<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Dompet & Aksesoris</a></li>
                                                    
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Alas Kaki</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Sandal</a></li>
                                                    <li><a href="#">Sepatu</a></li>
                                                                                                    </ul>
                                            </div><!-- End .col-5 -->
                                            
                                        </div><!-- End .row -->
                                    </div><!-- End .container -->
                                </div><!-- End .megamenu -->
                            </li>




							<li class="mmenu-container"><a href="#">Rumah & Dekorasi</a>
                                <div class="megamenu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="mmenu-title">Alat Makan & Dapur</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Piring & Mangkok</a></li>
													<li><a href="#">Sendok, Garpu, & Sumpit</a></li>
													<li><a href="#">Peralatan Makan</a></li>
													<li><a href="#">Peralatan Minum</a></li>
													<li><a href="#">Taplak Meja</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
											<div class="col-5">
                                                <div class="mmenu-title">Penyimpanan	</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Kotak Tisu<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Keranjang</a></li>
                                                    <li><a href="#"><span>Tempat Makeup<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Rak Buku<span class="tip">New</span></span></a></li>
                                                                                                    </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Hiasan Ruang</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li>
                                                        <a href="#"><span>Lampu Hias<span class="tip">New</span></span></a>
                                                    </li>
                                                    <li><a href="#">Bantal</a></li>
                                                    <li><a href="#"><span>Pot dan Vas<span class="tip">New</span></span></a></li>
                                                    <li><a href="#"><span>Ornamen<span class="tip">New</span></span></a></li>
                                                    <li><a href="#">Tirai</a></li>
                                                    <li><a href="#">Miniatur dan Mainan</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Hiasan Dinding</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#"><span>Poster dan Lukisan<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Bingkai</a></li>
                                                    <li><a href="#"><span>Papan Nama<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#"><span>Jam Dinding<span class="tip hot">Hot</span></span></a></li>
                                                    <li><a href="#">Hiasan Gantung</a></li>
                                                    
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            <div class="col-5">
                                                <div class="mmenu-title">Perabotan</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Lemari</a></li>
                                                    <li><a href="#">Sofa</a></li>
													<li><a href="#">Kursi</a></li>	
													<li><a href="#">Meja</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            
                                        </div><!-- End .row -->
                                    </div><!-- End .container -->
                                </div><!-- End .megamenu -->
                            </li>


						
							<li class="mmenu-container"><a href="#">Perayaan dan Hadiah</a>
                                <div class="megamenu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="mmenu-title">Pesta dan Perayaan</div><!-- End .mmenu-title -->
                                                <ul>
                                                    <li><a href="#">Bingkisan</a></li>
													<li><a href="#">Kartu Ucapan</a></li>
													<li><a href="#">Lilin Kue</a></li>
													<li><a href="#">Hiasan Kue</a></li>
													<li><a href="#">Dekorasi Pesta</a></li>
                                                </ul>
                                            </div><!-- End .col-5 -->
                                            
                                        </div><!-- End .row -->
                                    </div><!-- End .container -->
                                </div><!-- End .megamenu -->
                            </li>
                        </ul>
                    </nav>




                    <div class="header-search-container search-dropdown-fix">
                        <a href="#" class="search-dropdown-btn" title="Search Form">
                            <i class="icon icon-header-search"></i>
                        </a>

                        <form action="#" class="header-search-form">
                            <div class="dropdown search-dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Semua Kategori
                                    <i class="icon icon-search-arrow"></i>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="#">Pria</a></li>
                                    <li><a href="#">Wanita</a></li>
                                    <li><a href="#">Rumah dan Dekorasi</a></li>
                                    <li><a href="#">Perayaan dan Hadiah</a></li>
                                </ul><!-- End .dropdown -->
                            </div><!-- End .dropdown -->
                            <input type="search" class="form-control" placeholder="Cari" required>
                            <button type="submit" class="btn">
                                <i class="icon icon-header-search"></i>
                            </button>
                        </form>
                    </div><!-- End .header-search-container -->

                    <div class="dropdowns-container">
                        <button type="button" class="navbar-toggle">
                            <span class="toggle-wrapper">
                                <span class="toggle-text">Menu</span>
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                        </button>
                        <div class="dropdowns-wrapper">
                            <ul class="header-dropdown account-dropdown">
                                <li>
                                    <a href="#" title="Currency/Language">
                                        <i class="icon icon-header icon-user"></i>
                                        <span>My Account</span>
                                    </a>

                                    <ul>
                                        <li>
                                                <?php if ($this->session->userdata('username')) { ?>
                                            <a href="#">
                                                <i class="icon icon-dropdown icon-dropdown-user"></i>

                                                    <span>  Halo: <?= $this->session->userdata('username')?></span>
                                            </a>
                                        </li>                                        
                                        <li>
                                            <a href="#">
                                                <i class="icon icon-dropdown icon-dropdown-heart"></i>
                                                <span>Favorit</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon icon<?= base_url(); ?>konsumenPesanan-dropdown icon-dropdown-check"></i>
                                                <span>Pesan</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?= base_url();?>konsumenMasuk/keluar">
                                                <i class="icon icon-dropdown icon-dropdown-lock"></i>
                                                <span>Keluar</span>


                                                <?php } else { ?>
                                                <li>
                                            <a href="<?= base_url();?>konsumenMasuk">
                                                <i class="icon icon-dropdown icon-dropdown-user"></i>
                                                <span>Masuk atau Daftar</span>
                                            </a>
                                        </li>
                                                <?php } ?>  
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>



                            <div class="dropdown header-dropdown cart-dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon icon-header icon-cart"></i>
                                    <span class="dcart-total-count"><?php echo $this->cart->total_items() ?></span>
                                </a>

                                <div class="dropdown-menu">
                                    

                                    <p>Total: <b><?php echo $this->cart->total_items() ?> Barang </b> Dalam Keranjang Anda</span></p>
                                    <!-- <p>2 item(s) in your cart - <span>$665.00</span></p> -->
                                    <div class="dcart-products">
                                        <?php 
                                            $i=0;
                                            foreach ($this->cart->contents() as $items) : 
                                            $i++;
                                        ?>
                                        <div class="product product-sm">
                                            <figure>
                                                <a href="#">
                                                    <img style="width: 180px; height: 180px "  src="<?= base_url(); ?>uploads/<?php echo $items['foto'] ?>" alt="Product">
                                                </a>
                                            </figure>
                                            <div class="product-meta">
                                                <h5 class="product-title">
                                                    <a href="#"><?= $items['name']?></a><br>
                                                    <a href="#"><?= $items['qty']?> buah</a>
                                                </h5>
                                                <div class="product-price-container">
                                                    <span class="product-price"><?= number_format($items['price'],0,',','.')?></span>
                                                </div><!-- End .product-price-container -->
                                            </div><!-- End .product-meta -->
                                            <a href="#" class="icon delete-btn lighter" title="Delete Product"><span class="sr-only">Delete product</span></a>
                                        </div>
                                        <?php endforeach; ?>
                                        <!-- End .product -->


                                    
                                    </div><!-- End .dcart-products -->

                                    <div class="dcart-action-container">
                                        <div class="dcart-action-info">
                                            <p>Belanjaan: <span><?=  number_format($this->cart->total(),0,',','.');?></span></p>
                                            <p>Pajak: <span>Gratis</span></p>
                                            <p>Total: <span><?=  number_format($this->cart->total(),0,',','.');?></span></p>
                                        </div><!-- End .dcart-action-info -->

                                        <div class="dcart-action">
                                            <a href="#" class="btn btn-gray3 btn-sm">Lihat Keranjang</a>
                                            <a href="<?= base_url(); ?>konsumenPesanan" class="btn btn-custom btn-sm">Pesan Sekarang</a>
                                        </div><!-- End .dcart-action -->
                                    </div><!-- End .dcart-action-container -->
                                </div><!-- End .dropdown-menu -->
                            </div><!-- End .header-dropdown -->
                            <ul class="header-dropdown">
                                <li>
                                    <a href="#" title="Currency/Language">
                                        <i class="icon icon-header icon-globe"></i>
                                        <span>Currency/Language</span>
                                    </a>

                                    <ul>
                                        <li>
                                            <a href="<?= base_url(); ?>konsumenLacak">
                                                <i class="icon icon-dropdown icon-language"></i>
                                                <span>Lacak Kiriman</span>
                                            </a>

                                         </li>
                                        <li>
                                            <a href="#">
                                                <i class="icon icon-dropdown icon-currency"></i>
                                                <span>Hubungi</span>
                                            </a>                                         
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- End .dropdowns-wrapper -->
                    </div><!-- End .dropdowns-container -->

                </div><!-- End .container-fluid -->
            </header><!-- End .header -->