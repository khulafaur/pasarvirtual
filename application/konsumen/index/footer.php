
          <div> <br></div>

                <div class="footer-bottom">
                    <div class="container-fluid">
                        <div class="footer-left">
                            <div class="social-icons">
                                <a href="#" class="social-icon icon facebook" title="Facebook"><span class="sr-only">Facebook</span></a>
                                <a href="#" class="social-icon icon twitter" title="Twitter"><span class="sr-only">Twitter</span></a>
                                <a href="#" class="social-icon icon pinterest" title="Pinterest"><span class="sr-only">Pinterest</span></a>
                                <a href="#" class="social-icon icon instagram" title="Instagram"><span class="sr-only">Instagram</span></a>
                                <a href="#" class="social-icon icon flickr" title="Flickr"><span class="sr-only">Flickr</span></a>
                                <a href="#" class="social-icon icon skype" title="Skype"><span class="sr-only">Skype</span></a>
                                <a href="mailto:#" class="social-icon icon email" title="Email"><span class="sr-only">Email</span></a>
                            </div><!-- End .social-icons -->
                            <ul class="footer-menu">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Information</a></li>
                                <li><a href="#">Terms &amp; Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">Free Delivery</a></li>
                                <li><a href="#">Support</a></li>
                            </ul>
                        </div><!-- End .footer-right -->

                        <div class="footer-right">
                            <div class="payment-info">
                                <h5>100% Secure and Trusted Payment</h5>
                                <p>Easy returns. Free shipping on orders over $100. Need help? <a href="#">Help Center.</a></p>
                                <img src="<?= base_url(); ?>/assets/konsumen/images/cards.png" alt="Payment methods">
                            </div><!-- End .payment-info -->
                        </div><!-- End .footer-right -->

                        <a class="scroll-top icon" href="#top" title="Scroll top"><span class="sr-only">Scroll Top</span></a>
                    </div><!-- End .container-fluid -->
                </div><!-- End .footer-bottom -->
            </footer><!-- End .footer -->
        </div><!-- End #wrapper -->

        <!-- End -->
        <script src="<?= base_url(); ?>/assets/konsumen/js/plugins.min.js"></script>
        <script src="<?= base_url(); ?>/assets/konsumen/js/main.js"></script>
        
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/jquery.themepunch.revolution.min.js"></script>

        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
        (Load Extensions only on Local File Systems !  
        The following part can be removed on Server for On Demand Loading) -->  
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>/assets/konsumen/js/extensions/revolution.extension.video.min.js"></script>

        <script type="text/javascript">
            (function() {
                "use strict";
             
                $(document).ready(function() {
                    var revapi;
                    if ($("#rev_slider").revolution == undefined) {
                        revslider_showDoubleJqueryError("#rev_slider");
                    } else {
                        revapi = $("#rev_slider").show().revolution({
                            sliderType:"standard",
                            jsFileLocation:"assets/js/",
                            sliderLayout:"auto",  
                            dottedOverlay:"none",
                            delay:10000,
                            disableProgressBar: "on",
                            navigation: {
                                keyboardNavigation: "on",
                                keyboard_direction: "horizontal",
                                mouseScrollNavigation: "off",
                                onHoverStop: "off",
                                touch: {
                                    touchenabled: "on",
                                    swipe_threshold: 75,
                                    swipe_min_touches: 1,
                                    swipe_direction: "horizontal",
                                    drag_block_vertical: false
                                },
                                arrows: {
                                    style: "custom",
                                    enable: true,
                                    hide_onmobile: false,
                                    hide_under: 1200,
                                    hide_onleave: false,
                                    tmp: '',
                                    left: {
                                        h_align: "left",
                                        v_align: "center",
                                        h_offset: 58,
                                        v_offset: 0
                                    },
                                    right: {
                                        h_align: "right",
                                        v_align: "center",
                                        h_offset: 58,
                                        v_offset: 0
                                    }
                                },
                                bullets: {
                                    enable: true,
                                    hide_onmobile: false,
                                    hide_under: 767,
                                    style: "custom",
                                    hide_onleave: false,
                                    direction: "horizontal",
                                    h_align: "center",
                                    v_align: "bottom",
                                    h_offset: 0,
                                    v_offset: 40,
                                    space: 15
                                }
                            },
                            viewPort: {
                                enable:true,
                                outof:"pause",
                                visible_area:"80%"
                            },
                            responsiveLevels:[1800,1600,1200,992,767],
                            gridwidth:[1520, 1170,970,750,480],
                            gridheight:[850,680,500,420,280],
                            lazyType:"single",
                            parallax: {
                                type:"mouse",
                                origo:"slidercenter",
                                speed:2000,
                                levels:[2,3,4,5,6,7,12,16,10,50],
                            },
                            shadow:0,
                            spinner:"on",
                            stopLoop:"off",
                            stopAfterLoops:-1,
                            stopAtSlide:-1,
                            shuffle:"off",
                            autoHeight:"off",
                            hideThumbsOnMobile:"off",
                            hideSliderAtLimit:0,
                            hideCaptionAtLimit:0,
                            hideAllCaptionAtLilmit:0,
                            debugMode:false,
                            fallbacks: {
                                simplifyAll:"off",
                                nextSlideOnWindowFocus:"off",
                                disableFocusListener:false,
                            }
                        });

                        
                        $("#rev_slider").bind("revolution.slide.onloaded",function (e) {
                            BackgroundCheck.init({
                                targets: '.tparrows, .tp-bullets, .tp-caption',
                                images: '.tp-bgimg'
                            });
                        });

                        $("#rev_slider").bind("revolution.slide.onafterswap",function (e,data) {
                            BackgroundCheck.refresh();
                        });
                    }
                });

            })();
        </script>
    </body>
</html>