 <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
 <script src="<?=  base_url(); ?>assets/konsumen/js/plugins.min.js"></script>
<script src="<?=  base_url(); ?>assets/konsumen/main.js"></script>


            <div class="main">
                <div class="page-header">
                    <div class="container-fluid">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Contact Us</li>
                        </ol>
                    </div><!-- End .container-fluid -->
                </div><!-- End .page-header -->

                <div class="map-container">
                    <div id="map"></div><!-- End #map -->
                    <div class="form-container">
                        <h2>WRITE YOUR REVIEW</h2>
                        <form action="#">
                        <div class="form-group label-overlay">
                            <input type="text" class="form-control" required>
                            <label class="input-desc"><i class="icon input-icon input-user"></i>Enter your username <span class="input-required">*</span></label>
                        </div><!-- End .form-group -->
                        <div class="form-group label-overlay">
                            <input type="email" class="form-control" required>
                            <label class="input-desc"><i class="icon input-icon input-email"></i>Enter your email <span class="input-required">*</span></label>
                        </div><!-- End .form-group -->
                        <div class="form-group label-overlay">
                            <input type="text" class="form-control">
                            <label class="input-desc"><i class="icon input-icon input-subject"></i>Enter your subject</label>
                        </div><!-- End .form-group -->

                        <div class="form-group textarea-group mb30 label-overlay">
                            <textarea cols="30" rows="5" class="form-control min-height" required></textarea>
                            <label class="input-desc"><i class="icon input-icon input-message"></i>Write your message <span class="input-required">*</span></label>
                        </div><!-- End .form-group -->

                        <button type="submit" class="btn btn-custom">POST COMMENT</button>
                    </form>
                    </div><!-- End .form-container -->
                </div><!-- End .map-container -->
                
                <div class="contact-infos-container">
                    <div class="container-fluid">
                        <div class="contact-infos-wrapper">
                            <div class="row">
                                <div class="col-sm-4 contact-info-container">
                                    <div class="contact-info">
                                        <i class="icon contact-icon contact-pin-white"></i>
                                        <div class="contact-info-meta">
                                            <h3>our location</h3>
                                            <address>
                                                Oxford Street 48/188, London 02587, <br>United Kingdom, 24-157
                                            </address>
                                            <a href="#" class="more-link">Read more</a>
                                        </div><!-- end .contact-info-meta -->
                                    </div><!-- End .contact-info -->
                                </div><!-- End .col-sm-4 -->
                                <div class="col-sm-4 contact-info-container">
                                    <div class="contact-info">
                                        <i class="icon contact-icon contact-email-white"></i>
                                        <div class="contact-info-meta">
                                            <h3>Contact Details</h3>
                                            <ul>
                                                <li><span>Email:</span> <a href="mailto:#">sconto_shop@gmail.com</a></li>
                                                <li><span>Skype:</span> sconto_shop</li>
                                            </ul>
                                            <a href="#" class="more-link">Read more</a>
                                        </div><!-- end .contact-info-meta -->
                                    </div><!-- End .contact-info -->
                                </div><!-- End .col-sm-4 -->
                                <div class="col-sm-4 contact-info-container">
                                    <div class="contact-info">
                                        <i class="icon contact-icon contact-phone-white"></i>
                                        <div class="contact-info-meta">
                                            <h3>Contact Us</h3>
                                            <ul>
                                                <li><span>Phone:</span> 0203 - 980 - 14 - 79</li>
                                                <li><span>Mobile:</span>0203 - 478 - 12 - 96</li>
                                            </ul>
                                            <a href="#" class="more-link">Read more</a>
                                        </div><!-- end .contact-info-meta -->
                                    </div><!-- End .contact-info -->
                                </div><!-- End .col-sm-4 -->
                            </div><!-- End .row -->
                        </div><!-- End .contact-infos-wrapper -->
                    </div><!-- End .container-fluid -->
                </div><!-- end .contact-infos -container -->
            </div><!-- End .main -->

            <footer class="footer">
                


<script>
            (function() {
                "use strict";
                // Google Map api v3 - Map for contact pages
                if ( document.getElementById("map") && typeof google === "object" ) {
                    // Map pin coordinates and content of pin box
                    var locations = [
                        [
                            '<address><strong>Address:</strong> Oxford Street 48/188, London 02587, UK, 24-157 <br> <strong>Phone:</strong> 0203 - 980 - 14 - 79</address>',
                            41.994953,
                            12.585666
                        ]
                    ];

                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 5,
                        center: new google.maps.LatLng(39.532365, -5.871364), // Map Center coordinates
                        scrollwheel: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#e7f8fd"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"saturation":25},{"lightness":25}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"lightness":-20}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}]
                    });

                    var infowindow = new google.maps.InfoWindow();


                    var marker, i;

                    for ( i = 0; i < locations.length; i++ ) {  
                      marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: 'pin.png'
                      });

                      google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function() {

                          infowindow.setContent(locations[i][0]);
                          infowindow.open(map, marker);
                        }
                      })(marker, i));
                    }
                }
            })();
        </script>

