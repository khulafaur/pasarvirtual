            <div class="main">
                <div class="page-header custom larger mb60">
                    <div class="container-fluid">
                        <h1>Login or Register</h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Login or Register</li>
                        </ol>
                    </div><!-- End .container-fluid -->
                </div><!-- End .page-header -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row row-lg">
                                <div class="col-sm-6">
                                    <h2>PELANGGAN BARU</h2>

                                    <div class="new-customer-section">
                                        <p>Dengan membuat akun di toko kami, Anda akan dapat bergerak melalui proses pembayaran lebih cepat, menyimpan banyak alamat pengiriman, melihat dan melacak pesanan Anda di akun Anda dan banyak lagi.</p>
                                    </div><!-- End .new-customer-section -->

                                    <a href="<?= base_url(); ?>konsumenDaftar" class="btn btn-custom">BUAT AKUN</a>
                                </div><!-- End .col-sm-6 -->

                                <div class="mb60 visible-xs"></div><!-- margin -->

                                <div class="col-sm-6">
                                    <h2>SUDAH PUNYA AKUN?</h2>
                                    <div class="col-md-12">
                                        <?php echo $this->session->flashdata('info'); ?>
                                    </div>  
                                    <?php echo $this->session->flashdata('login'); ?>
                                    <div class="registered-customer-section">
                                        <p>Jika Anda memiliki akun dengan kami, silakan masuk.</p>
                                        <?=form_open('konsumenMasuk/prosesMasuk','id="registration_form"'); ?>
                                            <div class="form-group label-overlay">
                                                <input type="username" class="form-control" required name="username" id="username" >
                                                <label class="input-desc"><i class="icon input-icon input-user"></i>Masukkan Username <span class="input-required">*</span></label>
                                            </div><!-- End .form-group -->
                                            <div class="form-group label-overlay">
                                                <input type="password" class="form-control" required name="password" id="password" >
                                                <label class="input-desc"><i class="icon input-icon input-password"></i>Masukkan Password <span class="input-required">*</span></label>
                                            </div><!-- End .form-group -->

                                            <div class="mb20"></div><!-- margin -->
                                            
                                            <div class="form-action">
                                                <button type="submit" class="btn btn-custom">Masuk</button>
                                                <a href="#" class="forget-link">Lupa Password?</a>
                                            </div><!-- End .form-action -->
                                        <?=form_close(); ?>
                                    </div><!-- End .registered-customer-section -->
                                </div><!-- End .col-sm-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .col-md-9 -->

                        
                </div><!-- End .container-fluid -->
                <div class="mb80 visible-md visible-lg"></div><!-- margin -->
            </div><!-- End .main -->
