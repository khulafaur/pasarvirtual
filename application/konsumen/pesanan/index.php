<script type="text/javascript" src="<?php echo base_url(); ?>assets/konsumen/lacak/js/JQuery.min.js"></script>
<script>
$(document).ready(function(){

  $("#propinsi_asal").click(function(){
    $.post("<?php echo base_url(); ?>index.php/rajaongkir/getCity/"+$('#propinsi_asal').val(),function(obj){
      $('#origin').html(obj);
    });

  });

  $("#propinsi_tujuan").click(function(){
    $.post("<?php echo base_url(); ?>index.php/rajaongkir/getCity/"+$('#propinsi_tujuan').val(),function(obj){
      $('#destination').html(obj);
    });

  });

  /*
  $("#cari").click(function(){
    $.post("<?php echo base_url(); ?>index.php/rajaongkir/getCost/"+$('#origin').val()+'&dest='+$('#destination').val()+'&berat='+$('#berat').val()+'&courier='+$('#courier').val(),function(obj){
      $('#hasil').html(obj);
    });
  });

  */


});
</script>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9">
                            <?php if($this->cart->contents()==NULL): ?>

                            <div class="text-center">
                                <img src="<?=base_url('assets/konsumen/images/cart-empty.png');?>" alt="Barang">
                                <h3>Belum ada barang barang.</h3>
                            </div>

                            <?php else: ?>


                            <div class="table-responsive">
                                <table class="table table-bordered cart-table">
                                    <thead>
                                        <tr>
                                            <th class="product-col">Nama Produk</th>
                                            <!-- <th class="code-col">Product Code</th> -->
                                            <th class="price-col">Harga Per Satuan</th>
                                            <th class="quantity-col">Jumlah</th>
                                            <th class="subtotal-col">Subtotal</th>
                                            <th class="delete-col"><i class="icon delete-btn"></i></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                         <?php $i = 1; ?>
                                        <?php foreach($this->cart->contents() as $items): ?>
                                        <tr>
                                            <td class="product-col">
                                                <div class="product">
                                                    <div class="product-top">
                                                        <!-- <span class="product-label discount">-25%</span> -->
                                                        <figure>
                                                            <a href="#">
                                                                <img src="<?php echo base_url(); ?>uploads/<?php echo $items['foto']?>" alt="Product Image">
                                                            </a>
                                                        </figure>
                                                    </div><!-- End .product-top -->

                                                    <div class="product-content-wrapper">
                                                        <h3 class="product-title">
                                                            <a href="#"><?php echo $items['name']; ?></a>
                                                        </h3>
                                                        <ul>
                                                            <li><span>Color:</span> Black</li>
                                                            <li><span>Size:</span> XS</li>
                                                        </ul>
                                                    </div><!-- End .product-content-wrapper -->
                                                </div><!-- End .product -->
                                            </td>
                                            <!-- <td class="code-col">483 512 5609</td> -->
                                            <td class="price-col">
                                                <div class="product-price-container">
                                                    
                                                    <span class="product-price"><?php echo "Rp. " . number_format($items['price'],0,",",".") ; ?></span>
                                                </div><!-- End .product-price-container -->
                                            </td>
                                            <td class="quantity-col">
                                                <input type="text" class="form-control"  value="<?php echo $items['qty']; ?>">
                                            </td>
                                            <td class="subtotal-col"><?php echo number_format($items['subtotal']); ?></td>
                                            <td class="delete-col"><a href="#" class="icon delete-btn lighter"></a></td>
                                        </tr>

                                         <?php $i++; ?>
                                         <?php endforeach; ?>   

                                    </tbody>
                                </table>
                            </div><!-- End .table-responsive -->

                            <div class="row">
                                <div class="col-xs-12 col-lg-8">
                                    <div class="vertical-tab-container" role="tabpanel">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs text-uppercase" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#tab-tax" aria-controls="tab-tax" role="tab" data-toggle="tab">
                                                   Biaya Kirim
                                                </a>
                                            </li>

                                            <li role="presentation">
                                                <a href="#tab-discount" aria-controls="tab-discount" role="tab" data-toggle="tab">
                                                    Kode Diskon
                                                </a>
                                            </li>

                                            <li role="presentation">
                                                <a href="#tab-gift" aria-controls="tab-gift" role="tab" data-toggle="tab">
                                                    Voucher Pemberian
                                                </a>
                                            </li>
                                        </ul>

                                        <!-- Tab Panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="tab-tax">
                                                <p>Masukkan alamat tujuan anda untuk mendapatkan biaya kirim</p>
                                                <div class="form-group">
                                                    <label>Provinsi<span class="text-custom">*</span></label>
                                                    <select class="custom-select form-control" name="propinsi_asal" id="propinsi_asal">
                                                        <option value="" selected="" disabled="">-- Pilih Provinsi</option>
                                                        <?php $this->load->view('konsumen/lacak/getProvince'); ?>
                                                        
                                                    </select>
                                                </div><!-- end .form-group -->
                                                <div class="form-group">
                                                    <label>Kota/Kabupaten<span class="text-custom" for="descity">*</span></label>
                                                    <select class="custom-select form-control" name="origin" id="origin">
                                                        <option selected="">-- Pilih Kota/Kabupaten</option>
                                                        
                                                    </select>
                                                </div><!-- end .form-group -->
                                                <div class="form-group">
                                                    <label>Kode POS<span class="text-custom">*</span></label>
                                                    <input type="text" class="form-control with-btn" placeholder="12345">
                                                    <button type="button" onclick="tampil_data('data')" class="btn btn-border btn-gray2">Cek Ongkir</button>
                                                    <!-- <a href="#" class="btn btn-border btn-gray2">Cek Ongkir</a> -->
                                                </div><!-- end .form-group -->
                                            </div><!-- End .tab-pane -->

                                            <div role="tabpanel" class="tab-pane" id="tab-discount">
                                                <p>Fitur ini segera hadir!</p>
                                            </div><!-- End .tab-pane -->

                                            <div role="tabpanel" class="tab-pane" id="tab-gift">
                                                <p>Fitur ini segera hadir!</p>
                                            </div><!-- End .tab-pane -->
                                        </div><!-- End .tab-content -->
                                    </div><!-- end role[tabpanel] -->

                                    <a href="#" class="btn btn-custom min-width">Belanja Lagi</a>
                                </div><!-- End .col-lg-8-->

                                <div class="mb50 clearfix hidden-lg"></div><!-- margin -->

                                <div class="col-xs-12 col-lg-4">
                                    <div class="table-responsive mb30">
                                        <table class="table table-bordered total-table">
                                            <tbody>
                                                <div class="form-group">
                                                    
                                                    <select class="custom-select form-control" name="origin" id="origin">
                                                        <option>-- Pilih Kurir</option>
                                                         <option value="jne">JNE</option>
                                                          <option value="pos">POS</option>
                                                          <option value="tiki">TIKI</option>
                                                        
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div id="hasil">
                                                </div>
                                            </tbody>
                                        </table>    
                                        <br>
                                        <table class="table table-bordered total-table">
                                            <tbody>
                                               <!--  <tr>
                                                    <td>SUBTOTAL:</td>
                                                    <td>$737.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Shipping:</td>
                                                    <td>$6.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Tax (5%):</td>
                                                    <td>$37.00</td>
                                                </tr> -->
                                                <tr>
                                                     <td>Biaya Kirim</td>
                                                    <td><?php echo number_format($this->cart->total()); ?></td>
                                                </tr>
                                                <tr>

                                                    <td>Total</td>
                                                    <td><?php echo number_format($this->cart->total()); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="text-right">
                                        <a href="#" class="btn btn-custom min-width">Beli Sekarang</a>
                                    </div>
                                </div><!-- End .col-lg-4 -->
                            </div><!-- End .row -->

<script>
          function tampil_data(act){
                var w = $('#origin').val();
                var x = $('#destination').val();
                // var y = $('#berat').val();
                // var z = $('#courier').val();

                if(w == "" || x == "" ){
                  alert("harap isi data dengan lengkap");
                }else{
                  $.ajax({
                    url: "rajaongkir/getCost",
                    type: "GET",
                    data : {origin: w, destination: x, },
                    success: function (ajaxData){
                        //$('#tombol_export').show();
                        //$('#hasilReport').show();
                        $("#hasil").html(ajaxData);
                    }
                  });
                }


            };
        </script>
        <?php endif; ?>