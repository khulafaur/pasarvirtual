<?php
	class m_distributor extends CI_Model {

	function profil_distributor() {
		return $this->db->get('distributor')->result();
	}

	function get_one($ktp_dist){
      $param = array('ktp_distributor'=>$ktp_dist);
      return $this->db->get_where('distributor',$param);
	}
	function input_data_profil($data,$table){
		$this->db->insert($table,$data);
	}
	function edit_data_profil($where,$table){
		return $this->db->get_where($table,$where);
	}	
	// public function update_data_profil($ktp_dist,$data){
 //        $this->db->where('ktp_dist', $ktp_dist);
 //        $this->db->update('distributor', $data);
 //    }
	function update_data_profil($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	
	function GetArtikelId($id){
		$id=$this->db->select('*')
		->from('deskripsi_produk')
		->where('id',$id)
		->get();
		return $id;
	}
}
?>

