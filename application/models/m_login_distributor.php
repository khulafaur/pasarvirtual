<?php
	class m_login_distributor extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	public function check_username($username) {
			$this->db->select('*');
			$this->db->from('distributor');
			$this->db->where('username', $username);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			}
				return false;
		}

	public function ambil_data_distributor($username){
			$this->db->where("username",$username);
			return $this->db->get("distributor");
	}

	public function rehash($newhash, $username){
		$this->db->where("username",$username);
		return $this->db->update("distributor",$newhash);
	}

	public function registration_insert($data) {
	// Query to check whether username already exist or not
	$condition = "username =" . "'" . $data['username'] . "'" . " or " ."ktp_distributor =" . "'" . $data['ktp_distributor'] . "'";
	$this->db->select('*');
	$this->db->from('distributor');
	$this->db->where($condition);
	$this->db->limit(1);
	$query = $this->db->get();
	if ($query->num_rows() == 0) {
		// Query to insert data in database
		$this->db->insert('distributor', $data);
		if ($this->db->affected_rows() > 0) {
		return true;
		}
		} else {
		return false;
		}
	}
}