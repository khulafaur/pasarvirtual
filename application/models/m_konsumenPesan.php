<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_konsumenPesan extends CI_Model {

		public function prosesPemesanan(){
	        $tagihan = array(
	            'tanggal' => date('Y-m-d H:i:s'),
	            'batas_tanggal' => date('Y-m-d H:i:s', 
	                mktime(date ('H'), date('i'),date('s'),
	                date('m'), date('d')+1,date('Y'))),
	            'status' => 0,
	            'id_konsumen'=> $this->session->userdata['sedangLogin']['konsumen_id']
	        );
	        $this->db->insert('tagihan',$tagihan);
	        $id_tagihan = $this->db->insert_id();

	        foreach ($this->cart->contents() as $item){
	            $data =  array(		
	                'id_tagihan'    => $id_tagihan,
	                'id_produk'    => $item['id'],
	                'nama_produk'  => $item['name'],
	                'jumlah'           => $item['qty'],
	                'harga'         => $item['price']
	            );
	            $this->db->insert('pemesanan', $data);
	        }
	        RETURN TRUE;
	    }
    
	}
?>