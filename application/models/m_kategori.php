<?php
	
	class M_kategori extends CI_Model {
		public function __construct() {
			parent::__construct();
		}


		public function getAllKategoriUmum(){
	        $this->db->select('*');
			$this->db->from('kategori_umum');
	        $tampung = $this->db->get();
	        return $tampung->result();
		}

		public function getAllKategori(){
	        $this->db->select('*');
			$this->db->from('kategori');
	        $tampung = $this->db->get();
	        return $tampung->result();
		}

		public function getAllKategoriKhusus(){
	        $this->db->select('*');
			$this->db->from('kategori_khusus');
	        $tampung = $this->db->get();
	        return $tampung->result();
		}

		
	}

?>