<?php
	class m_tracking extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function tampil_tracking(){
		$this->db->select('t.id_transaksi,t.no_resi,d.ktp_dist, d.nama_distributor, t.nominal, t.status_transaksi, t.jasa_kirim, t.tanggal');
		$this->db->from('distributor d');
		$this->db->join('transaksi t', 'd.ktp_dist = t.ktp_dist');
		$this->db->where('t.status_transaksi = "Dikirim"');
		$query = $this->db->get();
	    return $query->result();
	}
	}
	
?>