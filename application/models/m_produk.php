<?php
	class m_produk extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function tambah_data_produk($data) {
       $this->db->insert('produk', $data);
       $id = $this->db->insert_id();
       return (isset($id)) ? $id : FALSE;
	}

	public function tambah_data_aktivitas($data) {
       $this->db->insert('aktivitas_produsen', $data);
	}
		   
	public function tambah_stok($data) {
		$this->db->insert('produk_variasi', $data);
	}

	public function getAllKategori(){
		$response = array();
        $this->db->select('*');
		$this->db->from('kategori');
        $q = $this->db->get();
        return $q->result();
	}
	
	public function get_all_produk($ktp_dist){
		$this->db->select('p.nama_produk,p.deskripsi_produk, pv.nama_variasi, pv.harga, pv.gambar_produk, pv.id_produk_variasi');
		$this->db->from('produk p');
		$this->db->join('produk_variasi pv', 'p.id_produk = pv.id_produk');
		$this->db->join('produk_distributor pd','pd.id_produk = p.id_produk');
		$this->db->where('pd.ktp_dist',$ktp_dist);
		$this->db->where('pd.status_publish','1');
		$this->db->where('p.status_produk','1');

		$query = $this->db->get();
		return $query->result();
    }

	public function tampil_data_produk($produsen_id) {
		$this->db->select('p.id_produk, p.nama_produk, p.gambar,p.deskripsi_produk, kh.nama_kategori_khusus,k.nama_kategori,ku.nama_kategori_umum');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k', 'kh.id_kategori = k.id_kategori');
		$this->db->join('kategori_umum ku', 'k.id_kategori_umum = ku.id_kategori_umum');
		$this->db->where('p.status_produk', '1');
		$this->db->where('p.id_produsen', $produsen_id);
		$this->db->order_by('p.nama_produk', 'asc');
		$query=$this->db->get();
		return $query->result();
	}

	public function tampil_data_produk_trash($produsen_id) {
		$this->db->select('p.id_produk, p.nama_produk, p.gambar,p.deskripsi_produk, kh.nama_kategori_khusus,k.nama_kategori,ku.nama_kategori_umum');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k', 'kh.id_kategori = k.id_kategori');
		$this->db->join('kategori_umum ku', 'k.id_kategori_umum = ku.id_kategori_umum');
		$this->db->where('p.status_produk', '0');
		$this->db->where('p.id_produsen', $produsen_id);
		$this->db->order_by('p.nama_produk', 'asc');
		$query=$this->db->get();
		return $query->result();
	}

	public function tampil_data_produk_variasi($produsen_id) {
		$this->db->select('p.id_produk, p.nama_produk, pv.nama_variasi,pv.harga_jual, pv.id_produk_variasi, pv.gambar_produk, p.deskripsi_produk, kh.nama_kategori_khusus, pv.harga, pv.stok');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('produk_variasi pv', 'p.id_produk = pv.id_produk');
		$this->db->where('status_produk_variasi', '1');
		$this->db->where('p.id_produsen', $produsen_id);
		$this->db->order_by('nama_produk', 'asc');
		$query=$this->db->get();
		return $query->result();
	}

	public function find($id){
		$hasil = $this->db->where('id_produk_variasi',$id)
						  ->limit(1)
						  ->get('produk_variasi');
		if ($hasil->num_rows() > 0){
			return $hasil -> row();
		} else {
			return array();
		}
	}
	
	function update_onetable($primary_key,$variable,$table_name,$data){
       $this->db->where($variable,$primary_key);
       $this->db->update($table_name,$data);
	}

	public function getAllKategoriKhusus(){
		$response = array();
        $this->db->select('*');
		$this->db->from('kategori_khusus');
        $q = $this->db->get();
        return $q->result();
	}
	
	function detail_produk($primarykey) {
	    $this->db->select('p.id_produk, p.nama_produk,p.deskripsi_produk, kh.nama_kategori_khusus,k.nama_kategori,ku.nama_kategori_umum,p.persen_produsen, p.persen_distributor');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k', 'kh.id_kategori = k.id_kategori');
		$this->db->join('kategori_umum ku', 'k.id_kategori_umum = ku.id_kategori_umum');
		$this->db->where('p.id_produk', $primarykey);
		$query=$this->db->get();
		return $query->result();
	}

	function detail_variasi($primarykey){
		$this->db->select('p.nama_produk, pv.nama_variasi,pv.harga_jual,pv.gambar_produk, pv.harga, pv.stok, ((p.persen_produsen * pv.harga_jual)-pv.harga) as laba_bersih');
		$this->db->from('produk p');
		$this->db->join('produk_variasi pv', 'p.id_produk = pv.id_produk');
		$this->db->where('p.id_produk',$primarykey);
		$query = $this->db->get();
	    return $query->result();
	}

	function detail_variasi_produk($primarykey){
		$this->db->select('p.nama_produk,pv.id_produk_variasi,pv.harga_jual, pv.nama_variasi, pv.gambar_produk, pv.harga, pv.stok');
		$this->db->from('produk p');
		$this->db->join('produk_variasi pv', 'p.id_produk = pv.id_produk');
		$this->db->where('pv.id_produk_variasi',$primarykey);
		$query = $this->db->get();
	    return $query->result();
	}

	function update_variasi_produk($primarykey,$object){
		$this->db->where("id_produk_variasi",$primarykey);
		$this->db->update("produk_variasi",$object);
	}

	function get_one1($id_produk){
      $this->db->select('p.id_produk, p.gambar,ku.id_kategori_umum, k.id_kategori, kh. id_kategori_khusus, p.nama_produk, p.deskripsi_produk, kh.nama_kategori_khusus, ku.nama_kategori_umum, k.nama_kategori, kh.nama_kategori_khusus');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k', 'kh.id_kategori = k.id_kategori');
		$this->db->join('kategori_umum ku', 'k.id_kategori_umum = ku.id_kategori_umum');
		$this->db->where('p.id_produk', $id_produk);
		$this->db->order_by('nama_produk', 'asc');
		$query=$this->db->get();
		return $query->result();
	}
	
	public function update_status_byid($set_id,$data,$data1){
		$this->db->where("id_produk",$set_id);
		$this->db->update("produk",$data);
		
		$this->db->where("id_produk",$set_id);
		$this->db->update("produk_variasi",$data1);
	}

	public function update_statusvariasi_byid($set_id,$data){
		$this->db->where("id_produk_variasi",$set_id);
		$this->db->update("produk_variasi",$data);
	}
	
    function getUmum() {
		$response = array();
        $this->db->select('*');
        $q = $this->db->get('kategori_umum');
        $response = $q->result_array();
        return $response;
	}
	
	function getKatUmum($postData) {
		$response = array();
        $this->db->select('*');
		$this->db->from('kategori');
        $this->db->where('id_kategori_umum',$postData);
        $q = $this->db->get();
        return $q->result();
	}
	
	function getKatKatKhusus($postData){
        $response = array();
        $this->db->select('*');
		$this->db->from('kategori_khusus');
        $this->db->where('id_kategori', $postData);
        $q = $this->db->get();
        return $q->result();
    }

    function update_stok_produk($update_stok_produk,$id_variasi_produk) {
    	$this->db->where('id_produk_variasi',$id_variasi_produk);
       	$this->db->update('produk_variasi',$update_stok_produk);
    }
}
?>