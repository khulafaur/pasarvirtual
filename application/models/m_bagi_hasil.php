<?php
class m_bagi_hasil extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_data_produk($select,$from,$join,$condition,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->where($where);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_produk_byid($select,$from,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function update_produk($object,$id_produk){
		$this->db->where("id_produk",$id_produk);
		return $this->db->update("produk",$object);
	}
}