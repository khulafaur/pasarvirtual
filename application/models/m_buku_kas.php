<?php
class m_buku_kas extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_data_pemasukan($select,$from,$join,$condition,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_transfer($select,$from,$join,$condition,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_penarikan($select,$from,$join,$condition,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_transfer_byId($select,$from,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_penarikan_byId($select,$from,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_aktivitas($select,$from,$join,$condition){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition,'left');
		$this->db->order_by('d.day','asc');
		$query = $this->db->get();
		return $query->result();
	}
}