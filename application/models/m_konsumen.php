<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_konsumen extends CI_Model {


	public function cekAkun($username,$password)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$query = $this->db->get('konsumen');

		return $query;
	}


	 public function prosesLogin($user,$pass){
        $this->db->where('username',$user);
        $this->db->where('password',$pass);
        return $this->db->get('konsumen')->row();
    }


	public function get_data_session($email)
	{
		$this->db->where('email', $email);
		$this->db->select('*');
		$this->db->limit(1);

		$query = $this->db->get($this->_tabel);

		return $query->row();
	}

	public function getKonsumen()
	{
		return $this->db->get('konsumen');
	}


}

?>
