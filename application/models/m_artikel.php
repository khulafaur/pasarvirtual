<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_artikel extends CI_Model {

	public function add($data){
		$this->db->insert('artikel', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function getArtikelById($id){
		$this->db->where('id_artikel', $id);
		$data = $this->db->get('artikel');
		return $data->row();
	}

	public function getArtikelByPublis(){
		$this->db->order_by('tgl_post', 'desc');
		$data = $this->db->get_where("artikel", "status = 'publis'");
		return $data->result_array();
	}

	public function getArtikelByFeature(){
		$this->db->order_by('tgl_post', 'desc');
		$data = $this->db->get_where("artikel", "status = 'feature'");
		return $data;
	}

	public function getRecentPost(){
		$this->db->order_by('tgl_post', 'desc');
		$this->db->limit(3);
		$data = $this->db->get_where("artikel", "status = 'publis'");
		return $data->result_array();
	}

	public function getArtikel(){
		// $data = $this->db->get('artikel');
		$data['publis'] = $this->db->where("status = 'publis'")->or_where("status = 'feature'")->get("artikel");
		$data['draft'] = $this->db->get_where("artikel", "status = 'draft'");
		$data['sampah'] = $this->db->get_where("artikel", "status = 'trash'");
		return $data;
	}

	public function update($id, $data){
		$this->db->where('id_artikel', $id);
		$this->db->update('artikel', $data);
		return $this->db->affected_rows();
	}

	public function setPublis($id){
		$this->db->set('status', 'publis');
		$this->db->where('id_artikel', $id);
		$this->db->update('artikel');
		return $this->db->affected_rows();
	}

	public function setFeature($id){
		$this->db->set('status', 'feature');
		$this->db->where('id_artikel', $id);
		$this->db->update('artikel');
		return $this->db->affected_rows();
	}

	public function moveToTrash($id){
		$this->db->set('status', 'trash');
		$this->db->where('id_artikel', $id);
		$this->db->update('artikel');
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->delete('artikel', "id_artikel = $id");
	}

}

/* End of file m_artikel.php */
/* Location: ./application/models/m_artikel.php */
