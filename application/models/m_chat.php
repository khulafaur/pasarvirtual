<?php
	class m_chat extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function my_distributor($primary) {
		$this->db->select("pd.id_prod_dist,d.ktp_distributor,d.nama_toko,d.nama_distributor,d.email,d.hp,d.telp,d.foto_distributor,pd.id_distributor");
		$this->db->from("produsen_distributor pd");
		$this->db->join("distributor d","d.ktp_distributor=pd.id_distributor");
		$this->db->where("pd.id_produsen",$primary);

		$query = $this->db->get();
		return $query->result();
	}

	function form_chat($primary,$id_dist) {
		$this->db->select("pd.id_prod_dist,d.ktp_distributor,d.nama_toko,d.nama_distributor,d.email,d.hp,d.telp,d.foto_distributor,pd.id_distributor");
		$this->db->from("produsen_distributor pd");
		$this->db->join("distributor d","d.ktp_distributor=pd.id_distributor");
		$this->db->where("pd.id_produsen",$primary);
		$this->db->where("pd.id_distributor",$id_dist);

		$query = $this->db->get();
		return $query->result();
	}

	function insert_chat($object) {
		$this->db->insert('chat', $object);
	}

	function delivered($object,$id){
		$this->db->where('id',$id);
       $this->db->update('chat',$object);
	}

	function get_chat($id) {
		$this->db->select("DATE_FORMAT(c.chattime,'%d %M %Y, %r') as time ,c.chattext,d.foto_distributor,c.id_produsen,c.id_distributor,c.status_chat,p.foto_produsen,c.id");
		$this->db->from("chat c");
		$this->db->join("distributor d","d.ktp_distributor = c.id_produsen");
		$this->db->join("produsen p","p.ktp_produsen = c.id_produsen");
		$this->db->where( "delivered_stats=0");
		$query = $this->db->get();
		return $query->result();
	}

	function get_chat_data($id_produsen,$id_dist) {
		$this->db->select("DATE_FORMAT(c.chattime,'%d %M %Y, %r') as time ,c.chattext,d.foto_distributor,c.id_produsen,c.id_distributor,c.status_chat,p.foto_produsen");
		$this->db->from("chat c");
		$this->db->join("distributor d","d.ktp_distributor = c.id_produsen");
		$this->db->join("produsen p","p.ktp_produsen = c.id_produsen");
		$this->db->where( "c.id_produsen =".$id_produsen." and c.id_distributor =".$id_dist);
		$this->db->order_by( "c.chattime",'asc');
		$query = $this->db->get();
		return $query->result();
	}
}
?>
