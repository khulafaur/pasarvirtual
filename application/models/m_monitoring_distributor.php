<?php
class m_monitoring_distributor extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_data_distributor($select,$from,$join,$condition,$join1,$condition1,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->where($where);
		
		$query = $this->db->get();
		return $query->result();
	}
	public function get_data_dist_byid($select,$from,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function hapus_distributor_byid($set_id){
		$this->db->where("id_prod_dist",$set_id);
		return $this->db->delete("produsen_distributor");
	}

	public function approve_distributor_byid($set_id,$object){
		$this->db->where("id_prod_dist",$set_id);
		return $this->db->update("produsen_distributor",$object);
	}

	public function history_penjualan_dist($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$join3,$condition3,$group_by){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2);
		$this->db->join($join3,$condition3);
		$this->db->group_by($group_by);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_detail_trx_dist($select,$from,$join,$condition,$join1,$condition1,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->where($where);
		$this->db->group_by("t.id_transaksi");
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_detail_trx_byid($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2);
		$this->db->where($where);
		//$this->db->group_by("dt.id_produk_variasi");
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_leaderboard($select,$from,$join,$condition){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition,'left');
		$this->db->order_by("qty",'DESC');
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_penjualan_tahunan_dist($select,$from,$join,$condition)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition,'left');
		$this->db->order_by("d.day",'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail_aktivitas_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$join3,$condition3,$where,$group_by){

		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2);
		$this->db->join($join3,$condition3);
		$this->db->where($where);
		$this->db->group_by($group_by);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_dompet($select,$from,$where)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}

	public function kirim_bonus($id_dompet,$object,$object1)
	{
		$this->db->insert('aktivitas_transfer',$object1);
		$this->db->where("id_dompet",$id_dompet);
		$this->db->update("dompet",$object);
	}
}