<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_edit_logoprofile extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function getAllData() {
		$sql="SELECT * from produsen";
		$hasil=$this->db->query($sql);
		return $hasil->result();
	}

	function getDataById($id) {
		$this->db->from('produsen');
		$this->db->where('no_ktp', $id);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		}
	}

	function insertData($data) {
		$this->db->insert('produsen', $data);
		return TRUE;
	}

	function updateData($id, $data) {
		$this->db->where('no_ktp', $id);
		$this->db->update('produsen', $data);
		return TRUE;
	}

	function deleteData($id) {
		$this->db->where('no_ktp', $id);
		$this->db->delete('produsen');
		if ($this->db->affected_rows() == 1) {
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file m_produsen.php */
/* Location: ./application/models/m_produsen.php */