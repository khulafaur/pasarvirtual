<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pesanan extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function getAllData() {
		$sql="SELECT * from transaksi";
		$hasil=$this->db->query($sql);
		return $hasil->result();
	}
	// function liat_transaksi(){
	// 	$this->db->select('SUM(nominal) as total');
	// 	$this->db->from('transaksi');
	// 	return $this->db->get()->result();
	// }
	function pesanan($id) {
		$this->db->select('k.id_konsumen, k.namaDepan, k.namaBelakang,t.id_transaksi,sum(dt.qty * dt.harga_transaksi) as nominal, t.jasa_kirim, t.tanggal,t.status_transaksi, t.no_resi');
		$this->db->from('konsumen k');
		$this->db->join('transaksi t','k.id_konsumen = t.id_konsumen');
		$this->db->join('detail_transaksi dt','t.id_transaksi = dt.id_transaksi');
		$this->db->where('dt.id_dist',$id);
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
		return $query->result();
	}

	function detail_pesanan($primarykey,$id_distributor) {
		$this->db->select('t.id_transaksi, ps.ktp_produsen, ps.nama_produsen, ps.alamat_toko, ps.no_hp, ps.nama_toko, ps.email,ps.telp, p.id_produk, p.nama_produk, kh.nama_kategori_khusus, dt.harga_transaksi, dt.qty, (dt.qty * dt.harga_transaksi) as total_pesanan, t.tanggal,(dt.qty * dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.no_resi, k.namaDepan, k.namaBelakang, k.alamat, k.kelurahan, k.kota, k.provinsi, k.kodePos, k.nomorHp, k.email');
		$this->db->from('transaksi t');
		$this->db->join('konsumen k', 'k.id_konsumen = t.id_konsumen');
		$this->db->join('detail_transaksi dt', 'dt.id_transaksi = t.id_transaksi');
		$this->db->join('produk_variasi pv', 'pv.id_produk_variasi = dt.id_produk_variasi');
		$this->db->join('produk p' , 'pv.id_produk = p.id_produk');
		$this->db->join('produsen ps' , 'ps.ktp_produsen = p.id_produsen');
		$this->db->join('kategori_khusus kh','kh.id_kategori_khusus = p.id_kategori_khusus');
		$this->db->where('t.id_transaksi', $primarykey);
		$this->db->where('dt.id_dist', $id_distributor);
		$query = $this->db->get();
	    return $query->result();
	}

	function total($primarykey,$id_distributor) {
		$this->db->select('sum(dt.qty * dt.harga_transaksi) as total, sum(dt.qty) as qty ');
		$this->db->from('transaksi t');
		$this->db->join('detail_transaksi dt', 'dt.id_transaksi = t.id_transaksi');
		$this->db->where('t.id_transaksi', $primarykey);
		$this->db->where('dt.id_dist', $id_distributor);
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

}

/* End of file m_produsen.php */
/* Location: ./application/models/m_produsen.php */