<?php
	class m_myproduk extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function tambah_data_produk($data) {
		$query = $this->db->insert('produk', $data);
		return $query;
	}

	public function tampil_data_produk() {
		$query = $this->db->get('produk');
		return $query->result();
	}
	
	function update_onetable($primary_key,$variable,$table_name,$data){
       $this->db->where($variable,$primary_key);
       $this->db->update($table_name,$data);
	}

	function get_one1($id_produk){
      $param = array('id_produk'=>$id_produk);
      return $this->db->get_where('produk',$param);
	}
	
	function hapus_produk($id_produk){
        $this->db->delete('produk', array('id_produk' => $id_produk));
	}

	function gudang_distributor($id_distributor){
		$this->db->select('p.id_produk,p.nama_produk,p.gambar, pd.status_publish,p.deskripsi_produk,p.persen_produsen,p.persen_distributor,k.nama_kategori_khusus');
		$this->db->from('produk_distributor pd');
		$this->db->join('produk p','pd.id_produk = p.id_produk');
		$this->db->join('kategori_khusus k','k.id_kategori_khusus = p.id_kategori_khusus');
		$this->db->where('pd.ktp_dist',$id_distributor);

		return $this->db->get()->result();
	}


	function gudangById($id) {
		$this->db->from('produk');
		$this->db->where('id_produk', $id);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		}
	}

	function insert_barangku($object){
		$this->db->insert("produk_distributor",$object);
	}

	function insert_produsenku($object){
		$this->db->insert("produsen_distributor",$object);
	}

	function update_barang_cms($object1,$id_produk,$id_distributor){
		$this->db->where('id_produk', $id_produk);
		$this->db->where('ktp_dist', $id_distributor);
		$this->db->update('produk_distributor', $object1);
	}

	function update_to_zero($object){
		$this->db->update('produk_distributor', $object);
	}

	function detail_produk($primarykey) {
	    $this->db->select('p.id_produk, p.nama_produk,p.deskripsi_produk, kh.nama_kategori_khusus,k.nama_kategori,ku.nama_kategori_umum,p.persen_distributor');
		$this->db->from('produk p');
		$this->db->join('kategori_khusus kh', 'p.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k', 'kh.id_kategori = k.id_kategori');
		$this->db->join('kategori_umum ku', 'k.id_kategori_umum = ku.id_kategori_umum');
		$this->db->where('p.id_produk', $primarykey);
		$query=$this->db->get();
		return $query->result();
	}

	function detail_variasi($primarykey){
		$this->db->select('p.nama_produk, pv.nama_variasi, pv.gambar_produk, pv.harga, pv.stok, (p.persen_produsen * pv.harga) as laba_bersih');
		$this->db->from('produk p');
		$this->db->join('produk_variasi pv', 'p.id_produk = pv.id_produk');
		$this->db->where('p.id_produk',$primarykey);
		$query = $this->db->get();
	    return $query->result();
	}
}

?>