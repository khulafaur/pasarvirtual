<?php
class m_dompet extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_data_dompet($produsen_id){
		$this->db->select('*');
		$this->db->from('dompet');
		$this->db->where('id_pemilik',$produsen_id);
		$this->db->where('status_dompet','1');
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get_status_pengajuan($id_dompet){
		$this->db->select('*');
		$this->db->from('aktivitas_penarikan');
		$this->db->where('id_dompet',$id_dompet);
		$this->db->where('status_penarikan','0');
		
		$query = $this->db->get();
		return $query->result();
	}

	public function insert_data_pengajuan($object){
		$this->db->insert('aktivitas_penarikan',$object);
	}
}