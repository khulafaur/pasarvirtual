<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	public function login($username, $password){
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$data = array('status' => "On");
		$this->db->update('admin', $data);
		$this->db->select('admin_id');
		$login = $this->db->get('admin');
		return $login->row_array();
	}

	public function getAkun($id){
		$this->db->join('role', 'role.id_role = admin.id_role');
		$akun = $this->db->get_where('admin', 'admin_id = ' . $id);
		return $akun->row_array();
	}

	public function getRole(){
		return $this->db->get('role');
	}

	public function getAdmin(){
		return $this->db->get('admin');
	}

	public function getProdusen(){
		return $this->db->get('produsen');
	}

	public function getDistributor(){
		return $this->db->get('distributor');
	}

	public function getKonsumen(){
		return $this->db->get('konsumen');
	}

	public function cekPassword($id, $password){
		$this->db->where('admin_id', $id);
		$this->db->where('password', md5($password));
		$this->db->get('admin');
		return $this->db->affected_rows();
		// return $this->db->last_query();
	}

	public function addAdmin($data){
		$this->db->insert('admin', $data);
		return $this->db->insert_id();
	}

	public function addRole($id, $role){
		$data = array(
			'id_role' => $id,
			'nama' => $role
		);
		$this->db->insert('role', $data);
		return $this->db->affected_rows();
	}

	public function updateAdmin($id, $data){
		$this->db->set($data);
		$this->db->where('admin_id', $id);
		$this->db->update('admin');
		return $this->db->affected_rows();
	}

	public function delete($id){
		$this->db->where('admin_id', $id);
		$this->db->delete('admin');
	}

	public function last_query(){
		return $this->db->last_query();
	}

	public function logout($id){
		$this->db->where('admin_id', $id);
		$this->db->set('status', "Off");
		$this->db->update('admin');
	}

	public function getAllJumlahProdusenBulan(){
		// $this->db->select('count(no_ktp) as jumlah, WEEK(created_date) as pekan');
		$this->db->select('count(no_ktp) as jumlah, created_date as hari');
		$this->db->from("produsen");
		$this->db->group_by("hari");
		$this->db->where('year(created_date)', date('Y'));
		$this->db->where('month(created_date)', date('m'));
		return $this->db->get();
	}

	public function getFilterJumlahProdusenBulan($month, $year){
		$query = $this->db->query('SELECT count(no_ktp) as jumlah, created_date as hari FROM produsen WHERE month(created_date) ='.$month.' AND year(created_date) = '.$year.' GROUP BY hari');
		// $this->db->select("count(no_ktp) as jumlah, created_date as hari");
		// $this->db->from("produsen");
		// $this->db->group_by("hari");
		// $this->db->where('month(created_date)', $month);
		// $this->db->where('year(created_date)', $year);
		// return $this->db->get();
		return $query->result();
	}

}

/* End of file m_admin.php */
/* Location: ./application/models/m_admin.php */
