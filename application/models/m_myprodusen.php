<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_myprodusen extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function getAllData() {
		$sql="SELECT * from produsen";
		$hasil=$this->db->query($sql);
		return $hasil->result();
	}

	public function get_laporan_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$join3,$condition3,$where,$group_by){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2);
		$this->db->join($join3,$condition3);
		$this->db->where($where);
		$this->db->group_by($group_by);
		$this->db->order_by('qty','DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function ambil_data_produsen($username){
			$this->db->where("username",$username);
			return $this->db->get("produsen");
	}

	public function registration_insert($data) {
	// Query to check whether username already exist or not
	$condition = "username =" . "'" . $data['username'] . "'" . " or " ."no_ktp =" . "'" . $data['no_ktp'] . "'";
	$this->db->select('*');
	$this->db->from('produsen');
	$this->db->where($condition);
	$this->db->limit(1);
	$query = $this->db->get();
	if ($query->num_rows() == 0) {
		// Query to insert data in database
		$this->db->insert('produsen', $data);
		if ($this->db->affected_rows() > 0) {
		return true;
		}
		} else {
		return false;
		}
	}
	public function check_username($username) {
			$this->db->select('*');
			$this->db->from('produsen');
			$this->db->where('username', $username);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			}
				return false;
		}

	public function rehash($newhash, $username){
		$this->db->where("username",$username);
		return $this->db->update("produsen",$newhash);
	}

	function satuProdusen($id) {
		$this->db->select('*');
		$this->db->from('produsen');
		$this->db->where('ktp_produsen',$id);
		$hasil=$this->db->get();
		return $hasil->result();
	}

	function checkProdusen($no_ktp,$ktp_dist){
		    $this->db->select('*');
			$this->db->from('produsen_distributor');
			$this->db->where('id_produsen', $no_ktp);
			$this->db->where('id_distributor', $ktp_dist);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			}else{
				return false;
			}
	}
	function produkProdusen($id) {
		$this->db->select('p.id_produk,p.nama_produk,p.deskripsi_produk,p.gambar');
		$this->db->from('(select * from produk where id_produsen='.$id.') p');
		$this->db->join('produk_distributor pd','pd.id_produk = p.id_produk','left');
		$this->db->where('pd.id_produk is null');

		$query=$this->db->get();
		return $query->result();
	}
	
	function produsenAll ($id) {
		$this->db->select('produsen.ktp_produsen, produsen.nama_produsen,produsen.email,produsen.foto_produsen,produsen.username,produsen.password,produsen.alamat,produsen.tgl_lahir,produsen.jenis_kelamin,produsen.no_hp,produsen.telp,produsen.no_rekening,produsen.nama_rekening,produsen.nama_bank,produsen.nama_toko,produsen.logo_toko,produsen.alamat_toko,produsen.no_siup,produsen.foto_siup,produsen.deskripsi_toko,produsen.no_npwp,produsen.foto_npwp,produsen.t_daftar_perusahaan,produk.id_produk,produk.nama_produk,produk.deskripsi_produk,produk.gambar, produk.persen_distributor, produk.persen_produsen');
		$this->db->from('produsen');
		$this->db->join('produk','produsen.ktp_produsen = produk.id_produsen');
		$this->db->join('kategori_khusus kh','produk.id_kategori_khusus = kh.id_kategori_khusus');
		$this->db->join('kategori k','k.id_kategori = kh.id_kategori');
		$this->db->order_by('produk.created_date','desc');

  	
		if ($id != null) {
			$this->db->where('k.id_kategori',$id);
		}
		$query = $this->db->get();
  
		if ($query->num_rows() > 0) {
			
		}
		return $query->result();
	}

	function produsenAll2() {
		$this->db->select('*');
		$this->db->from('produsen');
		$this->db->order_by('nama_toko','asc');
		$query = $this->db->get();
		return $query->result();
	}

	function produsenKu () {
		$distributor_id=($this->session->userdata['logged_in']['distributor_id']);

		$this->db->select('p.id_produk,p.nama_produk,p.gambar_produk,p.created_date, pv.id_produk_variasi, pv.stok,pv.harga, pd.status_publish, pr.nama_produsen, pr.nama_toko, pr.logo_toko');
		$this->db->from('produk_distributor pd');
		$this->db->join('produk p','pd.id_produk = p.id_produk');
		$this->db->join('produsen pr', 'p.id_produsen = pr.ktp_produsen');
		$this->db->join('produk_variasi pv','pv.id_produk = p.id_produk');

		return $this->db->get()->result();
	}
	function myprodusen () {
		$distributor_id=($this->session->userdata['logged_in']['distributor_id']);
		$this->db->select('pd.id_prod_dist,pr.ktp_produsen,pr.nama_produsen, pr.username, pr.no_hp, pr.email, pr.jenis_kelamin, pr.tgl_lahir, pr.alamat_toko, pr.nama_bank, pr.nama_rekening, pr.no_rekening, pr.nama_toko, pr.telp');
		$this->db->from('produsen_distributor pd');
		$this->db->join('produsen pr', 'pd.id_produsen = pr.ktp_produsen');
		$this->db->where('pd.id_distributor',$distributor_id);

		return $this->db->get()->result();
	}


	function ProdusenById($id) {
		$this->db->from('produsen');
		$this->db->where('ktp_produsen', $id);

		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		}
	}

	public function hapus_produsen_byid($set_id){
		$this->db->where("id_prod_dist",$set_id);
		return $this->db->delete("produsen_distributor");
	}

	function insert_produsenku($object){
		$this->db->insert("produsen_distributor",$object);
	}

	function kategori_umum() {
		$this->db->select('*');
		$this->db->from('kategori_umum');
		return $this->db->get()->result();
	}

	function kategori() {
		$this->db->select('*');
		$this->db->from('kategori');
		return $this->db->get()->result();
	}

	function kategori_khusus() {
		$this->db->select('*');
		$this->db->from('kategori_khusus');
		return $this->db->get()->result();
	}

	function get_activity($id_distributor) {
		$this->db->select('ap.id,ap.id_aktivitas,ap.status_aktivitas,p.nama_produsen,ap.id_produsen,ap.waktu_aktivitas,p.nama_toko,p.logo_toko,TIMESTAMPDIFF(SECOND,ap.waktu_aktivitas,NOW()) as time');
		$this->db->from('aktivitas_produsen ap');
		$this->db->join('produsen_distributor pd','ap.id_produsen = pd.id_produsen');
		$this->db->join('produsen p','p.ktp_produsen = pd.id_produsen');
		$this->db->where('pd.id_distributor',$id_distributor);
		$this->db->where('pd.status_approve','1');
		$this->db->where('pd.created_date <= ap.waktu_aktivitas ');
		$this->db->order_by('ap.waktu_aktivitas','desc');
		return $this->db->get()->result();
	}

	function get_activity_produsen($id_prudusen) {
		$this->db->select('ap.id,ap.id_aktivitas,ap.status_aktivitas,p.nama_produsen,ap.id_produsen,ap.waktu_aktivitas,p.nama_toko,p.logo_toko,TIMESTAMPDIFF(SECOND,ap.waktu_aktivitas,NOW()) as time');
		$this->db->from('aktivitas_produsen ap');
		$this->db->join('produsen p','p.ktp_produsen = ap.id_produsen');
		$this->db->where('p.ktp_produsen',$id_prudusen);
		$this->db->order_by('ap.waktu_aktivitas','desc');
		return $this->db->get()->result();
	}

	function get_activity_inputbrg($id_activity) {
		$this->db->select('p.id_produk,p.nama_produk,p.deskripsi_produk,p.gambar,pv.nama_variasi,pv.harga,pv.stok,pv.gambar_produk');
		$this->db->from('aktivitas_produsen ap');
		$this->db->join('produk p','ap.id_aktivitas = p.id_produk');
		$this->db->join('produk_variasi pv','p.id_produk = pv.id_produk');
		$this->db->where('ap.id',$id_activity);
		return $this->db->get()->result();
	}

	function get_activity_updatebrg($id_activity) {
		$this->db->select('p.id_produk,p.nama_produk,p.deskripsi_produk,p.gambar,pv.nama_variasi,pv.harga,pv.stok,pv.gambar_produk as gambar_produk');
		$this->db->from('aktivitas_produsen ap');
		$this->db->join('produk_variasi pv','ap.id_aktivitas = pv.id_produk_variasi');
		$this->db->join('produk p','p.id_produk = pv.id_produk');
		$this->db->where('ap.id',$id_activity);
		return $this->db->get()->result();
	}

	function get_activity_variasi_baru($id_activity) {
		$this->db->select('p.id_produk,p.nama_produk,p.deskripsi_produk,p.gambar,pv.nama_variasi,pv.harga,pv.stok,pv.gambar_produk');
		$this->db->from('aktivitas_produsen ap');
		$this->db->join('produk p','ap.id_aktivitas = p.id_produk');
		$this->db->join('produk_variasi pv','p.id_produk = pv.id_produk');
		$this->db->where('ap.id',$id_activity);
		$this->db->where('pv.created_date = ap.waktu_aktivitas');
		return $this->db->get()->result();
	}
}