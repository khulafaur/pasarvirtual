<?php
class m_laporan extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	public function get_laporan_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$group_by){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2,'left');
		$this->db->group_by($group_by);
		$this->db->order_by('qty','DESC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_detail_penjualan($select,$from,$join,$condition,$join1,$condition1,$join2,$condition2,$where,$group_by){
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->join($join2,$condition2);
		$this->db->where($where);
		$this->db->group_by($group_by);
		$this->db->order_by('qty','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_produk($id_produk){
		$this->db->select("nama_produk");
		$this->db->from("produk");
		$this->db->where("id_produk",$id_produk);
		$query = $this->db->get();
		return $query;
	}

	public function get_penjualan_distributor($select,$from,$join,$condition,$join1,$condition1,$where,$group_by)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->join($join1,$condition1);
		$this->db->where($where);
		$this->db->group_by($group_by);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_penjualan_tahunan($select,$from,$join,$condition)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition,'left');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_data_produk($select,$from,$where)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_data_distributor($select,$from,$join,$condition,$where)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_penjualan_tahunan_dist($select,$from,$join,$condition)
	{
		$this->db->select($select);
		$this->db->from($from);
		$this->db->join($join,$condition,'left');
		$this->db->order_by("d.day",'ASC');
		$query = $this->db->get();
		return $query->result();
	}
}

