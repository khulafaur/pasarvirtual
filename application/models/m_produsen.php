<?php
	class m_produsen extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function profil_produsen($no_ktp) {
		$this->db->select('*');
		$this->db->from('produsen');
		$this->db->where('ktp_produsen', $no_ktp);
		return $this->db->get()->result();
	}

	function get_one($no_ktp){
      $param = array('ktp_produsen'=>$no_ktp);
      return $this->db->get_where('produsen',$param);
	}
	
	function update_profil_produsen($data, $no_ktp) {
		$this->db->where('ktp_produsen', $no_ktp);
       	$this->db->update('produsen', $data);
	}

	function getNamProd() {
        $this->db->select('*');
        $this->db->from('produk');
        return $this->db->get()->result();
	}

	function getDetailVariasi($postData) {
        $this->db->select('*');
		$this->db->from('produk_variasi');
        $this->db->where('id_produk_variasi',$postData);
        $q = $this->db->get();
        return $q->result();
	}

	public function ambil_data_produsen($username){
		$this->db->where("username",$username);
		return $this->db->get("produsen");
	}

	public function ambil_data_produsen_byId($id){
		$this->db->where("ktp_produsen",$id);
		return $this->db->get("produsen");
	}

	public function check_username($username) {
		$this->db->select('*');
		$this->db->from('produsen');
		$this->db->where('username', $username);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		}
		return false;
	}

	public function rehash($newhash, $username){
		$this->db->where("username",$username);
		return $this->db->update("produsen",$newhash);
	}

	public function registration_insert($data) {
		// Query to check whether username already exist or not
		$condition = "username =" . "'" . $data['username'] . "'" . " or " ."ktp_produsen =" . "'" . $data['ktp_produsen'] . "'";
		$this->db->select('*');
		$this->db->from('produsen');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {
			// Query to insert data in database
			$this->db->insert('produsen', $data);
			if ($this->db->affected_rows() > 0) {
				return true;
			}
		} else {
			return false;
		}
	}

	function getNamaProduk($postData) {
		$response = array();
        $this->db->select('nama_variasi,id_produk_variasi');
		$this->db->from('produk_variasi');
        $this->db->where('id_produk',$postData);
        $q = $this->db->get();
        return $q->result();
	}

	function input_stok_baru($data){
		$this->db->insert('stok_produk', $data);
	}

	function tampil_riwayat_stok($produsen_id){
		$this->db->select('sp.id_stok, p.id_produk, p.nama_produk, kh.nama_kategori_khusus, sp.stok_lama, sp.stok_baru, pv.harga, sp.total_stok, sp.tgl_stok, pv.nama_variasi');
		$this->db->from('stok_produk sp');
		$this->db->join('produk_variasi pv', 'sp.id_produk_variasi = pv.id_produk_variasi');
		$this->db->join('produk p', 'p.id_produk = pv.id_produk');
		$this->db->join('kategori_khusus kh', 'kh.id_kategori_khusus = p.id_kategori_khusus');
		$this->db->where('p.id_produsen',$produsen_id);
		$this->db->order_by('tgl_stok', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	function ambil_data_dompet($id){
		$this->db->select('*');
		$this->db->from('dompet');
		$this->db->where('id_pemilik', $id);
		$this->db->where('status_dompet', '1');
		$query = $this->db->get();
        return $query;
	}

		public function jml_dist($id_produsen){
		$this->db->select('count(id_distributor) as total_dist');
		$this->db->from('produsen_distributor');
		$this->db->where('id_produsen',$id_produsen);
		$query = $this->db->get();
		return $query->result();
	}

	public function jml_prod($id_produsen) {
		$this->db->select('count(id_produk) as total_prod');
		$this->db->from('produk');
		$this->db->where('id_produsen',$id_produsen);
		$query = $this->db->get();
		return $query->result();
	}

	public function jml_pendapatan($id_produsen) {
		$this->db->select('sum(dt.qty * dt.harga_transaksi) as total_pendapatan');
		$this->db->from('(select * from produk where id_produsen='.$id_produsen.') p');
		$this->db->join('produk_variasi pv','p.id_produk=pv.id_produk');
		$this->db->join('detail_transaksi dt','pv.id_produk_variasi=dt.id_produk_variasi');
		$query = $this->db->get();
		return $query->result();
	}

}
?>