<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function login_produsen($where){
		$hasil = $this->db->get_where('produsen', $where);
		return $hasil->row();
	}

	public function login_distributor($where){
		$hasil = $this->db->get_where('distributor', $where);
		return $hasil->row();
	}

}

/* End of file m_user.php */
/* Location: ./application/models/m_user.php */