<?php
	class m_penjualan extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function tampil_penjualan(){
		$this->db->select('t.id_transaksi, d.ktp_distributor, d.nama_distributor, sum(dt.qty*dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.tanggal');
		$this->db->from('distributor d');
		$this->db->join('detail_transaksi dt', 'd.ktp_distributor = dt.id_dist');
		$this->db->join('transaksi t', 'dt.id_transaksi = t.id_transaksi');
		$this->db->order_by('t.tanggal', 'asc');
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

	function get_detail_penjualan($primarykey) {
		$this->db->select('t.id_transaksi, p.id_produk, p.nama_produk,
			p.persen_produsen, pv.stok, dt.harga_transaksi, pv.nama_variasi, dt.qty, (dt.qty*dt.harga_transaksi) as total,t.status_transaksi, t.no_resi,
			t.jasa_kirim');
		$this->db->from('transaksi t');
		$this->db->join('detail_transaksi dt', 'dt.id_transaksi = t.id_transaksi');
		$this->db->join('produk_variasi pv', 'pv.id_produk_variasi = dt.id_produk_variasi');
		$this->db->join('produk p', 'p.id_produk = pv.id_produk');
		$this->db->where('t.id_transaksi', $primarykey);
		$query = $this->db->get();
	    return $query->result();
	}

	function total_penjualan($primarykey) {
		$this->db->select('sum(dt.qty*dt.harga_transaksi) as total_harga, sum(dt.qty) as total_qty');
		$this->db->from('transaksi t');
		$this->db->join('detail_transaksi dt', 'dt.id_transaksi = t.id_transaksi');
		$this->db->join('produk_variasi pv', 'pv.id_produk_variasi = dt.id_produk_variasi');
		$this->db->join('produk p', 'p.id_produk = pv.id_produk');
		$this->db->where('t.id_transaksi', $primarykey);
		$query = $this->db->get();
	    return $query->result();
	}

	function data_konsumen_penjual($primarykey) {
		$this->db->select('k.namaDepan,k.namaBelakang,k.nomorHp,k.email,k.alamat,k.kelurahan,k.provinsi,k.kota,k.kodePos,DATE_FORMAT(t.tanggal,"%d %M %Y") as tanggal,d.nama_distributor,t.id_transaksi,d.nama_toko,d.alamat_distributor as alamat_penjual,d.email as email_penjual,d.hp');
		$this->db->from('transaksi t');
		$this->db->join('konsumen k', 'k.id_konsumen = t.id_konsumen');
		$this->db->join('detail_transaksi dt', 'dt.id_transaksi = t.id_transaksi');
		$this->db->join('distributor d', 'dt.id_dist = d.ktp_distributor');
		$this->db->where('t.id_transaksi', $primarykey);
		$this->db->group_by('dt.id_dist');
		$query = $this->db->get();
	    return $query->result();
	}

	function tampil_belumbayar_penjualan() {
		$this->db->select('t.id_transaksi, d.ktp_distributor, d.nama_distributor, sum(dt.qty*dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.tanggal');
		$this->db->from('distributor d');
		$this->db->join('detail_transaksi dt', 'd.ktp_distributor = dt.id_dist');
		$this->db->join('transaksi t', 'dt.id_transaksi = t.id_transaksi');
		$this->db->where('t.status_transaksi = "0"');
		$this->db->order_by('t.tanggal', 'asc');
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

	function tampil_perludikirim_penjualan() {
		$this->db->select('t.id_transaksi, d.ktp_distributor, d.nama_distributor, sum(dt.qty*dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.tanggal');
		$this->db->from('distributor d');
		$this->db->join('detail_transaksi dt', 'd.ktp_distributor = dt.id_dist');
		$this->db->join('transaksi t', 'dt.id_transaksi = t.id_transaksi');
		$this->db->where('t.status_transaksi = "1"');
		$this->db->order_by('t.tanggal', 'asc');
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

	function tampil_sudahdikirim_penjualan() {
		$this->db->select('t.id_transaksi, d.ktp_distributor, d.nama_distributor, sum(dt.qty*dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.tanggal, t.no_resi');
		$this->db->from('distributor d');
		$this->db->join('detail_transaksi dt', 'd.ktp_distributor = dt.id_dist');
		$this->db->join('transaksi t', 'dt.id_transaksi = t.id_transaksi');
		$this->db->where('t.status_transaksi = "2"');
		$this->db->order_by('t.tanggal', 'asc');
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

	function tampil_sudahsampai_penjualan() {
		$this->db->select('t.id_transaksi, d.ktp_distributor, d.nama_distributor, sum(dt.qty*dt.harga_transaksi) as nominal, t.status_transaksi, t.jasa_kirim, t.tanggal');
		$this->db->from('distributor d');
		$this->db->join('detail_transaksi dt', 'd.ktp_distributor = dt.id_dist');
		$this->db->join('transaksi t', 'dt.id_transaksi = t.id_transaksi');
		$this->db->where('t.status_transaksi = "3"');
		$this->db->order_by('t.tanggal', 'asc');
		$this->db->group_by('t.id_transaksi');
		$query = $this->db->get();
	    return $query->result();
	}

	public function update_status($primarykey,$data) {
		$this->db->where('id_transaksi',$primarykey);
		$this->db->update('transaksi',$data);

	}

	function simpan_no_resi($no_resi,$primarykey) {
		$this->db->where('id_transaksi',$primarykey);
		$this->db->update('transaksi',$no_resi);
	}

	}
	
?>