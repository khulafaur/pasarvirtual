<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title text-center">Status Pengajuan</h5>
    </div>
<?php if($get_data){ ?>
    <table class="table datatable-save-state">
        <thead>
            <tr>
                <th>Tanggal</th>
                <th>Nominal</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($get_data as $row) { ?>
            <tr>
                <td><?php echo $row->tgl_penarikan ?></td>
                <td><?php echo $row->nominal ?></td>
                <td><span class="label label-danger">Belum di Appruve</span></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } else{?>
        <div class="panel-body">
            <div class="text-center" >
                 <button type="button" class="btn btn-success btn-float btn-float btn-rounded" data-toggle="dropdown"><i class=" icon-file-check2"></i></button>
                <h4 class="panel-title text-success"></i>Pengejuan telah di Appruve</h4>
            </div>
         </div>
<?php } ?>
</div>