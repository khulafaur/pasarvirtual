<div class="content">
<?php echo form_open('Produsen_pengajuan_dompet/insert_pengajuan','class="form-horizontal form-validate-jquery"')?>
    <div class="panel panel-flat">
        <div class="panel-body">
        	<fieldset class="content-group">
        		<?php foreach ($get_data as $row) { ?>
            		<legend class="text-bold">Form Pengajuan Pencairan <span class="text-success"><?php  echo $tanggal ?></span></legend>
                    <input type="hidden" name="id_dompet" value="<?php echo $row->id_dompet ?>" class="form-control">
        		<?php } ?>
                 <div class="form-group">
                    <label class="control-label col-lg-2">Keterangan <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea rows="5" name="keterangan" cols="5" name="textarea" class="form-control" required="required" placeholder="Default textarea"></textarea>
                    </div>
                </div>
        		<div class="form-group">
        			<label class="control-label col-lg-2">Input Nominal <span class="text-danger">*</span></label>
        			<div class="col-lg-9">
        				<div class="input-group">
        					<div class="input-group-addon"><i class="icon-cash"></i></div>
        					<input type="text" name="nominal" class="form-control" required="required" placeholder="Masukan Nominal">
        				</div>
        			</div>
        		</div>
        	</fieldset>
        </div>
     </div>
     <button name="button" value="ngisi" type="submit" class="btn btn-success btn-labeled btn-lg" style="float: right;"><b><i class=" icon-touch"></i></b>Kirim</button>
</div>
<?php echo form_close()?>




<script>
var validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }
            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            vali: "required",
            nominal: {
                range: [20000, <?php echo $saldo ?>]
            },
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });
</script>