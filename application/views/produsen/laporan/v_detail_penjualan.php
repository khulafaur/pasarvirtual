<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Detail Penjualan Produk dari Distributor </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>

		<div class="panel-body">
		</div>

		<table id="example" class="table datatable-basic table-bordered">
			<thead>
				<tr >
					<th>Nama Distributor</th>
					<th>Jumlah Terjual</th>
					<th>Total Pendapatan</th>
					<th>Detail</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				foreach($get_data as $row){ ?>
				<tr>
					<td><?php echo $row->nama_distributor; ?></td>
					<td><?php echo $row->qty; ?></td>
					<td><?php echo $row->pendapatan; ?></td>

					<td width="120 px" id="detail"><a class=<?php echo $row->ktp_distributor ?>><span class="label label-default">detail</span></a></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>

<?php 
	$this->load->view('produsen/footer');
?>


<script>
$(document).on("click","#detail a",function(e){
				var id_dist = $(this).attr("class");
				var id_produk = "<?php echo $id_produk?>";
				var tahun = "<?php echo $year?>";
				var bulan = "<?php echo $month?>";
				$.ajax({
					type:"POST",
					data:{id:id_dist,id_prdk:id_produk,month:bulan,year:tahun},
					url:"<?php echo site_url('Produsen_laporan_penjualan/detail_penjualan_distrubutor/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
				e.preventDefault();
				$("#myModal").modal('show');
			});
</script>