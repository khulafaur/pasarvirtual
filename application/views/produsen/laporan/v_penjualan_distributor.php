<div class="content">
					<!-- Default ordering -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h2 class="text-bold"> Detail Penjualan Distributor</h2>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr >
							<th>variasi produk</th>
							<th>Jumlah Terjual</th>
							<th>Total Pendapatan</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach($get_data as $row){ ?>
						<tr>
							<td><span class="label border-left-success label-striped"><?php echo $row->nama_variasi; ?></span></td>
							<td><span class="badge badge-success"><?php echo $row->qty; ?></span></td>
							<td><span class="badge badge-success"><?php echo $row->pendapatan; ?></span></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>