<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Laporan Penjualan Produk </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>

		<div class="panel-body">
			<div class="tabbable">
				<ul class="nav nav-tabs nav-tabs-highlight">
					<li class="active"><a href="#fade-tab1" data-toggle="tab">Active</a></li>
					<li class=""><a href="#fade-tab2" data-toggle="tab">Grafik Penjualan Produk</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade in active" id="fade-tab1">
						<?php echo form_open('Produsen_laporan_penjualan'); ?>
						<div class="form-group" style="float: right;">
							<div class="input-group">
								<div class="input-group-addon">
									<i class=" icon-calendar2"></i>
								</div>
								<select class="select-fixed" data-placeholder="Select Month..." name="bulan">
									<option></option>
									<option value="01">Januari</option>
									<option value="02">Februari</option>
									<option value="03">Maret</option>
									<option value="04">April</option>
									<option value="05">Mei</option>
									<option value="06">juni</option>
									<option value="07">Juli</option>
									<option value="08">Agustus</option>
									<option value="09">September</option>
									<option value="10">September</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
								<select class="select-fixed-short" data-placeholder="Year..."  required="required" name="tahun">
									<option>
										<?php if(isset($year)){ echo $year; } ?>
									</option>
								<?php for ($i=0; $i < count($getRangeYear) ; $i++) { ?>
									 <option value="<?php echo $getRangeYear[$i]?>"><?php echo $getRangeYear[$i]?></option>
								<?php } ?>
								</select>

								<div class="input-group-btn">
									<button name="cari" value="submit" type="submit" class="btn btn-success dropdown-toggle btn-icon">
										<i class="icon-search4"></i>
									</button>
								</div>
							</div>
						</div>
					<?php echo form_close(); ?>
					<table id="example" class="table datatable-basic table-bordered">
						<thead>
							<tr >
								<th>Nama Produk</th>
								<th>Kategori</th>
								<th>Jumlah Terjual</th>
								<th>Total Pendapatan</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							foreach($get_data as $row){ ?>
							<tr>
								<td><?php echo $row->nama_produk; ?></td>
								<td><?php echo $row->nama_kategori_khusus; ?></td>
								<td><?php echo $row->qty; ?></td>
								<td><?php echo $row->total; ?></td>

								<td width="120 px" id="detail" class="<?php echo $row->id_produk ?>"><a href="<?php echo site_url("/Produsen_laporan/".$row->id_produk."/".$month."/".$year);  ?>"><span class="label label-default">detail</span></a></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					</div>
					<div class="tab-pane fade" id="fade-tab2">
						<div class="content">
							<!-- Pies -->
							<div class="row">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h6 class="panel-title text-semibold">Basic pie chart</h6>
									</div>

									<div class="panel-body">
										<div class="chart-container text-center">
											<div  class="display-inline-block" id="c3-donut-chart"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php 
	$this->load->view('produsen/footer');
?>

<script>
    // Generate chart
   var donut_chart = c3.generate({
        bindto: '#c3-donut-chart',
        size: { width: 350 },
        color: {
            pattern: ['#3F51B5', '#FF9800', '#4CAF50', '#00BCD4', '#F44336']
        },
        data: {
            columns: [
            <?php foreach($get_data as $row){ ?>
                ['<?php echo $row->nama_produk; ?>', <?php echo $row->qty; ?>],
            <?php } ?>
            ],
            type : 'donut'
        },
        donut: {
            title: "<?php echo $date ?>"
        }
    });
</script>

