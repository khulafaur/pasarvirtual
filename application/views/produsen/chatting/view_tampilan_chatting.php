<?php
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
?>


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">


			<!-- Secondary sidebar -->
			<div class="sidebar sidebar-secondary sidebar-default">
				<div class="sidebar-content">
					<!-- Online users -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>My users</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<?php foreach ($get_data as $row) { ?>
						<div class="category-content no-padding ">
							<ul class="media-list media-list-linked">
								<li class="media" id="select">
									<a id="<?php echo $row->id_distributor ?>" class="media-link">
										<div class="media-left"><img src="<?php echo base_url();?>assets/images/<?php echo $row->foto_distributor ?>" class="img-circle" alt=""></div>
										<div class="media-body">
											<span class="media-heading text-semibold"><?php echo $row->nama_distributor ?></span>
											<span class="text-size-small text-muted display-block">Toko <?php echo $row->nama_toko ?></span>
										</div>
										<div class="media-right media-middle">
											<span class="status-mark bg-success"></span>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<?php } ?>
					</div>
					<!-- /online users -->

				</div>
			</div>
			<!-- /secondary sidebar -->


	<?php $this->load->view('produsen/page_header'); ?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Content area -->
				<div class="content">
					<div id="chatform"></div>
					<!-- Basic layout -->
					


<?php $this->load->view("produsen/footer"); ?>

<script type="text/javascript">
	$(document).on("click","#select a",function(e){
	var id_dist = $(this).attr("id");
	console.log(id_dist);
		$.ajax({
			type:"POST",
			data:{id_distributor:id_dist},
			url:"<?php echo site_url('Produsen_chat/getPanelChat/') ?>",
			success:function(msg){
				$("#chatform").html(msg);
			},
			error: function(result){
				$("#chatform").html("<b>error!</b>");
			}
		});
		e.preventDefault();
	});
</script>

