<div class="panel panel-flat">
	<div class="panel-body">
		<?php foreach ($get_data_dist as $row){ ?>
		<div class="thumbnail">
				<img src="<?php echo base_url(); ?>assets/images/<?php echo $row->foto_distributor; ?>" alt="" style="width:100%; height: 250px" >
		</div>
		<div class="caption">
		<?php foreach ($get_data_transfer as $row1){ ?>
			<ul class="media-list media-list-linked media-list-bordered">
				<li class="media">
					<a href="#" class="media-link">
						<div class="media-left">
							<img src="<?php echo base_url();?>assets/images/bar_code_price-512.png" class="img-circle img-lg" alt="">
						</div>

						<div class="media-body">
							<h6 class="media-heading">No Jurnal</h6>
							<text style="width:120px; word-wrap:break-word; display:inline-block;"><?php echo $row1->id_aktivitas_transfer; ?></text>
						</div>

						<div class="media-left">
							<img src="<?php echo base_url();?> assets/images/date.png" class="img-circle img-lg" alt="">
						</div>

						<div class="media-body" >
							<h6 class="media-heading">Tanggal</h6>
							 <text style="width:100px; word-wrap:break-word; display:inline-block;"> <?php echo $row1->tgl_transfer; ?></text>
						</div>
					</a>
				</li>
				<li class="media">
					<a href="#" class="media-link">
						<div class="media-left">
							<img src="<?php echo base_url();?> assets/images/person-flat.png" class="img-circle img-lg" alt="">
						</div>

						<div class="media-body">
							<h6 class="media-heading">Tujuan</h6>
							<text style="width:120px; word-wrap:break-word; display:inline-block;"><?php echo $row->nama_distributor; ?></text>
						</div>

						<div class="media-left">
							<img src="<?php echo base_url();?>assets/images/Purse-512.png" class="img-circle img-lg" alt="">
						</div>

						<div class="media-body" >
							<h6 class="media-heading">Nominal</h6>
							 <text style="width:100px; word-wrap:break-word; display:inline-block;"> <?php echo number_format($row1->nominal,0,",","."); ?></text>
						</div>
					</a>
				</li>
			</ul>
		<?php } ?>	
		</div>
		<?php } ?>
	</div>
</div>

