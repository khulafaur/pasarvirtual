<div class="panel panel-flat">
    <div class="panel-body">
        <div class="panel-heading text-center">
            <h5 class="panel-title"> Detail Penarikan </h5>
            <div class="heading-elements">
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-lg">
                <tbody>
                <?php foreach ($get_data_penarikan as $row) {  ?>
                    <tr>
                        <td>
                        	<h6 class="no-margin">Id Jurnal</h6>
                    	</td>
                        <td>
                            <h6 class="no-margin"><?php echo $row->id_aktivitas_tarik?></h6>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <h6 class="no-margin">Tanggal</h6>
                        </td>
                        <td>
                            <h6 class="no-margin"><?php echo $row->tgl_penarikan ?></h6>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <h6 class="no-margin">Keterangan</h6>
                        </td>
                        <td>
                            <h6 class="no-margin"><?php echo $row->ket_penarikan ?></h6>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <h6 class="no-margin">Nominal</h6>
                        </td>
                        <td>
                            <h6 class="no-margin"><?php echo number_format($row->nominal,0,",",".") ?></h6>
                        </td>
                    </tr>
                
                <?php } ?>
                </tbody>
               
            </table>
        </div>
    </div>
</div>