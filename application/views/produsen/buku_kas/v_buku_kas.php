<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>	
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> Buku Kas </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>

		<div class="panel-body">
			
			<div class="tabbable">
				<?php echo form_open('Produsen_buku_kas/list_arus_kas'); ?>
					<div class="form-group" style="float: right;">
						<div class="input-group">
							<div class="input-group-addon">
								<i class=" icon-calendar2"></i>
							</div>
					     	<select class="select-fixed-short" data-placeholder="Year..."  required="required" name="tahun">
								<option>
									<?php if(isset($year)){ echo $year; } ?>
								</option>
								<?php for ($i=0; $i < count($getRangeYear) ; $i++) { ?>
									<option value="<?php echo $getRangeYear[$i]?>"><?php echo $getRangeYear[$i]?></option>
								<?php } ?>
							</select>
							<div class="input-group-btn">
								<button name="cari" value="submit" type="submit" class="btn btn-success dropdown-toggle btn-icon">
									<i class="icon-search4"></i>
								</button>
							</div>
						</div>
					</div>
				<?php echo form_close() ?>
				<ul class="nav nav-tabs nav-tabs-highlight">
					<li class="<?php echo $active_tab ?>"><a href="#fade-tab1" data-toggle="tab">Buku Kas</a></li>
					<li class="<?php echo $active_tab1 ?>"><a href="#fade-tab2" data-toggle="tab">Grafik Buku Kas</a></li>
				</ul>

				<div class="tab-content">
					<div class="<?php echo $tabs ?>" id="fade-tab1">	
						<table class="table table-lg invoice-archive">
							<thead>
								<tr>
									<th>No Jurnal</th>
									<th>Period</th>
					                <th class="text-center">Ket</th>
					                <th>Keterangan</th>
					                <th>Nominal</th>
					                <th>Tanggal</th>
					                <th>Saldo</th>
					                <th>Detail</th>
					            </tr>
							</thead>
							<tbody>
							<?php 
							foreach($get_data_pemasukan as $row){ ?>
								<tr>
									<td><?php echo $row->id_aktivitas_pemasukan; ?></td>
									<td><?php echo $row->periode; ?></td>
					                <td class="text-center">
					                	<span class="label label-success">pemasukan</span>
				                	</td>
				                	<td><?php echo $row->ket_pemasukan ?></td>
					                <td><h6 class="no-margin text-bold">Rp.<?php echo number_format($row->nominal,0,",","."); ?></h6></td>
					                <td><span class="label label-success"><?php echo $row->tgl_pemasukan; ?></span></td>
					                <td>
					                	<h6 class="no-margin text-bold left-potition">Rp.<?php echo number_format($row->sisa_saldo,0,",","."); ?></h6>
					                </td>
									<td id="invoice" class="<?php echo $row->id_transaksi ?>">
										<button type="button" class="btn border-warning text-warning-600 btn-flat btn-icon btn-rounded"><i class="icon-file-eye"></i></button>
									</td>
					            </tr>
					            <?php } 

							foreach($get_data_transfer as $row){ ?>
								<tr>
									<td><?php echo $row->id_aktivitas_transfer; ?></td>
									<td><?php echo $row->periode; ?></td>
					                <td class="text-center">
					                	<span class="label label-warning">Bonus Distributor</span>
				                	</td>
				                	<td><?php echo $row->ket_transfer ?></td>
					                <td><h6 class="no-margin text-bold">Rp.<?php echo number_format($row->nominal,0,",","."); ?></h6></td>
					                <td><span class="label label-success"><?php echo $row->tgl_transfer; ?></span></td>
					                <td>
					                	<h6 class="no-margin text-bold left-potition">Rp.<?php echo number_format($row->sisa_saldo,0,",","."); ?></h6>
					                </td>
									<td id="transfer" class="<?php echo $row->tujuan_transfer ?>">
										<button type="button" id="<?php echo $row->id_aktivitas_transfer ?>" class="btn border-warning text-warning-600 btn-flat btn-icon btn-rounded"><i class="icon-file-eye"></i></button>
									</td>
					            </tr>
					            <?php } 

					            foreach($get_data_penarikan as $row){ ?>
								<tr>
									<td><?php echo $row->id_aktivitas_tarik; ?></td>
									<td><?php echo $row->periode; ?></td>
					                <td class="text-center">
					                	<span class="label label-danger">pengeluaran</span>
				                	</td>
				                	<td><?php echo $row->ket_penarikan ?></td>
					                <td><h6 class="no-margin text-bold">Rp.<?php echo number_format($row->nominal,0,",","."); ?></h6></td>
					                <td><span class="label label-success"><?php echo $row->tgl_penarikan; ?></span></td>
					                <td>
					                	<h6 class="no-margin text-bold left-potition">Rp.<?php echo number_format($row->sisa_saldo,0,",","."); ?></h6>
					                </td>
									<td id="tarik" class="<?php echo $row->id_aktivitas_tarik ?>">
										<button type="button" class="btn border-warning text-warning-600 btn-flat btn-icon btn-rounded"><i class="icon-file-eye"></i></button>
									</td>
					            </tr>
					            <?php } ?>
					        </tbody>
					    </table>
				    </div> 
				     <div class="<?php echo $tabs1 ?>" id="fade-tab2">
					     <div class="panel-heading">
							<h5 class="panel-title">Tahun <?php echo $year ?></h5>
						 </div>
				    	<div class="chart" id="c3-chart-buku_kas"></div>
				    </div>
				</div>
			</div>
		</div>
<?php 
	$this->load->view('produsen/footer');
?>

<script type="text/javascript">
	var axis_tick_rotation = c3.generate({

        bindto: '#c3-chart-buku_kas',
        size: { height: 400 },
        data: {
            x : 'x',
            columns: [
            		['x', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
         
            		["pemasukan", <?php foreach($get_data_masuk as $row){ echo $row->nominal ?>,<?php } ?>],
            		["penarikan", <?php foreach($get_data_tarik as $row){ echo $row->nominal ?>,<?php } ?>],
            		["transfer", <?php foreach($get_data_transf as $row){ echo $row->nominal ?>,<?php } ?>],
            		["LabaBersih", <?php foreach($get_data_keuntungan as $row){ echo $row->pendapatan ?>,<?php } ?>],
            	
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#4CAF50', '#F4511E', '#1E88E5', '#00BCD4']
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: -90
                },
                height: 80
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });
	
	$(document).on("click","#invoice button",function(e){
	var id_transaksi = $(this).parent().attr("class");
	$.ajax({
		type:"POST",
		data:{id_trx:id_transaksi},
		url:"<?php echo site_url('Produsen_buku_kas/detail_pemasukan/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#myModal").modal('show');
	});

	$(document).on("click","#transfer button",function(e){
	var id_tujuan = $(this).parent().attr("class");
	var id_transfer = $(this).attr("id");
	$.ajax({
		type:"POST",
		data:{id_dist:id_tujuan,id_trans:id_transfer},
		url:"<?php echo site_url('Produsen_buku_kas/detail_transfer/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#myModal").modal('show');
	});

	$(document).on("click","#tarik button",function(e){
	var id_tarik = $(this).parent().attr("class");
	$.ajax({
		type:"POST",
		data:{id_penarikan:id_tarik},
		url:"<?php echo site_url('Produsen_buku_kas/detail_penarikan/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#myModal").modal('show');
	});

</script>