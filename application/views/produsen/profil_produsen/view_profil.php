<?php 
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>

				<?php
					foreach($detail as $row) 
				{?>
				
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
								<td class="text-right col-md-2">
									<a href="<?php echo base_url().'Produsen/edit_profil_produsen/'?>">
										<button type="button" class="btn btn-success"><i class=" icon-pencil5 position-right"></i> Edit Profil</button></a>
											<a class="control-arrow" data-toggle="collapse" data-target="#demo1"></a>
								</td>
							</div>
							
							<legend class="text-semibold">
										<h5>Profil Produsen</h5>
								</legend>
							
							<legend class="text-semibold">
									<i class="icon-file-text2 position-left"></i>
										Informasi Produsen
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
								</legend>
								
								
									
								
								<table class="table table-striped">
								   <div class="container">
									<thead align="center">
									<tbody width="20">
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Foto Profil</th>
										<td>
						                    <a href="<?php echo base_url();?>/assets/images/fotoprodusen/<?php echo $row->foto_produsen;?>" data-popup="lightbox">
							                    <img src="<?php echo base_url();?>/assets/images/fotoprodusen/<?php echo $row->foto_produsen;?>" alt="" class="img-rounded img-preview img-thumbnail">
						                    </a>
			                   			</td>
			                   		</tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor KTP</th>
										 <td> <?php echo $row->ktp_produsen; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nama Lengkap</th>
										 <td> <?php echo $row->nama_produsen; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Tanggal Lahir</th>
										 <td> <?php echo $row->tgl_lahir; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Jenis Kelamin</th>
										 <td> <?php echo $row->jenis_kelamin; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Alamat</th>
										 <td> <?php echo $row->alamat; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor HP</th>
										 <td> <?php echo $row->no_hp; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor Telfon</th>
										 <td> <?php echo $row->telp; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Email</th>
										 <td> <?php echo $row->email; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Username</th>
										 <td> <?php echo $row->username; ?></td>
									  </tr>
										
									</tbody>
									</thead>
								</div>
								</table>
								
								<legend class="text-semibold">
									<i class=" icon-store position-left"></i>
										Informasi Toko Produsen
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
									</legend>
									 
									<table class="table table-striped">
								   <div class="container">
									<thead align="center">
									<tbody width="20">
									<tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Logo Toko</th>
										<td>
						                    <a href="<?php echo base_url();?>/assets/images/fotoprodusen/<?php echo $row->logo_toko;?>" data-popup="lightbox">
							                    <img src="<?php echo base_url();?>/assets/images/fotoprodusen/<?php echo $row->logo_toko;?>" alt="" class="img-rounded img-preview img-thumbnail">
						                    </a>
			                   			</td>
			                   		</tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nama Toko</th>
										 <td> <?php echo $row->nama_toko; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Alamat</th>
										 <td> <?php echo $row->alamat_toko; ?></td>
									  </tr>
									  <!-- <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor SIUP</th>
										 <td> <?php echo $row->no_siup; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">SIUP</th>
										 <td> <?php echo $row->siup; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor NPWP</th>
										 <td> <?php echo $row->no_npwp; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">NPWP</th>
										 <td> <?php echo $row->npwp; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Tanda Daftar Perusahaan</th>
										 <td> <?php echo $row->tanda_daftar_perusahaan; ?></td>
									  </tr> -->
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Deskripsi Toko</th>
										 <td> <?php echo $row->deskripsi_toko; ?></td>
									  </tr>
									</tbody>
									</thead>
								</div>
								</table>
								
								<legend class="text-semibold">
									<i class=" icon-credit-card2 position-left"></i>
										Informasi Bank Produsen
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
									</legend>
								
								<table class="table table-striped">
								   <div class="container">
									<thead align="center">
									<tbody width="20">
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nama Bank</th>
										 <td> <?php echo $row->nama_bank; ?></td>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Nomor Rekening</th>
										 <td> <?php echo $row->no_rekening; ?></td>
									  </tr>
									  <tr height="18px">
										 <th width="200px" height="18" padding-top="20px">Atas Nama</th>
										 <td> <?php echo $row->nama_rekening; ?></td>
									  </tr>
									  <?php
											}
										?>
									</tbody>
									</thead>
								</div>
								</table>

					</div>
				</div>
	<?php $this->load->view("produsen/footer"); ?>