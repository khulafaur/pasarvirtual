<?php
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>

		<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h4 class="panel-title">Profilku</h4>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<?php echo form_open_multipart('Produsen/edit_profil_produsen/','class="form-horizontal form-validate-jquery"');?>
					
								<fieldset class="content-group">
								
								<legend class="text-semibold">
									<i class="icon-file-text2 position-left"></i>
										Masukan Data Diri Anda
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
								</legend>
								
									<div class="form-group">
										<label class="control-label col-lg-2">Foto Profil</label>
											<div class="col-lg-10">
												<input type="file" class="file-styled-primary" name="foto_produsen">
												<input type="hidden" name="foto_produsen_val" class="form-control" value="<?php echo $profil['foto_produsen'];?>">
											</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Nomor KTP</label>
										<div class="col-lg-10">
											<input type="text" name="no_ktp" class="form-control" value="<?php echo $profil['ktp_produsen'];?>" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Nama Lengkap</label>
										<div class="col-lg-10">
											<input type="text" name="nama_lengkap" class="form-control" value="<?php echo $profil['nama_produsen'];?>" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Tanggal Lahir</label>
										<div class="col-lg-10">
											<input type="date" name="tgl_lahir" class="form-control" value="<?php echo $profil['tgl_lahir'];?>" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Alamat</label>
										<div class="col-lg-10">
											<textarea name="alamat" class="form-control" ><?php echo $profil['alamat'];?></textarea>
										</div>
									</div>
					                			
									<div class="form-group">
										<label class="control-label col-lg-2">Jenis Kelamin </label>
										<div class="col-lg-10">
											<select name="jenis_kelamin" class="form-control" required="required" aria-required="true" aria-invalid="false">
												<option value="<?php echo $profil['jenis_kelamin'] ?>"><?php echo $profil['jenis_kelamin'] ?></option> 
												<option value="laki-laki">Laki-laki</option>
												<option value="perempuan">Perempuan</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nomor Handphone</label>
										<div class="col-lg-10">
											<input type="text" name="no_hp" class="form-control" value="<?php echo $profil['no_hp'];?>" required="required">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nomor Telpon</label>
										<div class="col-lg-10">
											<input type="text" name="telp" class="form-control" value="<?php echo $profil['telp'];?>" required="required">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Email</label>
										<div class="col-lg-10">
											<input type="text" name="email" class="form-control" value="<?php echo $profil['email'];?>" required="required">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Username</label>
										<div class="col-lg-10">
											<input type="text" name="username" class="form-control" value="<?php echo $profil['username'];?>" required="required">
										</div>
									</div>
									
									<legend class="text-semibold">
									<i class=" icon-store position-left"></i>
										Masukan Data Toko Anda
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
									</legend>
									
									<div class="form-group">
											<label class="control-label col-lg-2">Logo Usaha/Toko</label>
											<div class="media-left">
													<a href="#"><img src="assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
												</div>
											
											<div class="col-lg-10">
												<input type="file" name="logo_toko" class="file-styled-primary">
												<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
												<input type="hidden" name="logo_toko_val" class="form-control" value="<?php echo $profil['logo_toko'];?>">
											</div>
										</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nama Usaha/Toko</label>
										<div class="col-lg-10">
											<input type="text" name="nama_toko" class="form-control" value="<?php echo $profil['nama_toko'];?>" required="required">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Alamat Toko</label>
										<div class="col-lg-10">
											<textarea required="required" name="alamat_toko" class="form-control" ><?php echo $profil['alamat_toko'];?></textarea>
										</div>
									</div>
									
									<!-- <div class="form-group">
										<label class="control-label col-lg-2">Nomor SIUP</label>
										<div class="col-lg-10">
											<input type="text" name="no_siup" class="form-control" value="<?php echo $profil['no_siup'];?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Upload SIUP</label>
									<div class="col-lg-10">
											<div class="uploader bg-warning"><input type="file" name="siup" class="file-styled-primary"><span class="filename" style="user-select: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-plus2"></i></span></div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nomor NPWP</label>
										<div class="col-lg-10">
											<input type="text" name="no_npwp" class="form-control" value="<?php echo $profil['no_npwp'];?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Upload NPWP</label>
									<div class="col-lg-10">
											<div class="uploader bg-warning"><input type="file" name="npwp" class="file-styled-primary"><span class="filename" style="user-select: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-plus2"></i></span></div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Tanda Daftar Perusahaan</label>
									<div class="col-lg-10">
											<div class="uploader bg-warning"><input type="file" name="tanda_daftar_perusahaan" class="file-styled-primary"><span class="filename" style="user-select: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-plus2"></i></span></div>
										</div>
									</div>
									 -->
									<div class="form-group">
										<label class="control-label col-lg-2">Deskripsi Toko</label>
										<div class="col-lg-10">
											<textarea required="required" name="deskripsi_toko" class="form-control" ><?php echo $profil['deskripsi_toko'];?></textarea>
										</div>
									</div>
									
									<legend class="text-semibold">
									<i class=" icon-credit-card2 position-left"></i>
										Masukan Data Akun Bank Anda
										<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
											<i class="icon-circle-down2"></i>
										</a>
									</legend>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nama Bank</label>
										<div class="col-lg-10">
											<input required="required" type="text" name="nama_bank" class="form-control" value="<?php echo $profil['nama_bank'];?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Nomor Rekening</label>
										<div class="col-lg-10">
											<input required="required" type="text" name="no_rekening" class="form-control" value="<?php echo $profil['no_rekening'];?>">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Atas Nama</label>
										<div class="col-lg-10">
											<input required="required" type="text" name="nama_rekening" class="form-control" value="<?php echo $profil['nama_rekening'];?>">
										</div>
									</div>
									
									<div class="text-right">
										<button type="submit" class="btn btn-primary" value="submit" name="submit" id="submit">Submit</button>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					

<?php 
	$this->load->view('produsen/footer');
	
?>