<div class="content">
<?php echo form_open("Produsen_kelola_bagihasil/update_bagihasil",'class="form-horizontal"'); ?>		
	<div class="panel panel-flat">
		<div class="panel-body">
			<fieldset class="content-group">
				<?php foreach($get_data as $row) {?>
				<h4>Produk <?php echo $row->nama_produk ?></h4>
				<legend class="text-bold">Update Presentasi Keuntungan</legend>
				<div class="form-group">
					<label class="control-label col-lg-2">Produsen</label>
					<div class="col-lg-10">
						<div class="input-group">
							<div class="input-group-addon">
								<i class=" icon-calendar2"></i>
							</div>
							<select id="mySelect" class="select-fixed" name="persen_prdsen" onchange="myFunction()">
								<option value="<?php echo $row->persen_produsen ?>"><?php echo str_replace(".", "", ltrim($row->persen_produsen, '0'));?>%</option>
								<option value="50">50%</option>
								<option value="55">55%</option>
								<option value="60">60%</option>
								<option value="65">65%</option>
								<option value="70">70%</option>
								<option value="75">75%</option>
								<option value="80">80%</option>
								<option value="85">85%</option>
								<option value="90">90%</option>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Distributor</label>
					<div class="col-lg-8">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
							<input name="persen_dist" id="replace"  readonly="readonly" value="<?php echo str_replace(".", "", ltrim($row->persen_distributor, '0'));?>%" type="text" class="form-control">
							<input name="id_produk" id="replace" value="<?php echo $row->id_produk ?>" type="hidden" class="form-control">
						</div>
					</div>
				</div>
				<?php } ?>
			</fieldset>
		</div>
	</div>
					<button name="submit" type="submit" class="btn btn-success btn-labeled btn-lg" style="float: right;"><b><i class=" icon-touch"></i></b>Submit</button>
			<?php echo form_close(); ?>
</div>
<script>
function myFunction() {
    var x = document.getElementById("mySelect").value;
    console.log(x);
    var hasil = 100-x; 
    document.getElementById("replace").value = hasil + "%";
}

 	$('.select-fixed').select2({
        minimumResultsForSearch: "-1",
        width: 250
    });
</script>