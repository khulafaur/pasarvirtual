<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>

<div class="content">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Kelola BagiHasil</h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>Nama Produk</th>
                    <th>Kategori</th>
                    <th>Persentasi Keuntungan</th>
                    <th>Gambar Produk</th>
                    <th class="text-center text-muted" style="width: 30px;"><i class="icon-pencil6"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($get_data as $row) { ?>

                <tr>
                    <td>
                        <div class="text-semibold"><a><?php echo $row->nama_produk ?></a></div>
                        <div class="text-muted" style="width:150px; word-wrap:break-word; display:inline-block;"><?php echo $row->deskripsi_produk ?></div>
                    </td>
                    <td><?php echo $row->nama_kategori_khusus ?></td>
                    <td>
                        <div>Produsen <a><?php echo str_replace(".", "", ltrim($row->persen_produsen, '0')); ?>%</a></div>
                        <div>Distributor <a><?php echo str_replace(".", "", ltrim($row->persen_distributor, '0')); ?>%</a></div>
                    </td>
                    <td>
                        <img src="<?php echo base_url();?>uploads/<?php echo $row->gambar ?>" class="img-circle img-lg" alt=""></a>
                    </td>
                     <td id="detail" class=<?php echo $row->id_produk ?>>
                        <button type="button" class="btn btn-warning btn-xs"><i class="icon-pencil6 position-left"></i> Update</button>
                    </td>
                </tr>

                <?php } ?>
            </tbody>
        </table>
    </div>
<!-- /task manager table -->

<?php 
    $this->load->view('produsen/footer');
?>

<script>
$(document).on("click","#detail button",function(e){
                var id_produk = $(this).parent().attr("class");
                $.ajax({
                    type:"POST",
                    data:{id:id_produk},
                    url:"<?php echo site_url('Produsen_kelola_bagihasil/edit_kelola_bagihasil/') ?>",
                    success:function(msg){
                        $(".modal-body").html(msg);
                    },
                    error: function(result){
                        $(".modal-body").html("Error");
                    }
                });
                e.preventDefault();
                $("#myModal").modal('show');
            });
</script>