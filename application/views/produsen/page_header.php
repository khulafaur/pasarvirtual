<!-- Page header -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				                <h4 class="modal-title" id="myModalLabel"></h4>
				            </div>
				            <div class="modal-body">
				            </div>
				        </div>
				    </div>
				</div>
			<div class="content-wrapper">
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo $data1 ?></span>  <?php echo $date ?> </h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
							
								 <button type="button" class="btn btn-danger btn-float btn-float btn-rounded" data-toggle="dropdown"><i class="icon-wallet"></i></button>

								 <ul class="dropdown-menu dropdown-menu-right">
									<li id="pengajuan"><a><i class="icon-coins"></i> Ajukan Pencairan </a></li>
									<li id="status"><a><i class="icon-file-check"></i> Status Pengajuan</a></li>
								</ul>
							
							<a href="#" class="btn btn-link btn-float has-text"><h6 class="text-semibold">Rp.<?php echo number_format($saldo,0,",",".") ?></h6></a>
								
							</div>


						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo site_url($data2) ?>"><i class="icon-home2 position-left"></i> Home</a></li>
							<?php if (isset($data5)) {
										echo "<li><a href=".site_url($data4).">$data3</a></li>";
										echo "<li class= $active>$data5</li>";
									}else{
										echo "<li class= $active>$data3</li>";
									}
							?>
							
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
			<script>
			$(document).on("click","#pengajuan a",function(e){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url('Produsen_pengajuan_dompet/form_pengajuan/') ?>",
				success:function(msg){
					$(".modal-body").html(msg);
				},
				error: function(result){
					$(".modal-body").html("Error");
				}
			});
				e.preventDefault();
				$("#myModal").modal('show');
			});

			$(document).on("click","#status a",function(e){
			$.ajax({
				type:"POST",
				url:"<?php echo site_url('Produsen_pengajuan_dompet/form_status_pengajuan/') ?>",
				success:function(msg){
					$(".modal-body").html(msg);
				},
				error: function(result){
					$(".modal-body").html("Error");
				}
			});
				e.preventDefault();
				$("#myModal").modal('show');
			});
		</script>
