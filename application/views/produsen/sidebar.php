
					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">
			
								<li class="navigation-header"><span>MENU</span> <i class="icon-menu" title="Forms"></i></li>
								<li class=<?php echo $dashboard ?>><a href="<?php echo site_url(); ?>Produsen_dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li><a href="#"><i class="icon-cabinet"></i> <span>Produk</span></a>
								    <ul>
										<li class=<?php echo $kelola_produk ?>><a href="<?php echo site_url(); ?>Produsen_kelola_produk">Kelola Produk</a></li>
										<li class=<?php echo $stok ?>><a href="<?php echo base_url();?>Produsen/view_riwayat_tambah_stok">Stok</a></li>
									</ul>
	                           </li>
	                           <li class=<?php echo $pesanan ?>><a href="<?php echo base_url();?>Produsen_penjualan/tampil_semua_penjualan"><i class=" icon-cart2"></i> <span>Pesanan</span></a></li>
	                           <li class=<?php echo $chat ?>><a href="<?php echo base_url();?>Produsen_chat/index"><i class="icon-comments"></i> <span>Chatting</span></a></li>
	                           <li class=<?php echo $tracking ?>><a href="<?php echo base_url();?>tracking/index_tracking"><i class=" icon-location4"></i> <span>Tracking</span></a></li>
								<li><a href="#"><i class=" icon-users4"></i> <span>Distributor</span></a>
									<ul>
										<li class="<?php echo $verifikasi ?>"><a href="<?php echo site_url(); ?>Produsen_monitoring_distributor/verifikasi_distributor">Verifikasi Distributor</a></li>
										<li class="<?php echo $distributor ?>"><a href="<?php echo site_url(); ?>Produsen_listing_distributor">Monitoring Distributor</a></li>
									</ul>
								</li>
								<li class=<?php echo $aruskas ?>><a href="<?php echo site_url(); ?>Produsen_arus_kas"><i class="icon-calculator2"></i> <span>Buku Kas</span></a></li>
								<li class=<?php echo $bagi_hasil ?>><a href="<?php echo site_url(); ?>Produsen_kelola_bagihasil"><i class="icon-coins"></i> <span>Kelola BagiHasil</span></a></li>
								<li class=<?php echo $penjualan ?>><a href="<?php echo site_url(); ?>Produsen_laporan_penjualan"><i class="icon-bookmark4"></i> <span>Laporan Penjualan</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->
				</div>
			</div>
			<!-- /main sidebar -->
					<!-- /user menu -->