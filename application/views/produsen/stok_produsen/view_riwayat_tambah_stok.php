<?php
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>
			
	<div class="content">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title"> Riwayat Tambah Stok</h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
		        		<li><a data-action="reload"></a></li>
		        		<li><a data-action="close"></a></li>
		        	</ul>
		    	</div>
			</div>
			<div class="panel-body">
			<button style="float: right;" type="button" class="btn btn-success" data-toggle="modal" id="tambah"><i class="icon-comment-discussion position-right"></i> Tambah Stok</button>
				<table class="table datatable-basic table-bordered">
					<thead>
						<tr>
							<th style="width:20px;">No</th>
							<th>Tanggal</th>
							<th>Produk</th>
							<th>Kategori</th>
							<th>Harga</th> 
							<th>In Stock</th>
							<th>Stok Baru</th>
							<th>Total Stok</th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; 
						foreach($riwayat_stok as $row) { ?>
						<tr>
							<td><?php echo $no++;?></td>
							<td><?php echo $row->tgl_stok;?></td>
							<td><?php echo $row->nama_produk;?> <?php echo $row->nama_variasi;?></td>
							<td><?php echo $row->nama_kategori_khusus;?></td>
							<td><?php echo $row->harga;?></td>
							<td><?php echo $row->stok_lama;?></td>
							<td><?php echo $row->stok_baru;?></td>
							<td><?php echo $row->total_stok;?></td>
						</tr>
					</tbody>
					<?php } ?>
				</table>
			</div>
		</div>

	<?php $this->load->view("produsen/footer"); ?>

	<script type="text/javascript">
			$(document).on("click","#tambah",function(e){
				var id_stx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_stok:id_stx},
					url:"<?php echo site_url('Produsen/form_tambah_stok/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});
	</script>