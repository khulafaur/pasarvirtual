<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Tambah Stok Produk</h5>
		</div>

	<div class="panel-body">
		<?php echo form_open('Produsen/tambah_riwayat_stok/','class="form-horizontal form-validate-jquery"');?>
			<fieldset class="content-group">
				<div class="form-group">
					<label class="control-label col-lg-2">Nama Produk </label>
						<div class="col-lg-10">
							<select class="select-search" name="id_produk" id="nama_produk" required="required" aria-required="true" aria-invalid="false">
								<option value="" selected="selected">Pilih Produk</option> 
									<?php 
										foreach($nama_produk as $count) { ?>
										<option value="<?php echo $count->id_produk ?>">
											<?php echo $count->nama_produk ?></option> 
								
								    <?php	} ?> 
							</select>
						</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Variasi Produk </label>
						<div class="col-lg-10">
							<select name="id_produk_variasi" id="produk_variasi" class="form-control" required="required" aria-required="true" aria-invalid="false">
								<option value="" selected="selected">Pilih variasi</option> 
							</select>
						</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Stok</label>
						<div class="col-lg-10">
							<input id="stok" type="text" name="stok_lama" class="form-control" readonly value="" onkeyup="sum();">
						</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Stok Baru</label>
						<div class="col-lg-10">
							<input id="stokbaru" type="text" name="stok_baru" class="form-control" onkeyup="sum();">
						</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Total Stok</label>
						<div class="col-lg-10">
							<input id="totalstok" type="text" name="total_stok" class="form-control" readonly>
						</div>
				</div>

				<button type="submit" class="btn btn-primary pull-right" value="submit" name="submit">Submit</button>
			
				</fieldset>

		<?php echo form_close(); ?>
	</div>

</div>
</div>

<script>
	function sum() {
      var txtFirstNumberValue = document.getElementById('stok').value;
      var txtSecondNumberValue = document.getElementById('stokbaru').value;
      var result = parseInt(txtFirstNumberValue) + parseInt(txtSecondNumberValue);
      if (!isNaN(result)) {
         document.getElementById('totalstok').value = result;
      }
	}

	$('#produk_variasi').change(function(){
    var name = $(this).val();
    console.log(name);
    $.ajax ({
        type: "POST",
        data: {produk_variasi:name},
        url: "<?php echo site_url('produsen/get_data_produk') ?>",
        dataType: 'json',
        success: function(data) {
            // console.debug(data);
            $('#stok').val(data.stok);
        }
    });
});
	 $('.select-search').select2();

	</script>

	<script type='text/javascript'>
        // baseURL variable
    var baseURL= "<?php echo base_url();?>";
        
        $(document).ready(function(){
            
            // City change
            $('#nama_produk').change(function(){
                var namprod = $(this).val();

                // AJAX request
                $.ajax({
                    url:'<?=base_url()?>index.php/Produsen/getVariasi',
                    method: 'post',
                    data: {namprod: namprod},
                    dataType: 'json',
                    success: function(response){

                        // Remove options
                        $('#produk_variasi').find('option').not(':first').remove();

                        // Add options
                        $.each(response,function(index,data){
                            $('#produk_variasi').append('<option value="'+data['id_produk_variasi']+'">'+data['nama_variasi']+'</option>');
                        });
                    }
                });
            });
        });

        var validator = $(".form-validate-jquery").validate({
		        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
		        errorClass: 'validation-error-label',
		        successClass: 'validation-valid-label',
		        highlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },
		        unhighlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },

		        // Different components require proper error label placement
		        errorPlacement: function(error, element) {

		            // Styled checkboxes, radios, bootstrap switch
		            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
		                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                    error.appendTo( element.parent().parent().parent().parent() );
		                }
		                 else {
		                    error.appendTo( element.parent().parent().parent().parent().parent() );
		                }
		            }

		            // Unstyled checkboxes, radios
		            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
		                error.appendTo( element.parent().parent().parent() );
		            }

		            // Inline checkboxes, radios
		            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                error.appendTo( element.parent().parent() );
		            }

		            // Input group, styled file input
		            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
		                error.appendTo( element.parent().parent() );
		            }
		            else {
		                error.insertAfter(element);
		            }
		        },
		        validClass: "validation-valid-label",
		        success: function(label) {
		            label.addClass("validation-valid-label").text("Success.")
		        },
		        rules: {
		            vali: "required",
		    
		            "stok_baru": {
		                number: true,
		                min: 1
		            },
		        },
		        messages: {
		            custom: {
		                required: "This is a custom error message",
		            },
		            agree: "Please accept our policy"
		        }
		    });
    </script>