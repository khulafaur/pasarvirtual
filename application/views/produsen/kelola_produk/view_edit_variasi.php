<div class="content">
<?php echo form_open_multipart("Produsen_produk/view_edit_variasi",'class="form-horizontal form-validate-jquery"'); ?>		
	<div class="panel panel-flat">
		<div class="panel-body">
			<fieldset class="content-group">
				<?php foreach($get_data as $row) {?>
				<legend class="text-bold">Edit Produk <span class="text-success"><?php echo $row->nama_produk ?></span></legend>
				
				<div class="form-group">
					<label class="control-label col-lg-2">Gambar</label>
					<div class="col-lg-8">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-camera"></i></span>
							<input type="file" class="file-styled" name="gambar_produk">
							<input type="hidden" class="form-control" name="value_gambar" value="<?php echo $row->gambar_produk ?>">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama Variasi</label>
					<div class="col-lg-8">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-bag"></i></span>
							<input name="nama_variasi" value="<?php echo $row->nama_variasi ?>" type="text" class="form-control" required="required">
							
							<input name="id_variasi" value="<?php echo $row->id_produk_variasi ?>" type="hidden" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Harga</label>
					<div class="col-lg-8">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-coins"></i></span>
							<input name="harga" value="<?php echo $row->harga ?>" type="text" class="form-control" required="required">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Harga Jual</label>
					<div class="col-lg-8">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-price-tag"></i></span>
							<input name="harga_jual" value="<?php echo $row->harga_jual ?>" type="text" class="form-control" required="required">
						</div>
					</div>
				</div>

				<?php } ?>
			</fieldset>
		</div>
	</div>
					<button name="submit" type="submit" value="submit" class="btn btn-success btn-labeled btn-lg" style="float: right;"><b><i class=" icon-touch"></i></b>Submit</button>
			<?php echo form_close(); ?>
</div>

<script type="text/javascript">
			$(".file-styled").uniform({
				fileButtonHtml: '<i class="icon-plus2"></i>'
			});
			var validator = $(".form-validate-jquery").validate({
		        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
		        errorClass: 'validation-error-label',
		        successClass: 'validation-valid-label',
		        highlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },
		        unhighlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },

		        // Different components require proper error label placement
		        errorPlacement: function(error, element) {

		            // Styled checkboxes, radios, bootstrap switch
		            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
		                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                    error.appendTo( element.parent().parent().parent().parent() );
		                }
		                 else {
		                    error.appendTo( element.parent().parent().parent().parent().parent() );
		                }
		            }

		            // Unstyled checkboxes, radios
		            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
		                error.appendTo( element.parent().parent().parent() );
		            }

		            // Inline checkboxes, radios
		            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                error.appendTo( element.parent().parent() );
		            }

		            // Input group, styled file input
		            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
		                error.appendTo( element.parent().parent() );
		            }
		            else {
		                error.insertAfter(element);
		            }
		        },
		        validClass: "validation-valid-label",
		        success: function(label) {
		            label.addClass("validation-valid-label").text("Berhasil")
		        },
		        rules: {
		            vali: "required",
		            deskripsi_produk: {
		                minlength: 25
		            },
		            "harga": {
			                number: true,
			                min: 500
		            },
		            "harga_jual": {
		                number: true,
		                min: 500
		            },
		            "stok": {
		                number: true
		            },
		        },
		        messages: {
		            custom: {
		                required: "This is a custom error message",
		            },
		            agree: "Please accept our policy"
		        }
		    });
</script>