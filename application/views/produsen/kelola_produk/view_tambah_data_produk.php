
<?php 
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>
<div class="content">
	<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Tambah Data Produk</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<?php echo form_open_multipart('Produsen_produk/tambah_data_produk/','class="form-horizontal form-validate-jquery"');?>
				
				<fieldset class="content-group">

					<div class="form-group">
						<label class="control-label col-lg-2">Gambar Produk</label>
						<div class="col-lg-10">
							<input type="file" class="file-styled" name="gambar_produk" required="required">
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-lg-2">Nama Produk</label>
						<div class="col-lg-10">
							<input type="text" name="nama_produk" class="form-control" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-2">Variasi Produk</label>
						<div class="row">
	
							<div class="col-md-2">
								<label>Gambar</label>
	                        	<input type="file" class="file-styled" name="gambar_variasi[]" required="required">
							</div>
	            				       
							<div class="col-md-2">
								<label>Nama Variasi</label>
	                        	<input name="nama_variasi[]" type="text" class="form-control" placeholder="" required="required">
	                        	
							</div>		
	            		
							<div class="col-md-1">
								<label>Stok</label>
	                        	<input name="stok[]" type="text" class="form-control" placeholder="" required="required">
							</div>
	            			            		
							<div class="col-md-2">
								<label>Harga Modal</label>
									<input name="harga[]" type="text" class="form-control" placeholder="" required="required">
							</div>
		          
		          			<div class="col-md-2">
			            		<div>
									<label>Harga Jual</label>
		                        	<div class="input-group">
										<input name="harga_jual[]" type="text" class="form-control col-md-2" placeholder="" required="required">
										<div class="input-group-btn">
											<a class="btn btn-default btn-icon btn-warning add"><i class="icon-plus3"></i></a>
										</div>
									</div>
								</div>
		            		</div>

	            		</div>
					</div>
					<div id="variasi_produk"></div>

					<div class="form-group">
						<label class="control-label col-lg-2">Kategori Produk </label>
						<div class="col-lg-10">
							<select name="id_kategori_umum" id="kategori_umum" class="form-control" required="required" aria-required="true" aria-invalid="false">
								<option value="" selected="selected">Pilih Kategori Produk</option> 
									<?php 
									foreach($kategori_umum as $count) {
										echo "<option value='".$count['id_kategori_umum']."'>".$count['nama_kategori_umum']."</option>";
									} ?> 
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-lg-2">Subkategori Produk </label>
						<div class="col-lg-10">
							<select name="id_kategori" id="kategori" class="form-control" required="required" aria-required="true" aria-invalid="false">
								<option value="" selected="selected">Pilih Subkategori Produk</option> 
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-lg-2">Subkategori Produk </label>
						<div class="col-lg-10">
							<select name="id_kategori_khusus" id="kategori_khusus" class="form-control" required="required" aria-required="true" aria-invalid="false">
								<option value="" selected="selected">Pilih Subkategori Produk</option> 
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Bagi Hasil Produsen </label>
						<div class="col-lg-10">
							<select id="mySelect" name="persen_produsen" class="form-control" required="required" aria-required="true" aria-invalid="false" onchange="myFunction()">
								<option value="">Pilih option</option> 
								<option value="70">70%</option>
								<option value="75">75%</option>
								<option value="80">80%</option>
								<option value="85">85%</option>
								<option value="90">90%</option>
								<option value="95">95%</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Bagi Hasil Distributor</label>
						<div class="col-lg-10">
							<input id="mySelect1" type="text" readonly="readonly" name="persen_distributor" class="form-control" required="required">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-lg-2">Deskripsi Produk</label>
						<div class="col-lg-10">
							<textarea required="required" rows="5" cols="5" name="deskripsi_produk" class="form-control"></textarea>
						</div>
					</div>
					<button type="submit" name="submit" class="btn btn-primary">Submit</button>
				</fieldset>
			<?php echo form_close(); ?>
		</div>
	</div>
		<script>
		// Default file input style

			$( "body" ).on( "click", "a.add", function(e) {
				e.preventDefault();

				var dokumen = '<div class="form-group">';
					dokumen += ' <label class="control-label col-lg-2"></label>';
					dokumen += ' <div class="row">';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		<input type="file" class="file-styled" name="gambar_variasi[]" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		 <input name="nama_variasi[]" type="text" class="form-control" placeholder="" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-1">';
					dokumen += '		 <input name="stok[]" type="text" class="form-control" placeholder="" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		 <input name="harga[]" type="text" class="form-control" placeholder="" required="required">';
					dokumen += '   </div>';
				    dokumen += '   <div class="col-md-2">';
			     	dokumen += '		<div class="input-group">';
				    dokumen += '     		<input name="harga_jual[]" type="text" class="form-control" placeholder="" required="required">';
				    dokumen += '   			<div class="input-group-btn">';
				    dokumen += '   				<a class="btn btn-default btn-icon btn-warning dlt"><i class="icon-minus3"></i></a>';
				    dokumen += '   			</div>';
				    dokumen += '        </div>';
				    dokumen += '   </div>';
					dokumen += ' </div>';
				$( "#variasi_produk" ).append( dokumen );

				$(".file-styled").uniform({
					fileButtonHtml: '<i class="icon-plus2"></i>'
				});
			});

			$( "body" ).on( "click", "a.dlt", function() {
				$( this ).parent().parent().parent().parent().parent().remove();
			});

								

			function myFunction() {
			    var x = document.getElementById("mySelect").value;
			    var hasil = 100-x; 
			    document.getElementById("mySelect1").value = hasil + "%";
			}

			var validator = $(".form-validate-jquery").validate({
		        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
		        errorClass: 'validation-error-label',
		        successClass: 'validation-valid-label',
		        highlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },
		        unhighlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },

		        // Different components require proper error label placement
		        errorPlacement: function(error, element) {

		            // Styled checkboxes, radios, bootstrap switch
		            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
		                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                    error.appendTo( element.parent().parent().parent().parent() );
		                }
		                 else {
		                    error.appendTo( element.parent().parent().parent().parent().parent() );
		                }
		            }

		            // Unstyled checkboxes, radios
		            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
		                error.appendTo( element.parent().parent().parent() );
		            }

		            // Inline checkboxes, radios
		            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                error.appendTo( element.parent().parent() );
		            }

		            // Input group, styled file input
		            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
		                error.appendTo( element.parent().parent() );
		            }
		            else {
		                error.insertAfter(element);
		            }
		        },
		        validClass: "validation-valid-label",
		        success: function(label) {
		            label.addClass("validation-valid-label").text("Berhasil")
		        },
		        rules: {
		            vali: "required",
		            deskripsi_produk: {
		                minlength: 25
		            },
		            "harga[]": {
		                number: true,
		                min: 500
		            },
		            "harga_jual[]": {
		                number: true,
		                min: 500
		            },
		            "stok[]": {
		                number: true,
		                min: 1
		            },
		        },
		        messages: {
		            custom: {
		                required: "This is a custom error message",
		            },
		            agree: "Please accept our policy"
		        }
		    });
		</script>

        <script type='text/javascript'>
            // baseURL variable
            var baseURL= "<?php echo base_url();?>";
            
            $(document).ready(function(){
                
                // City change
                $('#kategori_umum').change(function(){
                    var umum = $(this).val();

                    // AJAX request
                    $.ajax({
                        url:'<?=base_url()?>index.php/Produsen_produk/getKategori',
                        method: 'post',
                        data: {umum: umum},
                        dataType: 'json',
                        success: function(response){

                            // Remove options
                            $('#kategori_khusus').find('option').not(':first').remove();
                            $('#kategori').find('option').not(':first').remove();

                            // Add options
                            $.each(response,function(index,data){
                                $('#kategori').append('<option value="'+data['id_kategori']+'">'+data['nama_kategori']+'</option>');
                            });
                        }
                    });
                });
                
                // Department change
                $('#kategori').change(function(){
                    var kat = $(this).val();

                    // AJAX request
                    $.ajax({
                        url:'<?=base_url()?>index.php/Produsen_produk/getKatKhusus',
                        method: 'post',
                        data: {kat: kat},
                        dataType: 'json',
                        success: function(response){
                            
                            // Remove options
                            $('#kategori_khusus').find('option').not(':first').remove();

                            // Add options
                            $.each(response,function(index,data){
                                $('#kategori_khusus').append('<option value="'+data['id_kategori_khusus']+'">'+data['nama_kategori_khusus']+'</option>');
                            });
                        }
                    });
                });
                
            });
        </script>

<?php 
	$this->load->view('produsen/footer');
?>
