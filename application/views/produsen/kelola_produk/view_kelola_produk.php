
<?php 
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> Data Produk </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>
			
		<div class="panel-body">
		<a style="float: right;"  href="<?php echo base_url().'Produsen_produk/tambah_data_produk/'?>"><button type="button" class="btn btn-success"><i class="icon-comment-discussion position-right"></i> Tambah Data</button></a>
			<ul class="nav nav-tabs nav-tabs-highlight">
				<li class="active"><a href="#fade-tab1" data-toggle="tab">Produk</a></li>
				<li class=""><a href="#fade-tab2" data-toggle="tab">Variasi Produk</a></li>
				<li class=""><a href="#fade-tab3" data-toggle="tab">Gudang</a></li>
			</ul>

			<div class="tab-content">
				
				
				<div class="tab-pane fade in active" id="fade-tab1">
					<table class="table table-striped media-library table-lg">
                        <thead>
                            <tr>
                            	<th>No</th>
                                <th>Gambar Produk</th>
								<th>Nama Produk</th>
								<th>Kategori Khusus</th>
								<th>Kategori</th>
								<th>Kategori Umum</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
								$no = 1; foreach($hasil1 as $row) { ?>
                            <tr>
                            	<td><?php echo $no++; ?></td>
			                    <td>
				                    <a href="<?php echo base_url();?>/uploads/<?php echo $row->gambar;?>" data-popup="lightbox">
					                    <img src="<?php echo base_url();?>/uploads/<?php echo $row->gambar;?>" alt="" class="img-rounded img-preview img-thumbnail">
				                    </a>
			                   	</td>
		                         <td><?php echo $row->nama_produk; ?></td>
								 <td><?php echo $row->nama_kategori_khusus; ?></td>
								 <td><?php echo $row->nama_kategori; ?></td>
								 <td><?php echo $row->nama_kategori_umum; ?></td>
		                        
		                        <td class="text-center">
		                            <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li><a href="<?php echo base_url().'Produsen_produk/edit_data_produk/'.$row->id_produk;?>"><i class="icon-pencil7"></i> Edit Produk</a></li>
												<li id="detail"><a class="<?php echo $row->id_produk ?>"><i class="icon-eye"></i> Lihat Detail</a></li>
												<li class="divider"></li>
												<li id="delete"><a class="<?php echo $row->id_produk;?>"><i class="icon-bin"></i> Pindah ke Gudang</a></li>
												</i>
											</ul>
										</li>
									</ul>
		                        </td>
                            </tr>

							<?php } ?>		
	                        </tbody>
	                    </table>
                </div>
                <div class="tab-pane fade" id="fade-tab2">
	                <table class="table table-striped media-library table-lg">
                        <thead>
                            <tr>
                            	<th>No</th>
                                <th>Gambar Produk</th>
								<th>Nama Produk</th>
								<th>Kategori</th>
								<th>Harga Modal</th>
								<th>Harga Jual</th>
								<th>Stok</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
							$no = 1; foreach($hasil as $row) { ?>
                            <tr>
                            	<td><?php echo $no++; ?></td>
			                    <td>
				                    <a href="<?php echo base_url();?>/uploads/<?php echo $row->gambar_produk;?>" data-popup="lightbox">
					                    <img src="<?php echo base_url();?>/uploads/<?php echo $row->gambar_produk;?>" alt="" class="img-thumbnail img-rounded img-preview">
				                    </a>
			                   	</td>
		                         <td><?php echo $row->nama_produk; ?> <?php echo $row->nama_variasi; ?></td>
								 <td><?php echo $row->nama_kategori_khusus; ?></td>
								 <td>Rp. <?php echo number_format($row->harga,0,",","."); ?></td>
								 <td>Rp. <?php echo number_format($row->harga_jual,0,",","."); ?></td>
								 <td><?php echo $row->stok; ?></td>
		                        
		                        <td class="text-center">
		                            <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li id="edit"><a class="<?php echo $row->id_produk_variasi;?>"><i class="icon-pencil7"></i> Edit Variasi</a></li>
												<li id="delete_variasi"><a class="<?php echo $row->id_produk_variasi;?>"><i class="icon-bin"></i> Delete </a></li>
											</ul>
										</li>
									</ul>
		                        </td>
                            </tr>

							<?php } ?>		
                        </tbody>
                    </table>
            	</div>
            	<div class="tab-pane fade" id="fade-tab3">
	                <table class="table table-striped media-library table-lg">
                        <thead>
                            <tr>
                            	<th>No</th>
                                <th>Gambar Produk</th>
								<th>Nama Produk</th>
								<th>Kategori Khusus</th>
								<th>Kategori</th>
								<th>Kategori Umum</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
								$no = 1; foreach($hasil2 as $row) { ?>
                            <tr>
                            	<td><?php echo $no++; ?></td>
			                    <td>
				                    <a href="<?php echo base_url();?>/uploads/<?php echo $row->gambar;?>" data-popup="lightbox">
					                    <img src="<?php echo base_url();?>/uploads/<?php echo $row->gambar;?>" alt="" class="img-thumbnail img-rounded img-preview">
				                    </a>
			                   	</td>
		                         <td><?php echo $row->nama_produk; ?></td>
								 <td><?php echo $row->nama_kategori_khusus; ?></td>
								 <td><?php echo $row->nama_kategori; ?></td>
								 <td><?php echo $row->nama_kategori_umum; ?></td>
		                        
		                        <td class="text-center">
		                            <ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li id="detail"><a class="<?php echo $row->id_produk ?>"><i class="icon-eye"></i> Lihat Detail</a></li>
												<li class="divider"></li>
												<li id="deletet"><a class="<?php echo $row->id_produk;?>"><i class="icon-bin"></i> Pindah ke Daftar</a></li>
												</i>
											</ul>
										</li>
									</ul>
		                        </td>
                            </tr>

							<?php } ?>		
	                        </tbody>
	                    </table>
                </div>
            </div>
		</div>
	</div>
	<?php $this->load->view("produsen/footer"); ?>

<script>
	$(document).on("click","#detail a",function(e){
		var id_prdk_variasi = $(this).attr("class");
		$.ajax({
			type:"POST",
			data:{id:id_prdk_variasi},
			url:"<?php echo site_url('Produsen_produk/view_detail_produk/') ?>",
			success:function(msg){
				$(".modal-body").html(msg);
			},
			error: function(result){
				$(".modal-body").html("Error");
			}
		});
		e.preventDefault();
		$("#myModal").modal('show');
	});

	$(document).on("click","#edit a",function(e){
		var id_prdk = $(this).attr("class");
		$.ajax({
			type:"POST",
			data:{id:id_prdk},
			url:"<?php echo site_url('Produsen_produk/view_edit_variasi/') ?>",
			success:function(msg){
				$(".modal-body").html(msg);
			},
			error: function(result){
				$(".modal-body").html("Error");
			}
		});
		e.preventDefault();
		$("#myModal").modal('show');
	});

	$('#delete a').on('click', function() {
		var thisRow = $(this).parents('tr');
		var id_produk = $(this).attr("class");

		swal({
			title: "Apakah Anda Yakin?",
			text: "Produk ini tidak akan dapat ditemukan oleh distributor!",
			type: "warning", 
			showCancelButton: true,
			confirmButtonColor: "EF5350",
			confirmButtonText: "Ya, Pindahkan!",
			cancelButtonText: "Tidak, Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: false
		},

		function(isConfirm){
			if (isConfirm){
				$.ajax({
					type:"POST",
					data:{id:id_produk},
					url:"<?php echo site_url('Produsen_produk/hapus_data_produk') ?>",
					success:function() {
						swal({
							title: "Terpindahkan!",
							text: "Produk ini Telah dipindahkan ke Gudang",
							confirmButtonColor: "#66BB6A",
							type: "success"
						},function() {
							thisRow.remove();
				            location.reload();
				        });

						thisRow.remove();
					}
				})
			} else {
				swal({
					title: "Dibatalkan",
					text: "Produk ini masih berada dalam daftar :)",
					confirmButtonColor: "#2196F3",
					type: "error"
				});
			}
		});
	});

	$('#deletet a').on('click', function() {
		var thisRow = $(this).parents('tr');
		var id_produk = $(this).attr("class");

		swal({
			title: "Apakah Anda Yakin?",
			text: "Produk ini akan kembali ke daftar dan dapat ditemukan oleh distributor!",
			type: "warning", 
			showCancelButton: true,
			confirmButtonColor: "EF5350",
			confirmButtonText: "Ya, Pindahkan!",
			cancelButtonText: "Tidak, Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: false
		},

		function(isConfirm){
			if (isConfirm){
				$.ajax({
					type:"POST",
					data:{id:id_produk},
					url:"<?php echo site_url('Produsen_produk/active_data_produk') ?>",
					success:function() {
						swal({
							title: "Terpindahkan!",
							text: "Produk ini Telah dipindahkan ke Daftar",
							confirmButtonColor: "#66BB6A",
							type: "success"
						},function() {
							thisRow.remove();
				            location.reload();
				        });
					}
				})
			} else {
				swal({
					title: "Dibatalkan",
					text: "Produk ini masih berada dalam gudang :)",
					confirmButtonColor: "#2196F3",
					type: "error"
				});
			}
		});
	});

	$('#delete_variasi a').on('click', function() {
		var thisRow = $(this).parents('tr');
		var id_variasi = $(this).attr("class");

		swal({
			title: "Apakah Anda Yakin?",
			text: "Anda tidak akan dapat memulihkan produk variasi ini!",
			type: "warning", 
			showCancelButton: true,
			confirmButtonColor: "EF5350",
			confirmButtonText: "Ya, hapus!",
			cancelButtonText: "Tidak, Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: false
		},

		function(isConfirm){
			if (isConfirm){
				$.ajax({
					type:"POST",
					data:{id:id_variasi},
					url:"<?php echo site_url('Produsen_produk/hapus_data_variasi') ?>",
					success:function() {
						swal({
							title: "Terhapus!",
							text: "Produk Variasi ini Telah Terhapus.",
							confirmButtonColor: "#66BB6A",
							type: "success"
						});

						thisRow.remove();
					}
				})
			} else {
				swal({
					title: "Dibatalkan",
					text: "Produk variasi ini telah aman :)",
					confirmButtonColor: "#2196F3",
					type: "error"
				});
			}
		});
	});
</script>
