<?php 
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>

<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Edit Data Produk</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<?php echo form_open_multipart('Produsen_produk/edit_data_produk/','class="form-horizontal form-validate-jquery"');?>
								<fieldset class="content-group">
									<?php foreach ($produk as $row) { ?>

									<div class="form-group">
										<label class="control-label col-lg-2">Gambar Produk</label>
										<div class="col-lg-10">
											<input type="file" class="file-styled" name="gambar_produk">
											<input type="hidden" name="value_gambar" class="form-control" value="<?php echo $row->gambar?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Nama Produk</label>
										<div class="col-lg-10">
											<input type="text" name="nama_produk" class="form-control" value="<?php echo $row->nama_produk?>">
											<input type="hidden" name="id_produk" class="form-control" value="<?php echo $row->id_produk?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Kategori Produk </label>
										<div class="col-lg-10">
											<select name="id_kategori_umum" id="kategori_umum" class="form-control" required="required" aria-required="true" aria-invalid="false">
												<option value="<?php echo $row->nama_kategori_umum?>" selected="selected"><?php echo $row->nama_kategori_umum?></option> 
													<?php 
													foreach($kategori_umum as $count) {
														echo "<option value='".$count['id_kategori_umum']."'>".$count['nama_kategori_umum']."</option>";
													} ?> 
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Subkategori Produk </label>
										<div class="col-lg-10">
											<select name="id_kategori" id="kategori" class="form-control" required="required" aria-required="true" aria-invalid="false">
												<option value="<?php echo $row->nama_kategori?>" selected="selected"><?php echo $row->nama_kategori?></option> 
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Subkategori Produk </label>
										<div class="col-lg-10">
											<select name="id_kategori_khusus" id="kategori_khusus" class="form-control" required="required" aria-required="true" aria-invalid="false">
												<option value="<?php echo $row->id_kategori_khusus?>" selected="selected"><?php echo $row->nama_kategori_khusus?></option> 
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-2">Deskripsi Produk</label>
										<div class="col-lg-10">
											<textarea rows="5" cols="5" name="deskripsi_produk" class="form-control"><?php echo $row->deskripsi_produk?></textarea>
										</div>
									</div>

									<div class="form-group">
										<legend class="control-label col-lg-3 text-bold">Tambah Variasi Produk</legend>

										<a href="#" class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom btn-add"><i class="icon-plus3"></i></a>
									</div>
									<div id="variasi_produk"></div>
								<?php	} ?>
									<button type="submit" name="submit" class="btn btn-primary">Submit</button>
								</fieldset>
							<?php echo form_close(); ?>
						</div>
					</div>

<script>
			$( "body" ).on( "click", "a.btn-add", function(e) {
				e.preventDefault();
				var dokumen = '<div class="form-group">';
					dokumen += ' <label class="control-label col-lg-2"></label>';
					dokumen += ' <div class="row">';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		<input type="file" class="file-styled" name="gambar_variasi[]" placeholder="gambar" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		 <input name="nama_variasi[]" type="text" class="form-control" placeholder="nama variasi" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-1">';
					dokumen += '		 <input name="stok[]" type="text" class="form-control" placeholder="stok" required="required">';
					dokumen += '   </div>';
					dokumen += '   <div class="col-md-2">';
					dokumen += '		 <input name="harga[]" type="text" class="form-control" placeholder="harga modal" required="required">';
					dokumen += '   </div>';
				    dokumen += '   <div class="col-md-2">';
			     	dokumen += '		<div class="input-group">';
				    dokumen += '     		<input name="harga_jual[]" type="text" class="form-control" placeholder="harga jual" required="required">';
				    dokumen += '   			<div class="input-group-btn">';
				    dokumen += '   				<a class="btn btn-default btn-icon btn-warning dlt"><i class="icon-minus3"></i></a>';
				    dokumen += '   			</div>';
				    dokumen += '        </div>';
				    dokumen += '   </div>';
					dokumen += ' </div>';
						
				
				$( "#variasi_produk" ).append( dokumen );
				$(".file-styled").uniform({
					fileButtonHtml: '<i class="icon-plus2"></i>'
				});
				
			});

			$( "body" ).on( "click", "a.dlt", function() {
				$( this ).parent().parent().parent().parent().parent().parent().remove();
			});

			var validator = $(".form-validate-jquery").validate({
		        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
		        errorClass: 'validation-error-label',
		        successClass: 'validation-valid-label',
		        highlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },
		        unhighlight: function(element, errorClass) {
		            $(element).removeClass(errorClass);
		        },

		        // Different components require proper error label placement
		        errorPlacement: function(error, element) {

		            // Styled checkboxes, radios, bootstrap switch
		            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
		                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                    error.appendTo( element.parent().parent().parent().parent() );
		                }
		                 else {
		                    error.appendTo( element.parent().parent().parent().parent().parent() );
		                }
		            }

		            // Unstyled checkboxes, radios
		            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
		                error.appendTo( element.parent().parent().parent() );
		            }

		            // Inline checkboxes, radios
		            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
		                error.appendTo( element.parent().parent() );
		            }

		            // Input group, styled file input
		            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
		                error.appendTo( element.parent().parent() );
		            }
		            else {
		                error.insertAfter(element);
		            }
		        },
		        validClass: "validation-valid-label",
		        success: function(label) {
		            label.addClass("validation-valid-label").text("Success.")
		        },
		        rules: {
		            vali: "required",
		            deskripsi_produk: {
		                minlength: 10
		            },
		            "harga[]": {
			                number: true,
			                min: 500
		            },
		            "harga_jual[]": {
		                number: true,
		                min: 500
		            },
		            "stok[]": {
		                number: true
		            },
		        },
		        messages: {
		            custom: {
		                required: "This is a custom error message",
		            },
		            agree: "Please accept our policy"
		        }
		    });
		</script>
        <script type='text/javascript'>
            // baseURL variable
            var baseURL= "<?php echo base_url();?>";
            
            $(document).ready(function(){
                
                // City change
                $('#kategori_umum').change(function(){
                    var umum = $(this).val();

                    // AJAX request
                    $.ajax({
                        url:'<?=base_url()?>index.php/Produsen_produk/getKategori',
                        method: 'post',
                        data: {umum: umum},
                        dataType: 'json',
                        success: function(response){

                            // Remove options
                            $('#kategori_khusus').find('option').not(':first').remove();
                            $('#kategori').find('option').not(':first').remove();

                            // Add options
                            $.each(response,function(index,data){
                                $('#kategori').append('<option value="'+data['id_kategori']+'">'+data['nama_kategori']+'</option>');
                            });
                        }
                    });
                });
                
                // Department change
                $('#kategori').change(function(){
                    var kat = $(this).val();

                    // AJAX request
                    $.ajax({
                        url:'<?=base_url()?>index.php/Produsen_produk/getKatKhusus',
                        method: 'post',
                        data: {kat: kat},
                        dataType: 'json',
                        success: function(response){
                            
                            // Remove options
                            $('#kategori_khusus').find('option').not(':first').remove();

                            // Add options
                            $.each(response,function(index,data){
                                $('#kategori_khusus').append('<option value="'+data['id_kategori_khusus']+'">'+data['nama_kategori_khusus']+'</option>');
                            });
                        }
                    });
                });
                
            });
        </script>
					
<?php 
	$this->load->view('produsen/footer');
	
?>

