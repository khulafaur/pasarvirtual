<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> Verifikasi Distributor </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>

		<div class="panel-body">	
			<table class="table datatable-basic">
				<thead>
					<tr >
						<th>Nama Distributor</th>
						<th>Nama Toko</th>
						<th>email</th>
						<th>Nomor Hp</th>
						<th>Nomor Telpon</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					foreach($get_data as $row){ ?>
					<tr>
						<td><?php echo $row->nama_distributor; ?></td>
						<td><?php echo $row->nama_toko; ?></td>
						<td><?php echo $row->email; ?></td>
						<td><?php echo $row->hp; ?></td>
						<td><?php echo $row->telp; ?></td>
						<td class="text-center">
							<ul class="icons-list">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
										<i class="icon-menu9"></i>
									</a>

									<ul class="dropdown-menu dropdown-menu-right">
										<li id="detail"><a class="<?php echo $row->ktp_distributor; ?>"><i class="icon-file-eye"></i> Lihat Detail</a></li>
										<li id="delete"><a class="<?php echo $row->id_prod_dist; ?>"><i class="icon-accessibility"></i> Approve</a></li>
										<li id="delete"><a class="<?php echo $row->id_prod_dist; ?>"><i class=" icon-switch"></i> Hapus dari daftar</a></li>
									</ul>
								</li>
							</ul>
						</td>
				
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

<?php 
	$this->load->view('produsen/footer');
?>

<script>
$(document).on("click","#detail a",function(e){
	var id_dist = $(this).attr("class");
	$.ajax({
		type:"POST",
		data:{id:id_dist},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_distributor/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

$(document).on("click","#invoice button",function(e){
	var id_transaksi = $(this).parent().attr("class");
	$.ajax({
		type:"POST",
		data:{id_trx:id_transaksi},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_transaksi/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

 $('#delete a').on('click', function() {
 	 var thisRow = $(this).parents('tr');
 	 var id_dist = $(this).attr("class");

        swal({
            title: "Approve?",
            text: "Distributor ini akan menjadi bagian dari anda!",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#66BB6A",
            confirmButtonText: "Ya, Approve!",
            cancelButtonText: "Tidak, Abaikan!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        
        function(isConfirm){
            if (isConfirm) {
            	$.ajax({
					type:"POST",
					data:{id:id_dist},
					url:"<?php echo site_url('Produsen_monitoring_distributor/approve_distributor/') ?>",
					success:function(){
						swal({
		                    title: "Selamat!",
		                    text: "Distributor anda telah bertambah",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
                		});

                		thisRow.remove();
           			 }
           		})
            }else {
                swal({
                    title: "di Abaikan",
                    text: "Distributor ini masih berada dalam daftar verifikasi :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });

 	$('#deletet a').on('click', function() {
 	 var thisRow = $(this).parents('tr');
 	 var id_dist = $(this).attr("class");

        swal({
            title: "Apakah Anda Yakin?",
            text: "Distributor ini akan terhapus dari daftar Distributor Anda!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        
        function(isConfirm){
            if (isConfirm) {
            	$.ajax({
					type:"POST",
					data:{id:id_dist},
					url:"<?php echo site_url('Produsen_monitoring_distributor/hapus_distributor/') ?>",
					success:function(){
						swal({
		                    title: "Dihapuskan!",
		                    text: "Distributor ini telah dihapus dari daftar verifikasiss",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
                		});

                		thisRow.remove();
           			 }
           		})
            }else {
                swal({
                    title: "Dibatalkan",
                    text: "Distributor ini masih berada di daftar verifikasi :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });

</script>



