<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> Monitoring Distributor </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>

		<div class="panel-body">	
			<div class="tabbable">
				<ul class="nav nav-tabs nav-tabs-highlight">
					<li class="active"><a href="#fade-tab1" data-toggle="tab">Daftar Distributor</a></li>
					<li class=""><a href="#fade-tab2" data-toggle="tab">Penjualan Distributor</a></li>
				</ul>
				<a style="float: right;"  href="<?php echo site_url("/Produsen_monitoring_distributor/tren_penjualan_dist");  ?>">
				<button type="button" class="btn btn-warning btn-labeled"><b><i class=" icon-trophy2"></i></b> Peringkat Distributor</button></a>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="fade-tab1">
					<table class="table datatable-basic">
						<thead>
							<tr >
								<th>Nama Distributor</th>
								<th>Nama Toko</th>
								<th>email</th>
								<th>Nomor Hp</th>
								<th>Nomor Telpon</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							foreach($get_data as $row){ ?>
							<tr>
								<td><?php echo $row->nama_distributor; ?></td>
								<td><?php echo $row->nama_toko; ?></td>
								<td><?php echo $row->email; ?></td>
								<td><?php echo $row->hp; ?></td>
								<td><?php echo $row->telp; ?></td>
								<td class="text-center">
									<ul class="icons-list">
										<li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-menu9"></i>
											</a>

											<ul class="dropdown-menu dropdown-menu-right">
												<li ><a href="<?php echo site_url("/Produsen_monitoring_distributor/detail_aktivitas/".$row->ktp_distributor);  ?>"><i class="icon-search4"></i> Aktivitas Penjualan </a></li>
												<li id="detail"><a class="<?php echo $row->ktp_distributor; ?>"><i class="icon-file-eye"></i> Lihat Detail</a></li>
												<li id="delete"><a class="<?php echo $row->id_prod_dist; ?>"><i class=" icon-switch"></i> Non Aktif</a></li>
											</ul>
										</li>
									</ul>
								</td>
						
							</tr>
						<?php } ?>
						</tbody>
					</table>
					</div>

					<div class="tab-pane fade" id="fade-tab2">
						<table class="table table-lg invoice-archive">
							<thead>
								<tr>
									<th>Id Transaksi</th>
									<th>Period</th>
					                <th>Distributor</th>
					                <th>Picture</th>
					                <th>Terjual</th>
					                <th>Tanggal transaksi</th>
					                <th>Pendapatan</th>
					                <th class="text-center">Aksi</th>
					            </tr>
							</thead>
							<tbody>
							<?php 
							foreach($get_history_penjualan as $row){ ?>
								<tr>
									<td><?php echo $row->id_transaksi; ?></td>
									<td><?php echo $row->periode; ?></td>
					                <td>
					                	<h6 class="no-margin">
					                		<a href="#"><?php echo $row->nama_distributor; ?></a>
					                		<small class="display-block text-muted"><?php echo $row->nama_toko; ?></small>
				                		</h6>
				                	</td>
				                	<td><a href="#"><img src="<?php echo base_url();?>assets/images/<?php echo $row->foto_distributor; ?>" class="img-circle img-lg" alt=""></a></td>
					                <td><h6 class="no-margin text-bold"><?php echo $row->qty; ?> Pcs</h6></td>
					                <td><span class="label label-success"><?php echo $row->tanggal; ?></span></td>
					                <td>
					                	<h6 class="no-margin text-bold left-potition">Rp.<?php echo number_format($row->pendapatan,2); ?></h6>
					                </td>
									<td id="invoice" class="<?php echo $row->id_transaksi ?>">
										<button type="button" class="btn border-warning text-warning-600 btn-flat btn-icon btn-rounded"><i class="icon-file-eye"></i></button>
									</td>
					            </tr>
					            <?php } ?>
					        </tbody>
					    </table>
						
					</div>
				</div>
			</div>
		</div>
	</div>

<?php 
	$this->load->view('produsen/footer');
?>

<script>
$(document).on("click","#detail a",function(e){
	var id_dist = $(this).attr("class");
	$.ajax({
		type:"POST",
		data:{id:id_dist},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_distributor/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

$(document).on("click","#invoice button",function(e){
	var id_transaksi = $(this).parent().attr("class");
	$.ajax({
		type:"POST",
		data:{id_trx:id_transaksi},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_transaksi/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

 $('#delete a').on('click', function() {
 	 var thisRow = $(this).parents('tr');
 	 var id_dist = $(this).attr("class");

        swal({
            title: "Apakah Anda Yakin?",
            text: "Distributor ini akan terhapus dari daftar Distributor Anda!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        
        function(isConfirm){
            if (isConfirm) {
            	$.ajax({
					type:"POST",
					data:{id:id_dist},
					url:"<?php echo site_url('Produsen_monitoring_distributor/hapus_distributor/') ?>",
					success:function(){
						swal({
		                    title: "Dihapuskan!",
		                    text: "Distributor ini telah dihapus dari daftar",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
                		});

                		thisRow.remove();
           			 }
           		})
            }else {
                swal({
                    title: "Dibatalkan",
                    text: "Distributor ini masih berada di daftar distributor Anda :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });
</script>



