<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> LeaderBoard Penjualan </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>
		<div class="panel-body">

			<div class="tabbable">
				<ul class="nav nav-tabs nav-tabs-highlight">
					<li class="active"><a href="#fade-tab1" data-toggle="tab">LeaderBoard</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Grafik<span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#fade-tab2" data-toggle="tab">Pendapatan Distributor</a></li>
							<li><a href="#fade-tab3" data-toggle="tab">Penjualan Distributor</a></li>
						</ul>
					</li>
				</ul>
				<?php echo form_open('Produsen_monitoring_distributor/tren_penjualan_dist'); ?>
					<div class="form-group" style="float: right;">

						<div class="input-group">
							<div class="input-group-addon">
								<i class=" icon-calendar2"></i>
							</div>
							<select class="select-fixed" data-placeholder="Bulan..." name="bulan">
								<option><?php if(isset($bulan)){ echo $bulan; } ?></option>
								<option value="">Not Select</option>
								<option value="January">Januari</option>
								<option value="February">Februari</option>
								<option value="March">Maret</option>
								<option value="April">April</option>
								<option value="May">Mei</option>
								<option value="June">Juni</option>
								<option value="July">Juli</option>
								<option value="August">Agustus</option>
								<option value="September">September</option>
								<option value="October">Oktober</option>
								<option value="November">November</option>
								<option value="December">Desember</option>
							</select>
							<select class="select-fixed-short" data-placeholder="Tahun..."  required="required" name="tahun">
								<option>
									<?php if(isset($year)){ echo $year; } ?>
								</option>
								<?php for ($i=0; $i < count($getRangeYear) ; $i++) { ?>
									<option value="<?php echo $getRangeYear[$i]?>"><?php echo $getRangeYear[$i]?></option>
								<?php } ?>
							</select>
							<div class="input-group-btn">
								<button name="cari" value="submit" type="submit" class="btn btn-success dropdown-toggle btn-icon">
									<i class="icon-search4"></i>
								</button>
							</div>
						</div>
					</div>
				<?php echo form_close(); ?>
				<div class="tab-content">
					<div class="tab-pane fade in active" id="fade-tab1">
						<div class="tabbable">
							<table id="sort_by" class="table datatable-basic">
								<thead>
									<tr>
										<th>Rank</th>
										<th>Gambar</th>
						                <th>Distributor</th>
						                <th>Total Penjualan</th>
						                <th>Total Pendapatan</th>
						                <th class="text-center">Aksi</th>
						            </tr>
								</thead>
								<tbody>
								<?php $x=0;
								foreach($all as $row){ ?>
									<tr>
										<td><?php echo $x=$x+1; ?></td>
										<td><img src="<?php echo base_url();?>assets/images/<?php echo $row->foto_distributor ?>" class="img-circle img-lg" alt=""></a></td>
						                <td><?php echo $row->nama_distributor ?></td>
					                	<td><?php echo $row->qty ?></td>
						                <td>Rp.<?php echo $row->pendapatan; ?></td>
										<td id="send" class="text-center" >
						                   <span class=<?php echo $row->ktp_distributor ?>><button type="button" class="btn btn-warning btn-xs"><i class="icon-pencil6 position-left"></i> Kirim Bonus</button></span> 
						                </td>
						            </tr>
						            <?php } ?>
						        </tbody>
						    </table>
						</div>
					</div>
					<div class="tab-pane fade" id="fade-tab2">
						<div class="chart" id="c3-chart-penjualan"></div>
					</div>
					<div class="tab-pane fade" id="fade-tab3">
						<div class="chart" id="c3-chart-pendapatan"></div>
					</div>
				</div>
		</div>
	</div>
</div>

<?php 
	$this->load->view('produsen/footer');
?>

<script>
$(document).on("click","#send span",function(e){
	var id_dist = $(this).attr("class");
	$.ajax({
		type:"POST",
		data:{id:id_dist},
		url:"<?php echo site_url('Produsen_monitoring_distributor/kirim_dompet/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

var axis_tick_rotation = c3.generate({

        bindto: '#c3-chart-penjualan',
        size: { height: 400 },
        data: {
            x : 'x',
            columns: [
            		['x', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            	<?php $x=0; foreach($all as $row){ $x=$x+1; ?>
            		["<?php echo $row->nama_distributor; ?>", <?php foreach($a[$x] as $row){ echo $row->qty ?>,<?php } ?>],
            	<?php } ?>
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#4CAF50', '#F4511E', '#1E88E5', '#00BCD4']
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: -90
                },
                height: 80
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });



     var axis_tick_rotation = c3.generate({
        bindto: '#c3-chart-pendapatan',
        size: { height: 400 },
        data: {
            x : 'x', 
            columns: [
                ['x', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                <?php $x=0; foreach($all as $row){ $x=$x+1; ?>
            		["<?php echo $row->nama_distributor; ?>", <?php foreach($a[$x] as $row){ echo $row->pendapatan ?>,<?php } ?>],
            	<?php } ?>
            ],
            type: 'bar'
        },
        color: {
            pattern: ['#4CAF50', '#F4511E', '#1E88E5', '#00BCD4']
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    rotate: -90
                },
                height: 80
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });



</script>



