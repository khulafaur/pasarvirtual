
<div class="panel panel-flat">
	<?php foreach ($get_data as $row){ ?>
	<div class="thumbnail" style="background-image: url(">
			<img src="<?php echo base_url(); ?>assets/images/<?php echo $row->foto_distributor; ?>" alt="" style="width:100%; height: 250px" >
	</div>

	<div class="caption">
		<ul class="media-list media-list-linked media-list-bordered">
			<li class="media">
				<a href="#" class="media-link">
					<div class="media-left">
						<img src="<?php echo base_url(); ?>assets/images/shop.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body" >
						<h6 class="media-heading">Nama Toko</h6>
						 <text style="width:200px; word-wrap:break-word; display:inline-block;"> <?php echo $row->nama_toko; ?></text>
					</div>

					<div class="media-left">
						<img src="<?php echo base_url(); ?>assets/images/person-flat.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body">
						<h6 class="media-heading">Nama Distributor</h6>
						<text style="width:200px; word-wrap:break-word; display:inline-block;"><?php echo $row->nama_distributor; ?></text>
					</div>
				</a>
			</li>

			<li class="media">
				<a href="#" class="media-link">
					<div class="media-left">
						<img src="<?php echo base_url(); ?>assets/images/locationpin-1.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body">
						<h6 class="media-heading">Alamat</h6>
						<text style="width:200px; word-wrap:break-word; display:inline-block;"><?php echo $row->alamat_distributor; ?></text>
					</div>

					<div class="media-left"> 
						<img src="<?php echo base_url(); ?>assets/images/email-icon.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body">
						<h6 class="media-heading">Email</h6>
						<text style="width:200px; word-wrap:break-word; display:inline-block;"><?php echo $row->email; ?></text>
					</div>
				</a>
			</li>

			<li class="media">
				<a href="#" class="media-link">
					<div class="media-left">
						<img src="<?php echo base_url(); ?>assets/images/113993_media_512x512.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body">
						<h6 class="media-heading">No Telpon</h6>
						<text style="width:200px; word-wrap:break-word; display:inline-block;"><?php echo $row->telp; ?></text>
					</div>

					<div class="media-left">
						<img src="<?php echo base_url(); ?>assets/images/iPhone-icon.png" class="img-circle img-lg" alt="">
					</div>

					<div class="media-body">
						<h6 class="media-heading">Nomor Hp</h6>
						<text style="width:200px; word-wrap:break-word; display:inline-block;"><?php echo $row->hp; ?></text>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>


<?php } ?>
