<?php 
    $this->load->view('produsen/header');
    $this->load->view('produsen/navbar');
    $this->load->view('produsen/menu');  
    $this->load->view('produsen/sidebar');
    $this->load->view('produsen/page_header');
?>
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<?php foreach($get_data as $row){ ?>
			<h5 class="panel-title"> Aktivitas Penjualan <span class="text-bold text-success"><?php echo $row->nama_distributor ?></span></h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
	    	<?php } ?>
		</div>

		<div class="panel-body">
			<table class="table table-lg invoice-archive-detail">
				<thead>
					<tr>
						<th>Id Transaksi</th>
						<th>Period</th>
						<th>Tanggal transaksi</th>
		                <th>Terjual</th>
		                <th>Pendapatan</th>
		                <th>Actions</th>
		            </tr>
				</thead>
				<tbody>
				<?php 
				foreach($get_history_penjualan as $row){ ?>
					<tr>
						<td><?php echo $row->id_transaksi; ?></td>
						<td><?php echo $row->periode; ?></td>
						<td><span class="label label-success"><?php echo $row->tanggal; ?></span></td>
		                <td><h6 class="no-margin text-bold"><?php echo $row->qty; ?> Pcs</h6></td>
		                <td>
		                	<h6 class="no-margin text-bold left-potition">Rp.<?php echo number_format($row->pendapatan,2); ?></h6>
		                </td>
						<td id="invoice" class="<?php echo $row->id_transaksi ?>">
							<button type="button" class="btn border-warning text-warning-600 btn-flat btn-icon btn-rounded"><i class="icon-file-eye"></i></button>
						</td>
		            </tr>
		            <?php } ?>
		        </tbody>
		    </table>	
			
			</div>
		</div>
<?php 
	$this->load->view('produsen/footer');
?>

<script>
$(document).on("click","#detail a",function(e){
	var id_dist = $(this).attr("class");
	$.ajax({
		type:"POST",
		data:{id:id_dist},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_distributor/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

$(document).on("click","#invoice button",function(e){
	var id_transaksi = $(this).parent().attr("class");
	$.ajax({
		type:"POST",
		data:{id_trx:id_transaksi},
		url:"<?php echo site_url('Produsen_monitoring_distributor/detail_transaksi/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
	e.preventDefault();
	$("#myModal").modal('show');
});

 $('#delete a').on('click', function() {
 	 var thisRow = $(this).parents('tr');
 	 var id_dist = $(this).attr("class");

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel pls!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        
        function(isConfirm){
            if (isConfirm) {
            	$.ajax({
					type:"POST",
					data:{id:id_dist},
					url:"<?php echo site_url('Produsen_monitoring_distributor/hapus_distributor/') ?>",
					success:function(){
						swal({
		                    title: "Deleted!",
		                    text: "Your imaginary file has been deleted.",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
                		});

                		thisRow.remove();
           			 }
           		})
            }else {
                swal({
                    title: "Cancelled",
                    text: "Your imaginary file is safe :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });
</script>



