
<?php foreach ($get_data_dist as $row) { ?>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
    		<div class="col-md-6 col-lg-7 content-group">
    			<span class="text-muted">Detail Penjual:</span>
    				<ul class="list-condensed list-unstyled">
    				<li><h5><?php echo $row->nama_distributor ?></h5></li>
    				<li><?php echo wordwrap($row->alamat_distributor, 40, "<br />\n"); ?></li>
                    <li><?php echo $row->hp ?></li>
    				<li><a href="#"><?php echo $row->email ?></a></li>
    			</ul>
    		</div>

    		<div class="col-md-6 col-lg-5 content-group">
                <span class="text-muted">Detail Penjualan:</span>
                <ul class="list-condensed list-unstyled invoice-payment-details">
                    <li></li><li><br></li>
                    <li>Id Transaksi: <span class="text-bold">#<?php echo $row->id_transaksi ?></span></li>
                    <li>Tgl Transaksi: <span><?php echo $row->tanggal ?></span></li>
                    <li>Total Penjualan: <span>Rp.
                    <?php foreach ($total as $key) {  
                        echo number_format($key->total_harga,0,",","."); }
                     ?>     
                    </span></li>
                </ul>
            </div>
    	</div>
    	<?php } ?>


        <div class="table-responsive">
            <table class="table table-lg">
                <thead>
                    <tr>
                        <th>Nama Produk</th>
                        <th class="col-sm-2">Harga Jual</th>
                        <th class="col-sm-1">Qty</th>
                        <th class="col-sm-2">Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($get_data_penj as $row) {  ?>
                    <tr>
                        <td>
                        	<h6 class="no-margin"><?php echo $row->nama_produk." ".$row->nama_variasi ?></h6>
                    	</td>
                        <td><?php echo $row->harga_transaksi ?></td>
                        <td><?php echo $row->qty ?></td>
                        <td><span class="text-semibold">Rp.<?php echo number_format($row->total,0,",",".") ?></span></td>
                    </tr>
                
                <?php } foreach ($total as $key) {  ?>
                    <tr>
                        <td>
                            <h6 class="no-margin text-bold">Total</h6>
                        </td>
                        <td></td>
                        <td ><h6 class="no-margin text-bold"><?php echo $key->total_qty ?></h6></td>
                        <td><h6 class="text-bold">Rp.<?php echo number_format($key->total_harga,0,",",".") ?></h6></td>
                    </tr>
                 <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>