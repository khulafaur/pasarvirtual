
<!-- Content area -->
<div class="content">
	<div class="panel panel-white">

		<div class="panel-body no-padding-bottom">
			<div class="row">
			<?php foreach ($data_konsumen_penjual as $row) { ?>
				<div class="col-md-6 content-group">
					<div class="invoice-details">
						<h6 class="text-uppercase text-semibold">Invoice #<?php echo $row->id_transaksi ?></h6>
						<ul class="list-condensed list-unstyled">
							<li>Tanggal: <span class="text-semibold"><?php echo $row->tanggal ?></span></li>
						</ul>
					</div>
				</div>
			</div>
	

			<div class="row">
	    		<div class="col-md-6 col-lg-7 content-group">
	    			<span class="text-muted text-uppercase">Detail Penjual:</span>
	    				<ul class="list-condensed list-unstyled">
		    				<li><h5><?php echo $row->nama_distributor ?></h5></li>
		    				<li><?php echo wordwrap($row->alamat_penjual, 40, "<br />\n"); ?></li>
		                    <li><?php echo $row->hp ?></li>
		    				<li><a href="#"><?php echo $row->email_penjual ?></a></li>
	    				</ul>
	    		</div>

	    		<div class="col-md-6 col-lg-5 content-group">
	                <span class="text-muted text-uppercase" align="right">Detail Konsumen:</span>
		                <ul class="list-condensed list-unstyled ">
		                    <li><h5><?php echo $row->namaDepan." ".$row->namaBelakang ?></h5></li>
		                    <li><?php echo $row->alamat ?></li>
		                    <li><?php echo $row->kota." ".$row->provinsi ?></li>
		                    <li><?php echo $row->kelurahan." ".$row->kodePos ?></li>
		                    <li>Nomor Hp: <span><?php echo $row->nomorHp ?></span></li> 
		                </ul>
	            </div>
	    	</div>
	    	<?php } ?>


	        <div class="table-responsive">
	            <table class="table table-lg">
	                <thead>
	                    <tr>
	                        <th>Nama Produk</th>
	                        <th class="col-sm-2">Harga Satuan</th>
	                        <th class="col-sm-1">Qty</th>
	                        <th class="col-sm-2">Total</th>
	                    </tr>
	                </thead>
	                <tbody>
	                <?php foreach ($data_detail_penjualan as $row) {  ?>
	                    <tr>
	                        <td>
	                        	<h6 class="no-margin"><?php echo $row->nama_produk." ".$row->nama_variasi ?></h6>
	                    	</td>
	                        <td><?php echo $row->harga_transaksi ?></td>
	                        <td><?php echo $row->qty ?></td>
	                        <td><span class="text-semibold">Rp.<?php echo number_format($row->total,0,",",".") ?></span></td>
	                    </tr>
	                <?php } foreach ($total as $key) {  ?>
	                    <tr>
	                        <td>
	                            <h6 class="no-margin text-bold">Total</h6>
	                        </td>
	                        <td></td>
	                        <td ><h6 class="no-margin text-bold"><?php echo $key->total_qty ?></h6></td>
	                        <td><h6 class="text-bold">Rp.<?php echo number_format($key->total_harga,0,",",".") ?></h6></td>
	                    </tr>
                 	<?php } ?>

	                </tbody>
	            </table>
	        </div>
	    </div>
	</div>
</div>



