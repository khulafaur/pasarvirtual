<?php
	$this->load->view('produsen/header');
	$this->load->view('produsen/navbar');
	$this->load->view('produsen/menu');	
	$this->load->view('produsen/sidebar');
	$this->load->view('produsen/page_header');
?>

				<!-- Content area -->
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"> Pesanan </h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>
		<div class="panel-body">
			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#basic-tab1" data-toggle="tab">Semua</a></li>
					<li><a href="#basic-tab2" data-toggle="tab">Belum Bayar</a></li>
					<li><a href="#basic-tab3" data-toggle="tab">Perlu Dikirim</a></li>
					<li><a href="#basic-tab4" data-toggle="tab">Dikirim</a></li>
					<li><a href="#basic-tab5" data-toggle="tab">Selesai</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="basic-tab1">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Distributor</th>
								<!--th>Produk</th-->
								<th>Jumlah Harus Dibayar</th>
								<th>Tanggal Transaksi</th>
								<th>Status</th>
								<!--th div="getting-started">Hitungan Mundur</th-->
								<th>Jasa Kirim</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>

						<tbody>
						 	<?php
							    foreach ($hasil as $row) {		
							?>
							<tr>
								<td><?php echo $row->nama_distributor; ?></td>
								<!--<td><?php echo $row->nama_produk; ?></td-->
								<td><?php echo $row->nominal; ?></td>
								<td><?php echo $row->tanggal; ?></td>
								<td><?php echo $row->status_transaksi; ?></td>
								<td><?php echo $row->jasa_kirim; ?></td>
								<td class="<?php echo $row->id_transaksi; ?>" id="detail">
								    <a><span class="label bg-success-400">Detail Penjualan</span></a>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>	
				</div>

				<div class="tab-pane" id="basic-tab2">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Distributor</th>
								<!--th>Produk</th-->
								<th>Jumlah Harus Dibayar</th>
								<th>Tanggal Transaksi</th>
								<th>Status</th>
								<!--th div="getting-started">Hitungan Mundur</th-->
								<th>Jasa Kirim</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>

						<tbody>
						 	<?php
							    foreach ($hasil_belum_bayar as $row) {		
							?>
							<tr>
								<td><?php echo $row->nama_distributor; ?></td>
								<!--<td><?php echo $row->nama_produk; ?></td-->
								<td><?php echo $row->nominal; ?></td>
								<td><?php echo $row->tanggal; ?></td>
								<td><?php echo $row->status_transaksi; ?></td>
								<td><?php echo $row->jasa_kirim; ?></td>
								<td class="<?php echo $row->id_transaksi; ?>" id="detail1">
								    <a><span class="label bg-success-400">Detail Penjualan</span></a>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>	
				</div>
				
				<div class="tab-pane" id="basic-tab3">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Distributor</th>
								<!--th>Produk</th-->
								<th>Jumlah Harus Dibayar</th>
								<th>Tanggal Transaksi</th>
								<th>Status</th>
								<!--th div="getting-started">Hitungan Mundur</th-->
								<th>Jasa Kirim</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>

						<tbody>
						 	<?php
							    foreach ($hasil_perlu_dikirim as $row1) {		
							?>
							<tr>
								<td><?php echo $row1->nama_distributor; ?></td>
								<!--<td><?php echo $row->nama_produk; ?></td-->
								<td><?php echo $row1->nominal; ?></td>
								<td><?php echo $row1->tanggal; ?></td>
								<td><?php echo $row1->status_transaksi; ?></td>
								<td><?php echo $row1->jasa_kirim; ?></td>
								<td class="text-center">
									<ul class="icons-list">
										<li id="detail2" class="<?php echo $row1->id_transaksi; ?>"><a><span class="label bg-success-400">Detail Penjualan</span></a></li>
										<li><a href="<?php echo base_url().'index.php/Produsen_penjualan/sudah_kirim/'.$row1->id_transaksi;?>"><span class="label bg-danger-400">Sudah Dikirim</span></a></li>
									</ul>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>
				</div>

				<div class="tab-pane" id="basic-tab4">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Distributor</th>
								<!--th>Produk</th-->
								<th>Jumlah Harus Dibayar</th>
								<th>Tanggal Transaksi</th>
								<th>Status</th>
								<!--th div="getting-started">Hitungan Mundur</th-->
								<th>Jasa Kirim</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>

						<tbody>
						 	<?php
							    foreach ($hasil_sudah_dikirim as $row) {		
							?>
							<tr>
								<td><?php echo $row->nama_distributor; ?></td>
								<!--<td><?php echo $row->nama_produk; ?></td-->
								<td><?php echo $row->nominal; ?></td>
								<td><?php echo $row->tanggal; ?></td>
								<td><?php echo $row->status_transaksi; ?></td>
								<td><?php echo $row->jasa_kirim; ?></td>
								<td class="text-center">
									<ul class="icons-list">
									<li id="detail3" class="<?php echo $row->id_transaksi; ?>"><a><span class="label bg-success-400">Detail Penjualan</span></a></li>
									<?php if (isset($row->no_resi)){?>
										<li id="resi" class="<?php echo $row->id_transaksi; ?>"><a>
										<span class="label bg-warning-400" data-toggle="modal">Update Nomor Resi</span></a></li>
									<?php } else { ?>
										<li id="resi" class="<?php echo $row->id_transaksi; ?>"><a>
										<span class="label bg-danger-400" data-toggle="modal">Upload Nomor Resi</span></a></li>
										<?php } ?>
									</ul>
								</td>
							</tr>

							<?php  } ?>
						</tbody>
					</table>
				</div>
				
				

				<div class="tab-pane" id="basic-tab5">
					<table class="table datatable-basic">
						<thead>
							<tr>
								<th>Distributor</th>
								<!--th>Produk</th-->
								<th>Jumlah Harus Dibayar</th>
								<th>Tanggal Transaksi</th>
								<th>Status</th>
								<!--th div="getting-started">Hitungan Mundur</th-->
								<th>Jasa Kirim</th>
								<th class="text-center">Aksi</th>
							</tr>
						</thead>

						<tbody>
						 	<?php
							    foreach ($hasil_selesai as $row) {		
							?>
							<tr>
								<td><?php echo $row->nama_distributor; ?></td>
								<!--<td><?php echo $row->nama_produk; ?></td-->
								<td><?php echo $row->nominal; ?></td>
								<td><?php echo $row->tanggal; ?></td>
								<td><?php echo $row->status_transaksi; ?></td>
								<td><?php echo $row->jasa_kirim; ?></td>
								<td class="text-center">
									<ul class="icons-list">
										<li id="detail4" class="<?php echo $row->id_transaksi; ?>"><a><span class="label bg-success-400">Detail Penjualan</span></a></li>
									</ul>
								</td>
							</tr>
							<?php  } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
	$this->load->view('produsen/footer');
?>

			<script type="text/javascript">
			$(document).on("click","#resi a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/form_upload_noresi/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});

			$(document).on("click","#detail a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/detail_penjualan/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});

			$(document).on("click","#detail1 a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/detail_penjualan/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});

			$(document).on("click","#detail2 a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/detail_penjualan/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});

			$(document).on("click","#detail3 a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/detail_penjualan/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});

			$(document).on("click","#detail4 a",function(e){
				var id_trx = $(this).parent().attr("class");
				$.ajax({
					type:"POST",
					data:{id_transaksi:id_trx},
					url:"<?php echo site_url('Produsen_penjualan/detail_penjualan/') ?>",
					success:function(msg){
						$(".modal-body").html(msg);
					},
					error: function(result){
						$(".modal-body").html("Error");
					}
				});
					e.preventDefault();
					$("#myModal").modal('show');
			});
			</script>