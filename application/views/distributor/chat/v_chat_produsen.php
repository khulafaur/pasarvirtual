<?php
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
?>
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">


			<!-- Secondary sidebar -->
			<div class="sidebar sidebar-secondary sidebar-default">
				<div class="sidebar-content">
					<!-- Online users -->
					<div class="sidebar-category">
						<div class="category-title">
							<span>My users</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<?php foreach ($get_data as $row) { ?>
						<div class="category-content no-padding ">
							<ul class="media-list media-list-linked">
								<li class="media" id="select">
									<a id="<?php echo $row->ktp_produsen ?>" class="media-link">
										<div class="media-left"><img src="<?php echo base_url();?>assets/images/distributor/<?php echo $row->foto_produsen ?>" class="img-circle" alt=""></div>
										<div class="media-body">
											<span class="media-heading text-semibold"><?php echo $row->nama_produsen ?></span>
											<span class="text-size-small text-muted display-block">Toko <?php echo $row->nama_toko ?></span>
										</div>
										<div class="media-right media-middle">
											<span class="status-mark bg-success"></span>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<?php } ?>
					</div>
					<!-- /online users -->

				</div>
			</div>
			<!-- /secondary sidebar -->


	<?php $this->load->view('distributor/page_header'); ?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Content area -->
				<div class="content">
					<div id="chatform"></div>
				
					


<?php $this->load->view("distributor/footer"); ?>

<script type="text/javascript">
	$(document).on("click","#select a",function(e){
	var id_produsen = $(this).attr("id");
	console.log(id_produsen);
		$.ajax({
			type:"POST",
			data:{id_prdsn:id_produsen},
			url:"<?php echo site_url('chatting/getPanelChat/') ?>",
			success:function(msg){
				$("#chatform").html(msg);
			},
			error: function(result){
				$("#chatform").html("<b>error!</b>");
			}
		});
		e.preventDefault();
	});
</script>

