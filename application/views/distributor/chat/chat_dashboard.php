<script src="<?php echo base_url(); ?>assets/jquery.min.js"></script>

<div class="panel panel-flat">
    <div class="panel-heading">
        <?php foreach ($get_data as $value) { ?>
             <h6 class="panel-title"><?php echo $value->nama_produsen ?></h6>
        <?php } ?>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <ul class="media-list chat-list content-group">
          <li class="media date-step">
              <span>Lets start to chat</span>
          </li>
              <li class="media reversed">
                  <div class="media-body">
                      <span class="media-annotation display-block mt-10"><a href="#"></span>
                  </div>
              </li>
          <?php foreach ($get_chat_data as $value) { 
              if(($value->status_chat == 2)  ){ ?>

              <li class="media reversed">
                  <div class="media-body">
                      <div class="media-content"><?php echo $value->chattext ?></div>
                      <span class="media-annotation display-block mt-10"><?php echo $value->time ?><a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>
                  </div>

                  <div class="media-right">
                      <a href="<?php echo base_url();?>assets/images/distributor/<?php echo $value->foto_distributor ?>">
                          <img src="<?php echo base_url();?>assets/images/distributor/<?php echo $value->foto_distributor ?>" class="img-circle" alt="">
                      </a>
                  </div>
              </li>

          <?php }else{ ?>

              <li class="media">
                  <div class="media-left">
                      <a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $value->foto_produsen ?>">
                          <img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $value->foto_produsen ?>" class="img-circle" alt="">
                      </a>
                  </div>

                  <div class="media-body">
                      <div class="media-content" id="ajaxForm"><?php echo $value->chattext ?></div>
                      <span class="media-annotation display-block mt-10"><?php echo $value->time ?><a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>
                  </div>
              </li>

        <?php }
        } ?>
            <div id="view_ajax"></div>
        </ul>

        <textarea id="chatInput" name="enter-message" class="form-control content-group" rows="3" cols="1" placeholder="Enter your message..."></textarea>

        <div class="row">
            <div class="col-xs-6">
                <ul class="icons-list icons-list-extended mt-10">
                </ul>
            </div>

            <div class="col-xs-6 text-right">
                <button type="button" value="Send" id="btnSend" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

var lastTimeID = 0;

    $(document).ready(function() {
        $('#btnSend').click(function(){
            sendChatText();
            $('#chatInput').val("");
        });
         $('.chat-list, .chat-stacked').scrollTop($(this).height());
        startChat();
    });

    function startChat(){
     
        setInterval(function(){ getChatText(); }, 1500);
    }

    function getChatText(){
        $.ajax({
            type:"POST",
            data:{lastTimeID:lastTimeID},
            url:"<?php echo site_url('chatting/refresh/') ?>",
        }).done(function(data)
        {   var jsonData = JSON.parse(data);
            console.log(data);
             if (data !== '[]') {
                var html = "";
                  if(jsonData[0].status_chat == '2')  {
                     html += '<li class="media reversed">';
                     html += '<div class="media-body">';
                     html += '<div class="media-content">'+jsonData[0].chattext+'</div>';
                     html += '<span class="media-annotation display-block mt-10">'+jsonData[0].time+'<a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>';
                     html += '</div>';
                     html += '<div class="media-right">';
                     html += '<a href="<?php echo base_url(); ?>assets/images/distributor/'+jsonData[0].foto_distributor+'">';
                     html += '<img src="<?php echo base_url(); ?>assets/images/distributor/'+jsonData[0].foto_distributor+'" class="img-circle" alt="">';
                     html += '</a> </div> </li>';
                  }else{
                     html += '<li class="media">';
                     html += '<div class="media-left">';
                     html += '<a href="<?php echo base_url(); ?>assets/images/distributor/'+jsonData[0].foto_produsen+'">';
                     html += ' <img src="<?php echo base_url(); ?>assets/images/distributor/'+jsonData[0].foto_produsen+'" class="img-circle" alt=""> </a></div>';
                     html += '<div class="media-body">';
                     html += '<div class="media-content">'+jsonData[0].chattext+'</div>';
                     html += '<span class="media-annotation display-block mt-10">'+jsonData[0].time+'<a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>';
                     html += '</div> </li>';
                  }
                    $('#view_ajax').append(html).focus();
                    $('.chat-list, .chat-stacked').scrollTop($(this).height());
            }
        });
    }
    function sendChatText(){
        var chatInput = $('#chatInput').val();
        var id_tujuan = "<?php echo $id_produsen ?>";
        if(chatInput != ""){
            $.ajax({
               type:"POST",
               data:{id_tuju:id_tujuan,chattext:chatInput},
               url:"<?php echo site_url('chatting/submitChat/') ?>",
            });
        }
    }
</script>

