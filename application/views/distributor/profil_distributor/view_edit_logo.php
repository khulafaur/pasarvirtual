<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<div class="content">
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-heading">
			
				<form class="form-horizontal" action="#">
					<fieldset class="content-group">
							<div class="form-group">
								<label class="control-label col-lg-2">Edit Profil</label>
								<div class="media no-margin-top">
									<div class="media-left">
										<a href="#"><img src="<?php echo base_url(); ?>assets/images/placeholder.jpg" style="width: 58px; height: 58px; border-radius: 2px;" alt=""></a>
									</div>

									<div class="media-body">
										<input type="file" class="file-styled">
										<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
									</div>
								</div>
							</div>

							<div class="text-right">
							<button type="submit" class="btn btn-primary">Simpan<i class="position-right"></i></button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
