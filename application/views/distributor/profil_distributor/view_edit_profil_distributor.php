<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<div class="panel-body">
	<?php foreach($distributor as $rows){ ?>

	<form class="form-horizontal" action="<?php echo base_url().'distributor/update_profil_distributor';?>" method="post">
		<fieldset class="content-group">
		
		<legend class="text-semibold">
			<i class="icon-file-text2 position-left"></i>
				Enter your information
				<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
					<i class="icon-circle-down2"></i>
				</a>
		</legend>
		
		<!-- <div class="form-group">
			<label class="control-label col-lg-2">Foto Profil</label>
			<div class="col-lg-10">
				<div class="uploader bg-warning"><input type="file" name="foto_dist" class="file-styled-primary"><span class="filename" style="user-select: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-plus2"></i></span></div>
				<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
			</div>
		</div> -->

		<div class="form-group">
			<label class="control-label col-lg-2">Nomor KTP</label>
			<div class="col-lg-10">
				<input type="text" name="ktp_dist" class="form-control" value="<?php echo $rows->ktp_dist;?>">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-2">Nama Lengkap</label>
			<div class="col-lg-10">
				<input type="text" name="nama_lengkap" class="form-control" value="<?php echo $rows->nama_lengkap;?>">
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-2">Tanggal Lahir</label>
			<div class="col-lg-10">
				<input type="date" name="tgl_lahir" class="form-control" value="<?php echo $rows->tgl_lahir;?>">
			</div>
		</div>

							                			
		<div class="form-group">
			<label class="control-label col-lg-2">Jenis Kelamin </label>
			<div class="col-lg-10">
				<select name="jenis_kelamin" class="form-control" required="required" aria-required="true" aria-invalid="false">
					<option value="">Choose an option</option> 
					<option value="laki" <?php if($rows->jenis_kelamin =='laki'){ echo 'selected';} ?>>Laki-laki</option>
					<option value="perempuan" <?php if($rows->jenis_kelamin =='perempuan'){echo 'selected';} ?>>Perempuan</option>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Nomor Handphone</label>
			<div class="col-lg-10">
				<input type="text" name="hp" class="form-control" value="<?php echo $rows->hp;?>">
			</div>
		</div>
		
											
		<div class="form-group">
			<label class="control-label col-lg-2">Email</label>
			<div class="col-lg-10">
				<input type="text" name="email" class="form-control" value="<?php echo $rows->email;?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Username</label>
			<div class="col-lg-10">
				<input type="text" name="username" class="form-control" value="<?php echo $rows->username;?>">
			</div>
		</div>
		
		<legend class="text-semibold">
		<i class=" icon-store position-left"></i>
			Enter your shop information
			<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
				<i class="icon-circle-down2"></i>
			</a>
		</legend>
		
		<!-- <div class="form-group">
				<label class="control-label col-lg-2">Logo Toko</label>				
				<div class="col-lg-10">
					<div class="uploader bg-warning"><input type="file" name="logo_toko" class="file-styled-primary"><span class="filename" style="user-select: none;">No file selected</span><span class="action" style="user-select: none;"><i class="icon-plus2"></i></span></div>
					<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
				</div>
			</div> -->
		
		<div class="form-group">
			<label class="control-label col-lg-2">Nama Toko</label>
			<div class="col-lg-10">
				<input type="text" name="nama_toko" class="form-control" value="<?php echo $rows->nama_toko;?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Alamat Toko</label>
			<div class="col-lg-10">
				<textarea rows="5" cols="5" name="alamat" class="form-control" value=""><?php echo $rows->alamat;?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-lg-2">Nomor Telpon</label>
			<div class="col-lg-10">
				<input type="text" name="telp" class="form-control" value="<?php echo $rows->telp;?>">
			</div>
		</div>
		
		
		<legend class="text-semibold">
		<i class=" icon-credit-card2 position-left"></i>
			Enter your bank account information
			<a class="control-arrow" data-toggle="collapse" data-target="#demo1">
				<i class="icon-circle-down2"></i>
			</a>
		</legend>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Nama Bank</label>
			<div class="col-lg-10">
				<input type="text" name="nama_bank" class="form-control" value="<?php echo $rows->nama_bank;?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Nomor Rekening</label>
			<div class="col-lg-10">
				<input type="text" name="no_rekening" class="form-control" value="<?php echo $rows->no_rekening;?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-2">Atas Nama</label>
			<div class="col-lg-10">
				<input type="text" name="nama_rekening" class="form-control" value="<?php echo $rows->nama_rekening;?>">
			</div>
		</div>
		
		<div class="text-right">
			<button type="submit" class="btn btn-primary" value="submit" name="submit" id="submit">Submit</button>
		</div>
		</fieldset>
	</form>
	<?php } ?>
</div>
<!-- /form horizontal -->


<!-- Footer -->
<div class="footer text-muted">
	&copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
</div>
<!-- /footer -->
