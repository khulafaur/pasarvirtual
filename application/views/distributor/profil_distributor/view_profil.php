<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>		
<div class="content">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title"><i class="icon-users position-left"></i> Data Profil</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>

		<table  class="table datatable-basic table-bordered table-striped table-hover">
			<thead>
				<tr class="text-center">
					<th>Nama Lengkap</th>
					<th>Username</th>
					<th>Email</th>
					<th class="text-center">Aksi</th>				
			    </tr>
			</thead>
			<tbody>
				<?php
		         $no=1; foreach   ($row as $data) { ?>
					<tr class="text-center">
					  <!-- <td><?php echo $no; ?></td> -->
					  <th><?php echo $data->nama_lengkap; ?></th>
		              <th><?php echo $data->username; ?></th>
					  <th><?php echo $data->email; ?></th>
		              <td class="text-center">
						<ul class="icons-list">
							<li><a href="<?php echo site_url('distributor/edit_profil/'.$data->ktp_dist);?>" data-popup="tooltip" title="Edit"><i class="icon-pencil7"></i></a></li>
							<li><a data-toggle="modal" data-target="#modal_default" data-popup="tooltip" title="detail"><i class="icon-profile"></i></a></li>
						</ul>
					</td>
		              	
		              </td>
					</tr>
				
				<?php
					$no++;	}
				?>
			</tbody>
		</table>
	</div>
</div>

<!-- Basic detail Distributor -->
<div id="modal_default" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Detail</h6>
			</div>


			<div class="modal-body">
			<?php
         	$no=1; foreach ($row as $data) { ?>
				<div class="row">
					<div class="col-md-4">
						No KTP
					</div>
					<div class="col-offset-md-4 col-md-8">
						<b><?php echo $data->ktp_distributor; ?></b><br>
					</div>
					<div class="col-md-4">
						Nama Lengkap
					</div>
					<div class="col-offset-md-4 col-md-8">
						<b><?php echo $data->nama_lengkap;?></b><br>
					</div>
					<div class="col-md-4">
						Username
					</div>
					<div class="col-offset-md-4 col-md-8">
						<b><?php echo $data->username; ?></b><br>
					</div>
					<div class="col-md-4">
						Tanggal Lahir
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->tgl_lahir; ?><br>
					</div>
					<div class="col-md-4">
						Jenis Kelamin
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->jenis_kelamin; ?><br>
					</div>
					<div class="col-md-4">
						No. HP
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->hp; ?><br>
					</div>
					<div class="col-md-4">
						Email
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->email; ?><br>
					</div>
				</div>
				
				<hr>
				<div class="row">
					<div class="col-md-4">
						Nama Toko
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->nama_toko; ?><br>
					</div>
					<div class="col-md-4">
						Alamat
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->alamat; ?><br>
					</div>
					<div class="col-md-4">
						Telepon
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->telp; ?><br>
					</div>
				</div>

				<hr>
				<div class="row">
					<div class="col-md-4">
						Nama Bank
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->nama_bank; ?><br>
					</div>
					<div class="col-md-4">
						No. Rekening
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->no_rekening; ?><br>
					</div>
					<div class="col-md-4">
						Nama Rekening
					</div>
					<div class="col-offset-md-4 col-md-8">
						<?php echo $data->nama_rekening; ?><br>
					</div>
				</div>
			<?php
				$no++;	}
			?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>
<!-- /modal detail distributor -->
