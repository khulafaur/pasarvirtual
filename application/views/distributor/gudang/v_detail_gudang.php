
<div class="panel panel-flat">
	<div class="panel-heading">
	<?php
		foreach($detail_produk as $row) 
	{ ?>
		<legend class="text-semibold">
			<h6><b>Produk <?php echo $row->nama_produk ?></b></h6>
		</legend>
		<div class="row">
    		<div class="col-md-6 col-lg-7 content-group">
    			<ul class="list-condensed list-unstyled">
    				<li>Deskripsi Produk  : <?php echo wordwrap($row->deskripsi_produk, 60, "<br />\n"); ?></li>
                    <li><b>Persen Bagi Hasil  : <?php echo str_replace(".", "", ltrim($row->persen_distributor, '0')) ?>%</b></li>
    			</ul>
    		</div>
    	</div>
	<?php } ?>
    	 <div class="table-responsive">
            <table class="table table-lg">
                <thead>
                    <tr>
                        <th class="col-sm-2">Gambar</th>
                        <th class="col-sm-3">Nama Variasi</th>
                        <th class="col-sm-2">Stok</th>
                        <th class="col-sm-2">Harga</th>
                        <th class="col-sm-2">Laba Bersih</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($detail_variasi as $row1) {  ?>
                    <tr>
                        <td>
                            <a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" data-popup="lightbox">
                                <img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" alt="" class="img-rounded img-preview">
                            </a></td>
                        <td><?php echo $row1->nama_produk." ".$row1->nama_variasi ?></td>
                        <td><?php echo $row1->stok ?></td>
                        <td>Rp.<?php echo number_format($row1->harga,0,",",".") ?></td>
                        <td>Rp.<?php echo number_format($row1->laba_bersih,0,",",".") ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
	
	</div>
</div>

		