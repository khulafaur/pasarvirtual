<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<div id="modal_small" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Detail Gudang</h5>
			</div>

			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<body>
<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Content area -->
			<div class="content">

				<!-- Media library -->
				<?php echo form_open_multipart('produk/update_barang_cms') ?>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title text-semibold">Manajemen Gudang</h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
					<table class="table table-striped media-library table-lg">
                        <thead>
                            <tr>
                            	<th class="text-center">Pilih</th>
                                <th class="text-center">Gambar Produk</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">bagi hasil</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Detail</th>
                            </tr>
                        </thead>
                        
						<tbody>
							<?php foreach($rProdukProdusen as $data) : ?>
                            <tr>
                            	<?php if($data->status_publish=="1"){ ?>
                            	    <td><input type="checkbox" value="<?php echo $data->id_produk; ?>" class="styled control-success"  name="chek[]" checked="checked"></td>
                            	<?php }else{ ?>
                            		<td><input type="checkbox" value="<?php echo $data->id_produk; ?>" class="styled control-warning"  name="chek[]"></td>
                            	<?php } ?>
		                        <td align="center">
			                        <a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->gambar; ?>" data-popup="lightbox">
				                        <img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->gambar; ?>" alt="" class="img-rounded img-preview">
			                        </a>
		                        </td>
		                        <td class="text-center"><?php echo $data->nama_produk; ?></td>
		                        <td class="text-center"><?php echo $data->nama_kategori_khusus; ?></td>
		                        <td>
				                    <div>Produsen <a><?php echo str_replace(".", "", ltrim($data->persen_produsen, '0')); ?>%</a></div>
				                    <div>Distributor <a><?php echo str_replace(".", "", ltrim($data->persen_distributor, '0')); ?>%</a></div>
				                </td>
		                            <?php if($data->status_publish=="1"){ ?>
                            	     	<td align="center"><span class="label label-success">Tampil</span></td>
                            		<?php }else{ ?>
                            			<td align="center"><span class="label label-danger">Tidak Tampil</span></td>
                            	<?php } ?>
		                        <td align="center" id="id_detail" class="<?php echo $data->id_produk;?>">
									<a class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-pencil4"></i></a>
		                        </td>
                            </tr>
                            <?php
								endforeach;
							?>
                        </tbody>
					</table>
				</div>
				<div style="float: right">
					<button type="submit" name="update" value="submit" class="btn btn-primary btn-labeled btn-labeled-right"><b><i class=" icon-box-add"></i></b> Publish </button>
				</div>
				<?php echo form_close (); ?>
				<!-- Media library -->
			</div>
		</div>
		<!-- Page content -->
	</div>
	<!-- Page container -->
</body>

<script type="text/javascript">
	$(document).on("click","#id_detail a",function(e){
	var id_prd = $(this).parent().attr("class");
	console.log(id_prd);
	$.ajax({
		type:"POST",
		data:{id_produk:id_prd},
		url:"<?php echo site_url('produk/detail_gudang/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#modal_small").modal('show');
	});

	$(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });
</script>