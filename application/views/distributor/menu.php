<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<?php if($foto_distributor) {?>
								<a href="<?php echo base_url(); ?>distributor/<?php echo $foto_distributor ?>" class="media-left"><img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $foto_distributor ?>" class="img-circle img-sm" alt=""></a>
								<?php }else{ ?>
								<a href="<?php echo base_url(); ?>distributor/<?php echo $foto_distributor ?>" class="media-left"><img src="<?php echo base_url(); ?>assets/images/distributor/placeholder.jpg ?>" class="img-circle img-sm" alt=""></a>
								<?php } ?>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $nama_distributor ?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Bojong Soang
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="	<?php echo base_url(); ?>distributor/lihat_profil" class="media-left"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
