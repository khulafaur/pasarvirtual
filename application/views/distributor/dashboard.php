<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<div class="content">
	<div class="panel panel-flat">
		<div class="col-lg-4">
			<!-- Members online -->
				<div class="panel bg-teal-400">
					<div class="panel-body">
						<div class="heading-elements">
							<ul class="icons-list">
							    <li class="dropdown">
							        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#"><i class="icon-sync"></i> Update data</a></li>
											<li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
											<li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
											<li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
										</ul>
							    </li>
							</ul>
						</div>

						<h4 class="no-margin">Jumlah Produsen</h4>
						<div class="large">
						<?php 
							echo $this->db->query('select * from produsen_distributor')->num_rows();
						 ?>
					</div>
					</div>
				</div>
			<!-- /members online -->

		</div>



		<div class="col-lg-4">
			<!-- Current server load -->
				<div class="panel bg-pink-400">
					<div class="panel-body">
						<div class="heading-elements">
							<ul class="icons-list">
							    <li class="dropdown">
							        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#"><i class="icon-sync"></i> Update data</a></li>
											<li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
											<li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
											<li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
										</ul>
							    </li>
							</ul>
						</div>

						<h4 class="no-margin">Jumlah Produk</h4>
						<div class="large">
						<?php 
							echo $this->db->query('select * from produk_distributor')->num_rows();
						?>
					</div>
					</div>
				</div>
			<!-- /current server load -->

		</div>

		

		<div class="col-lg-4">
			<!-- Today's revenue -->
				<div class="panel bg-blue-400">
					<div class="panel-body">
						<div class="heading-elements">
							   <ul class="icons-list">
							    <li class="dropdown">
							        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#"><i class="icon-sync"></i> Update data</a></li>
											<li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
											<li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
											<li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
										</ul>
							    </li>
							</ul>
						</div>

						<h4 class="no-margin">Pendapatan</h4>
						<div class="large">
						<?php 
							echo "???";
						?>	
					</div>
					</div>
				</div>
			<!-- /today's revenue -->

		</div>
	</div>
</div>

				



		
		
