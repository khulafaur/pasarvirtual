<?php 
$this->load->view('distributor/header');
$this->load->view('distributor/navbar');
$this->load->view('distributor/menu');	
$this->load->view('distributor/sidebar');
$this->load->view('distributor/page_header');

?>
<div id="modal_small" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Detail Gudang</h5>
			</div>

			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<div class="content">

	<!-- User profile -->
	<div class="row">
		<div class="col-lg-12">
			<div class="tabbable">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title"><b>MONITORING PRODUSEN</b></h6>
						<div class="heading-elements">
							<ul class="icons-list">
		                		<li><a data-action="collapse"></a></li>
		                		<li><a data-action="reload"></a></li>
		                		<li><a data-action="close"></a></li>
		                	</ul>
	                	</div>
					</div>
				

					<div class="tabbable">
						<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
							<li class="active"><a href="#highlighted-justified-tab1" data-toggle="tab"><b>Aktivitas</b></a></li>
							<li><a href="#highlighted-justified-tab2" data-toggle="tab"><b>Daftar Produsenku</b></a></li>
						</ul>
					</div>


				<div class="tab-content">
					<div class="tab-pane fade in active" id="highlighted-justified-tab1">
						<!-- Timeline -->
						<div class="timeline timeline-left content-group">
							<div class="timeline-container">

								<?php $x=0;
								foreach($produsenku as $data) : ?>
								<!-- Blog post -->
								<div class="timeline-row">
									<div class="timeline-icon">
										<img href="#" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->logo_toko; ?>" alt="">
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="panel panel-flat timeline-content">
												<div class="panel-heading">
													<h6 class="panel-title"><b>Nama Produsen : <?php echo $data->nama_produsen; ?></b></h6>
													<span class="display-block text-muted"><?php echo $data->nama_toko; ?></span>
													<div class="heading-elements">
														<span class="heading-text"><i class="icon-checkmark-circle position-left text-success"></i><?php echo $time[$x] ?> Ago</span>
								                	</div>
												</div>
								<?php if($data->status_aktivitas == 1) { $y=0;
										foreach($activity[$x] as $row) { 
											if($y==0){ ?>

												<div class="panel-body">
													<div class="row">
														<div class="col-lg-3 col-sm-6">
															<div class="thumbnail">
																<div class="thumb">
																	<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row->gambar_produk; ?>" alt="">
																	<div class="caption-overflow">
																		<span>
																			<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row->gambar; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
																		</span>
																	</div>
																</div>
															</div>
														</div>

													    <?php foreach($activity[$x] as $row1) {  ?>	
														<div class="col-lg-3 col-sm-6">
															<div class="thumbnail">
																<div class="thumb">
																	<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" alt="">
																	<div class="caption-overflow">
																		<span>
																			<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="text-white">  <td><code>Variasi <?php echo $row1->nama_variasi; ?></code><br>
																			        <code>Rp.<?php echo number_format($row1->harga,0,',','.');  ?></code><br>
																			        <code>Stok    : 43433434</code></td>
																				
																			</a>
																		</span>
																	</div>
																</div>
															</div>
														</div>
									                    <?php }  ?>	
									                </div>

													<h6 class="content-group">
														<i class=" icon-menu6 position-left"></i>
														<td>Nama produk : <?php echo $row->nama_produk; ?></td>
													</h6>

													<div class="panel-footer">
														<blockquote>
															<footer><span class="label label-primary">Produk Baru</span>  , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite>
															<a href="<?php echo base_url(); ?>produsen/tampilan_produsen/<?php echo $data->id_produsen; ?>"><button style="float: right;" type="button" class="btn btn-success btn-labeled btn-sm"><b><i class="icon-enter"></i></b> pict me !</button></a></footer>
														</blockquote>
													</div>
												</div>
								<?php       $y=1; 
							                }
							            }   
								     } elseif ($data->status_aktivitas == 2) { $y=0;
								      		foreach($activity[$x] as $row2) { 
								      			if($y==0){ ?>
												<div class="panel-body">
													<div class="row">
													<?php foreach($activity[$x] as $row2) { ?>	
														<div class="col-lg-3 col-sm-6">
															<div class="thumbnail">
																<div class="thumb">
																	<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row2->gambar_produk; ?>" alt="">
																	<div class="caption-overflow">
																		<span>
																			<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row2->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white">
																				<td><code>Variasi <?php echo $row2->nama_variasi; ?></code><br>
																			        <code>Rp.<?php echo number_format($row2->harga,0,',','.');  ?></code><br>
																			        <code>Stok    : <?php echo $row2->stok ?></code>
																			    </td>

																			</a>
																		</span>
																	</div>
																</div>
															</div>
														</div>
													<?php } ?>
									                </div>

													<h6 class="content-group">
														<i class=" icon-menu6 position-left"></i>
														<td>Nama produk : <?php echo $row2->nama_produk; ?></td>
													</h6>

													<div class="panel-footer">
														<blockquote>
															<footer><span class="label label-success"> Variasi Baru</span> , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite></footer>
														</blockquote>
													</div>
												</div>
									<?php 		$y=1;
												}
										     } 

									    }else { 
									    	 foreach($activity[$x] as $row3) { ?>
												<div class="panel-body">
													<div class="row">	
														<div class="col-lg-3 col-sm-6">
															<div class="thumbnail">
																<div class="thumb">
																	<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row3->gambar_produk; ?>" alt="">
																	<div class="caption-overflow">
																		<span>
																			<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row3->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
																		</span>
																	</div>
																</div>
															</div>
														</div>
									                </div>

													<h6 class="content-group">
														<i class=" icon-menu6 position-left"></i>
														<td>Nama produk : <?php echo $row3->nama_produk; echo ' '.$row3->nama_variasi ?></td>
													</h6>

													<div class="panel-footer">
														<blockquote>
															<p>
																<td><i class="icon-price-tag position-left"></i> Harga : Rp. <?php echo number_format($row3->harga,2); ?><br><br></td>
																<td><i class="icon-box position-left"></i> Stok : <?php echo $row3->stok; ?><br></td>
															</p>
															<footer><span class="label label-warning"> Update Stok</span> , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite></footer>
														</blockquote>
													</div>
												</div>
									<?php   }
										} ?>
											</div>
										</div>
									</div>
								</div>
								<!-- /blog post -->
								<?php $x=$x+1;
								 endforeach; ?>
						    </div>
						</div>	
					</div>
				<!-- TAB KE2 -->
				<div class="tab-pane" id="highlighted-justified-tab2">
					<div class="panel panel-flat">
						<table class="table datatable-basic table-hover">
							<thead>
								<tr class="text-center">
									<td><b>Nama Produsen</b></td>
									<td><b>Nama Toko</b></td>
									<td><b>No HP</b></td>
									<td><b>No Telp</b></td>
									<td><b>Email</b></td>	
									<td><b>Detail</b></td>								
							    </tr>
							</thead>
							<tbody>
							<?php
					        foreach ($rLihatProdusen as $data) { ?>
								<tr>
									<td><?php echo $data->nama_produsen; ?></td>
									<td><?php echo $data->nama_toko; ?></td>
									<td><?php echo $data->no_hp; ?></td>
									<td><?php echo $data->telp; ?></td>
									<td><?php echo $data->email; ?></td>
						            <td class="text-center">
										<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li id="detail">
														<a class = "<?php echo $data->ktp_produsen ?>"><i class=" icon-eye"></i>Detail
														</a> 
													</li>
													<li><a href="<?php echo base_url(); ?>Produsen/tampilan_produsen/<?php echo $data->ktp_produsen; ?>"><i class=" icon-search4"></i> Kunjungi </a></li>
													<li id="delete"><a class="<?php echo $data->id_prod_dist ?>"><i class="icon-switch"></i> Hapus </a></li>
												</ul>
											</li>
										</ul>
									</td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END -->
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('distributor/footer');?>

<script type="text/javascript">
	$(document).on("click","#detail a",function(e){
	var id_prdusen = $(this).attr("class");
	console.log(id_prdusen);
	$.ajax({
		type:"POST",
		data:{id_produsen:id_prdusen},
		url:"<?php echo site_url('produsen/detail_produsen/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#modal_small").modal('show');
	});

	$(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });


    $('#delete a').on('click', function() {
 	 var thisRow = $(this).parents('tr');
 	 var id_prod_dist = $(this).attr("class");

        swal({
            title: "Apakah Anda Yakin?",
            text: "Produsen ini akan terhapus dari daftar Anda!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Tidak, Batalkan!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        
        function(isConfirm){
            if (isConfirm) {
            	$.ajax({
					type:"POST",
					data:{id:id_prod_dist},
					url:"<?php echo site_url('produsen/hapus_produsen/') ?>",
					success:function(){
						swal({
		                    title: "Dihapuskan!",
		                    text: "Produsen ini telah dihapus dari daftar",
		                    confirmButtonColor: "#66BB6A",
		                    type: "success"
                		});

                		thisRow.remove();
           			 }
           		})
            }else {
                swal({
                    title: "Dibatalkan",
                    text: "Distributor ini masih berada di daftar distributor Anda :)",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    });
</script>
