<!-- Content area -->
<div class="content">

	<!-- Invoice template -->
	<div class="panel panel-white">
		<?php
			 $total=0; $no=1; foreach ($detail_penjualan_distributor as $data) { 
		if($no==1) {
		?>
        
		<div class="panel-heading">
			<h6 class="modal_remote text-center"><b>FAKTUR PEMBELIAN PRODUK</b></h6>
			<h6 class="text-uppercase text-semibold"> No Transaksi : <b><?php echo $data->id_transaksi; ?></b></h6>
			<ul class="list-condensed list-unstyled">
				<li>Tanggal Transaksi: <span class="text-semibold"><td> <?php echo $data->tanggal; ?></td></span></li>
			</ul>
			
		</div>

		<div class="panel-body no-padding-bottom">
			<div class="row">
				<div class="col-md-6 col-lg-9 content-group">
					<span class="text-muted">Data Konsumen:</span>
						<ul class="list-condensed list-unstyled">
							<li><h5><b> <?php echo $data->namaDepan; ?> <?php echo $data->namaBelakang; ?></b></h5></li>
							<li><span class="text-semibold"><?php echo $data->alamat; ?> </span></li>
							<li><?php echo $data->kelurahan; ?></li>
							<li><?php echo $data->provinsi ; ?></li>
							<li><?php echo $data->kota; ?> </li>
							<li><?php echo $data->kodePos; ?></li>
							<li><?php echo $data->nomorHp; ?></li>
							<li><?php echo $data->email; ?></li>
						</ul>
				</div>
			</div>
		</div>

		<div class="table-responsive">
		    <table class="table datatable-basic table-bordered table-striped table-hover">
		        <thead>
		            <tr>
		            	<th>No</th>
		                <th>Nama Produk</th>
		                <th>Kategori Khusus</th>
		                <th>Harga Satuan</th>
		                <th>Qty</th>
		                <th>Total</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php $total=0; $no=1; foreach ($detail_penjualan_distributor as $data1) { ?>
		            <tr>
		                <td><?php echo $no; ?></td> 
						<td><?php echo $data->nama_produk; ?>
							<span class="display-block text-muted"><?php echo $data1->nama_produsen; ?></span>
							<span class="display-block text-muted"><?php echo $data1->nama_toko; ?></span>
						</td>
						<td><?php echo $data1->nama_kategori_khusus; ?></td>
						<td>Rp. <?php echo $data1->harga_transaksi; ?></td>
						<td><?php echo $data1->qty; ?></td>
						<td>Rp. <?php echo $data1->total_pesanan; ?></td>

		            </tr>
		            <?php } ?>
		        </tbody>
		        <tfoot>
			        <?php foreach ($total2 as $data2) { ?>
				        <td class="text-center" colspan="4"><b>Total</b></td>
				        <td><b><?php echo $data2->qty; ?>Pcs</b></td>
				        <td><b>Rp. <?php echo $data2->total; ?></b></td>
				    <?php } ?>
		        </tfoot>
		    </table>
		</div>
		<?php
			$no++;
				 }
			}
		?>
	</div>
	<!-- /invoice template -->
