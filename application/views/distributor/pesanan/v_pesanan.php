<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<!-- Modal with remote source -->
<div id="modal_remote" class="modal fade">
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body"></div>
	</div>
</div>
</div>
<!-- /modal with remote source -->


<div class="content">
	<div class="col-md-12">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Transaksi Pemesanan</h5>
			<div class="heading-elements">
				<ul class="icons-list">
	        		<li><a data-action="collapse"></a></li>
	        		<li><a data-action="reload"></a></li>
	        		<li><a data-action="close"></a></li>
	        	</ul>
	    	</div>
		</div>


		<table class="table datatable-basic table-bordered table-striped table-hover">
			<thead class="text-center">
				<tr>
					<th>No </th>
	                <th>Nama Konsumen</th>
	                <th>Tanggal Transaksi</th>
	                <th>Nominal</th>
	                <th>Status Transaksi</th>
	                <th>No Resi</th>
	                <th>Jasa Kirim</th>
	                <th>Aksi</th>
			    </tr>
			</thead>
			<tbody>
				<?php $no=1; foreach ($rPesanan as $data) { ?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $data->namaDepan; ?> <?php echo $data->namaBelakang; ?></td>
						<td><?php echo $data->tanggal; ?></td>
						<td><?php echo $data->nominal; ?></td>
						<td><?php echo $data->status_transaksi; ?></td>
						<td><?php echo $data->no_resi; ?></td>
						<td><?php echo $data->jasa_kirim; ?></td>
		             	
		             	<td id="id_detail" class="<?php echo $data->id_transaksi;?>">
		             		<button class="btn text-grey-400 border-grey btn-flat" type="button" >Detail</button>

		             	</td>
					  
						</td>
					</tr>
				<?php
					$no++;	}
				?>
			
			</tbody>
		</table>
	</div>
	</div>
</div>

<script type="text/javascript">
$(document).on("click","#id_detail button",function(e){
	var id_trx = $(this).parent().attr("class");
	console.log(id_trx);
	$.ajax({
		type:"POST",
		data:{id_transaksi:id_trx},
		url:"<?php echo site_url('Distributor_pesanan/detail_penjualan_distributor/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#modal_remote").modal('show');
});
</script>