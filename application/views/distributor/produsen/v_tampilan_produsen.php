<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
	$this->load->view('distributor/page_header');
	
?>

<div id="modal_small" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
			</div>

			
		</div>
	</div>
</div>

<div class="content">
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<div class="content-wrapper">
				<?php
				foreach($rSatuProdusen as $data) : ?>
				<div class="profile-cover">
					<div class="profile-cover-img">
						<img style="width:100%; height:350px;" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->logo_toko; ?>">
					</div>
					<div class="media">
						<div class="media-left">
							<a class="profile-thumb">
								<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->foto_produsen; ?>" class="img-circle" alt="">
							</a>
						</div>
						<div class="media-body">
				    		<h1>
				    			<td><?php echo $data->nama_toko; ?>
				    			<small class="display-block"><?php echo $data->email; ?></small>
				    			</td>
				    		</h1>
						</div>
					</div>
				</div>
				<!-- Page content -->
				<?php endforeach;?>
				<!-- Toolbar -->
				<div class="navbar navbar-default navbar-xs content-group">
					<ul class="nav navbar-nav visible-xs-block">
						<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
					</ul>

					<div class="navbar-collapse collapse" id="navbar-filter">
						<ul class="nav navbar-nav element-active-slate-400">
							<li class="active"><a href="#activity" data-toggle="tab"><i class="icon-stack3 position-left"></i> Produk </a></li>
							<li><a href="#schedule" data-toggle="tab"><i class="icon-store position-left"></i> Informasi Toko <span class="badge badge-success badge-inline position-right"></span></a></li>
						</ul>
					</div>
				</div>
				<!-- /toolbar -->

				<!-- Content area -->
				<div class="content">
					<!-- User profile -->
					<div class="row">
						<div class="col-lg-12">
							<div class="tabbable">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="activity">

										<!-- Timeline -->
										<div class="timeline timeline-left content-group">
											<div class="timeline-container">
												
												<!-- Sales stats -->
											<?php echo form_open_multipart('Distributor_produk/insert_barangku') ?>
												<div class="timeline-row">
													<input name="id_produsen" type="hidden" value="<?php echo $id_produsen ?>">
													<?php foreach($rSatuProdusen as $data) : ?>
													<div class="timeline-icon">
														<a href="#"><img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->logo_toko; ?>" alt=""></a>
													</div>
													<?php endforeach;?>

													<div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title text-semibold">Daftar Produk</h6>
															<div class="content">
																<div class="row">	
																<div class="col-md-12 col-sm-10">
																<?php
															    $x = 1;
															    error_reporting(0);
															    $no=0;
															    foreach($rProdukProdusen as $data) : ?>	
																<div class="col-lg-4">
																	<div class="thumbnail">
																		<div class="thumb">
																			<img style="width:150%; height:250px;" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->gambar; ?>">
																			<div class="caption">	
																				<td><i class=" icon-menu6 position-left"></i>Nama Produk : <?php echo $data->nama_produk; ?><br><br></td>
																				<td><i class="icon-price-tag position-left"></i><a id="detail" class="<?php echo $data->id_produk; ?>"><span class ="label label-success">Click Here for Detail</span></a><br><br></td>   	
																             	<div class="panel-footer">
																					<div class="checkbox checkbox-right">
																						<label>
																							<input type="checkbox" value="<?php echo $data->id_produk; ?>" class="styled" name="chek[]">
																							<td> <b>Pilih Produk</b> </td>
																						</label>
																					</div>
																				</div>	
																		    </div>
																		</div>
																		<?php
																			if($y%3==0){
																				echo "<div class='col-sm-10'><br></div>";
																			}
																		?>
																	</div>
																</div>
																<?php endforeach; ?>
																</div>
																</div>
															</div>
														</div>
													</div>
													<div class="text-right">
														<button type="submit" name="insert" value="submit" class="btn btn-primary">Ajukan dan Distribusikan</button>
													</div>
												</div>
												<!-- /sales stats -->
												<?php echo form_close (); ?>
											</div>
										</div>
										<!-- Timeline -->
									</div>

									<div class="tab-pane fade" id="schedule">
										<!-- Blog post -->
										<div class="media">
											
											<div class="panel panel-flat timeline-content">
												<div class="panel-body">
													<h6>
														<i class="icon-books position-left"></i>
														Informasi & Aktivitas Toko
													</h6>
													<?php
														foreach($rSatuProdusen as $data) : ?>
														<div class="panel-body">
															<div class="col-lg-6">
																<div class="thumbnail">
																	<div class="thumb">
																		<a href="<?php echo base_url(); ?>assets/images/distributor/flat-store.jpg" data-popup="lightbox">
																			<img style="width:100%; height:100%;" src="<?php echo base_url(); ?>assets/images/distributor/flat-store.jpg">
																			<span class="zoom-image"><i class="icon-plus2"></i></span>
																		</a>
																	</div>
																</div>
															</div>

															<div class="col-lg-6">
																<ul class="media-list content-group">
																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/person-flat.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>Nama Produsen</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->nama_produsen; ?></span>
																		</div>
																	</li>

																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/email-icon.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>Email</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->email; ?></span>
																		</div>
																	</li>

																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/113993_media_512x512.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>No HP/Telp</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->no_hp; echo " / ". $data->telp; ?></span>
																		</div>
																	</li>
																</ul>

																<ul class="media-list content-group">
																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/shop.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>Nama Toko</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->nama_toko; ?></span>
																		</div>
																	</li>

																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/locationpin-1.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>Alamat Toko</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->alamat_toko; ?></span>
																		</div>
																	</li>	
																</ul>

																<ul class="media-list content-group">
																	<li class="media stack-media-on-mobile">
																		<div class="media-left">
																			<div class="thumb">
																				<a href="#">
																					<img src="<?php echo base_url(); ?>assets/images/icon/shop.png" class="img-circle img-lg" alt="">
																				</a>
																			</div>
																		</div>

																		<div class="media-body">
																			<h6 class="media-heading"><b>Deskripsi Toko</b></h6>
														            		<span class="display-block text-muted"><?php echo $data->deskripsi_toko; ?></span>
																		</div>
																	</li>
																</ul>
															</div>			
								
														<?php endforeach;?>
												</div>
											</div>
										</div>
										<div class="timeline timeline-left content-group ">
											<div class="timeline-container">
												<?php $x=0;
												foreach($produsen_activity as $data) : ?>
												<!-- Blog post -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<img href="#" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->logo_toko; ?>" alt="">
													</div>

													<div class="row">
														<div class="col-lg-12">
															<div class="panel panel-flat timeline-content">
																<div class="panel-heading">
																	<h6 class="panel-title"><b><?php echo $data->nama_produsen; ?></b></h6>
																	<span class="display-block text-muted"><?php echo $data->nama_toko; ?></span>
																	<div class="heading-elements">
																		<span class="heading-text"><i class="icon-checkmark-circle position-left text-success"></i><?php echo $time[$x] ?> Ago</span>
												                	</div>
																</div>
												<?php if($data->status_aktivitas == 1) { $y=0;
														foreach($activity[$x] as $row) { 
															if($y==0){ ?>

																<div class="panel-body">
																	<div class="row">
																		<div class="col-lg-3 col-sm-6">
																			<div class="thumbnail">
																				<div class="thumb">
																					<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row->gambar_produk; ?>" alt="">
																					<div class="caption-overflow">
																						<span>
																							<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row->gambar; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>

																	    <?php foreach($activity[$x] as $row1) {  ?>	
																		<div class="col-lg-3 col-sm-6">
																			<div class="thumbnail">
																				<div class="thumb">
																					<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" alt="">
																					<div class="caption-overflow">
																						<span>
																							<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row1->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="text-white">  <td><code>Variasi <?php echo $row1->nama_variasi; ?></code><br>
																							        <code>Rp.<?php echo number_format($row1->harga,0,',','.');  ?></code><br>
																							        <code>Stok    : 43433434</code></td>
																								
																							</a>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
													                    <?php }  ?>	
													                </div>

																	<h6 class="content-group">
																		<i class=" icon-menu6 position-left"></i>
																		<td>Nama produk : <?php echo $row->nama_produk; ?></td>
																	</h6>

																	<div class="panel-footer">
																		<blockquote>
																			<footer><span class="label label-primary">Produk Baru</span>  , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite>
																			<a href="<?php echo base_url(); ?>produsen/tampilan_produsen/<?php echo $data->id_produsen; ?>"><button style="float: right;" type="button" class="btn btn-success btn-labeled btn-sm"><b><i class="icon-enter"></i></b> Kujungi !</button></a></footer>
																		</blockquote>
																	</div>
																</div>
												<?php       $y=1; 
											                }
											            }   
												     } elseif ($data->status_aktivitas == 2) { $y=0;
												      		foreach($activity[$x] as $row2) { 
												      			if($y==0){ ?>
																<div class="panel-body">
																	<div class="row">
																	<?php foreach($activity[$x] as $row2) { ?>	
																		<div class="col-lg-3 col-sm-6">
																			<div class="thumbnail">
																				<div class="thumb">
																					<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row2->gambar_produk; ?>" alt="">
																					<div class="caption-overflow">
																						<span>
																							<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row2->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white">
																								<td><code>Variasi <?php echo $row2->nama_variasi; ?></code><br>
																							        <code>Rp.<?php echo number_format($row2->harga,0,',','.');  ?></code><br>
																							        <code>Stok    : <?php echo $row2->stok ?></code>
																							    </td>

																							</a>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
																	<?php } ?>
													                </div>

																	<h6 class="content-group">
																		<i class=" icon-menu6 position-left"></i>
																		<td>Nama produk : <?php echo $row2->nama_produk; ?></td>
																	</h6>

																	<div class="panel-footer">
																		<blockquote>
																			<footer><span class="label label-success"> Variasi Baru</span> , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite></footer>
																		</blockquote>
																	</div>
																</div>
													<?php 		$y=1;
																}
														     } 

													    }else { 
													    	 foreach($activity[$x] as $row3) { ?>
																<div class="panel-body">
																	<div class="row">	
																		<div class="col-lg-3 col-sm-6">
																			<div class="thumbnail">
																				<div class="thumb">
																					<img src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row3->gambar_produk; ?>" alt="">
																					<div class="caption-overflow">
																						<span>
																							<a href="<?php echo base_url(); ?>assets/images/distributor/<?php echo $row3->gambar_produk; ?>" data-popup="lightbox" rel="gallery" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus3"></i></a>
																						</span>
																					</div>
																				</div>
																			</div>
																		</div>
													                </div>

																	<h6 class="content-group">
																		<i class=" icon-menu6 position-left"></i>
																		<td>Nama produk : <?php echo $row3->nama_produk; echo ' '.$row3->nama_variasi ?></td>
																	</h6>

																	<div class="panel-footer">
																		<blockquote>
																			<p>
																				<td><i class="icon-price-tag position-left"></i> Harga : Rp. <?php echo number_format($row3->harga,2); ?><br><br></td>
																				<td><i class="icon-box position-left"></i> Stok : <?php echo $row3->stok; ?><br></td>
																			</p>
																			<footer><span class="label label-warning"> Update Stok</span> , <cite title="Source Title"><?php echo $data->waktu_aktivitas; ?></cite></footer>
																		</blockquote>
																	</div>
																</div>
													<?php   }
														} ?>
															</div>
														</div>
													</div>
												</div>
												<!-- /blog post -->
												<?php $x=$x+1;
												 endforeach; ?>
										    </div>
										</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	$(document).on("click","#detail span",function(e){
	var id_prd = $(this).parent().attr("class");
	console.log(id_prd);
	$.ajax({
		type:"POST",
		data:{id_produk:id_prd},
		url:"<?php echo site_url('Distributor_produk/detail_gudang/') ?>",
		success:function(msg){
			$(".modal-body").html(msg);
		},
		error: function(result){
			$(".modal-body").html("Error");
		}
	});
		e.preventDefault();
		$("#modal_small").modal('show');
	});
</script>