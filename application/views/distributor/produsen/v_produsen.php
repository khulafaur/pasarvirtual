<?php 
	$this->load->view('distributor/header');
	$this->load->view('distributor/navbar');
	$this->load->view('distributor/menu');	
	$this->load->view('distributor/sidebar');
?>

<!-- Secondary sidebar -->

<div class="sidebar sidebar-secondary sidebar-default">
	<div class="sidebar-content no-padding">
		<ul class="navigation navigation-main navigation-accordion">
		<?php foreach($rKategori_umum as $data){ ?>

		<!-- Sidebar search -->
		<div class="sidebar-category">
			<div class="category-title">
				<span><?php echo $data->nama_kategori_umum; ?></span>
				
				<ul class="icons-list">
					<li><a href="#" data-action="collapse"></a></li>
				</ul>
			</div>

			<div class="category-content no-padding">
				<ul class="navigation navigation-alt navigation-accordion">
					<?php foreach($rKategori as $data1){ 
					if ($data->id_kategori_umum==$data1->id_kategori_umum){
						?>
					<li><a href="<?php echo site_url("produsen/lihat_produsen/".$data1->id_kategori);?>"><?php echo $data1->nama_kategori; ?></a></li>
					
					<?php }  } ?>
				</ul>
			</div>
		</div>
		<!-- /sidebar search -->
		 <?php } ?>
		</ul>
	</div>
</div>
<!-- /secondary sidebar -->

<?php 
	$this->load->view('distributor/page_header');
?>


<div class="content">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="panel panel-flat">
				<div class="panel-body">
					<div class="tabbable">
						<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
							<li class="active"><a href="#highlighted-justified-tab1" data-toggle="tab"><b>Daftar Produk</b></a></li>
							<li><a href="#highlighted-justified-tab2" data-toggle="tab"><b>Daftar Produsen</b></a></li>
						</ul>
					</div>

					<div class="tab-content">
						<div class="tab-pane active" id="highlighted-justified-tab1">
							<?php
							if($rLihatProduk){
								foreach($rLihatProduk as $data) : ?>
								<div class="col-lg-4">
									<div class="thumbnail">
										<div class="thumb">
											<img style="width:170%; height:250px;" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->gambar; ?>">
											<div class="caption">
												<i class="icon-checkmark-circle position-left text-success pull-right"></i>
												<h6 class="no-margin-top text-semibold">
													<a href="<?php echo site_url("produsen/tampilan_produsen/".$data->ktp_produsen);?>" class="text-default"><?php echo $data->nama_toko; ?></a> 
												</h6>
												<p> Nama Produk : <?php echo $data->nama_produk; ?><br></p>	
												<p> Deskripsi Produk : Pilih Tombol kiri untuk detail</p>
												<div class="text-center">
													<a id="detail_produk" class="btn btn-primary btn-icon btn-rounded"><i id="<?php echo $data->id_produk ?>" class="icon-info22"></i></a>									
													<a href="<?php echo site_url("produsen/tampilan_produsen/".$data->ktp_produsen);?>" class="btn btn-primary btn-icon btn-rounded"><i class="icon-home2"></i></a>
												</div>
										    </div>
										</div>
									</div>
								</div>
						<?php endforeach; 
							}else{ ?>
								<h6 class="text-center text-muted"> No Data Left</h6>
						<?php	}
						?>
						</div>

						<div class="tab-pane" id="highlighted-justified-tab2">
							<?php
						    $x = 1;
						    error_reporting(0);
						    $no=0;
							foreach($rLihatProdusen as $data) : ?>
							<div class="col-lg-4">
								<div class="thumbnail">
									<div class="thumb">

										<img style="width:170%; height:250px;" src="<?php echo base_url(); ?>assets/images/distributor/<?php echo $data->logo_toko; ?>">
										<div class="caption">
											<i class="icon-checkmark-circle position-left text-success pull-right"></i>
											<h6 class="no-margin-top text-semibold">
												<a class="text-default"><?php echo $data->nama_toko; ?></a> 
											</h6>
											<p> Alamat Toko : <?php echo $data->alamat_toko; ?><br></p>
											<div class="caption text-center">
												<a href="<?php echo site_url("produsen/tampilan_produsen/".$data->ktp_produsen);?>" class="btn bg-teal"><i class="icon-paperplane position-left"></i> Kunjungi </a>
											</div>
									    </div>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modal_small" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Detail Gudang</h5>
				</div>
				<div class="modal-body">
				</div>

				
			</div>
		</div>
	</div>

<script type="text/javascript">
	$(document).on("click","#detail_produk i",function(e){
		var id_prd = $(this).attr("id");
		$.ajax({
			type:"POST",
			data:{id_produk:id_prd},
			url:"<?php echo site_url('produsen/detail_produk/') ?>",
			success:function(msg){
				$(".modal-body").html(msg);
			},
			error: function(result){
				$(".modal-body").html("Error");
			}
		});
		e.preventDefault();
		$("#modal_small").modal('show');
	});
</script>