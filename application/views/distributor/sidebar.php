
					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<li class="navigation-header"><span>MENU</span> <i class="icon-menu" title="Forms"></i></li>
								<li class=<?php echo $dashboard ?>><a href="<?php echo site_url('dashboard/dashboard');?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li class=<?php echo $explore ?>><a  href="<?php echo site_url('produsen/lihat_produsen');?>"><i class="icon-users4"></i> <span>Daftar Mitra Kerja</span></a>
								<li class=<?php echo $monitor ?>><a href="<?php echo site_url('produsen/lihat_produsenku');?>"><i class="icon-user"></i><span>Mitra kerjaku</span></a></li>
	                           	</li>
								<li class=<?php echo $gudang ?>><a href="<?php echo base_url('Distributor_produk/gudang');?>"><i class="icon-box"></i> <span>Manajemen Gudang</span></a></li>
								<li class=<?php echo $pesanan ?>><a href="<?php echo site_url('Distributor_pesanan/penjualan_distributor');?>"><i class="icon-list-ordered"></i> <span>Pesanan</span></a></li>
								<li class=<?php echo $chat ?>><a href="<?php echo site_url('chatting/chat');?>"><i class="icon-bubbles"></i> <span>Chatting</span></a></li>
								</li>
								<li><a href="#"><i class="icon-calculator2"></i> <span>Tren Penjualan Barang</span></a></li>
								<li><a href="#"><i class="icon-cash3"></i> <span>Laporan Penjualan</span></a></li>
								<li><a href="#"><i class="icon-bookmark4"></i> <span>Alur Stok</span></a>
								<li><a href="#"><i class="icon-bookmark4"></i> <span>Riwayat Transaksi</span></a>
								<li><a href="#"><i class="icon-location3"></i> <span>Tracking</span></a></li>
								</li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->
	</div>
</div>
