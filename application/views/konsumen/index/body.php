
            <div class="main">

                <div id="rev_slider_wrapper" class="rev_slider_bgcheck rev_slider_wrapper rev_container_1 fullwidthbanner-container" data-alias="classicslider1">
                    <div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                        <ul>
                            <!-- SLIDE  -->
                            <li data-index="rs-1" data-transition="slideoverup" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200"  data-thumb="<?= base_url(); ?>/assets/konsumen/images/homesliders/index2/slide1.jpg"  data-rotate="0"  data-saveperformance="on"  data-title="Corporate">

                                <!-- MAIN IMAGE -->
                                <img src="<?= base_url(); ?>/assets/konsumen/images/homesliders/index2/slide1.jpg"  alt="Slider bg 1"  data-bgposition="center center" data-duration="11000" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption revslider-title text-white text-uppercase text-center tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-1-layer-1" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['0','0','0','0', '20']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-40','-10']" 
                                    data-fontsize="['80','80','64','50','32']"
                                    data-lineheight="['80','80','64','50','32']"
                                    data-fontweight="700"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="800" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.1" 
                                    style="z-index: 5; white-space: nowrap;">Dikembangkan Oleh <br> D3MI 2015
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption revslider-subtitle text-uppercase tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-1-layer-2" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['190','190','140','104','76']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-176','-176','-140','-112','-60']" 
                                    data-width="none"
                                    data-height="none"
                                    data-fontsize="['18','18','16','13','10']"
                                    data-lineheight="['18','18','16','13','10']"
                                    data-fontweight="600"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1300" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    style="z-index: 6; white-space: nowrap;"> Pasar Virtual Indonesia
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption revslider-text text-center tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-1-layer-3" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['55','55','30','0','0']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['60','60','45','40','25']" 
                                    data-width="['500px','500px','420px','380px','280px']"
                                    data-height="none"
                                    data-fontsize="['17','17','15','13','10']"
                                    data-lineheight="['23','23','20','18','16']"
                                    data-whitespace="normal"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1300" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    style="z-index: 6;">Idhin, Geri, Dinda, Laily, Laras, Nonita    
                                </div>

                                <!-- LAYER NR. 4 -->
                                <a class="tp-caption btn btn-white tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-1-layer-4" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['230','230','180','134','96']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['138','138','110','100','50']" 
                                    data-width="['156px','156px', '138px', '120px', 80px']"
                                    data-fontsize="['15','14','13','12','9']"
                                    data-paddingtop="['13','13','11','9','5']"
                                    data-paddingbottom="['10','10','7','6','2']"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="2000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    style="z-index: 6; white-space: nowrap;"
                                    href="#">Belanja Sekarang
                                </a>
                            </li>
                            <!-- SLIDE  -->
                            <li data-index="rs-2" data-transition="slideoverdown" data-slotamount="7" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1200" data-thumb="<?= base_url(); ?>/assets/konsumen/images/homesliders/index2/slide2.jpg" data-rotate="0" data-saveperformance="on" data-title="Business">

                                <!-- MAIN IMAGE -->
                                <img src="<?= base_url(); ?>/assets/konsumen/images/homesliders/index2/slide2.jpg"  alt="Slider bg 2"  data-bgposition="center center" data-duration="11000" data-ease="Linear.easeNone" class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->

                                <!-- LAYER NR. 1 -->
                                <div class="tp-caption revslider-title darker text-uppercase text-center tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-2-layer-1" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['0','0','0','0', '20']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['-60','-60','-50','-40','-10']" 
                                    data-fontsize="['80','80','64','50','32']"
                                    data-lineheight="['80','80','64','50','32']"
                                    data-fontweight="700"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="800" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    data-elementdelay="0.05" 
                                    style="z-index: 5; white-space: nowrap;">Semoga  <br>Kami <span class="text-custom">Cepat Wisud</span>
                                </div>

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption  revslider-subtitle text-uppercase tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-2-layer-2" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['220','220','167','128','95']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-176','-176','-140','-112','-60']" 
                                    data-width="none"
                                    data-height="none"
                                    data-fontsize="['18','18','16','13','10']"
                                    data-lineheight="['18','18','16','13','10']"
                                    data-fontweight="600"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1300" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on"
                                    style="z-index: 6; white-space: nowrap;">Doakan
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption revslider-text text-center tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-2-layer-3" 
                                   data-x="['right','right','right','right','right']" data-hoffset="['55','55','30','0','0']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['60','60','45','40','25']" 
                                    data-width="['500px','500px','420px','380px','280px']"
                                    data-height="none"
                                    data-fontsize="['17','17','15','13','10']"
                                    data-lineheight="['23','23','20','18','16']"
                                    data-whitespace="normal"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1300" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    style="z-index: 6;">Idhin, Geri, Dinda, Laily, Laras, Nonita
                                </div>

                                <!-- LAYER NR. 4 -->
                                <a class="tp-caption btn btn-custom tp-resizeme rs-parallaxlevel-0" 
                                    id="slide-2-layer-4" 
                                    data-x="['right','right','right','right','right']" data-hoffset="['230','230','180','134','96']" 
                                    data-y="['middle','middle','middle','middle','middle']" data-voffset="['138','138','110','100','50']" 
                                    data-width="['156px','156px', '138px', '120px', 80px']"
                                    data-fontsize="['15','14','13','12','9']"
                                    data-paddingtop="['13','13','11','9','5']"
                                    data-paddingbottom="['10','10','7','6','2']"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="2000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-responsive_offset="on" 
                                    style="z-index: 6; white-space: nowrap;"
                                    href="#">Belanja Sekarang
                                </a>

                            </li>
                        </ul>
                        <div class="tp-bannertimer" style="height: 4px; background-color: rgba(255, 255, 255, 0.5);"></div>  
                    </div><!-- End rev_slider -->
                </div><!-- End rev_slider_wrapper -->

                <div class="mb80 mb60-sm mb50-xs"></div><!-- margin -->

                <div class="container-fluid">
                    <div class="products-tab-container" role="tabpanel">
                        <!-- Nav tabs -->
                       <!--  <ul class="nav nav-pills nav-bordered text-center text-uppercase" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#tab-special" aria-controls="tab-special" role="tab" data-toggle="tab">
                                    Spesial
                                </a>
                            </li> -->

                           <!--  <li role="presentation">
                                <a href="#tab-latest" aria-controls="tab-latest" role="tab" data-toggle="tab">
                                    Barang Terakhir
                                </a>
                            </li>

                            <li role="presentation">
                                <a href="#tab-bestsellers" aria-controls="tab-bestsellers" role="tab" data-toggle="tab">
                                    Penjualan Terbaik
                                </a>
                            </li> -->

                            <!-- <li role="presentation">
                                <a href="#tab-features" aria-controls="tab-features" role="tab" data-toggle="tab">
                                    Features
                                </a>
                            </li>
                        </ul> -->

                        <!-- Tab Panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tab-special">
                                <div class="row product-container-row">
                                    <div class="products-container max-col-5" data-layoutmode="fitRows">
                                        
                                        <?php foreach ($produk as $valproduk) : ?>
                                        <div class="product-column">
                                            <div class="product product1">
                                               <!--  <span class="product-label discount">-25%</span> -->
                                                <div class="product-top">
                                                    <figure>
                                                        <a href="#">
                                                            <?php if($valproduk->gambar_produk) { ?>
                                                            <img style="width: 321px; height: 321px " src="<?= base_url(); ?>/uploads/<?= $valproduk->gambar_produk ?>" alt="Product Image">
                                                            <?php }else{ ?>
                                                            <img style="width: 321px; height: 321px " src="<?= base_url(); ?>/assets/konsumen/images/products/product1.jpg   ?>" alt="Product Image">
                                                            <?php } ?>


                                                        </a>
                                                    </figure>
                                                    <a href="#" class="btn-quickview icon" title="View"><span class="sr-only">View</span></a>
                                                </div><!-- End .product-top -->
                                                <div class="product-meta">
                                                    <div class="product-brand">
                                                        <a href="#"><?= $valproduk->nama_variasi ?></a>
                                                    </div><!-- End .product-brand -->
                                                    <div class="ratings-container">
                                                        <div class="ratings" style="width: 0%;"></div><!-- End .ratings -->
                                                    </div><!-- End .ratings -->
                                                </div><!-- End .product-meta -->

                                                <h3 class="product-title">
                                                    <a href="#"><?= $valproduk->deskripsi_produk ?></a>
                                                </h3>

                                                <div class="product-price-container">
                                                    <span class="product-old-price">Rp. <?= number_format($valproduk->harga+($valproduk->harga*0.2),0,",",".") ?></span>
                                                    <span class="product-price">Rp. <?= number_format($valproduk->harga,0,",",".") ?></span>
                                                </div><!-- End .product-price-container -->

                                                <div class="product-action">
                                                    <a href="<?= base_url();?>konsumenBeranda/add_to_cart/<?php echo $valproduk->id_produk_variasi ?>" class="btn-add-cart" title="Add to Cart"><i class="icon icon-cart"></i> <span>Tambahkan ke Keranjang</span></a>
                                                    <a href="#" class="icon btn-product-like" title="Like"><span class="sr-only">Suka</span></a>
                                                    <!-- <a href="#" class="icon btn-product-compare" title="Compare"><span class="sr-only">Compare</span></a> -->
                                                </div><!-- End .product-action -->
                                            </div><!-- End .product -->
                                        </div><!-- End .product-col -->

                                    <?php endforeach; ?>

                                       

                                    </div><!-- End .product-container-->
                                </div><!-- End .row -->
                            </div><!-- End .tab-pane -->

               <!--  <div class="mb120 mb70-sm mb35-xs"></div> --><!-- margin -->

                <div class="bg-custom7 pt60 pb70">
                    <div class="container-fluid">
                        <h2 class="carousel-title text-white text-center">APA YANG MEREKA KATAKAN</h2>
                        <div class="swiper-container testimonials-3col-carousel">
                            <!-- Add Navigation -->
                            <div class="swiper-nav-wrapper">
                                <div class="swiper-button-prev icon light"></div><!-- End .button-prev -->
                                <div class="swiper-button-next icon light"></div><!-- End .button-next -->
                            </div><!-- End .swiper-nav-wrapper -->

                            <div class="swiper-wrapper">
                                <div class="swiper-slide testimonial">
                                    <blockquote>
                                        <p> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posu ere cubilia Curae; Quisque scelerisque mollis nisl vel volutpat. Aenean vitae eros magna. Aenean eleifend ligula at lacus.</p>
                                    </blockquote>
                                    <div class="testimonial-owner">
                                        <figure>
                                            <img src="<?= base_url(); ?>/assets/konsumen/images/blog/users/mark.png" alt="Mark">
                                        </figure>
                                        <div class="owner-meta">
                                            Mark Lewis,<br>
                                            06.04.2016
                                        </div>
                                    </div><!-- End .testimonial-owner -->
                                </div><!-- End .testimonial -->

                                <div class="swiper-slide testimonial">
                                    <blockquote>
                                        <p>Phasellus pulvinar, odio vestibulum porta suscipit, dolor tortor gravida nunc, nec blandit enim turpis vel odio. Mauris laoreet lectus eget mattis sodales. Praesent vel rhon cus sapien donec tempus.</p>
                                    </blockquote>
                                    <div class="testimonial-owner">
                                        <figure>
                                            <img src="<?= base_url(); ?>/assets/konsumen/images/blog/users/anna.png" alt="Grace">
                                        </figure>
                                        <div class="owner-meta">
                                            Anna Kendrick,<br>
                                            12.04.2016
                                        </div>
                                    </div><!-- End .testimonial-owner -->
                                </div><!-- End .testimonial -->

                                <div class="swiper-slide testimonial">
                                    <blockquote>
                                        <p>Aenean mattis at nulla a varius. Integer luctus eu nisi sit amet facilisis. Sed elemen porttitor, at egestas orci. Ut sit amet metus nisl. Nullam ligula turpis, blandit nec tellus et, pretium.</p>
                                    </blockquote>
                                    <div class="testimonial-owner">
                                        <figure>
                                            <img src="<?= base_url(); ?>/assets/konsumen/images/blog/users/grace.png" alt="Mark">
                                        </figure>
                                        <div class="owner-meta">
                                            Grace Lobot,<br>
                                            02.04.2016
                                        </div>
                                    </div><!-- End .testimonial-owner -->
                                </div><!-- End .testimonial -->

                                <div class="swiper-slide testimonial">
                                    <blockquote>
                                        <p>Sasellus pulvinar, odio vestibulum porta suscipit, dolor tortor gravida nunc, nec blandit enim turpis vel odio. Mauris laoreet lectus eget mattis sodales. Praesent vel rhon cus sapien donec tempusu.</p>
                                    </blockquote>
                                    <div class="testimonial-owner">
                                        <figure>
                                            <img src="<?= base_url(); ?>/assets/konsumen/images/blog/users/david.png" alt="Mark">
                                        </figure>
                                        <div class="owner-meta">
                                            David Lobot,<br>
                                            02.04.2016
                                        </div>
                                    </div><!-- End .testimonial-owner -->
                                </div><!-- End .testimonial -->
                            </div><!-- End .swiper-wrapper -->
                        </div><!-- End .swiper-container -->
                    </div><!-- End .container-fluid -->
                </div><!-- bg-custom7 -->

                <!-- <div class="mb120 mb90-sm mb70-xs"></div> --><!-- margin -->

                <div class="mb20"></div><!-- margin -->

                <div class="container-fluid">
                    <h2 class="carousel-title text-center">INSTAGRAM FEED</h2>
                    <div class="swiper-container instagram-carousel">
                        <!-- Add Navigation -->
                        <div class="swiper-nav-wrapper">
                            <div class="swiper-button-prev icon"></div><!-- End .button-prev -->
                            <div class="swiper-button-next icon"></div><!-- End .button-next -->
                        </div><!-- End .swiper-nav-wrapper -->

                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item1.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#office_gadgets</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>125</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>8</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->
                            
                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item2.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#smart_living</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>34</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>13</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->

                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item3.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#mobile-accessories</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>14</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>7</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->

                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item4.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#mobile-gadgets</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>22</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>5</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->

                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item5.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#organization</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>37</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>6</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->

                            <div class="swiper-slide">
                                <div class="ins-feed">
                                    <figure>
                                        <img src="<?= base_url(); ?>/assets/konsumen/images/instagram/item6.jpg" alt="Item">
                                        <figcaption>
                                            <a href="#" class="btn btn-custom btn-instagram">View Item</a>
                                        </figcaption>
                                    </figure>
                                    <div class="ins-meta">
                                        <a href="#" class="ing-hashtag">#mobile-app</a>
                                        <div class="ins-info">
                                            <a href="#"><i class="icon ins-icon ins-heart"></i><span>41</span></a>
                                            <a href="#"><i class="icon ins-icon ins-comment"></i><span>7</span></a>
                                        </div><!-- End .ins-info -->
                                    </div><!-- End .ins-meta -->
                                </div><!-- End .ins-feed -->
                            </div><!-- End .swiper-slide -->
                        </div><!-- End .swiper-wrapper -->
                    </div><!-- End .swiper-container -->
                </div><!-- End .container-fluid -->

               <!--  <div class="mb140 mb100-sm mb80-xs"> --></div><!-- margin -->

            </div><!-- End .main -->
            