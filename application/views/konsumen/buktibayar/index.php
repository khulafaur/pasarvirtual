
<body>
  
  <div class="container" style="margin-top:200px;">
    <div class="well text-center">
      <h1>Upload bukti bayar!</h1>
      <hr>
      <div class="col-md-8 col-md-offset-2">
        <?php echo form_open_multipart("konsumenBukti/tambah", array('enctype'=>'multipart/form-data')); ?>

            <div class="input-group">
              <label class="input-group-btn">
                <span class="btn btn-danger btn-lg">
                  Browse&hellip; <input type="file" id="media" name="input_gambar" style="display: none;" required>
                </span>
              </label>
              <input type="text" class="form-control input-lg" size="40" readonly required>
            </div>
            <div class="input-group">
              <input type="submit" name="submit" class="btn btn-lg btn-primary" value="Start upload">
            </div>
        </form>
        <br>
        <div class="progress" style="display:none">
          <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
            <span class="sr-only">0%</span>
          </div>
        </div>
        <div class="msg alert alert-info text-left" style="display:none"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  
  <script src="<?php echo base_url(); ?>/assets/konsumen/bukti/js/jquery.js"></script>
  <script src="<?php echo base_url(); ?>/assets/konsumen/bukti/js/bootstrap.min.js"></script>
<!--     <script src="<?php echo base_url(); ?>/assets/konsumen/bukti/js/upload.js"></script> -->
    <script>
  $(function() {
    $(document).on('change', ':file', function() {
    var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
          input.val(log);
        } else {
          if( log ) alert(log);
        }

      });
    });
    
  });
  </script>
</body>
</html>
