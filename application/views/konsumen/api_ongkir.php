<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_ongkir extends CI_Controller {

	public function showProvince()
	{
		$curl=curl_init();
		curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://api.rajaongkir.com/basic/province",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",
	      CURLOPT_HTTPHEADER => array(
	        "key:3d747e0770966bfe449323240e0964ce"
	      ),
	    ));

		$response=curl_exec($curl);
		$err=curl_error($curl);
		curl_close($curl);
		
		if($err){
      		echo "cURL Error #:" . $err;	
		}else{
			echo $response;
		}
	}

	public function showCity($idprovince='')
	{
		$curl=curl_init();
		curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://api.rajaongkir.com/basic/city?province=$idprovince",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "GET",
	      CURLOPT_HTTPHEADER => array(
	        "key:3d747e0770966bfe449323240e0964ce"
	      ),
	    ));

		$response=curl_exec($curl);
		$err=curl_error($curl);
		curl_close($curl);
		
		if($err){
      		echo "cURL Error #:" . $err;	
		}else{
			echo $response;
		}
	}

	public function test($id)
	{
		$result = $this->m_order->get_weight($id);
		print_r($result);
	}
	
	public function hitungOngkir($destination,$courier,$service)
	{
		$totalberat=0;
		$id_kota = 0;
		foreach ($this->cart->contents() as $items) {
			$totalberat += $items['berat'];
			$id_kota = $items['id_kota'];
		}

		$post_field = array(
				'origin' 		=> $id_kota,
				'destination' 	=> $destination,
				'weight' 		=> $totalberat,
				'courier' 		=> 'jne'
			);

		$curl=curl_init();
		curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://api.rajaongkir.com/basic/cost",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => $post_field,
	      CURLOPT_HTTPHEADER => array(
	        "key:3d747e0770966bfe449323240e0964ce"
	      ),
	    ));

	    $response	=curl_exec($curl);
		$err 		=curl_error($curl);
		curl_close($curl);

		if($err){
			$result='error';
			return 'error';
		}else{
			$costarray=json_decode($response);
			$res = $costarray->rajaongkir->results;
			if(!empty($res)):
				foreach($res as $r):
					foreach($r->costs as $rc):
						foreach($rc->cost as $rcc):
							if($rc->service == $service){
								echo $rcc->value;
							}
							// echo "<tr>
							// 			<td>".$r->code."</td>
							// 			<td>".$rc->service."</td>
							// 			<td>".$rc->description."</td>
							// 			<td>".$rcc->etd."</td>
							// 			<td>".number_format($rcc->value)."</td>
							// 	</tr>";
							// return $response;
						endforeach;
					endforeach;
				endforeach;
			else:
				echo 'kosong';
				echo $totalberat;
				print_r($post_field);
			endif;
		}
	}

	public function getService($destination,$courier)
	{
		$totalberat=0;
		$id_kota = 0;
		foreach ($this->cart->contents() as $items) {
			$totalberat += $items['berat'];
			$id_kota = $items['id_kota'];
		}

		$post_field = array(
				'origin' 		=> $id_kota,
				'destination' 	=> $destination,
				'weight' 		=> $totalberat,
				'courier' 		=> $courier
			);

		$curl=curl_init();
		curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://api.rajaongkir.com/basic/cost",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => $post_field,
	      CURLOPT_HTTPHEADER => array(
	        "key:3d747e0770966bfe449323240e0964ce"
	      ),
	    ));

	    $response	=curl_exec($curl);
		$err 		=curl_error($curl);
		curl_close($curl);

		if($err){
			echo 'error';
		}else{
			$costarray=json_decode($response);
			$res = $costarray->rajaongkir->results;
			if(!empty($res)):
				$output = '';
				foreach($res as $r):
					foreach($r->costs as $rc):
						$s = $rc->service;
						if($s == 'OKE' || $s == 'YES' || $s == 'REG'){
							$output .= '<option value="'.$s.'">'.$s.'</option>';
						}
					endforeach;
				endforeach;
				echo $output;
			else:
				echo 'kosong';
			endif;
		}
	}
}

/* End of file idmoreRO.php */
/* Location: ./application/controllers/idmoreRO.php */