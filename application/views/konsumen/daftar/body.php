<div class="main">
                <div class="page-header custom larger mb50">
                    <div class="container-fluid">
                        <h1>Daftar Akun</h1>
                        <ol class="breadcrumb">
                            <li><a href="#">Beranda</a></li>
                            <li class="active">Daftar Akun</li>
                        </ol>
                    </div><!-- End .container-fluid -->
                </div><!-- End .page-header -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-9">
                            <?php echo form_open('konsumenDaftar/prosesDaftar'); ?>
                                <div class="row row-lg">
                                    <div class="col-sm-6">
                                        <h2>INFORMASI PRIBADI ANDA</h2>

                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="namaDepan" id="namaDepan" required>
                                            <label class="input-desc"><i class="icon input-icon input-user"></i>Masukkan Nama Depan Anda <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="namaBelakang" id="namaBelakang" required>
                                            <label class="input-desc"><i class="icon input-icon input-user"></i>Masukkan Nama Belakang Anda <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="email" class="form-control" name="email" id="email" required>
                                            <label class="input-desc"><i class="icon input-icon input-email"></i>Masukkan Email Anda <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="username" id="username" required>
                                            <label class="input-desc"><i class="icon input-icon input-user"></i>Masukkan Username Anda <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="nomorHp" id="nomorHp" required>
                                            <label class="input-desc"><i class="icon input-icon input-phone"></i>Masukkan Nomor HP Anda <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                    </div><!-- End .col-sm-6 -->

                                    <div class="mb40 visible-xs"></div><!-- margin -->

                                    <div class="col-sm-6">
                                        <h2>ALAMAT ANDA</h2>
                                        
                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="alamat" id="alamat" required>
                                            <label class="input-desc"><i class="icon input-icon input-pin"></i>Alamat <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="kelurahan" id="kelurahan" >
                                            <label class="input-desc"><i class="icon input-icon input-pin"></i>Kelurahan </label>

                                        </div><!-- End .form-group -->
                                        


										<div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="provinsi" id="provinsi" >
                                            <label class="input-desc"><i class="icon input-icon input-state"></i>Provinsi </label>

                                        


			                            </div><!-- End .form-group -->


                                        <div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="kota" id="kota"  required>
                                            <label class="input-desc"><i class="icon input-icon input-pin"></i>Kota <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->


										<div class="form-group label-overlay">
                                            <input type="text" class="form-control" name="kodePos" id="kodePos" required>
                                            <label class="input-desc"><i class="icon input-icon input-postcode"></i>Kode POS <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->                                 


                                    </div><!-- End .col-sm-6 -->
                                </div><!-- End .row -->

                                <div class="mb80 mb60-xs"></div><!-- margin -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>PASSWORD ANDA</h2>
                                        <div class="form-group label-overlay">
                                            <input type="password" class="form-control" name="password" id="password" required>
                                            <label class="input-desc"><i class="icon input-icon input-password"></i>Enter your password <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                        <div class="form-group label-overlay">
                                            <input type="password" class="form-control" name="password2" id="password2" required>
                                            <label class="input-desc"><i class="icon input-icon input-password"></i>Enter your password <span class="input-required">*</span></label>
                                        </div><!-- End .form-group -->
                                    </div><!-- End .col-sm-6 -->

                                    <div class="mb40 visible-xs"></div><!-- margin -->

                                    <div class="col-sm-6">
                                        <h2>BERITA</h2>
                                        <div class="checkbox">
                                          <label class="custom-checkbox-wrapper">
                                            <span class="custom-checkbox-container">
                                                <input type="checkbox" name="jeans" value="jeans">
                                                <span class="custom-checkbox-icon"></span>
                                            </span>
                                           <span>Saya ingin berlangganan buletin Pasar Virtual Indonesia.</span>
                                          </label>
                                        </div><!-- End .checkbox -->

                                        <div class="checkbox">
                                          <label class="custom-checkbox-wrapper">
                                            <span class="custom-checkbox-container">
                                                <input type="checkbox" name="jeans" value="jeans">
                                                <span class="custom-checkbox-icon"></span>
                                            </span>
                                           <span>Saya telah membaca dan menyetujui <a href="#">Kebijakan pribadi</a>.</span>
                                          </label>
                                        </div><!-- End .checkbox -->

                                        <div class="mb5"></div><!-- margin -->
                                        <button type="submit" class="btn btn-custom">BUAT AKUN</button>
                                        <?php if ($this->session->flashdata('verifEmail') == TRUE ) {
                                            echo $this->session->flashdata('verifEmail');
                                        }?>
									<?php echo form_close(); ?>
                                    </div><!-- End .col-sm-6 -->
                                </div><!-- end .row -->
                            </form>
                        </div><!-- End .col-md-9 -->


                            <div class="widget">
                                <h3 class="widget-title">Testimoni</h3>
                                <div class="widget-body">
                                    <div class="swiper-container testimonials-slider">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide testimonial">
                                                <blockquote>
                                                    <p>Semoga PA nya lancar ya.</p>
                                                </blockquote>
                                                <div class="testimonial-owner">
                                                    <figure>
                                                        <img src="assets/images/blog/users/mark.png" alt="Mark">
                                                    </figure>
                                                    <div class="owner-meta">
                                                        Khulafaur Rasyidin,<br>
                                                        06.04.2016
                                                    </div>
                                                </div><!-- End .testimonial-owner -->
                                            </div><!-- End .testimonial -->
                                            <div class="swiper-slide testimonial">
                                                <blockquote>
                                                    <p>Cepat-Cepat Wisuda!</p>
                                                </blockquote>
                                                <div class="testimonial-owner">
                                                    <figure>
                                                        <img src="assets/images/blog/users/grace.png" alt="Grace">
                                                    </figure>
                                                    <div class="owner-meta">
                                                        Grace Lewis,<br>
                                                        03.04.2016
                                                    </div>
                                                </div><!-- End .testimonial-owner -->
                                            </div><!-- End .testimonial -->
                                        </div><!-- End .swiper-wrapper -->

                                        <div class="swiper-button-next icon"></div>
                                        <div class="swiper-button-prev icon"></div>
                                    </div><!-- end .swiper-container -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .widget -->
                        </aside>
                    </div><!-- End .row -->
                </div><!-- End .container-fluid -->
                <div class="mb40 visible-md visible-lg"></div><!-- margin -->
            </div><!-- End .main -->
