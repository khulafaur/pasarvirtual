<script>
	// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"

  $("form[name='registrasi']").validate({
 //  	onkeyup: function (element, event) {
	//     if (event.which === 9 && this.elementValue(element) === "") {
	//         return;
	//     } else {
	//         this.element(element);
	//     }
	// },
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      first_name: {
      	required	: true,
      	minlength	: 3
      },
      last_name: {
      	required	: true,
      	minlength	: 3
      },
      email: {
        required: true,
        email	: true,
        remote	: "registrasi/check_email_exist"
      },
      password: {
        required: true,
        minlength: 5
      },
      cpassword:{
      	equalTo: "#pass"
      },
     telepon: "number"
    },
    // Specify validation error messages
    messages: {
      namaDepan: {
      	required :"Nama depan tidak boleh kosong",
      	minlength:"Nama minimal 3 karakter"
      },
      
      namaBelakang: {
      	required	:"Nama belakang tidak boleh kosong",
      	minlength	:"Nama minimal 3 karakter"
      },

      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      cpassword:{
      	equalTo: "Password tidak cocok"
      },
      email: {
      	email : "Alamat email harus valid",
      	remote: "Email sudah terdaftar"
      },
      telepon: "Telepon harus angka"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>



<body> 
	 
	<div class="content">
		<div class="container"> 			         
		<div class="register">
			<?php echo $this->session->flashdata('info'); ?>
		  	  <?php echo form_open('konsumenDaftar/prosesDaftar'); ?>
				 <div class="  register-top-grid">
					<h3>Masukkan Data Anda</h3>
					<div class="mation">
						<span>Nama Depan<label>*</label></span>
						<input type="text" name="namaDepan"> 
					
						<span>Nama Belakang<label>*</label></span>
						<input type="text" name="namaBelakang"> 

						<span>Username<label>*</label></span>
						 <input type="text" name="username"> 
					 
						 <span>Alamat Email<label>*</label></span>
						 <input type="text" name="email" > 

						 <span>Password<label>*</label></span>
						 <input type="password" name="password" placeholder="Masukkan Password" tabindex="4" required> 

						 <input type="password" name="password" placeholder="Masukkan Password Sekali Lagi">

						 <span>Nomor HP<label>*</label></span>
						 <input type="text" name="nomorHp"> 
					</div>
					 <div class="clearfix"> </div>
					   <a class="news-letter" href="#">
						 <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Sign Up</label>
						 <td><input type="submit" name="daftar" value="Daftar"/></td>
					   </a>
					 </div>
				     
				</form>
		   </div>
		 </div>
	</div>
	<!---->
	<div class="footer">
		<div class="container">
				<div class="footer-class">
				<div class="class-footer">
					<ul>
						<li ><a href="index.html" class="scroll">HOME </a><label>|</label></li>
						<li><a href="men.html" class="scroll">MEN</a><label>|</label></li>
						<li><a href="women.html" class="scroll">WOMEN</a><label>|</label></li>
						<li><a href="collection.html" class="scroll">COLLECTION</a><label>|</label></li>
						<li><a href="collection.html" class="scroll">STORE PRODUCTS</a><label>|</label></li>
						<li><a href="collection.html" class="scroll">LATEST  PRODUCT</a></li>
					</ul>
					 <p class="footer-grid">&copy; 2014 Template by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
				</div>	 
				<div class="footer-left">
					<a href="index.html"><img src="<?=base_url();?>assets/konsumen/images/logo1.png" alt=" " /></a>
				</div> 
				<div class="clearfix"> </div>
			 	</div>
		 </div>
	</div>
</body>
</html>